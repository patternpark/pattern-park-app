# Pattern Park

The Pattern Park is an application to teach students the basics of patterns.<br>
This project is the main application of the Pattern Park and mainly written in Java.<br>
It is fully designed and written by students.

## Installation

### System requirements

- Windows or Linux
- Java (requires Version 8)


### Download

Releases can be found at [GitLab.](https://gitlab.hochschule-stralsund.de/pattern-park/pattern-park-app/-/releases)

### Running the Application

#### Windows

1. Download a [Release from GitLab](https://gitlab.hochschule-stralsund.de/pattern-park/pattern-park-app/-/releases)
2. Unzip Archive
3. Run the Executable (*pattern-park.exe*)

If the Executable does not start properly you can try to run the `.jar` file directly:

1. Make sure that you are in the ``pattern-park`` folder
2. Run the following command: `java -jar PatternPark.jar`

<br>

#### Linux

1. Download a [Release from GitLab](https://gitlab.hochschule-stralsund.de/pattern-park/pattern-park-app/-/releases)
2. Unzip Archive
3. Make sure that you are in the ``pattern-park`` folder
4. Run the launch script: `sh ./launch.sh`

Once the launch script has been run at least once you can also use `java -jar PatternPark.jar`

<br>

### Compiling the source code

Compiling the source code requires:

- [Maven](https://maven.apache.org/)

To compile the project execute the following command:

- `mvn clean compile`

After being compiled the Application can be run by starting the `Start.java` class.

## Contributors

- University Siegen:
    - specialist group „Didaktik der Informatik und E-Learning“ summer semester 2007
- University Stralsund:
    - course group "Softwareprojektorganisation" winter semester 2021
    - Aniketos Stamatios - aniketos.stamatios@protonmail.com
    - Erik der Glückliche - contact [at] lui-studio.net
    - NorthernSeaCharting - aasas@mail.de
    - Soto - zauntormczaun@gmail.com
    - GreenBird
    - Kibarius der Erzmagier

      <br>

## Libraries

- [Java Swing](https://mvnrepository.com/artifact/org.openjfx/javafx-swing)
- [jgraph](https://mvnrepository.com/artifact/jgraph/jgraph)
- [jdom](https://mvnrepository.com/artifact/org.jdom/jdom)
- [rsyntaxtextarea](https://mvnrepository.com/artifact/com.fifesoft/rsyntaxtextarea)

<br>

## License

This application is licensed under the GNU General Public License, Version 2.

[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

<br>
