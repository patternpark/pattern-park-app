// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Alias)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

package emi.enums;

/**
 * Specifies the state in which the Electron application should be started.
 *
 * @author Erik der Glückliche (Pseudonym)
 */
public enum EElectronApplicationType {
    /**
     * The following call arguments are interpreted as relative URL.
     */
    EXERCISE_URL("url"),
    /**
     * The following call arguments are interpreted as a relative URL to a video.
     */
    MOVIE("movie"),
    /**
     * The following call arguments are interpreted as a relative URL to a video, as well as a path to a JSON file
     * containing the subtitles.
     */
    MOVIE_WITH_SUBTITLES("movie-subtitles")
    ;

    /**
     * The value of the enum.
     */
    private final String text;

    /**
     * @param text The value of the enum.
     */
    EElectronApplicationType(final String text) {
        this.text = text;
    }

    /**
     * Gets the value of the enum.
     *
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
