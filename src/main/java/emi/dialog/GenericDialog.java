package emi.dialog;

import emi.GUIElementNames;
import emi.session.UserSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
// TODO: outsource magic numbers
public class GenericDialog extends JDialog {

    protected final boolean isResizable;

    /**
     * OK button
     */
    private JButton _jbAccept = null;
    /**
     * Cancel Button
     */
    private JButton _jbCancel = null;
    private JComponent _jlQuestion = null;

    private final ArrayList<ActionListener> _acceptListeners = new ArrayList<>();
    private final ArrayList<ActionListener> _cancelListeners = new ArrayList<>();

    private final String _dialogText;
    private final String _cancelBtnText;
    private final String _acceptBtnText;

    private KeyListener _keyListener;

    public GenericDialog( Frame owner, boolean modal, String dialogText, boolean isResizable ) throws HeadlessException {
        this(
                owner, modal, dialogText, isResizable, UserSettings.getGUITextFromName( GUIElementNames.DIALOG_YES ),
                UserSettings.getGUITextFromName( GUIElementNames.DIALOG_NO )
        );
    }

    public GenericDialog( Frame owner, boolean modal, String dialogText, boolean isResizable, String acceptBtnText, String cancelBtnText )
            throws HeadlessException {
        this(
                owner, modal, dialogText, isResizable, UserSettings.getGUITextFromName( GUIElementNames.APP_NAME ), acceptBtnText,
                cancelBtnText
        );
    }

    public GenericDialog(
            Frame owner, boolean modal, String dialogText, boolean isResizable, String dialogTitle, String acceptBtnText, String cancelBtnText
    ) throws HeadlessException {
        super( owner, modal );
        this._dialogText = dialogText;
        this._acceptBtnText = acceptBtnText;
        this._cancelBtnText = cancelBtnText;
        this.setTitle( dialogTitle );
        this.isResizable = isResizable;
    }

    public void addOnAcceptListener( ActionListener listener ) {
        this._acceptListeners.add( listener );
    }

    public void addOnCancelListener( ActionListener listener ) {
        this._cancelListeners.add( listener );
    }


    /**
     * This method initializes this JDialog
     */
    public void initialize( int width, int height ) {
        this._keyListener = initKeyListener();

        this.setSize( width, height );
        this.setResizable( this.isResizable);
        this.setContentPane( initJContentPane() );

        this.addKeyListener( this._keyListener );
        if ( this._jbAccept != null ) this._jbAccept.addKeyListener( this._keyListener );
        if ( this._jbCancel != null ) this._jbCancel.addKeyListener( this._keyListener );
        if ( this._jlQuestion != null ) this._jlQuestion.addKeyListener( this._keyListener );
    }

    protected void setJbCancelValues( JButton jbCancel ) {
        jbCancel.setBounds( new Rectangle( 180, 90, 106, 32 ) );
        jbCancel.setText( _cancelBtnText );
    }

    protected JComponent initDialogQuestion() {
        JLabel jlQuestion = new JLabel();
        jlQuestion.setBounds( new Rectangle( 15, 45, 271, 16 ) );
        jlQuestion.setHorizontalTextPosition( SwingConstants.LEADING );
        jlQuestion.setHorizontalAlignment( SwingConstants.CENTER );
        jlQuestion.setText( _dialogText );

        return jlQuestion;
    }

    /**
     * This method initializes jContentPane
     *
     * @return javax.swing.JPanel
     */
    protected JPanel initJContentPane() {
        _jlQuestion = this.initDialogQuestion();
        JPanel jContentPane = new JPanel();
        jContentPane.setLayout( null );
        jContentPane.add(_jlQuestion, null );
        jContentPane.add( initJbAccept(), null );
        jContentPane.add( initJbCancel(), null );

        return jContentPane;
    }

    /**
     * This method initializes jbAccept
     *
     * @return javax.swing.JButton
     */
    protected JButton initJbAccept() {
        _jbAccept = new JButton();
        _jbAccept.setBounds( new Rectangle( 15, 90, 106, 32 ) );
        _jbAccept.setText( _acceptBtnText );
        _jbAccept.addActionListener(e -> _acceptListeners.forEach(l -> l.actionPerformed( e ) ) );

        return _jbAccept;
    }

    /**
     * This method initializes jbCancel
     *
     * @return javax.swing.JButton
     */
    protected JButton initJbCancel() {
        _jbCancel = new JButton();
        this.setJbCancelValues(_jbCancel);
        _jbCancel.addActionListener(e -> _cancelListeners.forEach(l -> l.actionPerformed( e ) ) );

        return _jbCancel;
    }

    protected KeyListener getKeyListener() {
        return this._keyListener;
    }

    private KeyListener initKeyListener() {
        return new KeyListener() {
            public void keyTyped( KeyEvent e ) {
            }

            public void keyPressed( KeyEvent e ) {
                if ( _jbAccept != null && ( e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_J ) ) {
                    _jbAccept.doClick();
                } else if ( _jbCancel != null &&
                        ( e.getKeyCode() == KeyEvent.VK_ESCAPE || e.getKeyCode() == KeyEvent.VK_N ) ) {
                    _jbCancel.doClick();
                }
            }

            public void keyReleased( KeyEvent e ) {
            }
        };
    }
}
