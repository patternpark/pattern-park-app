package emi.dialog;

import emi.session.StartObject;
import emi.session.UserSettings;
import emi.util.OSHelperUtil;

import javax.swing.*;
import java.awt.*;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7.0.0), GreenBird (v1.7.0.1)
 * @version 1.7.0.1
 */
// TODO: outsource magic numbers
public final class StartDialog extends GenericDialog {

    public StartDialog(Frame owner, boolean modal, String dialogTitle, String acceptBtnText, String cancelBtnText) throws HeadlessException {
        super(owner, modal, null, false, dialogTitle, acceptBtnText, cancelBtnText);
        initialize(296, 176);
    }

    public StartDialog(Frame owner, boolean modal, String acceptBtnText, String cancelBtnText) throws HeadlessException {
        super(owner, modal, null, false, acceptBtnText, cancelBtnText);
        initialize(296, 176);
    }

    public StartDialog(Frame owner, boolean modal) throws HeadlessException {
        super(owner, modal, null, false);
        initialize(296, 176);
    }

    @Override
    public void initialize(int width, int height) {
        super.initialize(width, height);

        addOnAcceptListener((l) -> {
            StartObject startObject = UserSettings.getStartObject();
            if(UserSettings.getConfiguration().getSettings().isSoundState())
            	OSHelperUtil.startFlash(startObject.getIntroMovie());
            else
            	OSHelperUtil.startFlash(startObject.getIntroMovieOmu());

            this.setVisible(false);
        });

        addOnCancelListener((l) -> this.setVisible(false));
    }

    @Override
    protected JComponent initDialogQuestion() {
        JTextArea jl_question = new JTextArea();
        jl_question.setBounds(new Rectangle(12, 14, 263, 53));
        jl_question.setBackground(new Color(238,238,238));
        jl_question.setEditable(false);
        jl_question.setFont(new Font("Arial", Font.BOLD, 12));
        jl_question.setText(UserSettings.getStartObject().getTitle());

        return jl_question;
    }

    @Override
    protected void setJbCancelValues(JButton jbCancel) {
        super.setJbCancelValues(jbCancel);

        jbCancel.setBounds(new Rectangle(165, 90, 106, 32));
    }
}
