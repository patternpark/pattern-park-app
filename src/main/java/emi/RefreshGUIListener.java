package emi;

import emi.models.configuration.Configuration;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public interface RefreshGUIListener {
    void onRefreshGUI(Configuration configuration);
}
