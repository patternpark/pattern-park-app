package emi;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import emi.dialog.GenericDialog;
import emi.dialog.StartDialog;
import emi.map.*;
import emi.models.ClickableButton;
import emi.models.ExtraWindow;
import emi.models.SpecialButton;
import emi.models.WindowContent;
import emi.serializers.AppContentDeserializer;
import emi.serializers.ConfigurationSerializer;
import emi.serializers.XMLConstants;
import emi.session.UserSettings;
import emi.util.OSHelperUtil;
import emi.window.InfocenterWindow;
import emi.window.WindowPanel;
import emi.util.JavaSwingHelperUtil;

import java.util.stream.Collectors;

import static emi.util.JavaSwingHelperUtil.makegbc;

/**
 * This is the mainframe class by Type JFrame it include all the other GUI
 * Panels and Dialogs and the main method also here are methods to load images
 * from the file
 *
 * @author Rudolf Koslowski (v1.4)
 * @author Erik der Glückliche (v1.7.0.0)
 * @version 1.7.0.0
 */
public class Start extends JFrame implements MouseClickedListener, MouseMovedListener, MouseLeftListener {
	/**
	 * referrence to the InterfacePanel class used for the image map
	 */
	private ClickableMap _map;

	/**
	 * exit dialog
	 */
	private GenericDialog exitDialog = null;

	/**
	 * Start dialog
	 */
	private StartDialog startDialog = null;

	/**
	 * Variables for the images
	 */
	BufferedImage on = null;

	BufferedImage off = null;

	BufferedImage up = null;

	BufferedImage down = null;

	BufferedImage map = null;



	/**
	 * this Array has all the Pattern_Overview panels
	 */
	private final LinkedHashMap<Integer, WindowPanel<?>> _overviewList = new LinkedHashMap<>();
	private InfocenterWindow _infocenter;
	private int _infocenterMapNumberID;

	private AppContentDeserializer deserializer;

	private JPanel _mainPanel;

	/**
	 * This is the default constructor
	 */
	public Start() {
		super();
		init();
	}

	/**
	 * Method to initialize this
	 *
	 */
	public void init() {
		loadConfig();
		OSHelperUtil.detectOS();

		initWindow();
		_mainPanel = initMainPanel();
		initMap();
		// initialize();
		initExitDialog();
		initStartDialog();
		initInfocenter();
		resetInterfacePanel(); // init patterns, add map to panel, add infocenter to panel

		this._mainPanel.add(this._map);
		this._mainPanel.add(this._infocenter);
		showMap();
		this.startDialog.setVisible(true);
	}

	private JPanel initMainPanel() {
		// mainPanel represents the window content
		JPanel mainPanel = new JPanel();
		GridBagLayout gbl = new GridBagLayout();
		mainPanel.setLayout(gbl);

		// mainPanelFake holds the content and ensures that this is centered
		JPanel mainPanelFake = new JPanel();
		Dimension d1 = new Dimension(GuiDefs.OVERVIEW_PANEL_SIZE);
		mainPanelFake.setMinimumSize(d1);
		mainPanelFake.setMaximumSize(d1);
		mainPanelFake.setPreferredSize(d1);
		mainPanelFake.setSize(d1);

		// generate a scrollable panel for "mainPanel"
		// scrollbarMainPanel ensures that the content is scrollable if the window is too small
		JScrollPane scrollbarMainPanel = new JScrollPane(mainPanelFake);
		scrollbarMainPanel.setMinimumSize(GuiDefs.OVERVIEW_PANEL_SCROLL_PANE_SIZE);
		scrollbarMainPanel.setPreferredSize(GuiDefs.OVERVIEW_PANEL_SCROLL_PANE_SIZE);

		// generate grid bag to center the content
		GridBagConstraints gbc = makegbc(0, 0, 1, 1);
		gbc.weightx = 100;
		gbc.weighty = 100;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;

		gbl.setConstraints(scrollbarMainPanel, gbc);
		mainPanel.add(scrollbarMainPanel);

		add(mainPanel);
		return mainPanelFake;
	}

	private void initWindow() {
		setTitle(UserSettings.getGUITextFromName(GUIElementNames.APP_TITLEBAR));
		setResizable(false);
		setSize(1024, 768);

		try {
			setIconImage(ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResource(GuiDefs.PARK_LOGO_PATH))));
		} catch (IOException e) {
			e.printStackTrace();
		}

		JavaSwingHelperUtil.setWindowInCenter(this);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				exitDialog.setVisible(true);
			}
		});
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}

	/**
	 * Method to init the interface Panel
	 */
	public void initMap() {
		loadImages();
		_map = new ClickableMap(on, off, up, down, map, this.deserializer.getClickableMapObjects());
		_map.addActionListener(this);
		_map.addMouseMovedListener(this);
		_map.addMouseLeftListener(this);
	}

	/**
	 * Method load the config file explicit
	 */
	public void loadConfig() {
		this.deserializer = new AppContentDeserializer(XMLConstants.CONFIGURATION_FILE);
	}

    private void initInfocenter() {
        Optional<WindowContent> infocenterContent = Arrays.stream( this.deserializer.getWindows() )
                                                          .filter( ClickableButton::isActive )
                                                          .filter( c -> c.getId().equals( "infocenter" ) ).findFirst();

        if ( !infocenterContent.isPresent() ) return;

		WindowContent content = infocenterContent.get();
		InfocenterWindow panel = new InfocenterWindow((ExtraWindow) content, this);
		panel.addRefreshGUIListener(configuration -> {
			UserSettings.setConfiguration(configuration);
			resetInterfacePanel();
			new ConfigurationSerializer(configuration).saveConfiguration();
			_infocenter.setVisible(true);
		});
		panel.addMouseClickedOnPatternParkIconListener(object -> showMap());

		this._infocenter = panel;
		this._infocenterMapNumberID = content.getMapNumber();
		this._overviewList.put(this._infocenterMapNumberID, this._infocenter);
	}

	/**
	 * initialize of the Exit Dialog
	 */
	private void initExitDialog() {
		exitDialog = new GenericDialog(this, true, UserSettings.getGUITextFromName(GUIElementNames.DIALOG_EXIT_TEXT), false);
		exitDialog.addOnAcceptListener(l -> this.exit());
		exitDialog.addOnCancelListener(l -> exitDialog.setVisible(false));
		exitDialog.initialize(309, 177);
		JavaSwingHelperUtil.setScreenFriendlySizeAndCentralize(exitDialog);
	}

	/**
	 * initialize of the Start Dialog
	 */
	private void initStartDialog() {
		startDialog = new StartDialog(this, true);
		JavaSwingHelperUtil.setScreenFriendlySizeAndCentralize(startDialog);
	}

	/**
	 * Method is used to set the boolean value for the action command and
	 * disable the button on the image map
	 */
	private void activatePatterns() {
		for(IClickableMapObject clickableObject : this.deserializer.getClickableMapObjects())
			_map.setButtonActive(clickableObject.getMapNumber(), clickableObject.isActive());
	}

	/**
	 * Method to reinitialize the Interface-Panel
	 */
	public void resetInterfacePanel() {
		Set<Integer> keys = new HashSet<>();
		for(Integer id : this._overviewList.keySet()) {
			if(id != this._infocenterMapNumberID) {
				_mainPanel.remove(this._overviewList.get(id));
				keys.add(id);
			}
		}

		for(Integer id : keys)
			this._overviewList.remove(id);

        for ( WindowContent pattern : Arrays.stream( this.deserializer.getWindows() ).filter( ClickableButton::isActive )
                                            .filter( c -> !c.getId().equals( "infocenter" ) )
                                            .collect( Collectors.toList() ) ) {
            WindowPanel<?> panel = WindowPanel.getInstance( pattern, this );

			_overviewList.put(pattern.getMapNumber(), panel);
			panel.setVisible(false);
			panel.addMouseClickedOnPatternParkIconListener(object -> showMap());
			_mainPanel.add(panel);
		}

		this.activatePatterns();
	}

	private void showMap() {
		visibleOff();
		this._map.setVisible(true);
		this._map.resetHighlightAndEnableMouseExit(true);
	}

	/**
	 * This Method handles the received action commands from the InterfacePanel
	 */
	@Override
	public void mouseClickedOnMapButton(JPanel panel, IClickableMapObject object) {
		if(object instanceof SpecialButton) {
			SpecialButton btn = (SpecialButton) object;
			if(btn.getId().equals("exit")) {
				exitDialog.setVisible(true);
			}
		}
		else if(this._overviewList.containsKey(object.getMapNumber())) {
			visibleOff();
			this._overviewList.get(object.getMapNumber()).setVisible(true);
		}
	}

	/**
	 * the exit method
	 *
	 */
	public void exit() {
		System.exit(1);
	}

	/**
	 * Method to load the images used for the InterfacePanel / image-map
	 */
	private void loadImages() {
		try {
			on = ImageIO.read(this.getClass().getClassLoader().getResource("on5.png"));
			off = ImageIO.read(this.getClass().getClassLoader().getResource("off5.png"));
			up = ImageIO.read(this.getClass().getClassLoader().getResource("on5.png"));
			down = ImageIO.read(this.getClass().getClassLoader().getResource("off5.png"));
			map = ImageIO.read(this.getClass().getClassLoader().getResource("karte5.gif"));
			on.flush();
			off.flush();
			up.flush();
			down.flush();
			map.flush();
		} catch (Exception e) {

			System.out.println(e.getMessage());
		}

	}

	/**
	 * Method to set off the visibility of all Panels
	 */
	public void visibleOff() {
		this._map.setVisible(false);
		for(WindowPanel<?> patternOverview : this._overviewList.values())
			patternOverview.setVisible(false);
	}

	/**
	 * This is the main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Start mainFrame = new Start();
		mainFrame.setVisible(true);
		mainFrame.setResizable(true);
	}

	@Override
	public void mouseMovedOnMapButton(JPanel object, IClickableMapObject clickableObject) {
		JavaSwingHelperUtil.setHandCursor(object);
	}

	@Override
	public void mouseLeftOnMapButton(JPanel object, IClickableMapObject clickableObject) {
		JavaSwingHelperUtil.setDefaultCursor(object);
	}
}
