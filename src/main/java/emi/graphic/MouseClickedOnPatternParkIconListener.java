package emi.graphic;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 */
public interface MouseClickedOnPatternParkIconListener {
    void mouseClickedOnPatternParkIcon(JPanel object);
}
