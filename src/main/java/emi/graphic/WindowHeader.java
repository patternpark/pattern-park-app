package emi.graphic;

import emi.GuiDefs;
import emi.models.WindowContent;

import java.awt.Font;
import java.awt.GridBagLayout;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.KeyEvent;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import static emi.util.JavaSwingHelperUtil.loadImage;

/**
 * This JPanel class used in all Overview Panels and Infocenter
 * contains description of the actual displayed Panel
 * and an return to the park map button
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
// TODO: outsource magic numbers
public class WindowHeader extends JPanel {
	private final String title;
	private final String subTitle;
	private final String windowPreviewIconPath;

	private final ArrayList<MouseClickedOnPatternParkIconListener> _listeners = new ArrayList<>();

	/**
	 * This is the default constructor
	 */
	public WindowHeader(String title, String subTitle, String windowPreviewIconPath) {
		super();
		this.title = title;
		this.subTitle = subTitle;
		this.windowPreviewIconPath = windowPreviewIconPath;
		initialize();
	}

	/**
	 * This is the default constructor
	 */
	public WindowHeader(String title, String subTitle) {
		this(title, subTitle, null);
	}

	/**
	 * This is the default constructor
	 */
	public WindowHeader(WindowContent windowContent) {
		this(windowContent.getTitle(), windowContent.getSubTitle(), windowContent.getIcon());
	}

	public void addClickedOnPatternParkIconListener(MouseClickedOnPatternParkIconListener listener) {
		this._listeners.add(listener);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints windowPreviewIconHolder = this.initWindowPreviewImageHolder();

		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridheight = 2;
		gridBagConstraints3.gridx = 0;
		gridBagConstraints3.gridy = 0;
		gridBagConstraints3.gridwidth = 2;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		// gridBagConstraints2.insets = new Insets(20, 318, 23, 19);
		gridBagConstraints2.insets = new Insets(1, 1, 1, 20);
		gridBagConstraints2.gridx = 1;
		gridBagConstraints2.gridy = 0;
		gridBagConstraints2.ipadx = 0;
		gridBagConstraints2.ipady = 0;
		gridBagConstraints2.gridheight = 2;
		gridBagConstraints2.fill = GridBagConstraints.NONE;
		gridBagConstraints2.anchor = GridBagConstraints.EAST;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.insets = new Insets(1, 18, 1, -317);
		gridBagConstraints1.gridy = 1;
		gridBagConstraints1.ipadx = 350;
		gridBagConstraints1.ipady = 14;
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.anchor = GridBagConstraints.WEST;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.insets = new Insets(8, 18, 1, -317);
		gridBagConstraints.gridy = 0;
		gridBagConstraints.ipadx = 350;
		gridBagConstraints.ipady = 14;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		this.setSize(GuiDefs.OVER_VIEW_LABEL_SIZE);
		this.setPreferredSize(GuiDefs.OVER_VIEW_LABEL_SIZE);
		this.setMinimumSize(GuiDefs.OVER_VIEW_LABEL_SIZE);
		this.setMaximumSize(GuiDefs.OVER_VIEW_LABEL_SIZE);
		this.setLayout(new GridBagLayout());
		this.add(initTitle(), gridBagConstraints);
		this.add(initSubTitle(), gridBagConstraints1);
		this.add(initWindowPreviewIcon(), gridBagConstraints2);
		if (windowPreviewIconPath != null) {
			this.add(initPatternLogo(windowPreviewIconPath), windowPreviewIconHolder);
		}
		this.add(initBlueHeaderBox(), gridBagConstraints3);
	}

	private GridBagConstraints initWindowPreviewImageHolder() {
		GridBagConstraints windowPreviewIconHolder = new GridBagConstraints();
		windowPreviewIconHolder.insets = new Insets(1, 1, 1, 123);
		windowPreviewIconHolder.gridx = 1;
		windowPreviewIconHolder.gridy = 0;
		windowPreviewIconHolder.ipadx = -20;
		windowPreviewIconHolder.ipady = -20;
		windowPreviewIconHolder.gridheight = 2;
		windowPreviewIconHolder.fill = GridBagConstraints.NONE;
		windowPreviewIconHolder.anchor = GridBagConstraints.EAST;

		return windowPreviewIconHolder;
	}

	private JLabel initBlueHeaderBox() {
		JLabel bluebox = new JLabel();
		bluebox.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);

		try {
			bluebox.setIcon(new ImageIcon(ImageIO.read(Objects.requireNonNull(this.getClass().getClassLoader().getResource(GuiDefs.BLUE_BOX_PATH)))));
		}
		catch (NullPointerException | IOException e) {
			// exists = false;
			System.out.println("Bluebox Bild konnte nicht geladen werden!");
		}
		return bluebox;
	}

	private JLabel initTitle() {
		JLabel title = new JLabel("JLabel1");
		title.setText(this.title);
		title.setFont(new Font("Arial", Font.BOLD, 22));

		return title;
	}

	private JLabel initSubTitle() {
		JLabel subTitle = new JLabel("JLabel2");
		subTitle.setText(this.subTitle);
		subTitle.setFont(new Font("Arial", Font.BOLD, 22));

		return subTitle;
	}

	/**
	 * Disabled Button just used like an label
	 * @param path
	 * @return
	 */
	private JButton initPatternLogo(String path) {
		JButton patternLogo = new JButton();
		patternLogo.setPreferredSize(new Dimension(50, 50));
		// patternLogo.setMinimumSize(new Dimension(100, 100));
		patternLogo.setIcon(loadImage(path));
		// patternLogo.setBorder(BorderFactory.createLineBorder(java.awt.Color.black,
		// 1));
		patternLogo.setRolloverEnabled(false);
		patternLogo.setDisabledIcon(loadImage(path));
		patternLogo.setEnabled(false);
		patternLogo.setBackground(java.awt.Color.white);

		return patternLogo;
	}

	private JButton initWindowPreviewIcon() {
		JButton parkButton = new JButton();
		parkButton.setPreferredSize(new Dimension(50, 50));
		parkButton.setSize(50, 50);
		parkButton.setBackground(java.awt.Color.white);

		try {
			parkButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(GuiDefs.MAP_BUTTON_PATH))));
		} catch (Exception e) {
			System.out.println("Laden des Bildes f�r den Park Button fehlgeschlagen");
		}
		parkButton.addActionListener(e ->
				this._listeners.forEach(l -> l.mouseClickedOnPatternParkIcon(this))
		);

		return parkButton;
	}
}
