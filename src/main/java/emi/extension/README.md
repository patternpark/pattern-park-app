# External Source

Although the newer developers have put a lot of effort into using external code as a Maven repo, 
unfortunately this is not always possible.

This package contains files (source code) from third-party developers, which has not been further modified.
The classes are mostly so small that they unfortunately couldn't be used as a Maven repo.
