package emi;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class GUIElementNames {
    public static final String APP_NAME = "app_name";
    public static final String APP_TITLEBAR = "app_titlebar";
    public static final String PASSWORD = "password";
    public static final String DIALOG_OKAY = "dialog_okay";
    public static final String DIALOG_YES = "dialog_yes";
    public static final String DIALOG_NO = "dialog_no";
    public static final String DIALOG_CANCEL = "dialog_cancel";
    public static final String DIALOG_ACCEPT = "dialog_accept";
    public static final String DIALOG_EXIT_TEXT = "dialog_exit_text";
    public static final String DIALOG_CHANGE_PASSWORD_BUTTON_TEXT = "dialog_change_password_btn";
    public static final String DIALOG_CHANGE_PASSWORD_NEW_AGAIN = "dialog_change_password_new_again";
    public static final String DIALOG_CHANGE_PASSWORD_NEW = "dialog_change_password_new";
    public static final String DIALOG_CHANGE_PASSWORD_OLD = "dialog_change_password_old";
    public static final String DIALOG_CHANGE_PASSWORD_DESCRIPTION = "dialog_change_password_description";
    public static final String DIALOG_INVALID_DIRECTORY_TITLE = "dialog_invalid_directory_title";
    public static final String DIALOG_INVALID_DIRECTORY_MESSAGE = "dialog_invalid_directory_message";
    public static final String SETTINGS_AUDIO = "settings_audio";
    public static final String SETTINGS_CHANGE_PASSWORD = "settings_change_password";
    public static final String SETTINGS_APPLY = "settings_apply";
    public static final String SETTINGS_SAVE_CHANGES = "settings_save_changes";

    public static final String SETTINGS_ENABLE_PATTERN = "settings_enable_pattern";
    public static final String SETTINGS_NAVIGATION_PREVIOUS = "settings_navigation_previous";
    public static final String SETTINGS_NAVIGATION_NEXT = "settings_navigation_next";
    public static final String SETTINGS_NAVIGATION_PAGE = "settings_navigation_page";
    public static final String SETTINGS_MISC_SETTING_NAME = "settings_misc_setting_name";

    public static final String START_ANIMATION = "start_animation";
    public static final String START_EXERCISES = "start_exercises";
    public static final String DURATION = "duration";
    public static final String TOPIC = "topic";
    public static final String SOURCE_IMAGES = "source_imgs";
    public static final String PATTERN_PARTICIPANTS = "pattern_participants";
    public static final String PATTERN_DISADVANTAGES = "pattern_disadvantages";
    public static final String PATTERN_ADVANTAGES = "pattern_advantages";
    public static final String PATTERN_DESCRIPTION = "pattern_description";
    public static final String PRECONDITIONS = "preconditions";
    public static final String LEARNING_TARGET = "learning_goal";

    private GUIElementNames() {
        throw new IllegalStateException("Utility class");
    }
}
