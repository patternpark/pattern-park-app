package emi;

import emi.tabs.PanelConstants;

import java.awt.Dimension;

/**
 * This abstract class contains 
 * programs component size and paths for images
 * @author Rudolf Koslowski
 * @version 1.0
 */
public class GuiDefs {

	/*
	 * Size and Point declarations used for size of the Panels 
	 */
	
	//Size of the OverviewLabel Label
	public static final Dimension OVER_VIEW_LABEL_SIZE = new Dimension(1000,100);
	//Size of the JPanels in the Pattern Overview Tabbed Pane
	public static final Dimension TABBED_PANE_PANEL_SIZE = new Dimension(980,585);
	//Size of the overviewPanel
	public static final Dimension OVERVIEW_PANEL_SIZE = new Dimension(1000,700);
	//Size of the overviewPanel JScrollPane
	public static final Dimension OVERVIEW_PANEL_SCROLL_PANE_SIZE = new Dimension(1024,740);
	//Size of the Pattern config Panels in Settings
	public static final Dimension PATTERN_CONFIG_SIZE = new Dimension(400,450);
	//Size of the code Pane ScrollPane
	public static final Dimension CODE_SCROLL_PANE_SIZE = new Dimension(980,565);
	//Size of the local URL ScrollPane
	public static final Dimension LOCAL_URL_SCROLL_PANE_SIZE = new Dimension(980,555);
	//Size of the ChangePasswordPanel
	public static final Dimension PASSWORD_PANEL = new Dimension(300,250);

	public static final int HTML_PANEL_SCROLL_SPEED = PanelConstants.TAB_PANEL_SCROLL_SPEED;

	/*
	 * Path declarations for Pictures (*.jpg, *.png...)  
	 */
	//Path of the blue box icon file
	public static final String BLUE_BOX_PATH = "data/grafiken/blue_box.png";
	//Path of the map icon used for Nav Button on the Jlabel that contains the blue box icon
	public static final String MAP_BUTTON_PATH = "data/grafiken/button_park.gif";
	//Path of the pattern icon used for Pattern Overview
	public static final String GLOSSARY_ICON_PATH = "data/grafiken/glossar.png";
	//Path of the animation icon used for Pattern Overview
	public static final String ANIMATION_ICON_PATH = "data/grafiken/animation.png";
	//Path of the sequence diagramm icon used for Pattern Overview
	public static final String SEQ_DIAG_ICON_PATH = "data/grafiken/uml.png";
	//Path of the class diagramm icon used for Pattern Overview
	public static final String CLASS_DIAG_ICON_PATH = "data/grafiken/puzzle.png";
	//Path of the participants icon used for Pattern Overview
	public static final String PARTICIPANTS_ICON_PATH = "data/grafiken/teilnehmer.png";
    //Path of the summary icon used for Pattern Overview
	public static final String SUMMARY_ICON_PATH = "data/grafiken/muster.png";
    //Path of the code icon used for Pattern Overview
	public static final String CODE_ICON_PATH = "data/grafiken/code.png";
	//Path of the settings icon used for infocenter
	public static final String SETTINGS_ICON_PATH = "data/grafiken/einstellungen.png";
	//Path of the links icon used for infocenter
	public static final String LINKS_ICON_PATH = "data/grafiken/links.png";
	//Path of the info icon used for infocenter
	public static final String INFO_ICON_PATH = "data/grafiken/info.png";
	//Path of the Pattern Park Logo used for the mainframe
	public static final String PARK_LOGO_PATH = "data/grafiken/park.gif";

	private GuiDefs() {
		throw new IllegalStateException("Utility class");
	}
}
