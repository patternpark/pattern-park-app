package emi.session;

import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class StartObject {
    private final String _title;
    private final String _introMovie;
    private final String _introMovieOmu;

    public StartObject(Element contentStartElement) {
        _title = contentStartElement.getChildText("title");
        _introMovie = contentStartElement.getChildText("intro_movie");
        _introMovieOmu = contentStartElement.getChildText("intro_movie_omu");
    }

    public String getTitle() {
        return _title;
    }

    public String getIntroMovie() {
        return _introMovie;
    }

    public String getIntroMovieOmu() {
        return _introMovieOmu;
    }
}
