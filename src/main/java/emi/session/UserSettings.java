package emi.session;

import emi.models.configuration.Configuration;
import org.jdom.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class UserSettings {
    private static Configuration _configuration = null;
    private static StartObject _startObject = null;
    private static final ArrayList<Language> _languages = new ArrayList<>();

    public static void readStartObject(Element contentRootElement) {
        _startObject = new StartObject(contentRootElement.getChild("start"));
    }

    public static void setConfiguration(Configuration settings) {
        if(settings == null) return;

        _configuration = settings;
    }

    public static Configuration getConfiguration() {
        return _configuration;
    }

    public static void readLanguages(Element languageRootElement) {
        _languages.clear();

        for(Element languageNode : (List<Element>)languageRootElement.getChildren()) {
            _languages.add(new Language(languageNode));
        }
    }

    public static String getGUITextFromName(String name) {
        Language lang = getLanguage();
        if(lang == null) {
            System.out.println("Language is null!");
            return "-";
        }

        return lang.getGUIElementText(name);
    }

    public static StartObject getStartObject() {
        return _startObject;
    }

    public static Language getLanguage() {
        if(_configuration == null) return null;

        Optional<Language> lang = _languages.stream().filter(l -> l.getId().equals(_configuration.getSettings().getLanguage())).findFirst();
        return lang.orElse(null);
    }
}
