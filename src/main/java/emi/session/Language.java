package emi.session;

import emi.models.RegexMatchFinder;
import org.jdom.Element;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class Language {
    private final String _id;
    private final HashMap<String, String> _guiElements = new HashMap<>();
    private final HashMap<String, String> _variables = new HashMap<>();
    private static final Pattern PATTERN_VARIABLE = Pattern.compile("%[A-Za-z0-9_-]+%");

    public Language(Element languageElement) {
        this._id = languageElement.getAttributeValue("id");

        Element variables = languageElement.getChild("variables");
        for(Element node : (List<Element>)variables.getChildren()) {
            _variables.put(node.getName(), node.getText());
        }

        Element guiNode = languageElement.getChild("gui");
        for(Element node : (List<Element>)guiNode.getChildren()) {
            _guiElements.put(node.getName(), resolveStringWithVariables(node.getText()));
        }
    }

    public String getId() {
        return this._id;
    }

    public String getGUIElementText(String name) {
        if(_guiElements.containsKey(name))
            return _guiElements.get(name);

        System.out.printf("GUI element name \"%s\" not found!\n", name);
        return "-";
    }

    public String resolveStringWithVariables(String text) {
        StringBuilder result = new StringBuilder(text);
        Matcher matcher = PATTERN_VARIABLE.matcher(text);
        ArrayList<RegexMatchFinder> matches = new ArrayList<>();
        while(matcher.find()) {
            matches.add(new RegexMatchFinder(matcher));
        }
        Collections.reverse(matches);

        for(RegexMatchFinder match : matches) {
            String variableName = match.value.substring(1, match.value.length() - 1); // +1 coz "%", -1 coz "%" (end)
            if(this._variables.containsKey(variableName)) {
                result.replace(match.start, match.end, this._variables.get(variableName));
            }
        }

        return result.toString();
    }
}
