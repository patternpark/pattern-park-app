package emi.serializers;

import emi.models.Pattern;
import emi.models.configuration.Configuration;
import emi.tabs.TabContent;
import emi.util.ConverterUtil;
import emi.util.XmlUtil;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class ConfigurationSerializer {
    private final Configuration _configuration;

    public ConfigurationSerializer(Configuration configuration) {
        this._configuration = configuration;
    }

    public void saveConfiguration() {
        XmlUtil.writeXML(generateWholeDocument(), XMLConstants.CONFIGURATION_FILE);
    }

    private Document generateWholeDocument() {
        Document doc = this.generateDocument();
        Element rootElement = this.generateRootElement();
        doc.setRootElement(rootElement);

        rootElement.addContent(this.generateFilesElement());
        rootElement.addContent(this.generateDirectoriesElement());
        for(Element patternElement : this.generatePatterns())
            rootElement.addContent(patternElement);
        rootElement.addContent(this.generateSettings());

        return doc;
    }

    private Document generateDocument() {
        Document document = new Document();
        DocType docType = new DocType(XMLConstants.CONFIGURATION_ROOT_ELEMENT, XMLConstants.CONFIGURATION_DTD_FILE);
        document.setDocType(docType);

        return document;
    }

    private Element generateRootElement() {
        return new Element(XMLConstants.CONFIGURATION_ROOT_ELEMENT);
    }

    private Element generateFilesElement() {
        Element files = new Element(XMLConstants.CONFIGURATION_FILES);

        for(Map.Entry<String, String> file : this._configuration.getFiles().entrySet()) {
            Element fileElement = new Element(file.getKey());
            fileElement.setText(file.getValue());
            files.addContent(fileElement);
        }

        return files;
    }

    private Element generateDirectoriesElement() {
        Element directories = new Element(XMLConstants.CONFIGURATION_DIRECTORIES);

        for(Map.Entry<String, String> dir : this._configuration.getDirectories().entrySet()) {
            Element dirElement = new Element(dir.getKey());
            dirElement.setText(dir.getValue());
            directories.addContent(dirElement);
        }

        return directories;
    }

    private List<Element> generatePatterns() {
        ArrayList<Element> result = new ArrayList<>();

        for(Pattern pattern : this._configuration.getPatterns()) {
            Element patternElement = new Element(XMLConstants.CONFIGURATION_PATTERN);
            patternElement.setAttribute(XMLConstants.CONFIGURATION_PATTERN_ID, pattern.getId());

            Element activeElement = new Element(XMLConstants.CONFIGURATION_PATTERN_ACTIVE);
            activeElement.setText(ConverterUtil.getStringFromBoolean(pattern.isActive()));

            patternElement.addContent(activeElement);

            for(TabContent tab : pattern.getTabs()) {
                Element tabElement = new Element(XMLConstants.CONFIGURATION_PATTERN_TAB);
                tabElement.setAttribute(XMLConstants.CONFIGURATION_PATTERN_TAB_ID, tab.getID());
                tabElement.setText(ConverterUtil.getStringFromBoolean(tab.getIsVisible()));

                patternElement.addContent(tabElement);
            }

            result.add(patternElement);
        }

        return result;
    }

    private Element generateSettings() {
        Element settings = new Element(XMLConstants.CONFIGURATION_SETTINGS);

        Element soundState = new Element(XMLConstants.CONFIGURATION_SETTINGS_SOUND_STATE);
        soundState.setText(ConverterUtil.getStringFromBoolean(this._configuration.getSettings().isSoundState()));

        Element passValue = new Element(XMLConstants.CONFIGURATION_SETTINGS_PASSWORD);
        passValue.setText(this._configuration.getSettings().getPassword());

        Element language = new Element(XMLConstants.CONFIGURATION_SETTINGS_LANGUAGE);
        language.setText(this._configuration.getSettings().getLanguage());



        settings.addContent(soundState);
        settings.addContent(passValue);
        settings.addContent(language);
        return settings;
    }
}
