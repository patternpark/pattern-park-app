package emi.serializers;

import emi.map.IClickableMapObject;
import emi.models.ExtraWindow;
import emi.models.Pattern;
import emi.models.SpecialButton;
import emi.models.WindowContent;
import emi.models.configuration.Configuration;
import emi.session.UserSettings;
import emi.tabs.*;
import emi.util.ConverterUtil;
import emi.util.PathUtil;
import emi.util.XmlUtil;
import org.jdom.Document;
import org.jdom.Element;

import java.io.File;
import java.util.*;

import static emi.util.PathUtil.getRelativePathFromFilePath;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class AppContentDeserializer {
    private static final String INDICATOR_XML_FILE = ".xml";

    private final Document _configXML;
    private Document _contentXML;
    private Document _languagesXML;

    private final Configuration _configuration;
    private final ArrayList<ExtraWindow> _extraWindows = new ArrayList<>();
    private final ArrayList<SpecialButton> _specialButtons = new ArrayList<>();
    private final HashMap<String, Document> _patternsXMLFiles = new HashMap<>();
    private final HashMap<String, Document> _extraWindowXMLFiles = new HashMap<>();

    public AppContentDeserializer( String relativePathConfigXML ) {
        String configXMLPath = getRelativePathFromFilePath( relativePathConfigXML );

        this._configXML = XmlUtil.readXMLDocument( relativePathConfigXML );
        this._configuration = new Configuration( this._configXML.getRootElement(), configXMLPath );

        this.init();
    }

    public IClickableMapObject[] getClickableMapObjects() {
        return this.getClickableMapObjectsList().toArray( new IClickableMapObject[ 0 ] );
    }

    public List<IClickableMapObject> getClickableMapObjectsList() {
        List<IClickableMapObject> result = new ArrayList<>();
        Collections.addAll( result, UserSettings.getConfiguration().getPatterns() );
        Collections.addAll( result, this._specialButtons.toArray( new IClickableMapObject[ 0 ] ) );
        Collections.addAll( result, this._extraWindows.toArray( new IClickableMapObject[ 0 ] ) );

        return result;
    }

    public WindowContent[] getWindows() {
        return this.getWindowsList().toArray( new WindowContent[ 0 ] );
    }

    public List<WindowContent> getWindowsList() {
        List<WindowContent> result = new ArrayList<>();
        Collections.addAll( result, UserSettings.getConfiguration().getPatterns() );
        Collections.addAll( result, this._extraWindows.toArray( new WindowContent[ 0 ] ) );

        return result;
    }

    private void init() {
        this.readSubFiles();
        UserSettings.setConfiguration( _configuration );
        UserSettings.readLanguages( this._languagesXML.getRootElement() );
        UserSettings.readStartObject( this._contentXML.getRootElement() );

        this.readPatternFilesFromDirectory();
        this.readExtraWindowFilesFromDirectory();
        this.readPatterns();
        this.readExtraWindows();
        this.readSpecialButtons();
    }

    private void readSubFiles() {
        this._contentXML = XmlUtil.readXMLDocument( this._configuration.getFile( "content" ) );
        this._languagesXML = XmlUtil.readXMLDocument( this._configuration.getFile( "languages" ) );
    }

    private void readPatternFilesFromDirectory() {
        File[] xmlFiles = PathUtil.getFilesFromDirectory(
                _configuration.getDirectory( "patterns" ),
                ( file -> file.getName().toLowerCase().endsWith( INDICATOR_XML_FILE ) )
        );
        for ( File file : xmlFiles ) {
            Document xmlFile = XmlUtil.readXMLDocumentFromAbsoluteFile( file.getAbsolutePath() );
            this._patternsXMLFiles.put( xmlFile.getRootElement().getAttributeValue( XMLConstants.PATTERN_ID ), xmlFile );
        }
    }

    private void readExtraWindowFilesFromDirectory() {
        File[] xmlFiles = PathUtil.getFilesFromDirectory(
                _configuration.getDirectory( "extra-windows" ),
                ( file -> file.getName().toLowerCase().endsWith( INDICATOR_XML_FILE ) )
        );
        for ( File file : xmlFiles ) {
            Document xmlFile = XmlUtil.readXMLDocumentFromAbsoluteFile( file.getAbsolutePath() );
            this._extraWindowXMLFiles.put( xmlFile.getRootElement().getAttributeValue( XMLConstants.PATTERN_ID ), xmlFile );
        }
    }

    private void readPatterns() {
        Element rootElement = this._configXML.getRootElement();
        for ( Element configPattern : (List<Element>) rootElement.getChildren( XMLConstants.CONFIGURATION_PATTERN ) ) {
            String patternID = configPattern.getAttributeValue( XMLConstants.CONFIGURATION_PATTERN_ID );
            if ( !this._patternsXMLFiles.containsKey( patternID ) ) {
                System.out.printf( "Pattern \"%s\" not found as file - skipped\n", patternID );
                continue;
            }

            List<Element> activeTabs = (List<Element>) configPattern.getChildren( XMLConstants.CONFIGURATION_PATTERN_TAB );
            Element filePattern = this._patternsXMLFiles.get( patternID ).getRootElement();
            Pattern modelPattern = new Pattern(
                    filePattern, ConverterUtil.getBooleanFromString(
                    configPattern.getChildText( XMLConstants.CONFIGURATION_PATTERN_ACTIVE ) ) );

            Element tabs = filePattern.getChild( XMLConstants.PATTERN_TABS );
            if ( tabs == null ) {
                System.out.printf( "Pattern %s has no tabs - skipped\n", patternID );
                continue;
            }

            for ( Element tabElement : (List<Element>) tabs.getChildren( XMLConstants.PATTERN_TABS_TAB ) ) {
                String tabID = tabElement.getAttributeValue( XMLConstants.PATTERN_ID );
                Optional<Element> activeTab = activeTabs.stream().filter(
                        t -> t.getAttributeValue( XMLConstants.CONFIGURATION_PATTERN_TAB_ID ).equals( tabID ) ).findFirst();
                if ( !activeTab.isPresent() ) {
                    System.out.printf( "Tab \"%s\" doesn't exit in config for tab \"%s\" - skipped\n", tabID, patternID );
                    continue;
                }

                TabContent tab = this.parseTabContentFromDocumentAndCrossElement(
                        tabElement, ConverterUtil.getBooleanFromString( activeTab.get().getText() ) );
                if ( tab == null ) {
                    System.out.printf(
                            "Tab class for \"%s\" doesn't exit\n",
                            tabElement.getAttributeValue( XMLConstants.PATTERN_TABS_TAB_TYPE )
                    );
                    continue;
                }

                modelPattern.addTab( tab );
            }

            this._configuration.addPattern( modelPattern );
        }
    }

    private void readExtraWindows() {
        for ( Document extraWindowDoc : this._extraWindowXMLFiles.values() ) {
            Element filePattern = extraWindowDoc.getRootElement();
            String patternID = filePattern.getAttributeValue( XMLConstants.PATTERN_ID );
            ExtraWindow modelPattern = new ExtraWindow( filePattern );

            Element tabs = filePattern.getChild( XMLConstants.PATTERN_TABS );
            if ( tabs == null ) {
                System.out.printf( "Pattern %s has no tabs - skipped\n", patternID );
                continue;
            }

            for ( Element tabElement : (List<Element>) tabs.getChildren( XMLConstants.PATTERN_TABS_TAB ) ) {
                TabContent tab = this.parseTabContentFromDocumentAndCrossElement( tabElement, true );
                if ( tab == null ) {
                    System.out.printf(
                            "Tab class for \"%s\" doesn't exit\n",
                            tabElement.getAttributeValue( XMLConstants.PATTERN_TABS_TAB_TYPE )
                    );
                    continue;
                }

                modelPattern.addTab( tab );
            }

            _extraWindows.add( modelPattern );
        }
    }

    private void readSpecialButtons() {
        Element rootElement = this._contentXML.getRootElement();

        for ( Element specialBtnNode : (List<Element>) rootElement.getChildren( "special_button" ) ) {
            SpecialButton btn = SpecialButton.getInstance( specialBtnNode );
            _specialButtons.add( btn );
        }
    }

    private TabContent parseTabContentFromDocumentAndCrossElement( Element patternElement, boolean isVisible ) {
        String type = patternElement.getAttributeValue( XMLConstants.PATTERN_TABS_TAB_TYPE );
        if ( type.equals( ETabTypeName.ANIMATION.toString() ) ) {
            return new AnimationTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.ANIMATION_WITH_SOURCES.toString() ) ) {
            return new AnimationTabContentWithSources( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.UML.toString() ) ) {
            return new UmlTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.OVERVIEW.toString() ) ) {
            return new OverviewTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.PARTICIPANTS.toString() ) ) {
            return new ParticipantTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.THE_PATTERN.toString() ) ) {
            return new ThePatternTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.PUZZLE.toString() ) ) {
            return new PuzzleTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.CODE.toString() ) ) {
            return new CodeTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.LOCAL_URLS_HTML.toString() ) ) {
            return new LocalURLsHTMLTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.EXTERNAL_URLS_HTML.toString() ) ) {
            return new ExternalURLsHTMLTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.INFO.toString() ) ) {
            return new InfoTabContent( patternElement, isVisible );
        } else if ( type.equals( ETabTypeName.SETTINGS.toString() ) ) {
            return new SettingsTabContent( patternElement, isVisible );
        }

        return null;
    }
}
