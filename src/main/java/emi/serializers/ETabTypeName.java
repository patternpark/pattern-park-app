package emi.serializers;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public enum ETabTypeName {
    ANIMATION("animation"),
    ANIMATION_WITH_SOURCES("animation-with-sources"),
    CODE("code"),
    UML("uml"),
    OVERVIEW("overview"),
    PARTICIPANTS("participants"),
    PUZZLE("puzzle"),
    THE_PATTERN("thePattern"),
    LOCAL_URLS_HTML("local-urls-html"),
    EXTERNAL_URLS_HTML("external-urls-html"),
    SETTINGS("settings"),
    INFO("info"),
    ;

    /**
     * The value of the enum.
     */
    private final String text;

    /**
     * @param text The value of the enum.
     */
    ETabTypeName(final String text) {
        this.text = text;
    }

    /**
     * Gets the value of the enum.
     *
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
