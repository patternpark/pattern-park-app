package emi.serializers;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class XMLConstants {
    private XMLConstants() {}

    // constants for "config.xml"
    public static final String CONFIGURATION_DTD_FILE = "config.dtd";
    public static final String CONFIGURATION_FILE = "data/config/config.xml";


    public static final String CONFIGURATION_ROOT_ELEMENT = "config";

    public static final String CONFIGURATION_FILES = "files";
    public static final String CONFIGURATION_DIRECTORIES = "directories";
    public static final String CONFIGURATION_SETTINGS = "settings";
    public static final String CONFIGURATION_PATTERN = "pattern";
    public static final String CONFIGURATION_PATTERN_ID = "id"; // attribute
    public static final String CONFIGURATION_PATTERN_ACTIVE = "active"; // element
    public static final String CONFIGURATION_PATTERN_TAB = "tab"; // element
    public static final String CONFIGURATION_PATTERN_TAB_ID = "id"; // attribute

    public static final String CONFIGURATION_SETTINGS_SOUND_STATE = "sound_state"; // element
    public static final String CONFIGURATION_SETTINGS_PASSWORD = "pass_value"; // element
    public static final String CONFIGURATION_SETTINGS_LANGUAGE = "language"; // element


    // constants for data/patterns
    public static final String PATTERN_ID = "id"; // attribute
    public static final String PATTERN_TABS = "tabs"; // element
    public static final String PATTERN_TABS_TAB = "tab"; // sub element of "tabs"
    public static final String PATTERN_TABS_TAB_TYPE = "type"; // attribute
}
