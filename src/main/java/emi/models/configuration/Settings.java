package emi.models.configuration;

import emi.serializers.XMLConstants;
import org.jdom.Element;

import static emi.util.ConverterUtil.getBooleanFromString;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class Settings implements Cloneable {
    private boolean _soundState;
    private String _password;
    private String _language;

    public Settings(Element settingsElement) {
        this._soundState = getBooleanFromString(settingsElement.getChildText(XMLConstants.CONFIGURATION_SETTINGS_SOUND_STATE));
        this._password = settingsElement.getChildText(XMLConstants.CONFIGURATION_SETTINGS_PASSWORD);
        this._language = settingsElement.getChildText(XMLConstants.CONFIGURATION_SETTINGS_LANGUAGE);
    }

    protected Settings(boolean soundState, String password, String language) {
        this._soundState = soundState;
        this._password = password;
        this._language = language;
    }

    public boolean isSoundState() {
        return _soundState;
    }

    public void setSoundState(boolean soundState) {
        this._soundState = soundState;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        this._password = password;
    }

    public String getLanguage() {
        return _language;
    }

    public void setLanguage(String language) {
        this._language = language;
    }

    @Override
    public Object clone() {
        Settings settings;
        try {
            settings = (Settings) super.clone();
        } catch (CloneNotSupportedException e) {
            settings = new Settings(this._soundState, this._password, this._language);
        }
        return settings;
    }
}
