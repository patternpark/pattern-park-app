package emi.models.configuration;

import emi.models.Pattern;
import emi.serializers.XMLConstants;
import org.jdom.Element;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static emi.util.PathUtil.combinePathParts;
import static emi.util.PathUtil.getAbsolutePathFromRelativeFile;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public class Configuration implements Cloneable {
    private final LinkedHashMap<String, String> _files = new LinkedHashMap<>();
    private final LinkedHashMap<String, String> _directories = new LinkedHashMap<>();
    private final ArrayList<Pattern> _patterns = new ArrayList<>();
    private final Settings _settings;
    private final String _configXMLPath;

    public Configuration(Element configRootElement, String configXMLPath) {
        this._configXMLPath = configXMLPath;

        Element files = configRootElement.getChild(XMLConstants.CONFIGURATION_FILES);
        for(Element fileNode : (List<Element>)files.getChildren())
            _files.put(fileNode.getName(), fileNode.getValue());

        Element directories = configRootElement.getChild(XMLConstants.CONFIGURATION_DIRECTORIES);
        for(Element dirNode : (List<Element>)directories.getChildren())
            _directories.put(dirNode.getName(), dirNode.getValue());

        Element settingsNode = configRootElement.getChild(XMLConstants.CONFIGURATION_SETTINGS);
        _settings = new Settings(settingsNode);
    }

    protected Configuration(Settings settings, String configXMLPath) {
        this._settings = settings;
        this._configXMLPath = configXMLPath;
    }

    public LinkedHashMap<String, String> getDirectories() {
        return new LinkedHashMap<>(this._directories);
    }

    public LinkedHashMap<String, String> getFiles() {
        return new LinkedHashMap<>(this._files);
    }

    public String getDirectory(String name) {
        if(this._directories.containsKey(name))
            return getAbsolutePathFromRelativeFile(this._directories.get(name));

        return null;
    }

    public String getFile(String name) {
        if(this._files.containsKey(name))
            return combinePathParts(this._configXMLPath, this._files.get(name));

        return null;
    }

    public Settings getSettings() {
        return this._settings;
    }

    public Pattern[] getPatterns() {
        return this._patterns.toArray(new Pattern[0]);
    }

    public void addPattern(Pattern pattern) {
        this._patterns.add(pattern);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Configuration config = new Configuration((Settings) this._settings.clone(), this._configXMLPath);
        config._directories.putAll(this.getDirectories());
        config._files.putAll(this.getFiles());
        for(Pattern pattern : _patterns) {
            config._patterns.add((Pattern) pattern.clone());
        }
        return config;
    }
}
