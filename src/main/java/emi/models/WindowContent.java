package emi.models;

import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class WindowContent extends ClickableButton implements Cloneable {
    private final String _name;
    private final String _icon;
    private final String _title;
    private final String _subTitle;

    protected WindowContent(Element contentWindowElement, boolean isActive) {
        this._name = contentWindowElement.getChildText("name");
        this._icon = contentWindowElement.getChildText("icon");
        this._title = contentWindowElement.getChildText("title");
        this._subTitle = contentWindowElement.getChildText("sub_title");

        setValues(this, contentWindowElement, isActive);
    }

    public String getName() {
        return _name;
    }

    public String getIcon() {
        return _icon;
    }

    public String getTitle() {
        return _title;
    }

    public String getSubTitle() {
        return _subTitle;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
