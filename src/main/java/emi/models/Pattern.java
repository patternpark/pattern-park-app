package emi.models;

import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class Pattern extends WindowContentWithTabs {
    public Pattern(Element patternElement, boolean isActive) {
        super(patternElement, isActive);
    }
}
