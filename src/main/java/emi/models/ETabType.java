package emi.models;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public enum ETabType {
    OVERVIEW,
    ANIMATION,
    ANIMATION_WITH_SOURCES,
    PARTICIPANT, // alias "no_uml"
    UML,
    PUZZLE,
    PATTERN,
    CODE,
    LOCAL_HTML_FILE,
    LOCAL_HTML_FILE_WITH_EXTERNAL_LINKS,
    SETTINGS,
    INFO,
}
