package emi.models;

import emi.map.IClickableMapObject;
import org.jdom.Element;

import static emi.util.ConverterUtil.getIntegerFromString;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class ClickableButton implements IClickableMapObject {
    private String _id;
    private String _tooltipText;
    private String _actionCommand;
    private int _mapNumber;
    private boolean _isActive;

    protected static void setValues(ClickableButton btn, Element contentPatternElement, boolean isActive) {
        btn._id = contentPatternElement.getAttributeValue("id");
        btn._tooltipText = contentPatternElement.getChildText("tooltip_text");
        btn._actionCommand = contentPatternElement.getChildText("action_command");

        btn._mapNumber = getIntegerFromString(contentPatternElement.getChildText("map_number"));
        btn._isActive = isActive;
    }

    protected static void setValues(ClickableButton btn, Element contentPatternElement) {
        setValues(btn, contentPatternElement, true);
    }

    public String getId() {
        return _id;
    }

    @Override
    public String getTooltipText() {
        return _tooltipText;
    }

    @Override
    public String getActionCommand() {
        return _actionCommand;
    }

    @Override
    public int getMapNumber() {
        return _mapNumber;
    }

    @Override
    public boolean isActive() {
        return this._isActive;
    }

    public void setActiveState(boolean isActive) {
        this._isActive = isActive;
    }
}
