package emi.models;

import java.util.regex.Matcher;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class RegexMatchFinder {
    public final int start;
    public final int end;
    public final String value;

    public RegexMatchFinder(int start, int end, String value) {
        this.start = start;
        this.end = end;
        this.value = value;
    }

    public RegexMatchFinder(Matcher matcher) {
        this(matcher.start(), matcher.end(), matcher.group());
    }
}
