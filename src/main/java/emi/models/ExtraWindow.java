package emi.models;

import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class ExtraWindow extends WindowContentWithTabs {
    public ExtraWindow(Element patternElement) {
        super(patternElement, true);
    }
}
