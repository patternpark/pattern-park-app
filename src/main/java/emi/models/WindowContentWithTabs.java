package emi.models;

import emi.tabs.TabContent;
import org.jdom.Element;

import java.util.ArrayList;
import java.util.Optional;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class WindowContentWithTabs extends WindowContent {
    private ArrayList<TabContent> _tabs = new ArrayList<>();

    protected WindowContentWithTabs( Element contentPatternElement, boolean isActive ) {
        super( contentPatternElement, isActive );
    }

    public TabContent[] getTabs() {
        return _tabs.stream().toArray( TabContent[]::new );
    }

    public void addTab( TabContent tab ) {
        this._tabs.add( tab );
    }

    public TabContent getTab( ETabType type ) {
        Optional<TabContent> tab = this._tabs.stream().filter( ( t -> t.getType() == type ) ).findFirst();

        return tab.orElse( null );
    }

    public TabContent[] getTabs( ETabType type ) {
        return this._tabs.stream().filter( ( t -> t.getType() == type ) ).toArray( TabContent[]::new );
    }

    public boolean isOneTabVisible() {
        return _tabs.stream().anyMatch( TabContent::getIsVisible );
    }

    @Override
    public boolean isActive() {
        return super.isActive() && this.isOneTabVisible();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        WindowContentWithTabs result = (WindowContentWithTabs) super.clone();

        result._tabs = new ArrayList<>();
        for ( TabContent tab : this._tabs ) {
            result._tabs.add( (TabContent) tab.clone() );
        }

        return result;
    }
}
