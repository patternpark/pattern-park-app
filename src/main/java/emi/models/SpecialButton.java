package emi.models;

import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class SpecialButton extends ClickableButton {
    private SpecialButton() {}

    public static SpecialButton getInstance(Element contentPatternElement) {
        SpecialButton result = new SpecialButton();

        setValues(result, contentPatternElement);
        return result;
    }
}
