package emi.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class HashUtil {
    private static MessageDigest _md5;

    static {
        try {
            _md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String getMD5String(String word){
        StringBuilder result = new StringBuilder();
        for(byte md5Byte : _md5.digest(word.getBytes())) {
            result.append(Integer.toHexString(md5Byte & 0xFF));
        }

        return result.toString();
    }
}
