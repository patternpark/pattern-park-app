package emi.util;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class ConverterUtil {
    private ConverterUtil() {}

    public static boolean getBooleanFromString(String value) {
        return value.equals("true");
    }

    public static int getIntegerFromString(String value) {
        try {
            return Integer.parseInt(value);
        }
        catch (NumberFormatException ex) {
            System.out.printf("Exception %s%n", ex.getMessage());
        }

        return Integer.MIN_VALUE;
    }

    public static String getStringFromBoolean(boolean bool) {
        return bool ? "true" : "false";
    }
}
