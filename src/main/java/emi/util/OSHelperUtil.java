// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Alias)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

package emi.util;

import emi.LinksFrame;
import emi.enums.EElectronApplicationType;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.Locale;

/**
 * Provides various functions for launching applications that are OS specific, as well as detecting the OS.
 *
 * @remark This class originates from the emi.Start class, among others, and was thus decoupled.
 * @author Rudolf Koslowski (v1.0)
 * @author Erik der Glückliche (Alias) (v1.7)
 * @version 1.7
 * @see emi.Start
 */
public final class OSHelperUtil {
    /**
     * Windows OS flag
     */
    private static boolean IS_WIN = false;

    /**
     * Linux OS flag
     */
    private static boolean IS_LIN = false;

    /**
     * Mac OS flag
     */
    private static boolean IS_MAC = false;

    private OSHelperUtil() {
    }

    /**
     * Detects the current Operating System based on the raw System Properties, and sets the flags accordingly
     * */
    public static void detectOS()
    {
        String os_name = System.getProperty("os.name").toLowerCase(Locale.ROOT);
        if(os_name.contains("win")){
            IS_WIN = true;
        } else if(os_name.contains("nux") || os_name.contains("nix") || os_name.contains("aix")){
            // this matches all unix-like systems
            IS_LIN = true;
        } else if(os_name.contains("mac")){
            IS_MAC = true;
        }
    }

    /**
     * @return the Linux flag isLin
     */
    public static boolean isLin() {
        return IS_LIN;
    }

    /**
     * @return the Mac flag isMac
     */
    public static boolean isMac() {
        return IS_MAC;
    }

    /**
     * @return the Windows flag isWin
     */
    public static boolean isWin() {
        return IS_WIN;
    }

    /**
     * Launches a Flash application/video based on the relative path.
     *
     * @param filePath The path to the application or Flash Movie (depending on OS) to be launched.
     */
    public static void startFlash(String filePath) {
        try {
            Runtime myRuntime = Runtime.getRuntime();

            String flashplayer = buildFlashPath();

            String film = PathUtil.combinePathParts(System.getProperty("user.dir"), filePath);
            String playcommando = String.format("%s file://%s", flashplayer, film);
            myRuntime.exec(playcommando);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Builds a String of the Path to the correct Flashplayer executable depending on the current operating System
     * */
    private static String buildFlashPath(){
        if (isLin()) {
            return PathUtil.combinePathParts(System.getProperty("user.dir"), "data", "players", "flashplayer");
        } else if (isMac()){
            return PathUtil.combinePathParts(System.getProperty("user.dir"), "data", "players", "flashplayer.dmg");
        } else {
            return PathUtil.combinePathParts(System.getProperty("user.dir"), "data", "players", "flashplayer.exe");
        }
    }

    /** constructs the command needed to launch a URL in the default-application
     * @source https://stackoverflow.com/questions/5226212/how-to-open-the-default-webbrowser-using-java
     * */
    public static String constructLaunchCommandForBrowser(String fileUrl) {
            if(isLin())
                return String.format("xdg-open %s",fileUrl);
            else if(isMac())
                return String.format("open %s",fileUrl);
            else
                return String.format("start \"%s\"",fileUrl);
    }

    /**
     * Launches the (newer) tasks of the Pattern Park as Electron application.
     *
     * @param url The url to the exercise.
     */
    public static void startElectronExercise(String url) {
        try {
            if (isWin())
                startElectronWin(EElectronApplicationType.EXERCISE_URL, url);
            else if (isMac())
                starElectronMac(EElectronApplicationType.EXERCISE_URL, url);
            else if (isLin())
                startElectronLinux(EElectronApplicationType.EXERCISE_URL, url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Launches the (newer) movies of the Pattern Park as Electron application.
     *
     * @param path The file path to the movie.
     */
    public static void startElectronMovie(String path) {
        try {
            if (isWin())
                startElectronWin(EElectronApplicationType.MOVIE, path);
            else if (isMac())
                starElectronMac(EElectronApplicationType.MOVIE, path);
            else if (isLin())
                startElectronLinux(EElectronApplicationType.MOVIE, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Launches the (newer) movies with subtitles of the Pattern Park as Electron application.
     *
     * @param path The file path to the movie.
     * @param subtitles The file path to the subtitles of the movie.
     */
    public static void startElectronMovie(String path, String subtitles) {
        try {
            if (isWin())
                startElectronWin(EElectronApplicationType.MOVIE_WITH_SUBTITLES, path, subtitles);
            else if (isMac())
                starElectronMac(EElectronApplicationType.MOVIE_WITH_SUBTITLES, path, subtitles);
            else if (isLin())
                startElectronLinux(EElectronApplicationType.MOVIE_WITH_SUBTITLES, path, subtitles);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Launches the Electron application under Windows OS.
     * @param type The type of task in which form the Electron application should be started.
     * @param fileOrUrl The file or a complete URL which should be displayed. Remark: The file path should be relative to the Electron application so that Electron can read the file.
     * @throws IOException Thrown if the file cannot be found or is invalid.
     */
    private static void startElectronWin(EElectronApplicationType type, String fileOrUrl) throws IOException {
        startElectronWin(type, fileOrUrl, "");
    }

    /**
     * Launches the Electron application under Mac OS.
     * @param type The type of task in which form the Electron application should be started.
     * @param fileOrUrl The file or a complete URL which should be displayed. Remark: The file path should be relative to the Electron application so that Electron can read the file.
     * @throws IOException Thrown if the file cannot be found or is invalid.
     */
    private static void starElectronMac(EElectronApplicationType type, String fileOrUrl) throws IOException {
        starElectronMac(type, fileOrUrl, "");
    }

    /**
     * Launches the Electron application under Linux OS.
     * @param type The type of task in which form the Electron application should be started.
     * @param fileOrUrl The file or a complete URL which should be displayed. Remark: The file path should be relative to the Electron application so that Electron can read the file.
     * @throws IOException Thrown if the file cannot be found or is invalid.
     */
    private static void startElectronLinux(EElectronApplicationType type, String fileOrUrl) throws IOException {
        startElectronLinux(type, fileOrUrl, "");
    }

    /**
     * Launches the Electron application under Windows OS.
     * @param type The type of task in which form the Electron application should be started.
     * @param fileOrUrl The file or a complete URL which should be displayed. Remark: The file path should be relative to the Electron application so that Electron can read the file.
     * @param subtitles The relative path to the subtitles to the video. This path should be relative to the Electron application so that Electron can read the file.
     * @throws IOException Thrown if the file cannot be found or is invalid.
     */
    private static void startElectronWin(EElectronApplicationType type, String fileOrUrl, String subtitles) throws IOException {
        String electronApp = PathUtil.combinePathParts(System.getProperty("user.dir"), "data", "exercises", "win32", "pattern-park-exercises.exe");
        new ProcessBuilder(electronApp, type.toString(), fileOrUrl, subtitles).start();
    }

    /**
     * Launches the Electron application under Mac OS.
     * @param type The type of task in which form the Electron application should be started.
     * @param fileOrUrl The file or a complete URL which should be displayed. Remark: The file path should be relative to the Electron application so that Electron can read the file.
     * @param subtitles The relative path to the subtitles to the video. This path should be relative to the Electron application so that Electron can read the file.
     * @throws IOException Thrown if the file cannot be found or is invalid.
     */
    private static void starElectronMac(EElectronApplicationType type, String fileOrUrl, String subtitles) throws IOException {
        String electronApp = PathUtil.combinePathParts(System.getProperty("user.dir"), "data", "exercises", "mac", "pattern-park-exercises.dmg");
        new ProcessBuilder(electronApp, type.toString(), fileOrUrl, subtitles).start();
    }

    /**
     * Launches the Electron application under Linux OS.
     * @param type The type of task in which form the Electron application should be started.
     * @param fileOrUrl The file or a complete URL which should be displayed. Remark: The file path should be relative to the Electron application so that Electron can read the file.
     * @param subtitles The relative path to the subtitles to the video. This path should be relative to the Electron application so that Electron can read the file.
     * @throws IOException Thrown if the file cannot be found or is invalid.
     */
    private static void startElectronLinux(EElectronApplicationType type, String fileOrUrl, String subtitles) throws IOException {
        String electronApp = PathUtil.combinePathParts(System.getProperty("user.dir"), "data", "exercises", "lin64", "pattern-park-exercises");
        new ProcessBuilder(electronApp, type.toString(), fileOrUrl, subtitles).start();
    }
}
