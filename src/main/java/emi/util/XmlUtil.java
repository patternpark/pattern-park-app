package emi.util;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static emi.util.PathUtil.getAbsolutePathFromRelativeFile;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public final class XmlUtil {
    private static final String OUTPUT_INDENT = "    ";

    private XmlUtil() {}

    public static Document readXMLDocument(String filePath) {
        File file = new File(filePath);
        if (file.isAbsolute()) {
            return readXMLDocumentFromAbsoluteFile(filePath);
        }
        return readXMLDocumentFromAbsoluteFile(file.getAbsolutePath());
    }

    public static Document readXMLDocumentFromAbsoluteFile(String absolutePath) {
        Document doc = null;
        SAXBuilder builder = new SAXBuilder();

        try {
            File file = new File(absolutePath);
            builder.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            doc = builder.build(file);
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }

        return doc;
    }

    public static void writeXML(Document document, String relativeOutputPath) {
        writeXMLToAbsoluteFile(document, getAbsolutePathFromRelativeFile(relativeOutputPath));
    }

    public static void writeXMLToAbsoluteFile(Document document, String absoluteOutputPath) {
        Format format = Format.getPrettyFormat();
        format.setIndent(OUTPUT_INDENT);
        XMLOutputter outputter = new XMLOutputter(format);
        try {
            FileOutputStream output = new FileOutputStream(absoluteOutputPath);
            outputter.output(document, output);
            output.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
