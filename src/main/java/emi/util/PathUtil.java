// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Alias)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

package emi.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Function;

/**
 * Provides various functions that affect a path/file.
 *
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class PathUtil {
    private PathUtil() {
    }

    /**
     * Gets an absolute path based on a relative path.
     *
     * @param relativePath The specified path, which should become absolute.
     * @return Return of the absolute path.
     */
    public static String getAbsolutePathFromRelativeFile( String relativePath ) {
        // TODO: just return new File(relativePath) (?)
        return String.format(
                "%s%s%s", System.getProperty( "user.dir" ), System.getProperty( "file.separator" ), relativePath );
    }

    /**
     * Combines a list of paths into one.
     *
     * @param files A list of folders and optionally a file at the end to be concatenated.
     * @return A concatenated path of all specified folders.
     */
    public static String combinePathParts( String... files ) {
        return String.join( System.getProperty( "file.separator" ), files );
    }

    /**
     * Gets the relative path of a file path which is itself relative.
     *
     * @param filePath The relative path of a file whose parent folder is to be returned.
     * @return Returns the relative folder path of the specified file path.
     */
    public static String getRelativePathFromFilePath( String filePath ) {
        File file = new File( filePath );
        // now file is absolute

        int fileNameLen = file.getName().length();
        if ( fileNameLen >= filePath.length() ) {
            return "";
        }

        return filePath.substring( 0, filePath.length() - 1 - fileNameLen );
    }

    /**
     * Gets the file extension of a file path.
     *
     * @param file The file as string whose extension should be returned.
     * @return The extension of the file, without a dot. If no extension is present, then an empty string is returned.
     */
    public static String getFileExtension( String file ) {
        return getFileExtension( new File( file ) );
    }

    /**
     * Gets the file extension of a file path.
     *
     * @param file The file whose extension should be returned.
     * @return The extension of the file, without a dot. If no extension is present, then an empty string is returned.
     */
    public static String getFileExtension( File file ) {
        String name = file.getName();
        int lastIndexOfDot = name.lastIndexOf( "." );
        if ( lastIndexOfDot < 0 || lastIndexOfDot >= name.length() ) {
            return "";
        }

        return name.substring( lastIndexOfDot + 1 ); // +1 coz remove the dot
    }

    public static String readFileFromRelativePath( String file ) {
        ;
        StringBuilder resultStringBuilder = new StringBuilder();
        Path path = Paths.get( getAbsolutePathFromRelativeFile( file ) );
        try ( BufferedReader br = Files.newBufferedReader(
                path, StandardCharsets.UTF_8 ) ) {
            String line;
            while ( ( line = br.readLine() ) != null ) {
                resultStringBuilder.append( line ).append( "\n" );
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return resultStringBuilder.toString();
    }

    public static File[] getFilesFromDirectoryRecursive( String path, Function<File, Boolean> filter ) {
        ArrayList<String> directories = new ArrayList<>( Collections.singletonList( path ) );
        ArrayList<File> result = new ArrayList<>();
        while ( !directories.isEmpty() ) {
            String directory = directories.get( 0 );

            directories.remove( 0 );

            File dir = new File( directory );
            File[] files = dir.listFiles();
            if ( files == null ) continue;

            for ( File file : files ) {
                if ( file.isDirectory() ) {
                    directories.add( file.getAbsolutePath() );
                } else if ( filter.apply( file ) ) {
                    result.add( file );
                }
            }
        }

        return result.stream().toArray( File[]::new );
    }

    public static File[] getFilesFromDirectoryRecursive( String path ) {
        return getFilesFromDirectoryRecursive( path, ( file -> true ) );
    }

    public static boolean isChild(Path child, String relativeParent) {
        Path parent = Paths.get(relativeParent).toAbsolutePath();
        return child.startsWith(parent);
    }

    public static File[] getFilesFromDirectory( String directory, Function<File, Boolean> filter ) {
        ArrayList<File> result = new ArrayList<>();
        File dir = new File( directory );
        File[] files = dir.listFiles();

        if ( files == null ) return new File[] {};

        for ( File file : files ) {
            if ( file.isFile() && filter.apply( file ) ) {
                result.add( file );
            }
        }

        return result.stream().toArray( File[]::new );
    }
}
