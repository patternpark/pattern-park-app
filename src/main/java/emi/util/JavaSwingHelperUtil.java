package emi.util;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class JavaSwingHelperUtil {
    private JavaSwingHelperUtil() {}

    /**
     * MEthod to generate GridBagConstraints, used for the GridBag Layout
     *
     * @param x
     * @param y
     * @param width
     * @param height
     * @return
     */
    public static GridBagConstraints makegbc(int x, int y, int width, int height) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = width;
        gbc.gridheight = height;
        gbc.insets = new Insets(1, 1, 1, 1);
        return gbc;
    }

    /**
     * This Method load an Image from the given path and test if this file
     * exists
     *
     * @param path
     * @return
     */
    public static ImageIcon loadImage(String path) {
        ImageIcon image = null;
        boolean exists = false;
        String test = null;
        try {
            test = System.getProperty("user.dir")
                    + System.getProperty("file.separator") + path;
            // System.out.println(test);
            exists = new File(test).exists();
            // System.out.println("exists :" + exists);
        }

        catch (NullPointerException e) {
            exists = false;
            // System.out.println("Laden der bilddatei gescheitert");
        }

        if (exists) {
            // jl_PatternPicture.setIcon(new ImageIcon(test));
            image = new ImageIcon(test);
            image.getImage().flush();
            return image;
        }
        else {
            // System.out.println("Bild f�r Muster�bersicht nicht geladen");
            return null;
        }
    }

    /**
     * Method to set the Hand Cursor
     *
     * @param c
     */
    public static void setHandCursor(Component c) {
        Cursor cur = new Cursor(Cursor.HAND_CURSOR);
        c.setCursor(cur);
    }

    /**
     * Method to set the default cursor
     *
     * @param c
     */
    public static void setDefaultCursor(Component c) {
        Cursor cur = new Cursor(Cursor.DEFAULT_CURSOR);
        c.setCursor(cur);

    }

    public static void setScreenFriendlySizeAndCentralize(Component component) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setScreenFriendlySize(component, screenSize);
        setWindowInCenter(component, screenSize);
    }

    public static void setScreenFriendlySize(Component component) {
        setScreenFriendlySize(component, Toolkit.getDefaultToolkit().getScreenSize());
    }

    public static void setWindowInCenter(Component component) {
        setWindowInCenter(component, Toolkit.getDefaultToolkit().getScreenSize());
    }

    private static void setScreenFriendlySize(Component component, Dimension screenSize) {
        Dimension frameSize = component.getSize();
        frameSize.height = (Math.min(frameSize.height, screenSize.height));
        frameSize.width = (Math.min(frameSize.width, screenSize.width));
    }

    private static void setWindowInCenter(Component component, Dimension screenSize) {
        Dimension frameSize = component.getSize();
        component.setLocation(
                (screenSize.width - frameSize.width) / 2,
                (screenSize.height - frameSize.height) / 2
        );
    }
}
