// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Alias)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

package emi.util;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

/**
 * Provides different functions for running the tasks or movies, depending on the version. The older tasks and movies
 * are still launched as Flash application/movie. Newer things (from v1.7) are started with an Electron application.
 *
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class LauncherUtil {
    /**
     * Specifies the indicator that the task is a newer one, which is available as Electron application.
     */
    private static final String HTML_INDICATOR = "html:";

    /**
     * Specifies a list of video extensions that HTML5 can render (officially). These extensions cause the Electron application to launch to display the video.
     */
    private static final List<String> HTML5_SUPPORTED_VIDEO_EXTENSIONS = Arrays.asList("mp4", "ogg", "webm");

    private LauncherUtil() {}

    /**
     * Starts a Flash or HTML5 movie, depending on whether subtitles are enabled.
     * @param movie The relative path specified in `anim_movie` in `content.xml`.
     * @param movieOmu The relative path specified in `anim_movie_omu` in `content.xml`.
     * @param withSound Specifies whether the video should be displayed with sound or not. If negated, the video is displayed with subtitles.
     */
    public static void startMovie(String movie, String movieOmu, boolean withSound) {
        if(LauncherUtil.isHTMLVideo(movie)) {
            if (withSound)
                OSHelperUtil.startElectronMovie(movie);
            else
                OSHelperUtil.startElectronMovie(movie, movieOmu);
        }
        else {
            if (withSound)
                OSHelperUtil.startFlash(movie);
            else
                OSHelperUtil.startFlash(movieOmu);
        }
    }

    /**
     * Starts a Flash or HTML5 movie, depending on what it is.
     * @param movie The relative path specified in `anim_movie` in `content.xml`.
     */
    public static void startMovie(String movie) {
        if(LauncherUtil.isHTMLVideo(movie))
            OSHelperUtil.startElectronMovie(movie);
        else
            OSHelperUtil.startFlash(movie);
    }

    /**
     * Starts a Flash or HTML5 exercise, depending on what it is.
     * @param movieOrUrl The relative path specified in `XYZ_movie` in `content.xml`.
     */
    public static void startExercise(String movieOrUrl) {
        if(movieOrUrl.startsWith(HTML_INDICATOR))
            OSHelperUtil.startElectronExercise(movieOrUrl.substring(HTML_INDICATOR.length()));
        else
            OSHelperUtil.startFlash(movieOrUrl);
    }

    /**
     * Gets whether the video is HTML5 video.
     * @param movie The relative path specified in `anim_movie` in `content.xml`.
     * @return Returns whether the video is supported by HTML5.
     */
    public static boolean isHTMLVideo(String movie) {
        String extension = PathUtil.getFileExtension(movie).toLowerCase();

        return HTML5_SUPPORTED_VIDEO_EXTENSIONS.contains(extension);
    }

    public static void launchBrowser(String fileUrl) {
        try {
            // see if we can use Desktop implementation (requires gnome-library dependencies to be installed on linux)
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(URI.create(fileUrl));
            } else {
                // Fallback for when awt.Desktop is not supported
                Runtime runtime = Runtime.getRuntime();
                String command = OSHelperUtil.constructLaunchCommandForBrowser(fileUrl);
                runtime.exec(command);
            }
        } catch(IOException e){
            System.err.println("Could not open URL in Browser: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
