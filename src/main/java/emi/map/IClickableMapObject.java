package emi.map;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public interface IClickableMapObject {
    String getActionCommand();

    String getTooltipText();

    int getMapNumber();

    default boolean isActive() {
        return true;
    }
}
