package emi.map;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public interface MouseMovedListener {
    void mouseMovedOnMapButton(JPanel object, IClickableMapObject clickableObject);
}
