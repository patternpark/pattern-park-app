package emi.map;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
final class MapButton implements IClickableMapObject {
    private int x1;
    private int y1;
    private int x2;
    private int y2;

    private BufferedImage offImage;
    private BufferedImage onImage;
    private BufferedImage HighLightImage;
    private BufferedImage PressDownImage;

    private boolean on = false;
    private boolean down = false;

    private boolean isActive;

    private String text;

    private Color textColor;
    private Point textPosition;

    private final IClickableMapObject _clickableMapObject;

    public MapButton(IClickableMapObject object, int imgWidth, int imgHeight) {
        this._clickableMapObject = object;
        x1 = imgWidth;
        y1 = imgHeight;
        x2 = 0;
        y2 = 0;

        onImage = null;
        HighLightImage = null;
        PressDownImage = null;
        this.isActive = object.isActive();
    }

    public void setText(String a) {

        text = a;

    }

    public void setTextPosition(Point a) {
        textPosition = a;
    }

    @Override
    public String getActionCommand() {
        return this._clickableMapObject.getActionCommand();
    }

    @Override
    public String getTooltipText() {
        return this._clickableMapObject.getTooltipText();
    }

    @Override
    public int getMapNumber() {
        return this._clickableMapObject.getMapNumber();
    }

    @Override
    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public IClickableMapObject getRealClickableObject() {
        return this._clickableMapObject;
    }

    public void setOn(boolean a) {
        on = a;
    }

    public boolean getOn() {
        return on;
    }

    public void setDown(boolean a) {
        down = a;
    }

    public boolean getDown() {
        return down;
    }

    public void setOffImage(BufferedImage a) {
        offImage = a;
    }

    public void setOnImage(BufferedImage a) {
        onImage = a;
    }

    public void setUpImage(BufferedImage a) {
        HighLightImage = a;
    }

    public void setDownImage(BufferedImage a) {
        PressDownImage = a;
    }

    public void setX1(int a) {
        x1 = a;
    }

    public void setY1(int a) {
        y1 = a;
    }

    public void setX2(int a) {
        x2 = a;
    }

    public void setY2(int a) {
        y2 = a;
    }

    public void drawOffImage(Graphics g) {
        if (on) {
            g.drawImage(onImage, x1, y1, null);
        } else if (down) {
            //g.drawImage(PressDownImage,x1,y1,null);
        } else {
            g.drawImage(offImage, x1, y1, null);
        }

        drawText(g);

    }

    public void drawUpImage(Graphics g) {

        if (on) {
            g.drawImage(onImage, x1, y1, null);
        } else if (down) {
            //	g.drawImage(PressDownImage,x1,y1,null); }
        } else {
            g.drawImage(HighLightImage, x1, y1, null);
        }

        drawText(g);

    }

    public void drawDownImage(Graphics g) {

        if (on) {
            g.drawImage(onImage, x1, y1, null);
        } else {
            // g.drawImage(PressDownImage,x1,y1,null);
        }

        drawText(g);
    }

    public void drawBaseImage(Graphics g) {

        if (on) {
            g.drawImage(onImage, x1, y1, null);
        } else if (down) {
            // g.drawImage(PressDownImage,x1,y1,null);
        }

        drawText(g);

    }

    public void drawText(Graphics g) {

        if (text != null) {

            if (textColor != null) {
                g.setColor(textColor);
            } else {
                g.setColor(Color.BLACK);
            }

            if (textPosition != null) {

                g.drawString(text, x1 + textPosition.x, y1 + textPosition.y);
            } else {

                FontMetrics fontmetrics = g.getFontMetrics();

                g.drawString(text, x1 + getWidth() / 2 - (fontmetrics.stringWidth(text) / 2), y1 + getHeight() / 2 + (fontmetrics.getAscent() / 2));
            }
        }

    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getWidth() {
        return (x2 - x1 + 1);
    }

    public int getHeight() {
        return (y2 - y1 + 1);
    }

    public void checkAndSetXY(int x, int y) {
        if (x < getX1())
            setX1(x);

        if (x > getX2())
            setX2(x);

        if (y < getY1())
            setY1(y);

        if (y > getY2())
            setY2(y);
    }

    /**
     * @return the pressDownImage
     */
    public BufferedImage getPressDownImage() {
        return PressDownImage;
    }

    /**
     * @param pressDownImage the pressDownImage to set
     */
    public void setPressDownImage(BufferedImage pressDownImage) {
        PressDownImage = pressDownImage;
    }
}
