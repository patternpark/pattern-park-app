package emi.map;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public interface MouseLeftListener {
    void mouseLeftOnMapButton(JPanel object, IClickableMapObject clickableObject);
}
