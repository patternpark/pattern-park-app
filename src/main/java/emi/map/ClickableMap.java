package emi.map;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

/* based on
 * Yura Button Panel
 * @author Yura Mamyrin (yura@yura.net)
 * Copyright (c) 2006 yura.net
 * YuraButtonPanel is under GPL
 * @author (Modified by) Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7.0.0)
 * @version 1.7.0.1
 */
public class ClickableMap extends JPanel implements MouseInputListener, KeyListener {
    private final ArrayList<MouseClickedListener> clickListeners = new ArrayList<>();
    private final ArrayList<MouseMovedListener> mouseMovedListeners = new ArrayList<>();
    private final ArrayList<MouseLeftListener> mouseLeftListeners = new ArrayList<>();
    private final HashMap<Integer, MapButton> ButtonImages = new HashMap<>();
    private int[][] map;
    private boolean _mouseExitDisabled = true;

    private final BufferedImage img;

    private int mapButtonNumber;
    private int pp;
    private int tt;

    public ClickableMap(
            BufferedImage hoverMap,
            BufferedImage ofMap,
            BufferedImage mouseUpMap,
            BufferedImage mouseDownMap,
            BufferedImage defaultMap,
            IClickableMapObject[] clickableMapObjects) {
        img = ofMap;

        init(hoverMap);

        if (clickableMapObjects.length == 0)
            return;

        initClickableMapObjects(hoverMap, ofMap, mouseUpMap, mouseDownMap, defaultMap, clickableMapObjects);
    }

    public void resetHighlightAndEnableMouseExit(boolean newValue){
        _mouseExitDisabled = newValue;
        if(ButtonImages.get(mapButtonNumber) != null){
            ButtonImages.get(mapButtonNumber).drawOffImage(getGraphics());
            mapButtonNumber = 255;
        }
    }

    public void setButtonActive(int mapNumber, boolean isActive){
        if(!ButtonImages.containsKey(mapNumber)) return;

        MapButton button = ButtonImages.get(mapNumber);
        button.setOn(!isActive);
        button.setDown(!isActive);
        button.setActive(isActive);
    }

    public void addActionListener(MouseClickedListener listener) {
        this.clickListeners.add(listener);
    }

    public void addMouseMovedListener(MouseMovedListener listener) {
        this.mouseMovedListeners.add(listener);
    }

    public void addMouseLeftListener(MouseLeftListener listener) {
        this.mouseLeftListeners.add(listener);
    }

    public void paintComponent(Graphics g) {
        // if something is on it should not highlight
        g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);

        if (map == null) return;

        for (MapButton button : ButtonImages.values()) {
            int mapNumber = button.getMapNumber();
            if (mapNumber == pp) {
                button.drawDownImage(g);
            } else if (mapNumber == this.mapButtonNumber) {
                button.drawUpImage(g);
            } else {
                button.drawBaseImage(g);
            }
        }
    }

    public void repaintButton(int i) {
        if (!ButtonImages.containsKey(i)) return;

        MapButton button = ButtonImages.get(i);
        if (i == pp) {
            button.drawDownImage(getGraphics());
        } else if (i == mapButtonNumber) {
            button.drawUpImage(getGraphics());
        } else {
            button.drawOffImage(getGraphics());
        }
    }

    public boolean contains(int x, int y) {
        if (map == null) return false;

        if (x < 0 || x >= img.getWidth() || y < 0 || y >= img.getHeight()) {
            tt = 255;
            return false;
        }
        int a = map[x][y];
        if (a == 255) {
            tt = 255;
            return false;
        }

        if (a != tt && ButtonImages.containsKey(a)) {
            MapButton button = ButtonImages.get(a);
            if(button.isActive()) {
                setToolTipText(button.getTooltipText());
                tt = a;
            }
            else {
                setToolTipText(null);
            }
        }

        return true;
    }

    //**********************************************************************
    //                     MouseListener Interface
    //**********************************************************************

    public void mouseMoved(MouseEvent e) {
        int newSelectedMapNumber = map[e.getX()][e.getY()];

        if (newSelectedMapNumber == mapButtonNumber) return;

        Graphics g = getGraphics();

        // cover up the old highlighted button
        if (ButtonImages.containsKey(mapButtonNumber))
            ButtonImages.get(mapButtonNumber).drawOffImage(g);

        mapButtonNumber = newSelectedMapNumber;

        if (!ButtonImages.containsKey(mapButtonNumber)) return;

        MapButton button = ButtonImages.get(mapButtonNumber);

        // draw the new highlighted button
        button.drawUpImage(g);

        if (!button.getOn() && button.isActive())
            mouseMovedListeners.forEach(btn -> btn.mouseMovedOnMapButton(this, button));
        else
            mouseLeftListeners.forEach(btn -> btn.mouseLeftOnMapButton(this, button));
    }

    public void mousePressed(MouseEvent e) {
        int a = map[e.getX()][e.getY()];

        if (!ButtonImages.containsKey(a)) return;

        pp = a; // set only, if button exists
        MapButton button = ButtonImages.get(a);
        button.drawDownImage(getGraphics());
    }

    public void mouseDragged(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();

        if (x < 0 || x >= img.getWidth() || y < 0 || y >= img.getHeight())
            return;

        int newSelectedMapNumber = map[x][y];
        if (newSelectedMapNumber == mapButtonNumber || pp == 255) return;

        mapButtonNumber = newSelectedMapNumber;
        Graphics g = getGraphics();

        if (newSelectedMapNumber != pp) {
            ButtonImages.get(pp).drawOffImage(g);
        } else {
            ButtonImages.get(pp).drawDownImage(g);
        }
    }

    public void mouseReleased(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();

        if (x < 0 || x >= img.getWidth() || y < 0 || y >= img.getHeight()) {
            pp = 255;
            return;
        }

        int mapNumber = map[x][y];

        if (mapNumber == pp && ButtonImages.containsKey(mapNumber)) {
            MapButton button = ButtonImages.get(mapNumber);
            if(button.isActive()) {
                _mouseExitDisabled = false;
                clickListeners.forEach(l -> l.mouseClickedOnMapButton(this, button.getRealClickableObject()));
                button.drawUpImage(getGraphics());
            }
        }

        pp = 255;
    }

    public void mouseExited(MouseEvent e) {
        if(!_mouseExitDisabled) return;

        if (mapButtonNumber == 255) return;
        ButtonImages.get(mapButtonNumber).drawOffImage(getGraphics());
        mapButtonNumber = 255;

    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }


    public void keyReleased(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.VK_TAB) {

            setToolTipText(null);

            int newSelectedMapNumber = 0;

            if (mapButtonNumber == 255) {
                requestFocus();

                if (event.isShiftDown() && !ButtonImages.isEmpty()) {
                    // set last item as "selected" map button
                    newSelectedMapNumber = getLastMapID();
                } else if (!event.isShiftDown() && !ButtonImages.isEmpty()) {
                    // set first item as "selected" map button
                    newSelectedMapNumber = getFirstMapID();
                }
            } else {
                if (event.isShiftDown())
                    newSelectedMapNumber = getPreviousID(mapButtonNumber);
                else
                    newSelectedMapNumber = getNextID(mapButtonNumber);

                if (newSelectedMapNumber >= 255) {
                    newSelectedMapNumber = 255;

                    transferFocus();

                    if (hasFocus())
                        newSelectedMapNumber = getFirstMapID();
                } else if (newSelectedMapNumber < 0) {
                    newSelectedMapNumber = 255;

                    transferFocusBackward();

                    if (hasFocus())
                        newSelectedMapNumber = getLastMapID();
                }
            }

            Graphics g = getGraphics();

            // cover up the old highlighted button
            if (ButtonImages.containsKey(mapButtonNumber))
                ButtonImages.get(mapButtonNumber).drawOffImage(g);

            mapButtonNumber = newSelectedMapNumber;

            // draw the new highlighted button
            if (ButtonImages.containsKey(mapButtonNumber))
                ButtonImages.get(mapButtonNumber).drawUpImage(g);
        }

        if ((event.getKeyCode() == KeyEvent.VK_SPACE || event.getKeyCode() == KeyEvent.VK_ENTER) && mapButtonNumber != 255) {
            MapButton button = ButtonImages.get(mapButtonNumber);
            _mouseExitDisabled = false;
            clickListeners.forEach(l -> l.mouseClickedOnMapButton(this, button.getRealClickableObject()));
        }
    }

    public void keyPressed(KeyEvent event) {
    }

    public void keyTyped(KeyEvent event) {
    }

    private void init(BufferedImage in_on) {
        setLayout(null);

        setFocusable(true);
        setRequestFocusEnabled(true);

        setFocusTraversalKeysEnabled(false);


        mapButtonNumber = 255;
        pp = 255;
        tt = 255;

        int width = in_on.getWidth();
        int height = in_on.getHeight();

        Dimension size = new Dimension(width, height);

        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
    }

    private void initClickableMapObjects(
            BufferedImage hoverMap,
            BufferedImage ofMap,
            BufferedImage mouseUpMap,
            BufferedImage mouseDownMap,
            BufferedImage defaultMap,
            IClickableMapObject[] clickableMapObjects) {
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(this);

        //create map
        int none = new Color(0, 0, 0, 0).getRGB();

        int ppX = defaultMap.getWidth();
        int ppY = defaultMap.getHeight();

        map = new int[ppX][ppY];

        for (IClickableMapObject object : clickableMapObjects) {
            if (ButtonImages.containsKey(object.getMapNumber())) continue;

            ButtonImages.put(object.getMapNumber(), new MapButton(object, ppX, ppY));
        }

        int[] pixels = defaultMap.getRGB(0, 0, ppX, ppY, null, 0, ppX);

        // create a very big 2d array with all the data from the image map
        for (int y = 0; y < ppY; y++) {
            for (int x = 0; x < ppX; x++) {
                int mapNumber = pixels[(ppX * y) + x] & 0xff;
                if (ButtonImages.containsKey(mapNumber) && mapNumber < 0xff) {
                    map[x][y] = mapNumber;
                    ButtonImages.get(mapNumber).checkAndSetXY(x, y);
                } else {
                    map[x][y] = 0xff;
                }
            }
        }

        // create the bufferd image for each country
        for (MapButton button : ButtonImages.values()) {
            int mapNumber = button.getMapNumber();
            int x1 = button.getX1();
            //int x2=ButtonImages[c].getX2();
            int y1 = button.getY1();
            int y2 = button.getY2();
            int w = button.getWidth();
            int h = button.getHeight();

            BufferedImage i_off = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            i_off.getGraphics().drawImage(ofMap.getSubimage(x1, y1, w, h), 0, 0, this);

            // BufferedImage i_on = new BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB );
            // i_on.getGraphics().drawImage( hoverMap.getSubimage(x1, y1, w, h) ,0,0,this);

            BufferedImage i_up = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            i_up.getGraphics().drawImage(mouseUpMap.getSubimage(x1, y1, w, h), 0, 0, this);

            //  BufferedImage i_down = new BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB );
            //  i_down.getGraphics().drawImage( mouseDownMap.getSubimage(x1, y1, w, h) ,0,0,this);

            for (int y = y1; y <= y2; y++) {
                for (int x = 0; x <= w - 1; x++) {
                    if (map[x + x1][y] == mapNumber) continue;

                    i_off.setRGB(x, (y - y1), none); // clear the un-needed area!
                    // i_on.setRGB( x, (y-y1), none); // clear the un-needed area!
                    i_up.setRGB(x, (y - y1), none); // clear the un-needed area!
                    // i_down.setRGB( x, (y-y1), none); // clear the un-needed area!
                }
            }

            button.setOffImage(i_off);
            // button.setOnImage(i_on);
            button.setUpImage(i_up);
            // button.setDownImage(i_down);
        }
    }

    private int getLastMapID() {
        Object[] keys = ButtonImages.keySet().toArray();
        return ButtonImages.get((int) keys[keys.length - 1]).getMapNumber();
    }

    private int getFirstMapID() {
        return ButtonImages.get((int) ButtonImages.keySet().toArray()[0]).getMapNumber();
    }

    private int getPreviousID(int id) {
        Integer[] numbers = ButtonImages.keySet().toArray(new Integer[0]);

        boolean returnNextValue = false;
        for (int i = numbers.length - 1; i >= 0; --i) {
            if (returnNextValue)
                return numbers[i];

            returnNextValue = numbers[i] == id;
        }

        return -1;
    }

    private int getNextID(int id) {
        Integer[] numbers = ButtonImages.keySet().toArray(new Integer[0]);

        boolean returnNextValue = false;
        for (Integer number : numbers) {
            if (returnNextValue)
                return number;

            returnNextValue = number == id;
        }

        return 255;
    }

}
