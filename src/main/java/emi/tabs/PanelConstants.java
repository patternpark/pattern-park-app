package emi.tabs;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
/** Constants class storing values for Positioning, and Styling the Preview Image and Text Elements inside the common Tabs
 * */
public final class PanelConstants {
    private PanelConstants(){}
    public static final int TAB_PANEL_SCROLLER_ITEM_WIDTH = 425;

    public static final int TAB_PANEL_PADDING_LEFT = 15;
    public static final int TAB_PANEL_PADDING_TOP = 15;
    public static final int TAB_PANEL_SCROLLER_GAP = 30;

    public static final int TAB_PANEL_PREVIEW_IMAGE_WIDTH = 500;
    public static final int TAB_PANEL_PREVIEW_IMAGE_HEIGHT = 500;
    public static final Border TAB_PANEL_PREVIEW_IMAGE_BORDER = BorderFactory.createLineBorder(Color.black, 1);

    public static final Dimension TAB_PANEL_SCROLLER_DIMENSION = new Dimension(TAB_PANEL_SCROLLER_ITEM_WIDTH + 20, 495);
    public static final Point TAB_PANEL_SCROLLER_LOCATION = new Point(TAB_PANEL_PADDING_LEFT + TAB_PANEL_PREVIEW_IMAGE_WIDTH + TAB_PANEL_SCROLLER_GAP, TAB_PANEL_PADDING_TOP);
    public static final int TAB_PANEL_SCROLL_SPEED = 20;

    public static final Font HEADING_FONT = new Font("Arial", Font.BOLD, 16);
    public static final Font DESCRIPTION_FONT = new Font("Arial", Font.PLAIN, 14);
    public static final Font SOURCES_FONT = new Font("Arial", Font.PLAIN, 13);

    public static final Dimension TAB_PANEL_HEADING_SIZE = new Dimension(TAB_PANEL_SCROLLER_ITEM_WIDTH, 30);
    public static final Dimension TAB_PANEL_SPACER_SIZE = new Dimension(TAB_PANEL_SCROLLER_ITEM_WIDTH, 30);
    public static final Dimension TAB_PANEL_DESCRIPTION_SIZE = new Dimension(TAB_PANEL_SCROLLER_ITEM_WIDTH, Integer.MAX_VALUE);

    public static final Point TAB_PANEL_BUTTON_LOCATION = new Point(665,510);
    public static final Dimension TAB_PANEL_BUTTON_SIZE = new Dimension(180,28);
    public static final Dimension TAB_PANEL_SIZE = new Dimension(980,585);
    public static final Color TAB_PANEL_BACKGROUND = Color.white;
}
