package emi.tabs;

import emi.models.ETabType;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public class AnimationTabContent extends TabContentWithPreviewImage {
    private final String _info;
    private final String _time;
    private final String _movie;
    private final String _movieOmu;

    public AnimationTabContent(Element tabElement, boolean isVisible) {
        this(tabElement, ETabType.ANIMATION, isVisible);
    }

    protected AnimationTabContent(Element tabElement, ETabType type, boolean isVisible) {
        super(tabElement, type, isVisible);

        this._info = Objects.requireNonNull(UserSettings.getLanguage()).resolveStringWithVariables(tabElement.getChildText("info"));
        this._time = tabElement.getChildText("time");
        this._movie = tabElement.getChildText("movie");
        this._movieOmu = tabElement.getChildText("movie_omu");
    }

    public String getInfo() {
        return _info;
    }

    public String getTime() {
        return _time;
    }

    public String getMovie() {
        return _movie;
    }

    public String getMovieOmu() {
        return _movieOmu;
    }
}
