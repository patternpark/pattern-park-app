package emi.tabs;

import emi.models.ETabType;
import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class CodeTabContent extends TabContent {
    private final String _codePath;

    public CodeTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.CODE, isVisible);

        this._codePath = tabElement.getChildText("code");
    }

    public String getCodePath() {
        return this._codePath;
    }
}
