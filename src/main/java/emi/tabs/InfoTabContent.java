package emi.tabs;

import emi.models.ETabType;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class InfoTabContent extends TabContent {
    private final String _image;
    private final String _description;

    public InfoTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.INFO, isVisible);

        this._image = tabElement.getChildText("image");
        this._description = Objects.requireNonNull(UserSettings.getLanguage()).resolveStringWithVariables(tabElement.getChildText("description"));
    }

    public String getImage() {
        return _image;
    }

    public String getDescription() {
        return _description;
    }
}
