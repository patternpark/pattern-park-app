package emi.tabs;

import emi.models.ETabType;
import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class ParticipantTabContent extends ExerciseTabContent {
    public ParticipantTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.PARTICIPANT, isVisible);
    }
}
