package emi.tabs;

import emi.models.ETabType;
import emi.session.Language;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class OverviewTabContent extends TabContentWithPreviewImage {
    private final String _description;
    private final String _useFor;
    private final String _notUseFor;

    public OverviewTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.OVERVIEW, isVisible);

        Language language = Objects.requireNonNull(UserSettings.getLanguage());
        this._description = language.resolveStringWithVariables(tabElement.getChildText("description"));
        this._useFor = language.resolveStringWithVariables(tabElement.getChildText("use_for"));
        this._notUseFor = language.resolveStringWithVariables(tabElement.getChildText("not_use_for"));
    }

    public String getDescription() {
        return _description;
    }

    public String getUseFor() {
        return _useFor;
    }

    public String getNotUseFor() {
        return _notUseFor;
    }
}
