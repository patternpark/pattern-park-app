package emi.tabs.jpanels.html;

import emi.tabs.ExternalURLsHTMLTabContent;
import emi.util.LauncherUtil;

import javax.swing.event.HyperlinkEvent;

public class ExternalURLsHTMLTabPanel extends HTMLTabPanel<ExternalURLsHTMLTabContent> {
    public ExternalURLsHTMLTabPanel(ExternalURLsHTMLTabContent content) {
        super(content);

        init();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent event)
    {
        HyperlinkEvent.EventType typ = event.getEventType();
        if (typ != HyperlinkEvent.EventType.ACTIVATED) return;

        LauncherUtil.launchBrowser(event.getURL().toExternalForm());
    }
}
