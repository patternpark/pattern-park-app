package emi.tabs.jpanels.html;

import emi.GuiDefs;
import emi.tabs.HTMLTabContent;
import emi.tabs.jpanels.TabPanel;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import javax.swing.*;
import javax.swing.event.HyperlinkListener;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;

import static emi.tabs.Constants.INDICATOR_FROM_JAR_RESOURCES;

public abstract class HTMLTabPanel<T extends HTMLTabContent> extends TabPanel<T> implements HyperlinkListener {
    protected JEditorPane jEditorPane;

    protected HTMLTabPanel(T content) {
        super(content);
    }

    protected void init() {
        this.add(initHTMLPane());
    }

    private JScrollPane initHTMLPane() {
        jEditorPane = new JEditorPane();
        jEditorPane.addHyperlinkListener(this);
        jEditorPane.setEditable(false);
        jEditorPane.setContentType(SyntaxConstants.SYNTAX_STYLE_HTML);

        try {
            jEditorPane.setPage(this.getURL());
        } catch (IOException e) {
            jEditorPane.setText("<html> <center>"
                    + "<h1>Error loading file.</h1>"
                    + "</center> </html>.");
        }

        JScrollPane jScrollPane = new JScrollPane(jEditorPane);
        jScrollPane.setPreferredSize(GuiDefs.LOCAL_URL_SCROLL_PANE_SIZE);
        jScrollPane.getVerticalScrollBar().setUnitIncrement(GuiDefs.HTML_PANEL_SCROLL_SPEED);
        jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        return jScrollPane;
    }

    private URL getURL() {
        String url = this.tabContent.getURL();
        try {
            if(url.startsWith(INDICATOR_FROM_JAR_RESOURCES)) {
                return Objects.requireNonNull(getClass().getClassLoader().getResource(url.substring(INDICATOR_FROM_JAR_RESOURCES.length())));
            }
            else {
                return new URL(url);
            }
        }
        catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
