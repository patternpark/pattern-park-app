package emi.tabs.jpanels.html;

import emi.tabs.LocalURLsHTMLTabContent;

import javax.swing.event.HyperlinkEvent;
import java.io.IOException;

public class LocalURLsHTMLTabPanel extends HTMLTabPanel<LocalURLsHTMLTabContent> {
    public LocalURLsHTMLTabPanel(LocalURLsHTMLTabContent content) {
        super(content);

        init();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent event) {
        HyperlinkEvent.EventType typ = event.getEventType();
        if (typ == HyperlinkEvent.EventType.ACTIVATED) {
            try {
                // setTitle( "" + event.getURL() );
                jEditorPane.setPage(event.getURL());
            } catch (IOException e) {
                System.out.println("Link oder URL geht nicht");
                // JOptionPane.showMessageDialog( this,
                // "Can't follow link to "
                // + event.getURL().toExternalForm(),
                // "Error",
                // JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
