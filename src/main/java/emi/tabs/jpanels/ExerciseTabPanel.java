package emi.tabs.jpanels;

import emi.GUIElementNames;
import emi.session.UserSettings;
import emi.tabs.ExerciseTabContent;
import emi.tabs.PanelConstants;
import emi.util.LauncherUtil;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class ExerciseTabPanel<T extends ExerciseTabContent> extends TabPanelWithPreviewImage<T> {
    protected ExerciseTabPanel(T content) {
        super(content);
    }

    protected void initialize() {
        this.add(initJbStartExercisesButton(), null);

        this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.TOPIC), ContentType.HEADING);
        this.addTabContent(this.tabContent.getInfo(), ContentType.DESCRIPTION);

        this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.DURATION), ContentType.HEADING);
        this.addTabContent(this.tabContent.getTime(), ContentType.DESCRIPTION);

        this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.LEARNING_TARGET), ContentType.HEADING);
        this.addTabContent(this.tabContent.getLearningGoal(), ContentType.DESCRIPTION);

        this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PRECONDITIONS), ContentType.HEADING);
        this.addTabContent(this.tabContent.getPreconditions(), ContentType.DESCRIPTION);
    }

    /**
     * This method initializes jbStartAnim
     *
     * @return javax.swing.JButton
     */
    protected JButton initJbStartExercisesButton() {
        JButton jbStartAnim = new JButton("Start button");
        jbStartAnim.setText(UserSettings.getGUITextFromName(GUIElementNames.START_EXERCISES));
        jbStartAnim.setLocation(PanelConstants.TAB_PANEL_BUTTON_LOCATION);
        jbStartAnim.setSize(PanelConstants.TAB_PANEL_BUTTON_SIZE);
        jbStartAnim.addActionListener(e -> LauncherUtil.startExercise(this.tabContent.getMovie()));

        return jbStartAnim;
    }
}
