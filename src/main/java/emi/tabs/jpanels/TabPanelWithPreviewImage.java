package emi.tabs.jpanels;

import emi.tabs.PanelConstants;
import emi.tabs.TabContentWithPreviewImage;

import javax.swing.*;
import java.awt.*;

import static emi.util.JavaSwingHelperUtil.loadImage;

public abstract class TabPanelWithPreviewImage<T extends TabContentWithPreviewImage> extends TabPanel<T> {
    private Container textContainer;

    // ContentType used for adding Tab Content, determines which Fonts, Size, Weight, will be used.
    enum ContentType {
        HEADING,
        DESCRIPTION,
        SOURCES
    }

    protected TabPanelWithPreviewImage(T content) {
        super(content);
        this.setPreferredSize(PanelConstants.TAB_PANEL_SIZE);
        this.setSize(PanelConstants.TAB_PANEL_SIZE);
        this.setBackground(PanelConstants.TAB_PANEL_BACKGROUND);
        this.setLayout(null);
        initializeTextContainer();
        this.initImagePreviewPanel();
    }

    /** Loads and sets the Preview Image of the current Tab
     * */
    private void initializeTextContainer(){
        JViewport scrollContent = new JViewport();
        scrollContent.setBackground(PanelConstants.TAB_PANEL_BACKGROUND);
        JScrollPane scroller = new JScrollPane();
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroller.setViewport(scrollContent);
        scroller.setSize(PanelConstants.TAB_PANEL_SCROLLER_DIMENSION);
        scroller.setLocation(PanelConstants.TAB_PANEL_SCROLLER_LOCATION);
        scroller.setBorder(BorderFactory.createEmptyBorder());
        scroller.getVerticalScrollBar().setUnitIncrement(PanelConstants.TAB_PANEL_SCROLL_SPEED);
        textContainer = new Container();
        scrollContent.add(textContainer);
        this.add(scroller);
    }

    /** Wrapper for creating and adding Content with correct Offsets to the textContainer
     * @param contentType Type of Content created, determines the Font-size and Font-Weight used
     * @param content Text that should be set
     * */
    protected void addTabContent(String content, ContentType contentType){
        // Step 1: figure out how much Offset there should be.
        int combinedComponentHeight = 0;
        for(int i=0; i<textContainer.getComponents().length; ++i){
            combinedComponentHeight += textContainer.getComponents()[i].getHeight();
        }
        // Step 2: create and add the JLabel or JTextArea to the scroller
        if(contentType == ContentType.HEADING){
            if(textContainer.getComponentCount() > 1){
                // add some space above headings when the heading isn't the first component inside the scroller
                textContainer.add(createSpacer(combinedComponentHeight),null);
                combinedComponentHeight += PanelConstants.TAB_PANEL_SPACER_SIZE.height;
            }
            textContainer.add(createHeading(content,combinedComponentHeight), null);
        } else if(contentType == ContentType.DESCRIPTION){
            textContainer.add(createDescriptionText(content,combinedComponentHeight),null);
        } else if(contentType == ContentType.SOURCES){
            // Sources are just normal description Texts with slightly smaller Font-Size, only used in the Infocenter
            JTextPane text = createDescriptionText(content, combinedComponentHeight);
            text.setFont(PanelConstants.SOURCES_FONT);
            textContainer.add(text);
        }
        // Step 3: set the new PreferredSize
        combinedComponentHeight += textContainer.getComponents()[textContainer.getComponentCount()-1].getHeight();
        textContainer.setPreferredSize(new Dimension(PanelConstants.TAB_PANEL_SCROLLER_ITEM_WIDTH,combinedComponentHeight));
    }


    /** Adds the Preview Image for the current tab.
     * */
    protected void initImagePreviewPanel() {
        JLabel jlPreviewImage = new JLabel();
        jlPreviewImage.setBounds(new Rectangle(PanelConstants.TAB_PANEL_PADDING_LEFT, PanelConstants.TAB_PANEL_PADDING_TOP, PanelConstants.TAB_PANEL_PREVIEW_IMAGE_WIDTH, PanelConstants.TAB_PANEL_PREVIEW_IMAGE_HEIGHT));

        try{
            jlPreviewImage.setIcon(loadImage(this.tabContent.getPreviewImage()));}
        catch (NullPointerException e){
            e.printStackTrace();
        }

        jlPreviewImage.setBorder(PanelConstants.TAB_PANEL_PREVIEW_IMAGE_BORDER);
        this.add(jlPreviewImage, null);
    }
    /** Private Method used to create Headings
    * */
    private JLabel createHeading(String headingText, int tabbedPanelHeadingYOffset){
        JLabel newHeading = new JLabel();
        newHeading.setSize(PanelConstants.TAB_PANEL_HEADING_SIZE);
        newHeading.setLocation(0,tabbedPanelHeadingYOffset);
        newHeading.setFont(PanelConstants.HEADING_FONT);
        newHeading.setText(headingText);

        return newHeading;
    }
    /** Private Method used to create Space before Headings
     * */
    private JLabel createSpacer(int tabbedPanelSpacingYOffset){
        JLabel spacer = new JLabel();
        spacer.setSize(PanelConstants.TAB_PANEL_SPACER_SIZE);
        spacer.setLocation(0, tabbedPanelSpacingYOffset);
        return spacer;
    }
    /** Private Method used to create Description Texts
     * */
    private JTextPane createDescriptionText(String descriptionText, int tabbedPanelDescriptionYOffset) {
        JTextPane newDescriptionText = new JTextPane();
        newDescriptionText.setLocation(0, tabbedPanelDescriptionYOffset);
        newDescriptionText.setEditable(false);
        newDescriptionText.setFont(PanelConstants.DESCRIPTION_FONT);
        newDescriptionText.setText(descriptionText);

        // calculate perfect TextPane size by using the FontSize, Width, and Text,
        // requires Width to be set first (thus assigning size twice!)
        newDescriptionText.setSize(PanelConstants.TAB_PANEL_DESCRIPTION_SIZE);
        newDescriptionText.setSize(PanelConstants.TAB_PANEL_SCROLLER_ITEM_WIDTH, newDescriptionText.getPreferredSize().height);

        return newDescriptionText;
    }
}
