package emi.tabs.jpanels;


import emi.GUIElementNames;
import emi.session.UserSettings;
import emi.tabs.ThePatternTabContent;

/**
 * An instance of this class describes
 * the current design pattern
 * Panel used in PatternOverview 
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class ThePatternPanel extends TabPanelWithPreviewImage<ThePatternTabContent> {

	public ThePatternPanel(ThePatternTabContent content) {
		super(content);

		initialize();
	}

	private void initialize() {
		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_DESCRIPTION), ContentType.HEADING);
		this.addTabContent(this.tabContent.getDescription(), ContentType.DESCRIPTION);

		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_ADVANTAGES), ContentType.HEADING);
		this.addTabContent(this.tabContent.getAdvantages(), ContentType.DESCRIPTION);

		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_DISADVANTAGES), ContentType.HEADING);
		this.addTabContent(this.tabContent.getDisadvantages(), ContentType.DESCRIPTION);

		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_PARTICIPANTS), ContentType.HEADING);
		this.addTabContent(this.tabContent.getParticipants(), ContentType.DESCRIPTION);
	}
}
