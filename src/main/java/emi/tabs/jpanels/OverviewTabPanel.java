package emi.tabs.jpanels;

import emi.GUIElementNames;
import emi.session.UserSettings;
import emi.tabs.OverviewTabContent;

/**
 * An instance of this class contains information
 * about the current design pattern 
 * Panel used in PatternOverview 
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class OverviewTabPanel extends TabPanelWithPreviewImage<OverviewTabContent> {

	public OverviewTabPanel(OverviewTabContent content) {
		super(content);

		initialize();
	}

	private void initialize() {
		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_DESCRIPTION), ContentType.HEADING);
		this.addTabContent(this.tabContent.getDescription(), ContentType.DESCRIPTION);

		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_ADVANTAGES), ContentType.HEADING);
		this.addTabContent(this.tabContent.getUseFor(), ContentType.DESCRIPTION);

		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.PATTERN_DISADVANTAGES), ContentType.HEADING);
		this.addTabContent(this.tabContent.getNotUseFor(), ContentType.DESCRIPTION);
	}

}
