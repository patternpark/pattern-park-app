package emi.tabs.jpanels;

import emi.tabs.UmlTabContent;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class UmlPanel extends ExerciseTabPanel<UmlTabContent> {
	public UmlPanel(UmlTabContent content) {
		super(content);
		initialize();
	}
}
