package emi.tabs.jpanels;

import emi.tabs.ParticipantTabContent;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class ParticipantsTabPanel extends ExerciseTabPanel<ParticipantTabContent> {
	public ParticipantsTabPanel(ParticipantTabContent content) {
		super(content);

		initialize();
	}
}
