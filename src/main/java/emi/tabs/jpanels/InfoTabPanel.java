package emi.tabs.jpanels;

import emi.GuiDefs;
import emi.tabs.InfoTabContent;
import emi.tabs.PanelConstants;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static emi.tabs.Constants.INDICATOR_FROM_JAR_RESOURCES;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class InfoTabPanel extends TabPanel<InfoTabContent> {
    public InfoTabPanel(InfoTabContent content) {
        super(content);

        initialize();
    }

    /**
     * This method initializes this
     */
    private void initialize() {

        this.setLayout(null);
        this.setSize(GuiDefs.TABBED_PANE_PANEL_SIZE);
        this.setPreferredSize(GuiDefs.TABBED_PANE_PANEL_SIZE);
        this.setBackground(PanelConstants.TAB_PANEL_BACKGROUND);
        this.add(initLogo(), null);
        this.add(initJtaInfoText(), null);

    }

    private JLabel initLogo() {
        JLabel jlLogo = new JLabel();
        jlLogo.setSize(new Dimension(300, 300));
        jlLogo.setPreferredSize(new Dimension(300, 300));
        jlLogo.setLocation(new Point(350, 15));

        try {
            jlLogo.setIcon(new ImageIcon(Objects.requireNonNull(getImage())));
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        jlLogo.setBorder(BorderFactory.createLineBorder(java.awt.Color.black, 1));

        return jlLogo;
    }

    /**
     * This method initializes jtaInfoText
     *
     * @return javax.swing.JTextArea
     */
    private JTextPane initJtaInfoText() {
        JTextPane jtaInfoText = new JTextPane();
        jtaInfoText.setBounds(new Rectangle(0, 335, 993, 236));
        jtaInfoText.setFont(PanelConstants.DESCRIPTION_FONT);
        jtaInfoText.setEditable(false);
        jtaInfoText.setBackground(PanelConstants.TAB_PANEL_BACKGROUND);
        jtaInfoText.setText(this.tabContent.getDescription());
        // Center Text
        StyledDocument doc = jtaInfoText.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0,doc.getLength(), center, false);

        return jtaInfoText;
    }

    private BufferedImage getImage() {
        String image = this.tabContent.getImage();
        try {
            if(image.startsWith(INDICATOR_FROM_JAR_RESOURCES)) {
                return ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResource(image.substring(INDICATOR_FROM_JAR_RESOURCES.length()))));
            }
            else {
                return ImageIO.read(new File(image));
            }
        }
        catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
