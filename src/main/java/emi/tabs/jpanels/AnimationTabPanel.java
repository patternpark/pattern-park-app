package emi.tabs.jpanels;

import emi.GUIElementNames;
import emi.session.UserSettings;
import emi.tabs.AnimationTabContent;
import emi.tabs.PanelConstants;
import emi.util.LauncherUtil;

import javax.swing.*;

/**
 * An instance of this class containing an exercise
 * about the current design pattern
 * Panel used in PatternOverview and Infocenter
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class AnimationTabPanel extends TabPanelWithPreviewImage<AnimationTabContent> {
	public AnimationTabPanel(AnimationTabContent content) {
		super(content);

		initialize();
	}

	protected AnimationTabPanel(AnimationTabContent content, boolean initialize) {
		super(content);

		if(initialize)
			initialize();
	}

	protected void initialize() {
		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.TOPIC), ContentType.HEADING);
		this.addTabContent(this.tabContent.getInfo(), ContentType.DESCRIPTION);

		this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.DURATION), ContentType.HEADING);
		this.addTabContent(this.tabContent.getTime(), ContentType.DESCRIPTION);

		this.add(initStartAnimationButton(),null);
	}

	/**
	 * This method initializes jb_start_anim
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton initStartAnimationButton() {
		JButton jbStartAnim = new JButton(UserSettings.getGUITextFromName(GUIElementNames.START_ANIMATION));
		jbStartAnim.setText(UserSettings.getGUITextFromName(GUIElementNames.START_ANIMATION));
		jbStartAnim.setLocation(PanelConstants.TAB_PANEL_BUTTON_LOCATION);
		jbStartAnim.setSize(PanelConstants.TAB_PANEL_BUTTON_SIZE);
		jbStartAnim
				.addActionListener(e -> LauncherUtil.startMovie(this.tabContent.getMovie(), this.tabContent.getMovieOmu(), UserSettings.getConfiguration().getSettings().isSoundState()));

		return jbStartAnim;
	}
}
