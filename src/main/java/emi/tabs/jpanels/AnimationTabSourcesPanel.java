package emi.tabs.jpanels;

import emi.GUIElementNames;
import emi.session.UserSettings;
import emi.tabs.AnimationTabContentWithSources;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public class AnimationTabSourcesPanel extends AnimationTabPanel {
    public AnimationTabSourcesPanel(AnimationTabContentWithSources content) {
        super(content, false);

        initialize();
    }

    @Override
    protected void initialize() {
        super.initialize();

        this.addTabContent(UserSettings.getGUITextFromName(GUIElementNames.SOURCE_IMAGES), ContentType.HEADING);
        this.addTabContent(((AnimationTabContentWithSources)tabContent).getSources(), ContentType.SOURCES);
    }
}
