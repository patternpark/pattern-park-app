package emi.tabs.jpanels;

import emi.tabs.TabContent;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class TabPanel<T extends TabContent> extends JPanel {
    protected final T tabContent;

    public TabPanel(T content) {
        this.tabContent = content;
    }
}
