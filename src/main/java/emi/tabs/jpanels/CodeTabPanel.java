package emi.tabs.jpanels;

import emi.GuiDefs;
import emi.tabs.CodeTabContent;
import emi.util.PathUtil;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * An instance of this JPanel class contains the java code example 
 * for the design pattern  
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class CodeTabPanel extends TabPanel<CodeTabContent> {
	public CodeTabPanel(CodeTabContent content) {
		super(content);
		init();
	}

	private void init(){
		this.add(this.initSourceCodeArea());
	}

	private JScrollPane initSourceCodeArea() {
		RSyntaxTextArea textArea = new RSyntaxTextArea();
		textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
		textArea.setFont(new Font("Courier New", textArea.getFont().getStyle(), textArea.getFont().getSize()));
		textArea.setText(PathUtil.readFileFromRelativePath(this.tabContent.getCodePath()));
		textArea.setEditable(false);
		textArea.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		textArea.getCaret().setVisible(true);

		textArea.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				textArea.getCaret().setVisible(true);
			}

			@Override
			public void focusLost(FocusEvent e) { }
		});

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(GuiDefs.CODE_SCROLL_PANE_SIZE);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		return scrollPane;
	}
}