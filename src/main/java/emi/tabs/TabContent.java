package emi.tabs;

import emi.models.ETabType;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class TabContent implements Cloneable {
    private final String _tabText;
    private final String _id;
    private final ETabType _type;
    private boolean _isVisible;

    protected TabContent(Element tabElement, ETabType type, boolean visible) {
        this._tabText = Objects.requireNonNull(UserSettings.getLanguage()).resolveStringWithVariables(tabElement.getChildText("tab_name"));
        this._id = tabElement.getAttributeValue("id");
        this._type = type;
        this._isVisible = visible;
    }

    public String getTabText() {
        return this._tabText;
    }

    public String getID() {
        return this._id;
    }

    public ETabType getType() {
        return this._type;
    }

    public boolean getIsVisible() {
        return this._isVisible;
    }

    public void setIsVisible(boolean val) {
        if (val == this._isVisible) return;

        this._isVisible = val;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
