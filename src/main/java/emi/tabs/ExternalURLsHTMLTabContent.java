package emi.tabs;

import emi.models.ETabType;
import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class ExternalURLsHTMLTabContent extends HTMLTabContent {
    public ExternalURLsHTMLTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.LOCAL_HTML_FILE_WITH_EXTERNAL_LINKS, isVisible);
    }
}
