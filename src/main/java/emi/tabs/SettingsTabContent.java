package emi.tabs;

import emi.models.ETabType;
import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class SettingsTabContent extends TabContent {
    public SettingsTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.SETTINGS, isVisible);
    }
}
