package emi.tabs;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class Constants {
    public static final String INDICATOR_FROM_JAR_RESOURCES = "internal:";
}
