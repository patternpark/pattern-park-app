package emi.tabs;

import emi.models.ETabType;
import emi.session.Language;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class ThePatternTabContent extends TabContentWithPreviewImage {
    private final String _description;
    private final String _advantages;
    private final String _disadvantages;
    private final String _participants;

    public ThePatternTabContent(Element tabElement, boolean isVisible) {
        super(tabElement, ETabType.PATTERN, isVisible);

        Language language = Objects.requireNonNull(UserSettings.getLanguage());
        this._description = language.resolveStringWithVariables(tabElement.getChildText("description"));
        this._advantages = language.resolveStringWithVariables(tabElement.getChildText("advantages"));
        this._disadvantages = language.resolveStringWithVariables(tabElement.getChildText("disadvantages"));
        this._participants = language.resolveStringWithVariables(tabElement.getChildText("participants"));
    }

    public String getDescription() {
        return _description;
    }

    public String getAdvantages() {
        return _advantages;
    }

    public String getDisadvantages() {
        return _disadvantages;
    }

    public String getParticipants() {
        return _participants;
    }
}
