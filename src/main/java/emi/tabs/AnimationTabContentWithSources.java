package emi.tabs;

import emi.models.ETabType;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class AnimationTabContentWithSources extends AnimationTabContent {
    private final String _sources;

    public AnimationTabContentWithSources(Element patternElement, boolean isVisible) {
        super(patternElement, ETabType.ANIMATION_WITH_SOURCES, isVisible);

        this._sources = Objects.requireNonNull(UserSettings.getLanguage()).resolveStringWithVariables(patternElement.getChildText("sources"));
    }

    public String getSources() {
        return this._sources;
    }
}
