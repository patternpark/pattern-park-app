package emi.tabs;

import emi.models.ETabType;
import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class TabContentWithPreviewImage extends TabContent {
    private final String _previewImage;

    protected TabContentWithPreviewImage(Element tabElement, ETabType type, boolean isVisible) {
        super(tabElement, type, isVisible);

        this._previewImage = tabElement.getChildText("preview");
    }

    public String getPreviewImage() {
        return this._previewImage;
    }
}
