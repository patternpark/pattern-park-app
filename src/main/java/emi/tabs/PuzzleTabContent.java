package emi.tabs;

import emi.models.ETabType;
import emi.session.Language;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class PuzzleTabContent extends TabContent {
    private final String _description;
    private final String _dataPath;
    private final String _solutionPath;

    public PuzzleTabContent( Element tabElement, boolean isVisible ) {
        super( tabElement, ETabType.PUZZLE, isVisible );
        Language language = Objects.requireNonNull( UserSettings.getLanguage() );
        this._description = language.resolveStringWithVariables(
                tabElement.getChildText( "description" ) );
        this._dataPath = tabElement.getChildText( "puzzle" );
        this._solutionPath = tabElement.getChildText( "solution" );

    }

    public String getDataPath() {
        return this._dataPath;
    }

    public String getSolutionPath() {
        return this._solutionPath;
    }

    public String getDescription() {
        return this._description;
    }
}
