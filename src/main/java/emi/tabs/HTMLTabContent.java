package emi.tabs;

import emi.models.ETabType;
import emi.util.ConverterUtil;
import org.jdom.Element;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class HTMLTabContent extends TabContent {
    private final String _url;

    protected HTMLTabContent(Element tabElement, ETabType type, boolean isVisible) {
        super(tabElement, type, isVisible);

        this._url = tabElement.getChildText("url");
    }

    public String getURL() {
        return this._url;
    }
}
