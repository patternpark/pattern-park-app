package emi.tabs;

import emi.models.ETabType;
import emi.session.Language;
import emi.session.UserSettings;
import org.jdom.Element;

import java.util.Objects;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class ExerciseTabContent extends TabContentWithPreviewImage {
    private final String _info;
    private final String _time;
    private final String _learningGoal;
    private final String _movie;
    private final String _preconditions;

    protected ExerciseTabContent(Element tabElement, ETabType type, boolean isVisible) {
        super(tabElement, type, isVisible);

        Language language = Objects.requireNonNull(UserSettings.getLanguage());
        this._info = language.resolveStringWithVariables(tabElement.getChildText("info"));
        this._time = tabElement.getChildText("time");
        this._learningGoal = language.resolveStringWithVariables(tabElement.getChildText("learning_goal"));
        this._movie = tabElement.getChildText("movie");
        this._preconditions = language.resolveStringWithVariables(tabElement.getChildText("preconditions"));
    }

    public String getInfo() {
        return _info;
    }

    public String getTime() {
        return _time;
    }

    public String getLearningGoal() {
        return _learningGoal;
    }

    public String getMovie() {
        return _movie;
    }

    public String getPreconditions() {
        return _preconditions;
    }
}
