package emi;

import emi.util.LauncherUtil;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.io.IOException;


/**
 * An instance of this class is able to load 
 * an html document and allows to use links with the system browser
 * @author Rudolf Koslowski (v1.0)
 * @version 1.7.0.1
 */
public class LinksFrame extends JPanel implements HyperlinkListener {

    private JEditorPane jEditorPane = null;

    /**
     * constructor
     */
    public LinksFrame() {
        super();
        init();
        // TODO Auto-generated constructor stub
    }

    private void init() {
        JScrollPane mySP = new JScrollPane();

        this.add( mySP.add( getJEditorPane() ), null );

    }

    /**
     * this Method allows the using links of the loaded html
     * document
     */
    public void hyperlinkUpdate( HyperlinkEvent event ) {
        HyperlinkEvent.EventType typ = event.getEventType();
        if ( typ == HyperlinkEvent.EventType.ACTIVATED ) {
            LauncherUtil.launchBrowser(event.getURL().toExternalForm());
        }
    }


    /**
     * This method initializes jEditorPane
     *
     * @return javax.swing.JEditorPane
     */
    private JEditorPane getJEditorPane() {
        if ( jEditorPane == null ) {
            try {
                jEditorPane = new JEditorPane();
                jEditorPane.setPreferredSize( new java.awt.Dimension( 900, 1000 ) );
                jEditorPane.setContentType( "text/html" );
                jEditorPane.setEditable( false );
                jEditorPane.setPage( this.getClass().getClassLoader().getResource( "html/Links.html" ) );
                jEditorPane.addHyperlinkListener( this );
            } catch ( IOException e ) {
                System.err.println( "Error Displaying" );
            }
        }
        return jEditorPane;
    }

}
