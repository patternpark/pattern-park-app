package emi.window.infocenter;

import emi.GUIElementNames;
import emi.GuiDefs;
import emi.models.configuration.Configuration;
import emi.session.UserSettings;

import javax.swing.*;
import java.awt.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public final class MiscSettingsPanel extends SubSettingsPanel {
    private Configuration _configuration;
    private JCheckBox jcbAudio;
    private final ChangePasswordDialog _changePasswordDialog;

    public MiscSettingsPanel(Configuration configuration, Frame dialogOwner) {
        this._configuration = configuration;
        this._changePasswordDialog = new ChangePasswordDialog(dialogOwner, _configuration);
        this.initialize();
    }

    public void refreshConfiguration(Configuration configuration) {
        this._configuration = configuration;

        this.jcbAudio.setSelected(this._configuration.getSettings().isSoundState());
        this._changePasswordDialog.refreshConfiguration(configuration);
    }

    private void initialize() {
        this._changePasswordDialog.addSettingsChangedListener(l -> {
            this._changePasswordDialog.setVisible(false);
            onSettingsChanged(this);
        });

        this.setLayout(new GridLayout(Constants.MISC_SETTINGS_AMOUNT_OF_SETTINGS, 0));
        this.setMinimumSize(GuiDefs.PATTERN_CONFIG_SIZE);
        this.add(initSoundCheckBox());
        this.add(initChangePasswordButton());
        this.setSize();
    }

    private JCheckBox initSoundCheckBox() {
        jcbAudio = new JCheckBox();
        jcbAudio.setText(UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_AUDIO));
        jcbAudio.setSelected(this._configuration.getSettings().isSoundState());
        jcbAudio.addItemListener((itemEvent -> {
            this._configuration.getSettings().setSoundState(jcbAudio.isSelected());
            onSettingsChanged(this);
        }));

        return jcbAudio;
    }

    private JComponent initChangePasswordButton() {
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel panel = new JPanel();
        panel.setLayout(gbl);

        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 0;

        JButton jBtnChangePw = new JButton();
        jBtnChangePw.setText(UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_CHANGE_PASSWORD));
        jBtnChangePw.addActionListener(actionEvent -> this._changePasswordDialog.setVisible(true));

        gbl.setConstraints(jBtnChangePw, gbc);
        panel.add(jBtnChangePw);
        return panel;
    }
}
