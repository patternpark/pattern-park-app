package emi.window.infocenter;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public interface ApplyChangesListener {
    void onApplyChanges();
}
