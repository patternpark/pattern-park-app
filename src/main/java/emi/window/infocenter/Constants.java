package emi.window.infocenter;

import java.awt.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
final class Constants {
    public static final int DIVIDED_PANEL_PREFERRED_SIZE = 300; // can actually take any value...
    public static final int DIVIDED_PANEL_MARGIN_TOP_BOTTOM = 18;
    public static final int AMOUNT_OF_NAVIGATION_LIST_ITEMS = 3;

    public static final float SETTINGS_PANEL_LEFT_SIDE_WIDTH = 0.3f; // in percent
    public static final float SETTINGS_PANEL_RIGHT_SIDE_WIDTH = 1.0f - SETTINGS_PANEL_LEFT_SIDE_WIDTH; // in percent

    public static final Dimension SUB_SETTING_DIMENSION = new Dimension(400, 300);
    public static final int PATTERN_SETTINGS_CHECK_BOXES_MARGIN_LEFT = 30; // in pixel

    /**
     * Defines how many "SelectableSetting" classes should be displayed on the left side of the settings.
     * @see SelectableSetting
     */
    public static final int SETTINGS_MAXIMUM_SELECTABLE_SETTINGS_PER_PAGE = 8;

    /**
     * Defines the number of settings that are displayed in the "MiscSettingsPanel" panel.
     * @see MiscSettingsPanel
     */
    public static final int MISC_SETTINGS_AMOUNT_OF_SETTINGS = 2;

    private Constants() {}
}
