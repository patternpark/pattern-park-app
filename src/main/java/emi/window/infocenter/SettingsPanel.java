package emi.window.infocenter;

import emi.GUIElementNames;
import emi.GuiDefs;
import emi.extension.RelativeLayout;
import emi.models.Pattern;
import emi.models.configuration.Configuration;
import emi.session.UserSettings;
import emi.tabs.SettingsTabContent;
import emi.tabs.jpanels.TabPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

import static emi.util.JavaSwingHelperUtil.loadImage;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class SettingsPanel extends TabPanel<SettingsTabContent> implements SettingsChangedListener {

    private static class SortByName implements Comparator<ConfigurationPatternPanel> {
        public int compare(ConfigurationPatternPanel a, ConfigurationPatternPanel b)
        {
            return a.getPattern().getName().compareTo(b.getPattern().getName());
        }
    }

    private Configuration _configuration;
    private final ArrayList<SettingsChangedListener> _settingsChangedListeners = new ArrayList<SettingsChangedListener>();
    private final ArrayList<ConfigurationPatternPanel> _patternConfigurations = new ArrayList<ConfigurationPatternPanel>();
    private final ArrayList<ApplyChangesListener> _applyChangesListeners = new ArrayList<ApplyChangesListener>();
    private final ArrayList<SelectableSetting> _selectableSettings = new ArrayList<SelectableSetting>();
    private MiscSettingsPanel _miscSettingsPanel;
    private final Frame _dialogOwner;
    private final int _maxPages;

    private int _currentPage = 0;

    private JButton _naviBtnPrevious;
    private JLabel _naviPageInfo;
    private JButton _naviBtnNext;

    private JPanel _currentSelectablePatterns;
    private JPanel _currentSettingsHolder;

    public SettingsPanel(SettingsTabContent content, Configuration configuration, Frame dialogOwner) {
        super(content);
        this._configuration = configuration;
        this._dialogOwner = dialogOwner;

        this.initializeSelectableSettings();
        this._maxPages = (int)Math.ceil((float)this._selectableSettings.size() / Constants.SETTINGS_MAXIMUM_SELECTABLE_SETTINGS_PER_PAGE) - 1; // -1 because we count at 0

        initialize();
        this.selectMiscTab();
    }

    public void addApplyChangesListeners(ApplyChangesListener listener) {
        this._applyChangesListeners.add(listener);
    }

    public void addSettingsChangedListener(SettingsChangedListener listener) {
        this._settingsChangedListeners.add(listener);
    }

    public void refreshConfiguration(Configuration configuration) {
        this._configuration = configuration;

        Pattern[] patterns = this._configuration.getPatterns();
        for (int i = 0; i < Math.min(patterns.length, _patternConfigurations.size()); ++i) {
            this._patternConfigurations.get(i).refreshPattern(patterns[i]);
        }

        this._miscSettingsPanel.refreshConfiguration(configuration);
        this._currentSettingsHolder.revalidate();
        this._currentSettingsHolder.repaint();
    }

    @Override
    public void onSettingsChanged(JComponent owner) {
        this._settingsChangedListeners.forEach(l -> l.onSettingsChanged(this));
    }

    private void initializeSelectableSettings() {
        for(Pattern pattern : this._configuration.getPatterns()) {
            ConfigurationPatternPanel patternConfig = new ConfigurationPatternPanel(pattern);
            patternConfig.addSettingsChangedListener(this);
            this._patternConfigurations.add(patternConfig);
        }

        ArrayList<ConfigurationPatternPanel> sortedPattersByName =
                new ArrayList<ConfigurationPatternPanel>(this._patternConfigurations);
        sortedPattersByName.sort(new SortByName());
        for(ConfigurationPatternPanel pattern : sortedPattersByName) {
            ImageIcon icon = loadImage(pattern.getPattern().getIcon());
            this._selectableSettings.add(new SelectableSetting(pattern.getPattern().getName(), icon, pattern));
        }

        this.initMiscSettings();
    }

    private void initialize() {
        this.removeAll();
        this.setSize(GuiDefs.TABBED_PANE_PANEL_SIZE);

        RelativeLayout rl = new RelativeLayout(RelativeLayout.X_AXIS);
        this.setLayout(rl);

        this.add(initSelectionPanel(), Constants.SETTINGS_PANEL_LEFT_SIDE_WIDTH);
        this.add(this.initRightSidePanel(), Constants.SETTINGS_PANEL_RIGHT_SIDE_WIDTH);
        this.setMinimumSize(this.getPreferredSize());
        this.setMaximumSize(this.getPreferredSize());
    }

    private JPanel initSelectionPanel() {
        this._currentSelectablePatterns = new JPanel();
        this._currentSelectablePatterns.setLayout(new GridLayout(Constants.SETTINGS_MAXIMUM_SELECTABLE_SETTINGS_PER_PAGE, 1));

        this.generateSelectablePatternsList();

        return this.generatePanelWithSmallBottomAnchorPanel(
                this._currentSelectablePatterns,
                GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL,
                initNavigation(),
                GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL
        );
    }


    private JPanel initNavigation() {
        JPanel navBox = new JPanel();
        navBox.setLayout(new GridLayout(1, Constants.AMOUNT_OF_NAVIGATION_LIST_ITEMS));

        navBox.add(this.initNavigationPreviousButton());
        navBox.add(this.initNavigationPageLabel());
        navBox.add(this.initNavigationNextButton());
        this.refreshNavigationButtonStatesAndPageText(); // set text and enable states of the buttons
        return navBox;
    }

    private JLabel initNavigationPageLabel() {
        this._naviPageInfo = new JLabel();
        this._naviPageInfo.setHorizontalAlignment(JLabel.CENTER);

        return this._naviPageInfo;
    }

    private JButton initNavigationPreviousButton() {
        this._naviBtnPrevious = new JButton(UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_NAVIGATION_PREVIOUS));
        this._naviBtnPrevious.addActionListener(e -> this.setPage(this._currentPage - 1));

        return this._naviBtnPrevious;
    }

    private JButton initNavigationNextButton() {
        this._naviBtnNext = new JButton(UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_NAVIGATION_NEXT));
        this._naviBtnNext.addActionListener(e -> this.setPage(this._currentPage + 1));

        return this._naviBtnNext;
    }

    private JPanel initRightSidePanel() {
        this._currentSettingsHolder = new JPanel();

        JButton jBtnChangePw = new JButton();
        jBtnChangePw.setText(UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_APPLY));
        jBtnChangePw.addActionListener(actionEvent -> this._applyChangesListeners.forEach(ApplyChangesListener::onApplyChanges));

        return this.generatePanelWithSmallBottomAnchorPanel(
                this._currentSettingsHolder,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                jBtnChangePw,
                GridBagConstraints.SOUTHEAST,
                GridBagConstraints.NONE
        );
    }

    private void initMiscSettings() {
        this._miscSettingsPanel = new MiscSettingsPanel(this._configuration, this._dialogOwner);
        this._miscSettingsPanel.addSettingsChangedListener(this);
        this._selectableSettings.add(
                new SelectableSetting(
                        UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_MISC_SETTING_NAME),
                        null,
                        _miscSettingsPanel
                )
        );
    }

    /**
     * Creates a panel whose "mainContent" takes up close to everything. In the lower area "navigationContent" is inserted,
     * which gets as much space as it is needed.
     * @return A layout which includes both items vertically.
     */
    private JPanel generatePanelWithSmallBottomAnchorPanel(
            JComponent mainContent,
            int anchorMainContent,
            int mainContentFill,
            JComponent navigationContent,
            int anchorNavigationContent,
            int navigationContentFill
    ) {
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(gbl);

        // set grid settings for "mainContent"
        gbc.fill = mainContentFill;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.anchor = anchorMainContent;
        gbl.setConstraints(mainContent, gbc);
        outputPanel.add(mainContent);

        // set grid settings for "navigationContent"
        gbc.fill = navigationContentFill;
        gbc.anchor = anchorNavigationContent;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 0;
        gbl.setConstraints(navigationContent, gbc);
        outputPanel.add(navigationContent);

        // workaround so that navigationContent is really at the bottom
        outputPanel.setPreferredSize(
                new Dimension(
                        Constants.DIVIDED_PANEL_PREFERRED_SIZE,
                        GuiDefs.TABBED_PANE_PANEL_SIZE.height - Constants.DIVIDED_PANEL_MARGIN_TOP_BOTTOM
                )
        );
        return outputPanel;
    }

    private void generateSelectablePatternsList() {
        this._currentSelectablePatterns.removeAll();

        int pageOffset = this._currentPage * Constants.SETTINGS_MAXIMUM_SELECTABLE_SETTINGS_PER_PAGE;
        int maximum = Math.min(pageOffset + Constants.SETTINGS_MAXIMUM_SELECTABLE_SETTINGS_PER_PAGE, this._selectableSettings.size());
        for(int i = pageOffset; i < maximum; ++i) {
            SelectableSetting setting = this._selectableSettings.get(i);
            JButton patternButton = new JButton(setting.getText(), setting.getIcon());
            patternButton.setOpaque(false);
            patternButton.setContentAreaFilled(false);
            patternButton.setHorizontalAlignment(SwingConstants.LEFT);
            patternButton.addActionListener(e -> this.refreshSettingsHolder(setting));

            this._currentSelectablePatterns.add(patternButton);
        }

        this._currentSelectablePatterns.revalidate();
        this._currentSelectablePatterns.repaint();
    }

    private void setPage(int page) {
        this._currentPage = Math.min(Math.max(0, page), this._maxPages);

        this.refreshNavigationButtonStatesAndPageText();
        this.generateSelectablePatternsList();
    }

    private void selectMiscTab() {
        Optional<SelectableSetting> miscSetting = Arrays.stream( this._selectableSettings.toArray(new SelectableSetting[0]) )
                .filter( s -> s.getComponent() instanceof MiscSettingsPanel ).findFirst();

        if ( !miscSetting.isPresent() ) return;

        this.refreshSettingsHolder(miscSetting.get());
    }

    private void refreshNavigationButtonStatesAndPageText() {
        this._naviBtnPrevious.setEnabled(this._currentPage > 0);
        this._naviPageInfo.setText(
                String.format(
                        UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_NAVIGATION_PAGE),
                        this._currentPage + 1, // index start at 0, but user expects at 1
                        this._maxPages + 1 // maxIndex start at 0, but user expects at 1
                )
        );
        this._naviBtnNext.setEnabled(this._currentPage < this._maxPages);
    }

    private void refreshSettingsHolder(SelectableSetting selectableSetting) {
        this._currentSettingsHolder.removeAll();
        this._currentSettingsHolder.add(selectableSetting.getComponent());
        this._currentSettingsHolder.setBorder(BorderFactory.createTitledBorder(selectableSetting.getText()));

        this._currentSettingsHolder.revalidate();
        this._currentSettingsHolder.repaint();
    }
}
