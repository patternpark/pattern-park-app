package emi.window.infocenter;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public interface SettingsChangedListener {
    void onSettingsChanged(JComponent owner);
}
