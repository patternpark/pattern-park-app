package emi.window.infocenter;

import emi.GUIElementNames;
import emi.models.Pattern;
import emi.session.UserSettings;
import emi.tabs.TabContent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public class ConfigurationPatternPanel extends SubSettingsPanel {
    private Pattern _pattern;
    private final ArrayList<JCheckBox> _tabCheckBoxes = new ArrayList<>();
    private JCheckBox jcbMainCheckBox;

    public Pattern getPattern() {
        return this._pattern;
    }

    public ConfigurationPatternPanel(Pattern pattern) {
        this._pattern = pattern;

        initialize();
    }

    public void refreshPattern(Pattern pattern) {
        this._pattern = pattern;

        TabContent[] tabs = this._pattern.getTabs();
        for(int i = 0; i < Math.min(tabs.length, this._tabCheckBoxes.size()); ++i)
            this._tabCheckBoxes.get(i).setSelected(tabs[i].getIsVisible());

        this.jcbMainCheckBox.setSelected(pattern.isActive());
    }

    private void initialize() {
        initTabCheckBoxes();

        if(this._tabCheckBoxes.size() == 1)
            this._tabCheckBoxes.get(0).setVisible(false);

        this.setLayout(new BorderLayout());
        this.add(initMainCheckBox(), BorderLayout.NORTH);
        this.add(initSelectionBox(), BorderLayout.CENTER);
        this.setSize();
    }

    private JCheckBox initMainCheckBox() {
        jcbMainCheckBox = new JCheckBox(
                UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_ENABLE_PATTERN),
                this._pattern.isActive() || this.isOneTabActive()
        );
        jcbMainCheckBox.setSelected(this._pattern.isActive() && this.isOneTabActive());
        jcbMainCheckBox.addItemListener(itemEvent -> {
            if (jcbMainCheckBox.isSelected() && !isOneTabActive()) {
                setAllBoxes(true);
            }
            else if(!jcbMainCheckBox.isSelected()) {
                setAllBoxes(false);
            }

            this._pattern.setActiveState(jcbMainCheckBox.isSelected());
            onSettingsChanged(this);
        });

        return jcbMainCheckBox;
    }

    private void initTabCheckBoxes() {
        for(TabContent tab : this._pattern.getTabs()) {
            JCheckBox checkBox = new JCheckBox(tab.getTabText(), tab.getIsVisible());
            checkBox.setSelected(tab.getIsVisible() && this._pattern.isActive());
            checkBox.addItemListener(itemEvent -> {
                if (checkBox.isSelected()) {
                    jcbMainCheckBox.setSelected(true);
                }
                else {
                    jcbMainCheckBox.setSelected(this.isOneTabActive());
                }

                tab.setIsVisible(checkBox.isSelected());
                onSettingsChanged(this);
            });
            this._tabCheckBoxes.add(checkBox);
        }
    }

    public JPanel initSelectionBox() {
        JPanel selectionBox = new JPanel();
        selectionBox.setBorder(
                BorderFactory.createEmptyBorder(
                        0,
                        Constants.PATTERN_SETTINGS_CHECK_BOXES_MARGIN_LEFT,
                        0,
                        0
                )
        );
        selectionBox.setLayout(new GridLayout(this._tabCheckBoxes.size(), 1, 0, 0));
        for(JCheckBox checkBox : this._tabCheckBoxes)
            selectionBox.add(checkBox);

        return selectionBox;
    }

    private boolean isOneTabActive() {
        for (JCheckBox box : this._tabCheckBoxes) {
            if(box.isSelected())
                return true;
        }
        return false;
    }

    /**
     * Sets the selection of all tab check boxes
     */
    private void setAllBoxes(boolean isSelected) {
        for (JCheckBox box : this._tabCheckBoxes) {
            box.setSelected(isSelected);
        }
    }
}
