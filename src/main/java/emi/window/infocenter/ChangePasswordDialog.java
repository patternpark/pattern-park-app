package emi.window.infocenter;

import emi.GUIElementNames;
import emi.GuiDefs;
import emi.dialog.GenericDialog;
import emi.models.configuration.Configuration;
import emi.session.UserSettings;
import emi.util.HashUtil;
import emi.util.JavaSwingHelperUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class ChangePasswordDialog extends GenericDialog {
    private JPasswordField jPasswordField1 = null;
    private JPasswordField jPasswordField2 = null;
    private JPasswordField jPasswordField3 = null;

    private Configuration _configuration;

    private final ArrayList<SettingsChangedListener> _settingsChangedListeners = new ArrayList<>();

    public ChangePasswordDialog(Frame owner, Configuration configuration) throws HeadlessException {
        super(owner, true, null, false);

        this._configuration = configuration;
        initialize(320, 300);
        JavaSwingHelperUtil.setWindowInCenter(this);
    }

    public void addSettingsChangedListener(SettingsChangedListener listener) {
        this._settingsChangedListeners.add(listener);
    }

    public void refreshConfiguration(Configuration configuration) {
        clearPwFields();
        this._configuration = configuration;
    }

    @Override
    public void initialize(int width, int height) {
        this.setSize(width, height);
        this.setPreferredSize(GuiDefs.PASSWORD_PANEL);
        this.setResizable(this.isResizable);
        this.setLayout(null);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setModal(true);

        jPasswordField1 = getJPasswordField1();
        jPasswordField2 = getJPasswordField2();
        jPasswordField3 = getJPasswordField3();
        this.add(initTitle(), null);
        this.add(initOldPasswordLabel(), null);
        this.add(jPasswordField1, null);
        this.add(initNewPasswordLabel(), null);
        this.add(jPasswordField2, null);
        this.add(initNewPasswordAgainLabel(), null);
        this.add(jPasswordField3, null);
        this.add(getJbCheckPw(), null);

        WindowListener windowClose = new WindowListener() {
            public void windowOpened(WindowEvent we) {}
            public void windowDeiconified(WindowEvent we) {}
            public void windowIconified(WindowEvent we) {}
            public void windowDeactivated(WindowEvent we) {}
            public void windowActivated(WindowEvent we) {}
            public void windowClosed(WindowEvent we) {}
            public void windowClosing(WindowEvent we) {
                clearPwFields();
            }
        };
        this.addWindowListener(windowClose);
    }

    private JButton getJbCheckPw() {
        JButton jbCheckPw = new JButton(UserSettings.getGUITextFromName(GUIElementNames.DIALOG_CHANGE_PASSWORD_BUTTON_TEXT));
        jbCheckPw.setBounds(new Rectangle(30, 190, 241, 26));
        jbCheckPw.addActionListener(e -> {
            checkPassword();
        });
        return jbCheckPw;
    }

    private JPasswordField getJPasswordField3() {
        JPasswordField jPasswordField3 = new JPasswordField();
        jPasswordField3.setBounds(new Rectangle(15, 138, 271, 20));

        return jPasswordField3;
    }

    private JPasswordField getJPasswordField2() {
        JPasswordField jPasswordField2 = new JPasswordField();
        jPasswordField2.setBounds(new Rectangle(15, 93, 271, 20));
        return jPasswordField2;
    }

    private JPasswordField getJPasswordField1() {
        JPasswordField jPasswordField1 = new JPasswordField();
        jPasswordField1.setBounds(new Rectangle(15, 48, 271, 20));
        return jPasswordField1;
    }

    private JLabel initTitle() {
        JLabel jlTitel = new JLabel();
        jlTitel.setBounds(new Rectangle(15, 0, 271, 22));
        jlTitel.setText(UserSettings.getGUITextFromName(GUIElementNames.DIALOG_CHANGE_PASSWORD_DESCRIPTION));

        return jlTitel;
    }

    private JLabel initOldPasswordLabel() {
        JLabel jlText1 = new JLabel();
        jlText1.setBounds(new Rectangle(15, 30, 271, 16));
        jlText1.setText(UserSettings.getGUITextFromName(GUIElementNames.DIALOG_CHANGE_PASSWORD_OLD));

        return jlText1;
    }

    private JLabel initNewPasswordLabel() {
        JLabel jlText2 = new JLabel();
        jlText2.setBounds(new Rectangle(15, 75, 271, 16));
        jlText2.setText(UserSettings.getGUITextFromName(GUIElementNames.DIALOG_CHANGE_PASSWORD_NEW));

        return jlText2;
    }

    private JLabel initNewPasswordAgainLabel() {
        JLabel jlText3 = new JLabel();
        jlText3.setBounds(new Rectangle(15, 120, 271, 16));
        jlText3.setText(UserSettings.getGUITextFromName(GUIElementNames.DIALOG_CHANGE_PASSWORD_NEW_AGAIN));

        return jlText3;
    }

    private void clearPwFields() {
        jPasswordField1.setText("");
        jPasswordField2.setText("");
        jPasswordField3.setText("");
    }

    private void checkPassword() {
        String oldPassword = _configuration.getSettings().getPassword();
        String oldPasswordUser = HashUtil.getMD5String(String.valueOf(this.jPasswordField1.getPassword()));
        if(!oldPassword.equals(oldPasswordUser)) {
            clearPwFields();
            return;
        }

        if(!Arrays.equals(this.jPasswordField2.getPassword(), this.jPasswordField2.getPassword())) {
            clearPwFields();
            return;
        }

        String newPasswordUser = HashUtil.getMD5String(String.valueOf(this.jPasswordField2.getPassword()));
        clearPwFields();
        this._configuration.getSettings().setPassword(newPasswordUser);
        this._settingsChangedListeners.forEach(l -> l.onSettingsChanged(null));
    }
}
