package emi.window.infocenter;

import javax.swing.*;
import java.util.ArrayList;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class SubSettingsPanel extends JPanel {
    private final ArrayList<SettingsChangedListener> _settingsChangedListeners = new ArrayList<>();

    public void addSettingsChangedListener(SettingsChangedListener listener) {
        this._settingsChangedListeners.add(listener);
    }

    protected void onSettingsChanged(JComponent owner) {
        _settingsChangedListeners.forEach(l -> l.onSettingsChanged(owner));
    }

    protected void setSize() {
        this.setPreferredSize(Constants.SUB_SETTING_DIMENSION);
    }
}
