package emi.window.infocenter;

import javax.swing.*;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public class SelectableSetting {
    private final String _text;
    private final ImageIcon _icon;
    private final JComponent _component;

    public String getText() {
        return _text;
    }

    public ImageIcon getIcon() {
        return _icon;
    }

    public JComponent getComponent() {
        return _component;
    }

    public SelectableSetting(String text, ImageIcon icon, JComponent component) {
        this._text = text;
        this._icon = icon;
        this._component = component;
    }
}
