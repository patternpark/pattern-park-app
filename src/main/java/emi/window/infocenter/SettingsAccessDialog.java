package emi.window.infocenter;

import emi.GUIElementNames;
import emi.dialog.GenericDialog;
import emi.models.configuration.Configuration;
import emi.session.UserSettings;
import emi.util.HashUtil;
import emi.util.JavaSwingHelperUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class SettingsAccessDialog extends GenericDialog {
    private Configuration _configuration;
    private JPasswordField _passwordField;
    private boolean _validPassword = false;

    public SettingsAccessDialog(Frame owner, Configuration configuration) throws HeadlessException {
        super(owner, true, null, false, UserSettings.getGUITextFromName(GUIElementNames.DIALOG_ACCEPT), UserSettings.getGUITextFromName(GUIElementNames.DIALOG_CANCEL));

        this._configuration = configuration;
        initialize(287, 176);
        JavaSwingHelperUtil.setWindowInCenter(this);
    }

    public boolean isPasswordValid() {
        return this._validPassword;
    }

    public void refreshConfiguration(Configuration configuration) {
        this._configuration = configuration;
    }

    @Override
    public void initialize(int width, int height) {
        super.initialize(width, height);

        this.addOnAcceptListener((l) -> {
            String enteredPassword = HashUtil.getMD5String(String.valueOf(this._passwordField.getPassword()));
            this._validPassword = this._configuration.getSettings().getPassword().equals(enteredPassword);
            if(this._validPassword) {
                setVisible(false);
            }
            else {
                this.clearPasswordField();
            }
        });
        this.addOnCancelListener(l -> setVisible(false));
        this.addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent e) {
                _validPassword = false;
                clearPasswordField();
            }

            public void componentHidden ( ComponentEvent e ) { }
        });
    }

    @Override
    protected JPanel initJContentPane() {
        JPanel jContentPane = new JPanel();
        jContentPane.setLayout(null);
        jContentPane.add(initPasswordField(), null);
        jContentPane.add(initPasswordLabel(), null);
        jContentPane.add(initJbAccept(), null);
        jContentPane.add(initJbCancel(), null);

        jContentPane.addKeyListener(this.getKeyListener());

        return jContentPane;
    }

    @Override
    protected JButton initJbAccept() {
        JButton button = super.initJbAccept();
        button.setBounds(new Rectangle(15, 89, 106, 32));
        return button;
    }

    @Override
    protected JButton initJbCancel() {
        JButton button = super.initJbCancel();
        button.setBounds(new Rectangle(150, 89, 106, 32));
        return button;
    }

    private JLabel initPasswordLabel() {
        JLabel jlPassword = new JLabel();
        jlPassword.setBounds(new Rectangle(15, 45, 106, 16));
        jlPassword.setHorizontalTextPosition(SwingConstants.LEADING);
        jlPassword.setHorizontalAlignment(SwingConstants.CENTER);
        jlPassword.setText(UserSettings.getGUITextFromName(GUIElementNames.PASSWORD));

        return jlPassword;
    }

    /**
     * This method initializes jtf_password
     *
     * @return javax.swing.JTextField
     */
    private JPasswordField initPasswordField() {
        this._passwordField = new JPasswordField();
        this._passwordField.setBounds(new Rectangle(135, 45, 106, 20));
        this._passwordField.addKeyListener(this.getKeyListener());
        return this._passwordField;
    }

    private void clearPasswordField() {
        this._passwordField.setText("");
    }
}
