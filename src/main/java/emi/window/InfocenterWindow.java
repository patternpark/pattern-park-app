package emi.window;

import emi.GUIElementNames;
import emi.GuiDefs;
import emi.RefreshGUIListener;
import emi.dialog.GenericDialog;
import emi.models.ExtraWindow;
import emi.models.configuration.Configuration;
import emi.session.UserSettings;
import emi.tabs.SettingsTabContent;
import emi.tabs.TabContent;
import emi.util.JavaSwingHelperUtil;
import emi.window.infocenter.SettingsAccessDialog;
import emi.window.infocenter.SettingsPanel;
import emi.window.infocenter.VetoableSingleSelectionModel;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * @author Rudolf Koslowski (v1.0), Erik der Glückliche (Pseudonym) (v1.7)
 * @version 1.7
 */
public class InfocenterWindow extends TabbedWindow<ExtraWindow> {
    private final HashMap<String, String> _icons = new HashMap<String, String>();
    private final ArrayList<RefreshGUIListener> _refreshGUIListeners = new ArrayList<>();
    private Configuration _changedConfiguration;
    private SettingsPanel _settingsPanel;
    private boolean _isConfigSaved = true;
    private GenericDialog _saveChangesDialog = null;
    private final Frame _dialogOwner;
    private SettingsAccessDialog _accessDialog = null;

    public InfocenterWindow(ExtraWindow window, Frame dialogOwner) {
        super(window);
        this.initIcons();
        try {
            this._changedConfiguration = (Configuration) UserSettings.getConfiguration().clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        this._dialogOwner = dialogOwner;
        this.initialize();
    }

    public void addRefreshGUIListener(RefreshGUIListener listener) {
        this._refreshGUIListeners.add(listener);
    }

    @Override
    public void mouseClickedOnPatternParkIcon(JPanel object) {
        if(!_isConfigSaved) {
            this._saveChangesDialog.setVisible(true);
            return;
        }

        super.mouseClickedOnPatternParkIcon(object);
    }

    @Override
    protected void initialize() {
        this.initAccessDialog();
        this.initSaveChangesDialog();
        super.initialize();
    }

    @Override
    protected void createTabs(JTabbedPane jtpPattern) {
        VetoableSingleSelectionModel model = new VetoableSingleSelectionModel();
        VetoableChangeListener validator = new VetoableChangeListener() {

            @Override
            public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
                if (isValidTab((int) evt.getNewValue())) return;

                throw new PropertyVetoException("change not valid", evt);
            }

            private boolean isValidTab(int newSelection) {
                ArrayList<TabContent> tbs = new ArrayList<>(tabs.keySet());
                if(newSelection >= 0 && newSelection < tabs.size() && tbs.get(newSelection).getID().equals("settings")) {
                    _accessDialog.setVisible(true);
                    return _accessDialog.isPasswordValid();
                }
                return true;
            }
        };
        model.addVetoableChangeListener(validator);
        jtpPattern.setModel(model);

        super.createTabs(jtpPattern);
    }

    @Override
    protected void createTabAndAddTab(JTabbedPane jtpPattern, TabContent tab) {
        if(tab.getID().equals("settings") && !this.tabs.containsKey(tab)) {
            _settingsPanel = new SettingsPanel((SettingsTabContent) tab, this._changedConfiguration, this._dialogOwner);
            _settingsPanel.addSettingsChangedListener(owner -> this._isConfigSaved = false);
            _settingsPanel.addApplyChangesListeners(() -> {
                this._refreshGUIListeners.forEach(l -> l.onRefreshGUI(this._changedConfiguration));
                this._isConfigSaved = true;
            });
            this.tabs.put(tab, _settingsPanel);
            // don't return, coz parent class will add the new tab
        }

        super.createTabAndAddTab(jtpPattern, tab);
    }

    @Override
    protected ImageIcon getTabIcon(TabContent tab) {
        if(_icons.containsKey(tab.getID()))
            return new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(_icons.get(tab.getID()))));

        return super.getTabIcon(tab);
    }

    private void initIcons() {
        this._icons.put("glossar", GuiDefs.GLOSSARY_ICON_PATH);
        this._icons.put("links", GuiDefs.LINKS_ICON_PATH);
        this._icons.put("settings", GuiDefs.SETTINGS_ICON_PATH);
        this._icons.put("info", GuiDefs.INFO_ICON_PATH);
    }

    private void initSaveChangesDialog() {
        this._saveChangesDialog = new GenericDialog(this._dialogOwner, true, UserSettings.getGUITextFromName(GUIElementNames.SETTINGS_SAVE_CHANGES), false);
        _saveChangesDialog.addOnCancelListener((l) -> {
            _saveChangesDialog.setVisible(false);
            try {
                this._changedConfiguration = (Configuration) UserSettings.getConfiguration().clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            this._settingsPanel.refreshConfiguration(this._changedConfiguration);
            this._accessDialog.refreshConfiguration(this._changedConfiguration);
            this._isConfigSaved = true;
            super.mouseClickedOnPatternParkIcon(this);
        });
        _saveChangesDialog.addOnAcceptListener(listener -> {
            _saveChangesDialog.setVisible(false);
            this._refreshGUIListeners.forEach(l -> l.onRefreshGUI(this._changedConfiguration));
            this._isConfigSaved = true;
            super.mouseClickedOnPatternParkIcon(this);
        });
        _saveChangesDialog.initialize(309, 177);
        JavaSwingHelperUtil.setScreenFriendlySizeAndCentralize(_saveChangesDialog);
        _saveChangesDialog.setVisible(false);
    }

    private void initAccessDialog() {
        this._accessDialog = new SettingsAccessDialog(this._dialogOwner, this._changedConfiguration);
        _accessDialog.setVisible(false);
    }
}
