package emi.window;

import emi.GuiDefs;
import emi.graphic.MouseClickedOnPatternParkIconListener;
import emi.graphic.WindowHeader;
import emi.models.ExtraWindow;
import emi.models.Pattern;
import emi.models.WindowContent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class WindowPanel<T extends WindowContent> extends JPanel implements MouseClickedOnPatternParkIconListener {
    private final ArrayList<MouseClickedOnPatternParkIconListener> _clickListeners = new ArrayList<>();

    protected T windowContent;

    protected WindowPanel(T windowContent) {
        this.windowContent = windowContent;
    }

    protected WindowHeader initWindowHeader() {
        WindowHeader header = new WindowHeader(this.windowContent);
        header.addClickedOnPatternParkIconListener(this);
        return header;
    }

    public static WindowPanel<?> getInstance(WindowContent content, JFrame dialogOwner) {
        if(content.getId().equals("infocenter"))
            return new InfocenterWindow((ExtraWindow)content, dialogOwner);
        else
            return new PatternWindow((Pattern)content);
    }

    public void addMouseClickedOnPatternParkIconListener(MouseClickedOnPatternParkIconListener listener) {
        this._clickListeners.add(listener);
    }

    @Override
    public void mouseClickedOnPatternParkIcon(JPanel object) {
        this._clickListeners.forEach(l -> l.mouseClickedOnPatternParkIcon(object));
    }

    protected abstract JComponent initMainContent();

    protected void initialize() {
        GridBagConstraints gbc1 = new GridBagConstraints();
        gbc1.insets = new Insets(1,1,1,1);
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        gbc1.weightx = 100;
        gbc1.weighty = 0;
        gbc1.gridheight = 1;
        gbc1.gridwidth = 1;
        gbc1.fill = GridBagConstraints.BOTH;

        GridBagConstraints gbc2 = new GridBagConstraints();
        gbc2.insets = new Insets(1,1,1,1);
        gbc2.gridx = 0;
        gbc2.gridy = 1;
        gbc2.weightx = 100;
        gbc2.weighty = 100;
        gbc2.gridheight = 2;
        gbc2.gridwidth = 1;
        gbc2.fill = GridBagConstraints.BOTH;

        Dimension d1 =  GuiDefs.OVERVIEW_PANEL_SIZE;
        this.setMinimumSize(d1);
        this.setMaximumSize(d1);
        this.setPreferredSize(d1);
        this.setSize(d1);
        this.setLayout(new GridBagLayout());

        this.add(initWindowHeader(), gbc1);
        this.setVisible(true);
        this.add(initMainContent(), gbc2);
    }
}
