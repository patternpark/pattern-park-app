package emi.window;

import emi.models.Pattern;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public class PatternWindow extends TabbedWindow<Pattern> {
    public PatternWindow(Pattern pattern) {
        super( pattern );
        this.initialize();
    }
}