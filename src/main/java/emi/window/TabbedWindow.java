package emi.window;

import emi.GuiDefs;
import emi.models.ETabType;
import emi.models.WindowContentWithTabs;
import emi.tabs.*;
import emi.tabs.jpanels.*;
import emi.tabs.jpanels.html.ExternalURLsHTMLTabPanel;
import emi.tabs.jpanels.html.LocalURLsHTMLTabPanel;
import umlPuzzle.panels.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Erik der Glückliche (Pseudonym)
 * @version 1.0
 */
public abstract class TabbedWindow<T extends WindowContentWithTabs> extends WindowPanel<T> {
    private final HashMap<ETabType, String> _icons = new HashMap<ETabType, String>();
    private final HashMap<ETabType, Function<TabContent, TabPanel<?>>> _ctors = new HashMap<ETabType, Function<TabContent, TabPanel<?>>>();
    private final List<ETabType> _scrollbarContents = Arrays.asList(
            ETabType.LOCAL_HTML_FILE,
            ETabType.LOCAL_HTML_FILE_WITH_EXTERNAL_LINKS
    );

    protected final LinkedHashMap<TabContent, JComponent> tabs = new LinkedHashMap<>();

    protected TabbedWindow(T pattern) {
        super(pattern);

        this.initIcons();
        this.initCtors();
    }

    @Override
    protected JComponent initMainContent() {
        JTabbedPane jtpPattern = new JTabbedPane();
        Dimension d1 =  new Dimension(980,720);
        jtpPattern.setMinimumSize(d1);
        jtpPattern.setMaximumSize(d1);
        jtpPattern.setPreferredSize(d1);
        jtpPattern.setSize(d1);

        this.createTabs(jtpPattern);

        return jtpPattern;
    }

    protected void createTabs(JTabbedPane jtpPattern) {
        for(TabContent content : Arrays.stream(this.windowContent.getTabs()).filter(TabContent::getIsVisible).collect(Collectors.toList())) {
            this.createTabAndAddTab(jtpPattern, content);
        }
    }

    protected void createTabAndAddTab(JTabbedPane jtpPattern, TabContent tab) {
        ETabType type = tab.getType();
        if(!this.tabs.containsKey(tab)) {
            if(!this._ctors.containsKey(type)) {
                System.out.printf("Constructor for tab \"%s\" doesn't found!\n", type.toString());
                return;
            }

            this.tabs.put(tab, this._ctors.get(tab.getType()).apply(tab));
        }

        JComponent tabComponent = this.tabs.get(tab);
        if(_scrollbarContents.contains(type))
            tabComponent = new JScrollPane(tabComponent);

        jtpPattern.addTab(tab.getTabText(), getTabIcon(tab), tabComponent, null);
    }

    protected ImageIcon getTabIcon(TabContent tab) {
        ETabType type = tab.getType();
        String iconPath = null;
        if(this._icons.containsKey(type))
            iconPath = this._icons.get(type);

        ImageIcon icon = null;
        if(iconPath != null)
            icon = new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(iconPath)));

        return icon;
    }

    private void initIcons() {
        this._icons.put(ETabType.OVERVIEW, GuiDefs.SUMMARY_ICON_PATH);
        this._icons.put(ETabType.ANIMATION, GuiDefs.ANIMATION_ICON_PATH);
        this._icons.put(ETabType.ANIMATION_WITH_SOURCES, GuiDefs.ANIMATION_ICON_PATH);
        this._icons.put(ETabType.PARTICIPANT, GuiDefs.PARTICIPANTS_ICON_PATH);
        this._icons.put(ETabType.UML, GuiDefs.SEQ_DIAG_ICON_PATH);
        this._icons.put(ETabType.PUZZLE, GuiDefs.CLASS_DIAG_ICON_PATH);
        this._icons.put(ETabType.PATTERN, GuiDefs.GLOSSARY_ICON_PATH);
        this._icons.put(ETabType.CODE, GuiDefs.CODE_ICON_PATH);
    }

    private void initCtors() {
        this._ctors.put(ETabType.OVERVIEW, (tab) -> new OverviewTabPanel((OverviewTabContent) tab));
        this._ctors.put(ETabType.ANIMATION, (tab) -> new AnimationTabPanel((AnimationTabContent) tab));
        this._ctors.put(ETabType.ANIMATION_WITH_SOURCES, (tab) -> new AnimationTabSourcesPanel((AnimationTabContentWithSources) tab));
        this._ctors.put(ETabType.PARTICIPANT, (tab) -> new ParticipantsTabPanel((ParticipantTabContent) tab));
        this._ctors.put(ETabType.UML, (tab) -> new UmlPanel((UmlTabContent) tab));
        this._ctors.put(ETabType.PUZZLE, (tab) -> new MainPanel( (PuzzleTabContent) tab));
        this._ctors.put(ETabType.PATTERN, (tab) -> new ThePatternPanel((ThePatternTabContent) tab));
        this._ctors.put(ETabType.CODE,(tab) -> new CodeTabPanel((CodeTabContent) tab));
        this._ctors.put(ETabType.INFO,(tab) -> new InfoTabPanel((InfoTabContent) tab));
        this._ctors.put(ETabType.LOCAL_HTML_FILE,(tab) -> new LocalURLsHTMLTabPanel((LocalURLsHTMLTabContent) tab));
        this._ctors.put(ETabType.LOCAL_HTML_FILE_WITH_EXTERNAL_LINKS,(tab) -> new ExternalURLsHTMLTabPanel((ExternalURLsHTMLTabContent) tab));
    }
}
