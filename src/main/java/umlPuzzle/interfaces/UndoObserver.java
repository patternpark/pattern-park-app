package umlPuzzle.interfaces;

import org.jdom.Element;

import java.util.List;

public interface UndoObserver {

    void reactToStepAdded();

    void reactToUndo( List<Element> classList );

    void reactToRedo( List<Element> classList );

    void reactToStepNotPossible( int typeOfWarning );

}
