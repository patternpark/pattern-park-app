package umlPuzzle.frames;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class CloseWindowListener implements WindowListener {
    public void windowOpened( WindowEvent we ) {
    }

    public void windowDeiconified( WindowEvent we ) {
    }

    public void windowIconified( WindowEvent we ) {
    }

    public void windowDeactivated( WindowEvent we ) {
    }

    public void windowActivated( WindowEvent we ) {
    }

    public void windowClosed( WindowEvent we ) {
    }

    public void windowClosing( WindowEvent we ) {
        ( (Frame) we.getWindow() ).exit();
    }
}
