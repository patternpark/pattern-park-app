package umlPuzzle.frames;

import org.jdom.Element;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.panels.DrawPanel;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Objects;

public abstract class Frame extends JFrame {

    protected final DrawPanel drawPanel;
    protected final JPanel mainPanel;

    public Frame( DrawPanel drawPanel ) {
        this.drawPanel = drawPanel;
        this.mainPanel = new JPanel();
    }

    public void initFrame() {
        // setzen des Layouts
        GridBagLayout layout = new GridBagLayout();
        this.mainPanel.setLayout( layout );

        try {
            BufferedImage emiIcon;
            emiIcon = ImageIO.read(
                    Objects.requireNonNull( this.getClass().getResource( Constants.PATTERN_PARK_GIF_PATH ) ) );
            this.setIconImage( emiIcon );
        } catch ( Exception ignored ) {
        }
        this.setTitle( ExerciseConstants.PATTERN_PARK_TITLE );
        this.setBackground( ExerciseConstants.WHITE );
        this.mainPanel.setBackground( ExerciseConstants.WHITE );
    }

    /**
     * Beendet das Fenster
     */
    public void exit() {
        this.setVisible( false );
        String helpText = this.drawPanel.getButton().getText().getStart();
        this.drawPanel.getButton().getHelpPanel().setHelp( helpText );
        this.drawPanel.getButton().setEverythingEnable( true );
    }

    public abstract void okAction();

    public void handleNewStep( int position, List<Element> classList ) {
        // Undo-Schritt festhalten
        this.drawPanel.getExercise().getUndo().addUndoStep( classList );

        if ( position == 0 ) {
            this.drawPanel.getExercise().showNewGraph( this.drawPanel.getExercise().getDatei() );
        } else {
            this.drawPanel.getExercise()
                          .showNewGraph( this.drawPanel.getExercise().getUndo().getCurrentUndoClassList() );
        }
    }

    public void setUpHelperFrame() {
        this.add( this.mainPanel );
        // automatically determine minimum window size
        this.pack();
        // relocate window to the middle of the frame
        this.setLocationRelativeTo( null );
        this.setResizable( false );
    }
}
