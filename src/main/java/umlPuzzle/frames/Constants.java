package umlPuzzle.frames;

import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;

import java.util.HashMap;

final class Constants {

    public static HashMap<Integer, String[]> ELEMENT_ID_TO_QUESTION_GIF_MAP = fillHashMap();

    public static final String RELATION_LINK = "%s zu %s";
    public static final String CLASS_MEMBER_DELETE_DESCRIPTION = "%s von %s";
    public static final String EMPTY_COMBO_BOX_HINT = "nicht vorhanden";
    public static final String RELATION_GIF_FILE_PAH = "grafiken/icons/%s";
    public static final String PATTERN_PARK_GIF_PATH = "data/grafiken/park.gif";

    public static final int DELETE_DIALOG_COLUMN_COUNT = 4;
    public static final int ARRAY_COUNT_RELATION = 3;
    public static final int ARRAY_COUNT_MEMBER = 2;
    public static final int QUESTION1_ARRAY_INDEX = 0;
    public static final int QUESTION2_ARRAY_INDEX = 1;
    public static final int IMAGE_ARRAY_INDEX = 2;
    public static final int DEFAULT_TEXTAREA_ROWS = 18;
    public static final int DEFAULT_TEXTAREA_COLUMNS = 20;

    private static final String ASSOCIATION_FILE = "association_02.gif";
    private static final String INHERITANCE_FILE = "inheritance.gif";
    private static final String AGGREGATION_FILE = "aggregation_02.gif";
    private static final String COMPOSITION_FILE = "composition_02.gif";

    private Constants() {
    }

    private static HashMap<Integer, String[]> fillHashMap() {
        HashMap<Integer, String[]> map = new HashMap<>();
        Text text = new Text();
        map.put( ExerciseConstants.ASSOCIATION, new String[] {
                text.getInsertAssociationFirst(), text.getInsertAssociationSecond(), String.format(
                RELATION_GIF_FILE_PAH,
                ASSOCIATION_FILE
        )
        } );
        map.put( ExerciseConstants.INHERITANCE, new String[] {
                text.getInsertInheritanceFirst(), text.getinsertInheritanceSecond(), String.format(
                RELATION_GIF_FILE_PAH,
                INHERITANCE_FILE
        )
        } );
        map.put( ExerciseConstants.AGGREGATION, new String[] {
                text.getInsertAggregationFirst(), text.getinsertAggregationSecond(), String.format(
                RELATION_GIF_FILE_PAH,
                AGGREGATION_FILE
        )
        } );
        map.put( ExerciseConstants.COMPOSITION, new String[] {
                text.getInsertcompositionFirst(), text.getInsertCompositionSecond(), String.format(
                RELATION_GIF_FILE_PAH,
                COMPOSITION_FILE
        )
        } );
        map.put( ExerciseConstants.ATTRIBUTE, new String[] {
                text.getInsertAttributeFirst(), text.getInsertAttributeSecond()
        } );
        map.put( ExerciseConstants.OPERATION, new String[] {
                text.getInsertOperationFirst(), text.getInsertOperationSecond()
        } );

        return map;
    }
}
