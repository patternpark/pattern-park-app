package umlPuzzle.frames;

// hier sind alle Text nach XML ausgegliedert worden!!!

import org.jdom.Element;
import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;
import umlPuzzle.enums.ERelationType;
import umlPuzzle.enums.EXMLElementType;
import umlPuzzle.panels.DrawPanel;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Frame for deletion of items, shows the required information of the items to be deleted and allows the user
 * to make the necessary selections.
 *
 * @author Lars Friedrich
 * @version 1.2 (28.11.06)
 */
public class DeleteFrame extends Frame {

    private final Text _text;
    private final ButtonGroup _check;

    private JRadioButton _deleteClassRadioButton;
    private JRadioButton _deleteInheritanceRadioButton;
    private JRadioButton _deleteAssociationRadioButton;
    private JRadioButton _deleteAggregationRadioButton;
    private JRadioButton _deleteCompositionRadioButton;
    private JRadioButton _deleteAttributeRadioButton;
    private JRadioButton _deleteOperationRadioButton;
    private JComboBox<String> _comboBox;

    private JButton _okButton;

    /**
     * Konstrultor
     *
     * @author Lars Friedrich
     */
    public DeleteFrame( DrawPanel draw ) {
        super( draw );
        this._text = new Text();
        this._check = new ButtonGroup();
        this.initFrame();
        this.initDeleteFrame();
    }

    /**
     * Initalisierung der Elemente
     *
     * @author Lars Friedrich
     */
    public void initDeleteFrame() {
        // Initialisation of elements
        this.initRadioButtons();
        this._comboBox = new JComboBox<>();
        this._okButton = new JButton();
        this._comboBox.setBackground( ExerciseConstants.WHITE );
        this._okButton.setText( this._text.getOkButton() );
        this._okButton.setPreferredSize( GUISets.BUTTON_DIMENSION );
        JLabel question1 = new JLabel();
        JLabel question2 = new JLabel();
        JButton cancelButton = new JButton();

        question1.setBackground( ExerciseConstants.WHITE );
        question2.setBackground( ExerciseConstants.WHITE );

        // add Radiobuttons to Button-Group
        this._check.add( this._deleteClassRadioButton );
        this._check.add( this._deleteInheritanceRadioButton );
        this._check.add( this._deleteAssociationRadioButton );
        this._check.add( this._deleteAggregationRadioButton );
        this._check.add( this._deleteCompositionRadioButton );
        this._check.add( this._deleteAttributeRadioButton );
        this._check.add( this._deleteOperationRadioButton );

        question1.setText( this._text.getQuestion1() );
        question2.setText( this._text.getQuestion2() );

        cancelButton.setText( this._text.getCancelButton() );

        // Determine size of the buttons
        cancelButton.setPreferredSize( GUISets.BUTTON_DIMENSION );

        KeyListener key = this.createKeyListener();

        question1.addKeyListener( key );
        question2.addKeyListener( key );
        List<AbstractButton> buttons = Collections.list( this._check.getElements() );
        for ( AbstractButton button : buttons ) {
            button.addKeyListener( key );
        }
        this._comboBox.addKeyListener( key );
        this._okButton.addKeyListener( key );
        cancelButton.addKeyListener( key );

        this.addWindowListener( new CloseWindowListener() );

        //ActionListener
        this._deleteClassRadioButton.addActionListener( e -> {
            showAllClasses();
            checkComboBox();
        } );
        this._deleteInheritanceRadioButton.addActionListener( e -> {
            showAllOfRelation( ERelationType.INHERITANCE );
            checkComboBox();
        } );
        this._deleteAssociationRadioButton.addActionListener( e -> {
            showAllOfRelation( ERelationType.ASSOCIATION );
            checkComboBox();
        } );
        this._deleteAggregationRadioButton.addActionListener( e -> {
            showAllOfRelation( ERelationType.AGGREGATION );
            checkComboBox();
        } );
        this._deleteCompositionRadioButton.addActionListener( e -> {
            showAllOfRelation( ERelationType.COMPOSITION );
            checkComboBox();
        } );
        this._deleteAttributeRadioButton.addActionListener( e -> {
            showMembers( EXMLElementType.ATTRIBUTE );
            checkComboBox();
        } );
        this._deleteOperationRadioButton.addActionListener( e -> {
            showMembers( EXMLElementType.METHOD );
            checkComboBox();
        } );
        this._okButton.addActionListener( e -> okAction() );
        cancelButton.addActionListener( e -> exit() );

        // alle Elemente dem Frame hinzuf�gen
        this.addRadioButtons();
        Utility.addComponent( this.mainPanel, question1, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 3 );
        Utility.addComponent( this.mainPanel, question2, GUISets.FIRST_CELL, GUISets.FIFTH_ROW, 3 );
        Utility.addComponent( this.mainPanel, this._comboBox, GUISets.FIRST_CELL, GUISets.SIXTH_ROW, 3 );
        Utility.addComponent( this.mainPanel, this._okButton, GUISets.FIRST_CELL, GUISets.SEVENTH_ROW, 1 );
        Utility.addComponent( this.mainPanel, cancelButton, GUISets.SECOND_CELL, GUISets.SEVENTH_ROW, 1 );

        this.setUpHelperFrame();

        this.showAllClasses();
        this.checkComboBox();
    }


    /**
     * diese Methode zeigt alle Klassen an
     *
     * @author Lars Friedrich
     */
    public void showAllClasses() {
        List<Element> classList = this.updateClassList();
        for ( Element current : classList ) {
            this._comboBox.addItem( current.getChildText( EXMLElementType.NAME.getXMLLabel() ) );
        }
        this.repaint();
    }

    public void showMembers( EXMLElementType elementType ) {
        List<Element> classList = this.updateClassList();
        for ( Element umlClass : classList ) {
            for ( Element member : (List<Element>) umlClass.getChildren( elementType.getXMLLabel() ) ) {
                this._comboBox.addItem(
                        String.format(
                                Constants.CLASS_MEMBER_DELETE_DESCRIPTION,
                                member.getChildText( EXMLElementType.NAME.getXMLLabel() ),
                                umlClass.getChildText( EXMLElementType.NAME.getXMLLabel() )
                        )
                );
            }

        }
        this.repaint();
    }

    public void showAllOfRelation( ERelationType relationType ) {
        List<Element> classList = this.updateClassList();

        for ( Element umlClass : classList ) {
            for ( Element relation : (List<Element>) umlClass.getChildren( relationType.getXMLLabel() ) ) {
                this._comboBox.addItem(
                        String.format(
                                Constants.RELATION_LINK,
                                umlClass.getChildText( EXMLElementType.NAME.getXMLLabel() ),
                                relation.getText()
                        )
                );
            }
        }
        this.repaint();
    }

    /**
     * Checks, whether the combobox is empty.
     */
    public void checkComboBox() {
        if ( this._comboBox.getItemCount() == 0 ) {
            this._comboBox.addItem( Constants.EMPTY_COMBO_BOX_HINT );
            this._comboBox.setEnabled( false );
            this._okButton.setEnabled( false );
        } else {
            this._comboBox.setEnabled( true );
            this._okButton.setEnabled( true );
        }
    }

    /**
     * Handles the confirmation-action by the user
     */
    @Override
    public void okAction() {
        int choose = this.drawPanel.getExercise().getUndo().getActualPosition();
        List<Element> classList = this.drawPanel.getExercise().getClassList();
        String selectedItem = (String) this._comboBox.getSelectedItem();

        if ( this._deleteClassRadioButton.isSelected() ) {
            Optional<Element> classToDelete = classList.stream().filter(
                    ( umlClass ) -> umlClass.getChildText( EXMLElementType.NAME.getXMLLabel() )
                                            .equals( selectedItem )
            ).findFirst();

            if ( !classToDelete.isPresent() ) {
                return;
            }

            classList.remove( classToDelete.get() );

            for ( Element currentClass : classList ) {
                for ( ERelationType relationType : ERelationType.values() ) {
                    List<Element> relationsOfType = (List<Element>) currentClass.getChildren(
                            relationType.getXMLLabel() );
                    relationsOfType.removeIf( relation -> relation.getText().equals( selectedItem ) );
                }
            }
        } else if ( this._deleteAttributeRadioButton.isSelected() ) {
            this.deleteMemberOfClass( EXMLElementType.ATTRIBUTE, selectedItem, classList );
        } else if ( this._deleteOperationRadioButton.isSelected() ) {
            this.deleteMemberOfClass( EXMLElementType.METHOD, selectedItem, classList );
        } else if ( this._deleteCompositionRadioButton.isSelected() ) {
            this.deleteRelationOfClass( ERelationType.COMPOSITION, selectedItem, classList );
        } else if ( this._deleteAssociationRadioButton.isSelected() ) {
            this.deleteRelationOfClass( ERelationType.ASSOCIATION, selectedItem, classList );
        } else if ( this._deleteInheritanceRadioButton.isSelected() ) {
            this.deleteRelationOfClass( ERelationType.INHERITANCE, selectedItem, classList );
        } else if ( this._deleteAggregationRadioButton.isSelected() ) {
            this.deleteRelationOfClass( ERelationType.AGGREGATION, selectedItem, classList );
        }
        this.handleNewStep( choose, classList );
        this.exit();
    }

    private void initRadioButtons() {
        this._deleteClassRadioButton = new JRadioButton( this._text.getDeleteClass(), true );
        this._deleteInheritanceRadioButton = new JRadioButton( this._text.getDeleteInheritance() );
        this._deleteAssociationRadioButton = new JRadioButton( this._text.getDeleteAssociation() );
        this._deleteAggregationRadioButton = new JRadioButton( this._text.getDeleteAggregation() );
        this._deleteCompositionRadioButton = new JRadioButton( this._text.getDeleteComposition() );
        this._deleteAttributeRadioButton = new JRadioButton( this._text.getDeleteAttirbute() );
        this._deleteOperationRadioButton = new JRadioButton( this._text.getDeleteOperation() );

        this._deleteClassRadioButton.setBackground( ExerciseConstants.WHITE );
        this._deleteInheritanceRadioButton.setBackground( ExerciseConstants.WHITE );
        this._deleteAssociationRadioButton.setBackground( ExerciseConstants.WHITE );
        this._deleteAggregationRadioButton.setBackground( ExerciseConstants.WHITE );
        this._deleteCompositionRadioButton.setBackground( ExerciseConstants.WHITE );
        this._deleteAttributeRadioButton.setBackground( ExerciseConstants.WHITE );
        this._deleteOperationRadioButton.setBackground( ExerciseConstants.WHITE );

        this._deleteClassRadioButton.setToolTipText( this._text.getScK() );
        this._deleteInheritanceRadioButton.setToolTipText( this._text.getScV() );
        this._deleteAssociationRadioButton.setToolTipText( this._text.getScS() );
        this._deleteAggregationRadioButton.setToolTipText( this._text.getScG() );
        this._deleteCompositionRadioButton.setToolTipText( this._text.getScM() );
        this._deleteAttributeRadioButton.setToolTipText( this._text.getScA() );
        this._deleteOperationRadioButton.setToolTipText( this._text.getScO() );
    }

    private void addRadioButtons() {
        List<AbstractButton> buttons = Collections.list( this._check.getElements() );
        for ( int i = 0; i < buttons.size(); ++i ) {
            int row = i < Constants.DELETE_DIALOG_COLUMN_COUNT ? GUISets.SECOND_ROW : GUISets.THIRD_ROW;
            this.mainPanel.add( buttons.get( i ), new GridBagConstraints(
                    ( GUISets.FIRST_CELL + i ) % Constants.DELETE_DIALOG_COLUMN_COUNT, row,
                    1, 1,
                    0.0, 0.0,
                    GridBagConstraints.NORTHWEST,
                    GridBagConstraints.NONE,
                    GUISets.STANDARD_INSETS,
                    0, 0
            ) );
        }

    }

    private List<Element> updateClassList() {
        this._comboBox.removeAllItems();
        return this.drawPanel.getExercise().getClassList();
    }

    private void deleteMemberOfClass( EXMLElementType memberType, String selectedItem, List<Element> classList ) {
        Optional<Element> selectedClass = classList.stream().filter(
                umlClass -> selectedItem.endsWith( umlClass.getChildText( EXMLElementType.NAME.getXMLLabel() ) )
        ).findFirst();

        if ( !selectedClass.isPresent() ) {
            return;
        }

        List<Element> members = (List<Element>) selectedClass.get().getChildren( memberType.getXMLLabel() );
        Optional<Element> memberToDelete = members.stream().filter(
                member -> selectedItem.startsWith( member.getChildText( EXMLElementType.NAME.getXMLLabel() ) )
        ).findFirst();

        if ( !memberToDelete.isPresent() ) {
            return;
        }

        members.remove( memberToDelete.get() );
    }

    private void deleteRelationOfClass( ERelationType relationType, String selectedItem, List<Element> classList ) {
        Optional<Element> selectedClass = classList.stream().filter(
                umlClass -> selectedItem.startsWith( umlClass.getChildText( EXMLElementType.NAME.getXMLLabel() ) )
        ).findFirst();

        if ( !selectedClass.isPresent() ) {
            return;
        }

        List<Element> relations = (List<Element>) selectedClass.get().getChildren( relationType.getXMLLabel() );
        Optional<Element> relationToDelete = relations.stream().filter(
                relation -> selectedItem.endsWith( relation.getText() )
        ).findFirst();

        if ( !relationToDelete.isPresent() ) {
            return;
        }

        relations.remove( relationToDelete.get() );
    }

    private KeyListener createKeyListener() {
        return new KeyListener() {
            public void keyTyped( KeyEvent e ) {
            }

            public void keyPressed( KeyEvent keyEvent ) {
                if ( keyEvent.getKeyCode() == KeyEvent.VK_ENTER ) {
                    if ( _okButton.isEnabled() ) {
                        okAction();
                    }
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_ESCAPE ) {
                    exit();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_K ) {
                    _deleteClassRadioButton.setSelected( true );
                    showAllClasses();
                    checkComboBox();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_A ) {
                    _deleteAttributeRadioButton.setSelected( true );
                    showMembers( EXMLElementType.ATTRIBUTE );
                    checkComboBox();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_O ) {
                    _deleteOperationRadioButton.setSelected( true );
                    showMembers( EXMLElementType.METHOD );
                    checkComboBox();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_V ) {
                    _deleteInheritanceRadioButton.setSelected( true );
                    showAllOfRelation( ERelationType.INHERITANCE );
                    checkComboBox();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_S ) {
                    _deleteAssociationRadioButton.setSelected( true );
                    showAllOfRelation( ERelationType.ASSOCIATION );
                    checkComboBox();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_G ) {
                    _deleteAggregationRadioButton.setSelected( true );
                    showAllOfRelation( ERelationType.AGGREGATION );
                    checkComboBox();
                }
                if ( keyEvent.getKeyCode() == KeyEvent.VK_M ) {
                    _deleteCompositionRadioButton.setSelected( true );
                    showAllOfRelation( ERelationType.COMPOSITION );
                    checkComboBox();
                }
            }

            public void keyReleased( KeyEvent e ) {
            }
        };
    }
}