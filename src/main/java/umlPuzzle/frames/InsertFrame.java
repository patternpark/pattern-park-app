package umlPuzzle.frames;

import org.jdom.Element;
import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;
import umlPuzzle.enums.ERelationType;
import umlPuzzle.enums.EXMLElementType;
import umlPuzzle.panels.DrawPanel;
import umlPuzzle.utility.Utility;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Hier�ber k�nnen Beziehungen zum UML-Diagramm hinzugef�gt werden
 *
 * @author Lars Friedrich
 * @version 1.3 (29.11.06)
 */
public class InsertFrame extends Frame {

    private final JComboBox<String> _firstClass;
    private final JComboBox<String> _secondClass;
    private final JTextField _name;
    private final JTextField _returnType;
    private final int _change;
    private final Text _text;

    /**
     * Konstruktor
     *
     * @param change: Integer mit der Art von Beziehung
     * @param draw:   Integer mit der Art der �bung
     */
    public InsertFrame( int change, DrawPanel draw ) {
        super( draw );
        this._change = change;
        this._text = new Text();
        this._firstClass = new JComboBox<>();
        this._secondClass = new JComboBox<>();
        this._name = new JTextField();
        this._returnType = new JTextField();
        this.initFrame();
        this.initInsertFrame();
    }

    /**
     * Initialisierung der Elemente
     *
     * @author Lars Friedrich
     */
    public void initInsertFrame() {
        JLabel question1 = new JLabel();
        JLabel question2 = new JLabel();
        JLabel inClass = new JLabel();
        JButton okButton = new JButton();
        JButton cancelButton = new JButton();
        JButton image = new JButton();
        image.setEnabled( false );

        inClass.setBackground( ExerciseConstants.WHITE );
        this._firstClass.setBackground( ExerciseConstants.WHITE );
        this._secondClass.setBackground( ExerciseConstants.WHITE );
        question1.setBackground( ExerciseConstants.WHITE );
        question2.setBackground( ExerciseConstants.WHITE );

        inClass.setText( this._text.getInsertClass() );

        okButton.setText( this._text.getOkButton() );
        cancelButton.setText( this._text.getCancelButton() );

        this._name.setPreferredSize( GUISets.BUTTON_DIMENSION );
        this._returnType.setPreferredSize( GUISets.BUTTON_DIMENSION );

        // Gr��e der Button bestimmen
        okButton.setPreferredSize( GUISets.BUTTON_DIMENSION );
        cancelButton.setPreferredSize( GUISets.BUTTON_DIMENSION );

        KeyListener key = new KeyListener() {
            public void keyTyped( KeyEvent e ) {
                checkOkButton( okButton );
            }

            public void keyPressed( KeyEvent e ) {
                checkOkButton( okButton );
                if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                    if ( okButton.isEnabled() ) {
                        okAction();
                    }
                }
                if ( e.getKeyCode() == KeyEvent.VK_ESCAPE ) {
                    exit();
                }
            }

            public void keyReleased( KeyEvent e ) {
                checkOkButton( okButton );
            }
        };

        for ( JComboBox<String> comboBox : Arrays.asList( this._firstClass, this._secondClass ) ) {
            comboBox.addKeyListener( key );
        }
        for ( JTextField jTextField : Arrays.asList( this._name, this._returnType ) ) {
            jTextField.addKeyListener( key );
        }
        for ( JLabel jLabel : Arrays.asList( question1, question2 ) ) {
            jLabel.addKeyListener( key );
        }
        for ( JButton jButton : Arrays.asList( okButton, cancelButton ) ) {
            jButton.addKeyListener( key );
        }

        this.addWindowListener( new CloseWindowListener() );
        this._firstClass.addActionListener( e -> checkOkButton( okButton ) );
        this._secondClass.addActionListener( e -> checkOkButton( okButton ) );

        okButton.addActionListener( e -> okAction() );
        cancelButton.addActionListener( e -> exit() );

        // Assoziation einf�gen:
        this.setUpInsertButtonInteraction( question1, question2, image );

        // Elemente der ComboBoxen hinzuf�gen:
        List<Element> classList = this.drawPanel.getExercise().getClassList();
        for ( Element current : classList ) {
            for ( JComboBox<String> jComboBox : Arrays.asList( this._firstClass, this._secondClass ) ) {
                jComboBox.addItem( current.getChildText( EXMLElementType.NAME.getXMLLabel() ) );
            }
        }
        checkOkButton( okButton );
        // alle Elemente dem Frame hinzuf�gen:
        // ------------------------------------------
        // -------------- bei Vererbungen -----------
        // ------------------------------------------
        if ( this._change == ExerciseConstants.INHERITANCE ) {
            Utility.addComponent( this.mainPanel, this._firstClass, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 2 );
            Utility.addComponent( this.mainPanel, question1, GUISets.SECOND_CELL, GUISets.FIRST_ROW, 2 );
            Utility.addComponent( this.mainPanel, image, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 2 );
            Utility.addComponent( this.mainPanel, this._secondClass, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 2 );
            Utility.addComponent( this.mainPanel, question2, GUISets.SECOND_CELL, GUISets.THIRD_ROW, 2 );
            Utility.addComponent( this.mainPanel, okButton, GUISets.FIRST_CELL, GUISets.FOURTH_ROW, 1 );
            Utility.addComponent( this.mainPanel, cancelButton, GUISets.SECOND_CELL, GUISets.FOURTH_ROW, 1 );
        } else if ( this._change == ExerciseConstants.ASSOCIATION ) {
            Utility.addComponent( this.mainPanel, question2, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 2 );
            Utility.addComponent( this.mainPanel, this._firstClass, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 2 );
            Utility.addComponent( this.mainPanel, image, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 2 );
            Utility.addComponent( this.mainPanel, question1, GUISets.FIRST_CELL, GUISets.FOURTH_ROW, 2 );
            Utility.addComponent( this.mainPanel, this._secondClass, GUISets.FIRST_CELL, GUISets.FIFTH_ROW, 2 );
            Utility.addComponent( this.mainPanel, okButton, GUISets.FIRST_CELL, GUISets.SIXTH_ROW, 1 );
            Utility.addComponent( this.mainPanel, cancelButton, GUISets.SECOND_CELL, GUISets.SIXTH_ROW, 1 );
        } else {
            Utility.addComponent( this.mainPanel, question1, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 2 );
            if ( this._change == ExerciseConstants.ATTRIBUTE || this._change == ExerciseConstants.OPERATION ) {
                Utility.addComponent( this.mainPanel, this._name, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 2 );
                Utility.addComponent( this.mainPanel, this._returnType, GUISets.FIRST_CELL, GUISets.FIFTH_ROW, 2 );
                Utility.addComponent( this.mainPanel, inClass, GUISets.FIRST_CELL, GUISets.SIXTH_ROW, 2 );
                Utility.addComponent( this.mainPanel, this._secondClass, GUISets.SECOND_CELL, GUISets.SIXTH_ROW, 2 );
                Utility.addComponent( this.mainPanel, okButton, GUISets.FIRST_CELL, GUISets.SEVENTH_ROW, 1 );
                Utility.addComponent( this.mainPanel, cancelButton, GUISets.SECOND_CELL, GUISets.SEVENTH_ROW, 1 );
                okButton.setEnabled( false );
            } else {
                Utility.addComponent( this.mainPanel, this._firstClass, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 2 );
                Utility.addComponent( this.mainPanel, image, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 2 );
                Utility.addComponent( this.mainPanel, this._secondClass, GUISets.FIRST_CELL, GUISets.FIFTH_ROW, 2 );
                Utility.addComponent( this.mainPanel, okButton, GUISets.FIRST_CELL, GUISets.SIXTH_ROW, 1 );
                Utility.addComponent( this.mainPanel, cancelButton, GUISets.SECOND_CELL, GUISets.SIXTH_ROW, 1 );
            }
            Utility.addComponent( this.mainPanel, question2, GUISets.FIRST_CELL, GUISets.FOURTH_ROW, 2 );
        }
        this.setUpHelperFrame();
    }

    /**
     *
     */
    public void checkOkButton( JButton okButton ) {
        if ( this._firstClass.getItemCount() == 0 ) {
            for ( JComboBox<String> jComboBox : Arrays.asList( this._firstClass, this._secondClass ) ) {
                jComboBox.addItem( Constants.EMPTY_COMBO_BOX_HINT );
                jComboBox.setEnabled( false );
            }
        } else {
            this._firstClass.setEnabled( true );
            this._secondClass.setEnabled( true );
        }
        this.enableOkButton( okButton );
    }

    /**
     * Beendet das Fenster
     */
    @Override
    public void okAction() {
        int choose = this.drawPanel.getExercise().getUndo().getActualPosition();
        // Elemente der ComboBoxen hinzufügen:
        this.drawPanel.getExercise().setAddToUndo( false );//orig
        List<Element> classList = this.drawPanel.getExercise().getClassList();
        String temp;
        if ( this._change == ExerciseConstants.ATTRIBUTE || this._change == ExerciseConstants.OPERATION ) {
            temp = "";
        } else {
            temp = (String) this._firstClass.getSelectedItem();
        }
        for ( Element current : classList ) {
            if ( current.getChildText( EXMLElementType.NAME.getXMLLabel() )
                        .equals( this._secondClass.getSelectedItem() ) ) {
                if ( this._change == ExerciseConstants.AGGREGATION ) {
                    Element verTemp = new Element( ERelationType.AGGREGATION.getXMLLabel() );
                    verTemp.setText( temp );
                    current.addContent( verTemp );
                } else if ( this._change == ExerciseConstants.ASSOCIATION ) {
                    Element verTemp = new Element( ERelationType.ASSOCIATION.getXMLLabel() );
                    verTemp.setText( temp );
                    current.addContent( verTemp );
                } else if ( this._change == ExerciseConstants.INHERITANCE ) {
                    Element verTemp = new Element( ERelationType.INHERITANCE.getXMLLabel() );
                    verTemp.setText( temp );
                    current.addContent( verTemp );
                } else if ( this._change == ExerciseConstants.COMPOSITION ) {
                    Element verTemp = new Element( ERelationType.COMPOSITION.getXMLLabel() );
                    verTemp.setText( temp );
                    current.addContent( verTemp );
                } else {
                    String elementLabel = this._change == ExerciseConstants.ATTRIBUTE
                            ? EXMLElementType.ATTRIBUTE.getXMLLabel()
                            : EXMLElementType.METHOD.getXMLLabel();
                    Element tempMember = new Element( elementLabel );
                    Element tempName = new Element( EXMLElementType.NAME.getXMLLabel() );
                    Element tempDatatype = new Element( EXMLElementType.DATATYPE.getXMLLabel() );
                    tempName.setText( this._name.getText() );
                    tempDatatype.setText( this._returnType.getText() );
                    tempMember.addContent( tempDatatype );
                    tempMember.addContent( tempName );
                    current.addContent( tempMember );
                }
            }
        }

        this.handleNewStep( choose, classList );
        this.exit();
    }

    private void enableOkButton( JButton okButton ) {
        if ( this._change == ExerciseConstants.ATTRIBUTE || this._change == ExerciseConstants.OPERATION ) {
            okButton.setEnabled( _name.getText().length() > 0 && _returnType.getText().length() > 0 );
        } else {
            if (
                    this._change == ExerciseConstants.INHERITANCE ||
                            this._change == ExerciseConstants.AGGREGATION ||
                            this._change == ExerciseConstants.COMPOSITION
            ) {
                okButton.setEnabled( this._firstClass.getSelectedItem() != this._secondClass.getSelectedItem() );
            } else {
                okButton.setEnabled( true );
            }
        }
    }

    private void setUpInsertButtonInteraction( JLabel question1, JLabel question2, JButton image ) {
        String[] insertProps = Constants.ELEMENT_ID_TO_QUESTION_GIF_MAP.get( this._change );
        if ( insertProps.length == Constants.ARRAY_COUNT_RELATION ) {
            question1.setText( insertProps[ Constants.QUESTION1_ARRAY_INDEX ] );
            question2.setText( insertProps[ Constants.QUESTION2_ARRAY_INDEX ] );
            try {
                image.setIcon(
                        new ImageIcon(ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResource(insertProps[ Constants.IMAGE_ARRAY_INDEX ] ))))
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if ( insertProps.length == Constants.ARRAY_COUNT_MEMBER ) {
            question1.setText( insertProps[ Constants.QUESTION1_ARRAY_INDEX ] );
            question2.setText( insertProps[ Constants.QUESTION2_ARRAY_INDEX ] );
        }
    }
}