package umlPuzzle.frames;

import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;
import umlPuzzle.panels.DrawPanel;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Klasse f�r alle Informations-Fenster
 *
 * @author Lars Friedrich
 */
public class InfoFrame extends Frame {

    /**
     * Konstruktor
     */
    public InfoFrame( int id, DrawPanel draw ) {
        super( draw );
        this.initFrame();
        this.initInfoFrame( id );
    }

    /**
     * Initialisierung
     */
    public void initInfoFrame( int id ) {
        Text text = new Text();
        JButton okButton = new JButton();
        JButton cancelButton = new JButton();
        JLabel textLabel = new JLabel();
        JTextArea textArea = new JTextArea(Constants.DEFAULT_TEXTAREA_ROWS, Constants.DEFAULT_TEXTAREA_COLUMNS);
        textLabel.setBackground( ExerciseConstants.WHITE );

        KeyListener key = new KeyListener() {
            public void keyTyped( KeyEvent e ) {
            }

            public void keyPressed( KeyEvent e ) {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                    okAction();
                }
                if ( e.getKeyCode() == KeyEvent.VK_ESCAPE ) {
                    exit();
                }
            }

            public void keyReleased( KeyEvent e ) {
            }
        };

        this.addKeyListener( key );
        this.mainPanel.addKeyListener( key );
        textLabel.addKeyListener( key );
        textArea.addKeyListener( key );
        okButton.addKeyListener( key );
        cancelButton.addKeyListener( key );

        this.addWindowListener( new CloseWindowListener() );

        // Texte darstellen
        okButton.setText( text.getOkButton() );
        cancelButton.setText( text.getCancelButton() );
        if ( id == ExerciseConstants.WARNING_UNDO || id == ExerciseConstants.WARNING_REDO ) {
            textLabel.setText( id == ExerciseConstants.WARNING_REDO ? text.getWarningRedo() : text.getWarningUndo() );
            // alle Elemente dem Frame hinzuf�gen
            Utility.addComponent( this.mainPanel, okButton, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 1 );
        } else if ( id == ExerciseConstants.EVALUATE ) {
            textLabel.setText( text.getEval() );
            textArea.setSize( GUISets.INFO_PANEL_DIMENSION );

            JScrollPane scroll = new JScrollPane( textArea );
            scroll.setHorizontalScrollBarPolicy( ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER );
            scroll.setVerticalScrollBarPolicy( ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED );
            textArea.setWrapStyleWord( true );
            textArea.setLineWrap( true );
            textArea.setEditable( false );
            textArea.setText( this.drawPanel.getExercise().evaluateExercises() );
            textArea.setCaretPosition(0);

            // alle Elemente dem Frame hinzufügen
            Utility.addComponent( this.mainPanel, scroll, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 1 );
            Utility.addComponent( this.mainPanel, okButton, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 1 );
        }
        Utility.addComponent( this.mainPanel, textLabel, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 1 );

        // Action-Listener
        okButton.addActionListener( e -> okAction() );
        cancelButton.addActionListener( e -> exit() );
        this.setUpHelperFrame();
    }

    /**
     * Action beim Klick auf den OK-Button
     */
    @Override
    public void okAction() {
        exit();
    }
}