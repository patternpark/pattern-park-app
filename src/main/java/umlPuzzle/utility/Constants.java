package umlPuzzle.utility;

final class Constants {

    public static final String UNDO_STEP_ELEMENT = "back";
    public static final int DEFAULT_GRID_HEIGHT = 1;
    public static final int DEFAULT_PAD_X = 0;
    public static final int DEFAULT_PAD_Y = 0;
    public static final double DEFAULT_GRID_WEIGHT_X = 0.0;
    public static final double DEFAULT_GRID_WEIGHT_Y = 0.0;


    private Constants() {
    }
}
