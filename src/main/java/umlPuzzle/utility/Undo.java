package umlPuzzle.utility;

import org.jdom.Element;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.enums.ERelationType;
import umlPuzzle.enums.EXMLElementType;
import umlPuzzle.interfaces.UndoObserver;
import umlPuzzle.observables.UndoObservable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * In undo wird die Undo-Funktion verwirklicht
 *
 * @author Lars Friedrich
 **/
public class Undo extends UndoObservable {
    /**
     * Contains List of steps made by the user, to enable returning to or redoing certain steps.
     */
    private final ArrayList<Element> _undoList = new ArrayList<>();

    /**
     * Counter mit der Position, bei der Navigation in der XML-Datei
     */
    private int _actualPosition;

    /**
     * Überprüfung des Undos
     */
    public void addUndoStep( List<Element> classList ) {
        this.overwrite();
        this.addStep( classList );
        // Button-Panel neu zeichnen, um Undo-Button zu aktivieren
        this.fireUndoStepAddedEvent();
    }

    /**
     * Diese Methode fügt der entsprechenden XML-Datei einen Undo Schritt hinzu
     *
     * @author Lars Friedrich
     */
    public void addStep( List<Element> classList ) {
        /*
         * Liste mit allen Klassen.<br>
         * Die Liste schreibt in die XML-Datei undo.xml.
         */

        Element newUndoStep = new Element( Constants.UNDO_STEP_ELEMENT );
        this._undoList.add( 0, newUndoStep );

        // The list of umlClasses currently active in the exercise is copied into the undo-xml and class list
        for ( Element referencedElement : classList ) {
            // jetzt wird die Klasse eingetragen
            newUndoStep.addContent( this.cloneClass( referencedElement ) );
        }
    }

    /**
     * Reset für die XML-Datei, die beim Start aufgerufen wird
     */
    public void reset() {
        this._undoList.clear();
    }

    /**
     * zeigt den letzten Schritt an
     *
     * @author Lars Friedrich
     */
    public void showStep() {
        if ( this._undoList.size() - 1 <= this._actualPosition ) {
            this.fireInvalidStepEvent( ExerciseConstants.WARNING_UNDO );
            return;
        }

        ++this._actualPosition;
        this.fireUndoStepEvent();
    }

    /**
     * zeigt den letzten Schritt an
     *
     * @author Lars Friedrich
     */
    public void redoStep() {
        if ( this._actualPosition <= 0 ) {
            this.fireInvalidStepEvent( ExerciseConstants.WARNING_REDO );
            return;
        }

        --this._actualPosition;
        this.fireRedoStepEvent();
    }

    /**
     * @return the actualPosition
     */
    public int getActualPosition() {
        return this._actualPosition;
    }

    /**
     * @param actualPosition the actualPosition to set
     */
    public void setActualPosition( int actualPosition ) {
        this._actualPosition = actualPosition;
    }

    public List<Element> getCurrentUndoClassList() {
        return this.getClassListFromStep( this._actualPosition );
    }

    @Override
    protected void fireUndoStepAddedEvent() {
        for ( UndoObserver observer : this.observers ) {
            observer.reactToStepAdded();
        }
    }

    @Override
    protected void fireUndoStepEvent() {
        for ( UndoObserver observer : this.observers ) {
            observer.reactToUndo( this.getCurrentUndoClassList() );
        }
    }

    @Override
    protected void fireRedoStepEvent() {
        for ( UndoObserver observer : this.observers ) {
            observer.reactToRedo( this.getCurrentUndoClassList() );
        }
    }

    @Override
    protected void fireInvalidStepEvent( int typeOfWarning ) {
        for ( UndoObserver observer : this.observers ) {
            observer.reactToStepNotPossible( typeOfWarning );
        }
    }

    /**
     * Function to override undo steps
     */
    private void overwrite() {
        int pos = this._actualPosition;
        if ( pos != 0 ) {
            for ( int i = 0; i < pos; ++i ) {
                this._undoList.remove( 0 );
                --this._actualPosition;
            }
        }
    }

    private Element cloneClass( Element classToClone ) {
        // jetzt werden die Klassen eingetragen
        Element tempClass = new Element( EXMLElementType.UML_CLASS.getXMLLabel() );
        // jetzt werden die Klassennamen eingetragen
        tempClass.addContent( this.copyElementContent( EXMLElementType.NAME.getXMLLabel(), classToClone ) );
        // jetzt wird die x-Position eingetragen
        tempClass.addContent( this.copyElementContent( EXMLElementType.XCOORD.getXMLLabel(), classToClone ) );
        // jetzt wird die y-Position eingetragen
        tempClass.addContent( this.copyElementContent( EXMLElementType.YCOORD.getXMLLabel(), classToClone ) );
        // jetzt wird Abstract eingetragen
        tempClass.addContent( this.copyElementContent( EXMLElementType.ABSTRACT.getXMLLabel(), classToClone ) );
        // Now the interface status is added in
        tempClass.addContent( this.copyElementContent( EXMLElementType.INTERFACE.getXMLLabel(), classToClone ) );
        // Now the relations are added in
        for ( ERelationType relationType : ERelationType.values() ) {
            List<Element> relationElements = classToClone.getChildren( relationType.getXMLLabel() );

            for ( Element relation : relationElements ) {
                Element cloneRelation = new Element( relationType.getXMLLabel() );
                cloneRelation.setText( relation.getText() );
                tempClass.addContent( cloneRelation );
            }
        }
        // jetzt werden die Attribute eingetragen
        tempClass.addContent( this.createMethodOrAttribute( EXMLElementType.ATTRIBUTE, classToClone ) );
        // jetzt werden die Methoden eingetragen
        tempClass.addContent( this.createMethodOrAttribute( EXMLElementType.METHOD, classToClone ) );

        return tempClass;
    }

    private List<Element> createMethodOrAttribute( EXMLElementType elementType, Element referencedElement ) {
        List<Element> elementList = new ArrayList<>();
        List<Element> members = referencedElement.getChildren( elementType.getXMLLabel() );

        for ( Element memberElement : members ) {
            Element tempNewMember = new Element( elementType.getXMLLabel() );
            Element tempDatatypeElement = this.copyElementContent( EXMLElementType.DATATYPE.getXMLLabel(), memberElement );
            Element tempNameElement = this.copyElementContent( EXMLElementType.NAME.getXMLLabel(), memberElement );

            if ( !Objects.equals( tempDatatypeElement.getText(), "" ) || !Objects.equals( tempNameElement.getText(), "" ) ) {
                tempNewMember.addContent( tempDatatypeElement );
                tempNewMember.addContent( tempNameElement );
                elementList.add( tempNewMember );
            }
        }

        return elementList;
    }

    private Element copyElementContent( final String elementName, final Element referenceElement ) {
        Element cloneElement = new Element( elementName );
        cloneElement.setText( referenceElement.getChildText( elementName ) );
        return cloneElement;
    }

    private List<Element> getClassListFromStep( int step ) {
        if ( step < 0 || step > this._undoList.size() - 1 ) {
            step = 0;
        }
        Element backElement = this._undoList.get( step );
        ArrayList<Element> classList = new ArrayList<>();

        for ( Element umlClass : (List<Element>) backElement.getChildren( EXMLElementType.UML_CLASS.getXMLLabel() ) ) {
            classList.add( this.cloneClass( umlClass ) );
        }

        return classList;
    }
}