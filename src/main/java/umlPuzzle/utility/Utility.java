package umlPuzzle.utility;

import settings.GUISets;

import javax.swing.JPanel;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Utility {

    public static void addComponent( JPanel panel, Component component, int cell, int row, int gridWidth ) {
        addComponent( panel, component, cell, row, gridWidth, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE );
    }

    public static void addComponent( JPanel panel, Component component, int cell, int row, int gridWidth, Insets insets ) {
        addComponent(
                panel, component, cell, row, gridWidth, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets
        );
    }

    public static void addComponent(
            JPanel panel, Component component, int cell, int row, int gridWidth, int anchor, int fill
    ) {
        addComponent( panel, component, cell, row, gridWidth, Constants.DEFAULT_GRID_HEIGHT, anchor, fill );
    }

    public static void addComponent(
            JPanel panel, Component component, int cell, int row, int gridWidth, int anchor, int fill, Insets insets
    ) {
        addComponent(
                panel, component, cell, row, gridWidth, Constants.DEFAULT_GRID_HEIGHT, Constants.DEFAULT_GRID_WEIGHT_X,
                Constants.DEFAULT_GRID_WEIGHT_Y, anchor, fill, insets
        );
    }

    public static void addComponent(
            JPanel panel, Component component, int cell, int row, int gridWidth, int gridHeight, int anchor, int fill
    ) {
        addComponent(
                panel, component, cell, row, gridWidth, gridHeight, Constants.DEFAULT_GRID_WEIGHT_X,
                Constants.DEFAULT_GRID_WEIGHT_Y, anchor, fill, GUISets.STANDARD_INSETS
        );
    }

    public static void addComponent(
            JPanel panel, Component component, int cell, int row, int gridWidth, int gridHeight, double weightX,
            double weightY, int anchor, int fill, Insets inset
    ) {
        panel.add(
                component, new GridBagConstraints(
                        cell, row,
                        gridWidth, gridHeight,
                        weightX, weightY,
                        anchor, fill, inset,
                        Constants.DEFAULT_PAD_X, Constants.DEFAULT_PAD_Y
                )
        );
    }
}
