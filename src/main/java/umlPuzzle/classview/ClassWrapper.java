package umlPuzzle.classview;


import javax.swing.tree.DefaultMutableTreeNode;

/**
 * A very basic wrapper storing a reference to a custom user object with a
 * label.
 *
 * @author rvalyi
 */
public class ClassWrapper {


    public Object[][] methodData, attributeData;

    public boolean isAbstract;

    public boolean isInterface;

    public FlyweightUIClass uiPanel = new FlyweightUIClass( null );

    private String label;

    /**
     * The wrapper where you put your real business object.
     * (An other solution is that your business object
     * implements a toString method, and you deal with it
     * in the editor)
     */
    private DefaultMutableTreeNode value;

    public String getLabel() {
        return label;
    }

    public void setLabel( String stringValue ) {
        this.label = stringValue;
    }

    public void setValue( DefaultMutableTreeNode value ) {
        this.value = value;
    }

    /**
     * Used by JGraph to render the cell label
     */
    public String toString() {
        return label;
    }
}
