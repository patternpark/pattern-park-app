package umlPuzzle.classview;
/*
 * Copyright (c) 2005, David Benson
 *
 */

import org.jgraph.graph.DefaultGraphCell;

/**
 * Graph model cell that has a number of fixed offset ports on the left
 * and right hand sides. It provides a number of utility methods to add
 * and remove those ports.
 */
public class ClassCell extends DefaultGraphCell {

    /**
     * Wrapper
     */
    public ClassWrapper myWrapper;

    /**
     * Constructor with user object to set for cell. Whatever the user object
     * returns with toString will appear in the vertex label.
     *
     * @param userObject the userObject to set
     */
    public ClassCell( Object userObject ) {
        super( userObject );
    }

    public void setMyWrapper( ClassWrapper myWrapper ) {
        this.myWrapper = myWrapper;
    }

}