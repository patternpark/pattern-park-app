package umlPuzzle.classview;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCellEditor;
import org.jgraph.graph.GraphCellEditor;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;


/**
 * basic in-place editor for custom business objects
 */
public class ClassEditor extends DefaultGraphCellEditor {

    /**
     * Returns a new RealCellEditor.
     */
    protected GraphCellEditor createGraphCellEditor() {
        return new RealCellEditor();
    }

    /**
     * Utility editor for rich text values.
     */
    static class RealCellEditor extends AbstractCellEditor implements GraphCellEditor {

        /**
         * Holds the component used for editing.
         */
        FlyweightUIClass editorComponent = new FlyweightUIClass( this );

        public Component getGraphCellEditorComponent(
                JGraph graph, Object value, boolean isSelected
        ) {
            Object cell = value;
            value = graph.getModel().getValue( value );
            if ( value instanceof ClassWrapper )
                editorComponent.installAttributes(
                        value, graph.getGraphLayoutCache().getMapping( cell, false ), true, graph );
            else {
                ClassWrapper wrapper = new ClassWrapper();

                if ( value instanceof DefaultMutableTreeNode ) wrapper.setValue( (DefaultMutableTreeNode) value );

                wrapper.setLabel( value.toString() );
                editorComponent.installAttributes(
                        wrapper, graph.getGraphLayoutCache().getMapping( cell, false ), true, graph );
            }
            Border aBorder = UIManager.getBorder( Constants.UIMANAGER_TREE_BORDER );
            editorComponent.setBorder( aBorder );
            return editorComponent;
        }

        /**
         * Returns the rich text value to be stored in the user object.
         */
        public ClassWrapper getCellEditorValue() {
            return editorComponent.getValue();
        }
    }
}

