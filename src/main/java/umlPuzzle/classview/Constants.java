package umlPuzzle.classview;

final class Constants {

    public static final String UIMANAGER_TREE_BORDER = "Tree.editorBorder";
    public static final String COLUMN_NAMES_COLUMN1 = "Datentyp";
    public static final String COLUMN_NAMES_COLUMN2 = "Bezeichner";
    public static final String CLASS_CELL_PLACEHOLDER_LABEL = "placeholder";
    public static final String OK_LABEL = "ok";

    public static final int MINIMUM_SCROLL_PANE_WIDTH = 150;
    public static final int CLASS_CELL_DEFAULT_COLUMN_AMOUNT = 10;
    public static final int TABLE_FIRST_COLUMN_DEFAULT_WIDTH = 15;
    public static final int ATTRIBUTE_PANE_VISIBLE_BORDER_WIDTH = 1;
    public static final int ATTRIBUTE_PANE_INVISIBLE_BORDER_WIDTH = 0;
    public static final int GRIDBAG_GRID_X_Y = 0;
    public static final int DIMENSION_WIDTH = 30;
    public static final int DIMENSION_HEIGHT = 20;
    public static final int DIMENSION_MAGIC_2 = 2;
    public static final int DIMENSION_HEIGHT_MULTIPLIER = 3;
    public static final int POINT_0 = 0;
    public static final int POINT_100 = 100;
    public static final int POINT_500 = 500;
    public static final int GRID_WIDTH = 1;
    public static final int GRID_HEIGHT = 1;
    public static final int INITIAL_IPAD_X = 0;
    public static final int IPAD_Y = 45;

    public static final double GRIDBAG_WEIGHT_X_Y = 1.0;
    public static final double GRIDBAG_WEIGHT_Y = 0.0;

    private Constants() {
    }
}
