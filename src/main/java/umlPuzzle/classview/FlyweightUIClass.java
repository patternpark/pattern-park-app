package umlPuzzle.classview;

import org.jgraph.JGraph;
import org.jgraph.graph.CellView;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphModel;
import settings.GUISets;
import umlPuzzle.classview.utility.DefaultTableModel;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.enums.EXMLElementType;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Point2D;


//TODO: Die Klassen m�ssen noch ordentlich gestylt werden!


public class FlyweightUIClass extends JPanel implements ActionListener,
        ItemListener, TableModelListener {

    public static boolean editorInsideCell = true;   //editor hier einschalten

    /**
     * a reference to the editor: here we only use it to force the end of the
     * editing after the OK button or Cancel button is pressed
     */
    private static CellEditor cellEditor;

    private final String[] _columnNames = { Constants.COLUMN_NAMES_COLUMN1, Constants.COLUMN_NAMES_COLUMN2 };
    private final JTable _jTableAttributes = new JTable( new DefaultTableModel( this._columnNames, this._rowDataAtt ) );
    private final JTable _jTableMethods = new JTable( new DefaultTableModel( this._columnNames, this._rowData ) );
    private final JTextField _classTextField = new JTextField();
    //checkboxes
    private final JCheckBox _abstractCheckbox = new JCheckBox( EXMLElementType.ABSTRACT.getXMLLabel() );
    private final JCheckBox _interfaceCheckbox = new JCheckBox( EXMLElementType.INTERFACE.getXMLLabel() );
    private final Object[][] _rowData = { { "", "" } }, _rowDataAtt = { { "", "" } };

    private JScrollPane _jAttributePane;
    private int _counter;

    /**
     * A temporary clone of the business object to work with before commiting
     * the change (allows to undo).
     */
    private ClassWrapper _newModel;

    /**
     * the old user object
     */
    private ClassWrapper _oldModel;


    public FlyweightUIClass( CellEditor editor ) {

        try {
            this.setLayout( new GridBagLayout() );
            this.setBackground( ExerciseConstants.WHITE );
            this.setBorder( BorderFactory.createLineBorder( ExerciseConstants.BLACK ) );

            cellEditor = editor;

            //colors
            JPanel classLabelPanel = new JPanel();
            classLabelPanel.setBackground( ExerciseConstants.WHITE );
            JPanel typeLabelPanel = new JPanel();
            typeLabelPanel.setBackground( ExerciseConstants.WHITE );
            this._abstractCheckbox.setBackground( ExerciseConstants.WHITE );
            this._interfaceCheckbox.setBackground( ExerciseConstants.WHITE );
            this._jTableMethods.setBackground( ExerciseConstants.WHITE );
            this._jTableAttributes.setBackground( ExerciseConstants.WHITE );

            //width of classTextField
            this._classTextField.setColumns( Constants.CLASS_CELL_DEFAULT_COLUMN_AMOUNT );
            //functionality of classTextField
            classLabelPanel.add( this._classTextField );
            this._classTextField.setText( Constants.CLASS_CELL_PLACEHOLDER_LABEL );
            this._classTextField.addActionListener( this );
            this._classTextField.setActionCommand( Constants.OK_LABEL );

            //add className-panel to layout
            Utility.addComponent(
                    this, classLabelPanel, GUISets.FIRST_CELL, GUISets.FIRST_ROW, Constants.GRID_WIDTH,
                    Constants.GRID_HEIGHT, Constants.GRIDBAG_WEIGHT_X_Y, Constants.GRIDBAG_WEIGHT_Y,//1,0.05
                    GridBagConstraints.PAGE_START,
                    GridBagConstraints.HORIZONTAL,
                    GUISets.DEFAULT_INSETS
            );

            // adds checkbox to panel
            typeLabelPanel.add( this._abstractCheckbox );
            typeLabelPanel.add( this._interfaceCheckbox );

            //add checkbox-panel to layout
            Utility.addComponent(
                    this, typeLabelPanel, GUISets.FIRST_CELL, GUISets.SECOND_ROW, Constants.GRID_WIDTH,
                    Constants.GRID_HEIGHT, Constants.GRIDBAG_WEIGHT_X_Y, Constants.GRIDBAG_WEIGHT_Y,
                    GridBagConstraints.PAGE_START, GridBagConstraints.HORIZONTAL,
                    GUISets.DEFAULT_INSETS
            );


            //set width, color of background and grid-borders for attributes and operations
            this.setUpTable( this._jTableAttributes );
            this.setUpTable( this._jTableMethods );

            this._jAttributePane = new JScrollPane( _jTableAttributes );
            this._jAttributePane.setBackground( java.awt.Color.white );
            this._jAttributePane.setForeground( java.awt.Color.white );
            this._jAttributePane.setBorder(
                    BorderFactory.createMatteBorder(
                            Constants.ATTRIBUTE_PANE_VISIBLE_BORDER_WIDTH,
                            Constants.ATTRIBUTE_PANE_INVISIBLE_BORDER_WIDTH,
                            Constants.ATTRIBUTE_PANE_VISIBLE_BORDER_WIDTH,
                            Constants.ATTRIBUTE_PANE_INVISIBLE_BORDER_WIDTH,
                            java.awt.Color.black
                    ) );

            //add attributes to layout
            Utility.addComponent(
                    this, this._jAttributePane, GUISets.FIRST_CELL, GUISets.THIRD_ROW,
                    Constants.GRID_WIDTH, Constants.GRID_HEIGHT,
                    Constants.GRIDBAG_WEIGHT_X_Y, Constants.GRIDBAG_WEIGHT_Y,
                    GridBagConstraints.PAGE_START,
                    GridBagConstraints.BOTH,
                    GUISets.DIAG_INSETS
            );

            JScrollPane jMethodPane = new JScrollPane( _jTableMethods );
            jMethodPane.setBackground( java.awt.Color.white );
            jMethodPane.setForeground( java.awt.Color.white );
            jMethodPane.setBorder( BorderFactory.createEmptyBorder() );
            jMethodPane.setMinimumSize(
                    new Dimension(
                            Constants.MINIMUM_SCROLL_PANE_WIDTH,
                            ( Constants.DIMENSION_HEIGHT_MULTIPLIER * _jTableMethods.getRowHeight() )
                    ) );

            //add operations to layout
            this.add( jMethodPane, new GridBagConstraints(
                    GUISets.FIRST_CELL, GUISets.FOURTH_ROW,
                    Constants.GRID_WIDTH, Constants.GRID_HEIGHT,      //  1,1
                    Constants.GRIDBAG_WEIGHT_X_Y, Constants.GRIDBAG_WEIGHT_X_Y,                            //0.0, 0.5
                    GridBagConstraints.PAGE_START,
                    GridBagConstraints.HORIZONTAL,
                    GUISets.DIAG_INSETS,
                    Constants.INITIAL_IPAD_X, Constants.IPAD_Y
            ) );


            this._abstractCheckbox.addItemListener( itemEvent -> {
                this._oldModel.isAbstract = this._abstractCheckbox.isSelected();
                if ( this._oldModel.isAbstract ) {
                    this._interfaceCheckbox.setSelected( false );
                    this._oldModel.isInterface = false;
                }
                cellEditor.stopCellEditing();
            } );
            this._interfaceCheckbox.addItemListener( itemEvent -> {
                this._oldModel.isInterface = this._interfaceCheckbox.isSelected();
                if ( this._oldModel.isInterface ) {
                    this._abstractCheckbox.setSelected( false );
                    this._oldModel.isAbstract = false;
                }
                cellEditor.stopCellEditing();
            } );

            this._jTableAttributes.getModel().addTableModelListener( this );
            this._jTableMethods.getModel().addTableModelListener( this );

        } catch ( NullPointerException ignored ) {
        }
    }

    public void paint( Graphics g ) {
        try {
            super.paint( g );
            super.setBackground( ExerciseConstants.WHITE );
        } catch ( NullPointerException ignored ) {
        }
    }

    /**
     * the editor is lightweight so you need to install the graph cell
     * properties before using it.
     *
     * @param value placeholder desc
     */
    private void installValue( ClassWrapper value ) {
        try {
            ( (DefaultTableModel) this._jTableMethods.getModel() ).setRowData( value.methodData );
            ( (DefaultTableModel) this._jTableAttributes.getModel() ).setRowData( value.attributeData );

            this._jAttributePane.setMinimumSize(
                    new Dimension(
                            Constants.MINIMUM_SCROLL_PANE_WIDTH,
                            ( value.attributeData.length * this._jTableAttributes.getRowHeight() ) +
                                    Constants.DIMENSION_MAGIC_2
                    ) );

            this._oldModel = value;
            this._newModel = value;

            this._classTextField.setText( this._newModel.getLabel() );
            this._classTextField.setEnabled( true );

            this._abstractCheckbox.setSelected( this._newModel.isAbstract );
            this._interfaceCheckbox.setSelected( this._newModel.isInterface );

            this._jTableAttributes.setEnabled( true );
            this._jTableMethods.setEnabled( true );
        } catch ( NullPointerException ignored ) {
        }
    }


    protected void installAttributes(
            Object value, CellView view,
            boolean isEditing, JGraph graph
    ) {

        try {
            if ( editorInsideCell && view.getBounds() != null ) {
                setPreferredSize( new Dimension(
                        (int) view.getBounds().getWidth(),
                        (int) view.getBounds().getHeight()
                ) );
            }

            if ( value instanceof ClassWrapper ) {
                installValue( (ClassWrapper) value );
            } else {
                ClassWrapper wrapper = new ClassWrapper();
                if ( value instanceof DefaultMutableTreeNode ) {
                    wrapper.setValue( (DefaultMutableTreeNode) value );
                }
                wrapper.setLabel( value.toString() );
                installValue( wrapper );
            }

            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.weighty = Constants.GRIDBAG_WEIGHT_X_Y;
            gridBagConstraints.weightx = Constants.GRIDBAG_WEIGHT_X_Y;
            gridBagConstraints.gridx = Constants.GRIDBAG_GRID_X_Y;
            gridBagConstraints.gridy = Constants.GRIDBAG_GRID_X_Y;

            GraphModel model = graph.getModel();
            int childCount = model.getChildCount( view.getCell() );
            for ( int i = 0; i < childCount; i++ ) {
                Object child = model.getChild( view.getCell(), i );
                if ( model.isPort( child ) ) {
                    CellView portView = graph.getGraphLayoutCache().getMapping(
                            child, false );
                    if ( portView != null ) {
                        Point2D point = GraphConstants.getOffset( portView.getAllAttributes() );
                        String label = (String) model.getValue( portView.getCell() );
                        JComponent portComponent;
                        if ( isEditing ) {
                            portComponent = new JTextField( label );
                        } else {
                            portComponent = new JLabel( label );
                        }
                        portComponent.setPreferredSize(
                                new Dimension( Constants.DIMENSION_WIDTH, Constants.DIMENSION_HEIGHT ) );
                        if ( point.getX() == Constants.POINT_0 ) {//left port
                            if ( point.getY() == Constants.POINT_100 ) {
                                gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
                            } else if ( point.getY() == Constants.POINT_500 ) {
                                gridBagConstraints.anchor = GridBagConstraints.LINE_START;
                            } else {
                                gridBagConstraints.anchor = GridBagConstraints.LAST_LINE_START;
                            }
                        } else if ( point.getX() == GraphConstants.PERMILLE ) {//right port
                            if ( point.getY() == Constants.POINT_100 ) {
                                gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_END;
                            } else if ( point.getY() == Constants.POINT_500 ) {
                                gridBagConstraints.anchor = GridBagConstraints.LINE_END;
                            } else {
                                gridBagConstraints.anchor = GridBagConstraints.LAST_LINE_END;
                            }
                        }
                    }
                }
            }
        } catch ( NullPointerException ignored ) {
        }
    }

    public ClassWrapper getValue() {
        return this._oldModel;
    }

    /**
     * Unique action entry point dispatching several component specific actions
     */
    public void actionPerformed( ActionEvent e ) {
        try {
            if ( Constants.OK_LABEL.equals( e.getActionCommand() ) ) {
                this._oldModel = this._newModel;
                this._oldModel.setLabel( this._classTextField.getText() );
                cellEditor.stopCellEditing();
            }

            cellEditor.stopCellEditing();
        } catch ( NullPointerException ignored ) {
        }
    }

    /**
     * Definition der Methode "tableChanged(TableModelEvent tableModelEvent)"
     * aufgrund der Implementation der Schnittstelle "TableModelListener"
     *
     * @param tableModelEvent tableModelEvent
     * @author Demian Franke
     */
    public void tableChanged( TableModelEvent tableModelEvent ) {
        try {
            this._oldModel.methodData = ( (DefaultTableModel) this._jTableMethods.getModel() ).getRowData();
            this._oldModel.attributeData = ( (DefaultTableModel) this._jTableAttributes.getModel() ).getRowData();

            ++this._counter;
            if ( this._counter == 1 ) {
                cellEditor.stopCellEditing();
                this._counter = 0;
            }
        } catch ( NullPointerException ignored ) {
        }
    }

    @Override
    public void itemStateChanged( ItemEvent itemEvent ) {

    }

    private void setUpTable( JTable table ) {
        table.getColumnModel().getColumn( 0 ).setPreferredWidth( Constants.TABLE_FIRST_COLUMN_DEFAULT_WIDTH );
        table.setBackground( Color.white );
        table.setGridColor( java.awt.Color.white );
        table.setSelectionBackground( Color.white );
        table.setShowGrid( false );
        table.setShowHorizontalLines( false );
        table.setShowVerticalLines( false );
        table.setTableHeader( null );
    }
}
