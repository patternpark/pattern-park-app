package umlPuzzle.classview.utility;

import javax.swing.table.AbstractTableModel;

public class DefaultTableModel extends AbstractTableModel {

    private final String[] _columnNames;
    private Object[][] _rowData;

    public DefaultTableModel( String[] columnNames, Object[][] rowData ) {
        this._columnNames = columnNames;
        this._rowData = rowData;
    }

    @Override
    public String getColumnName( int col ) {
        return this._columnNames[ col ];
    }

    @Override
    public int getRowCount() {
        return this._rowData.length;
    }

    public Object[][] getRowData() {
        return this._rowData;
    }

    @Override
    public int getColumnCount() {
        return this._columnNames.length;
    }

    @Override
    public Object getValueAt( int row, int col ) {
        return this._rowData[ row ][ col ];
    }

    @Override
    public boolean isCellEditable( int row, int col ) {
        return true;
    }

    public void setRowData( Object[][] rowData ) {
        this._rowData = rowData;
    }

    @Override
    public void setValueAt( Object value, int row, int col ) {
        this._rowData[ row ][ col ] = value;
        this.fireTableCellUpdated( row, col );
    }
}
