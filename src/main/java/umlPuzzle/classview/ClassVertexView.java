package umlPuzzle.classview;

import org.jgraph.JGraph;
import org.jgraph.graph.CellView;
import org.jgraph.graph.CellViewRenderer;
import org.jgraph.graph.GraphCellEditor;
import org.jgraph.graph.VertexView;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

/**
 *
 */
public class ClassVertexView extends VertexView {

    /**
     * the renderer for this view
     */
    protected static ClassRenderer renderer = new ClassRenderer();

    public GraphCellEditor getEditor() {
        return new ClassEditor();
    }

    /**
     * Creates new instance of <code>InstanceView</code> for the specified
     * graph cell.
     *
     * @param arg0 a graph cell to create view for
     */
    public ClassVertexView( Object arg0 ) {
        super( arg0 );
    }

    @Override
    public CellViewRenderer getRenderer() {
        return renderer;
    }

    private static class ClassRenderer extends JPanel implements CellViewRenderer {

        FlyweightUIClass uiPanel = new FlyweightUIClass( null );

        public Component getRendererComponent(
                JGraph graph, CellView view, boolean sel, boolean focus, boolean preview
        ) {

            Object value = graph.getModel().getValue( view.getCell() );
            if ( value instanceof ClassWrapper ) {
                ClassWrapper wrapper = (ClassWrapper) value;
                wrapper.uiPanel.installAttributes( value, view, false, graph );
                return wrapper.uiPanel;
            } else {
                ClassWrapper wrapper = new ClassWrapper();
                if ( value instanceof DefaultMutableTreeNode ) {
                    wrapper.setValue( (DefaultMutableTreeNode) value );
                }

                wrapper.setLabel( value.toString() );
                uiPanel.installAttributes( wrapper, view, true, graph );
                return uiPanel;
            }

        }
    }

}