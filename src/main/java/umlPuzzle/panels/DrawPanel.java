package umlPuzzle.panels;

import emi.tabs.PuzzleTabContent;
import settings.GUISets;
import umlPuzzle.exercise.Exercise;

import javax.swing.*;
import java.awt.*;

/**
 * in diesem Panel wird das UML-Diagramm gezeichnet
 *
 * @author Lars Friedrich
 */
public class DrawPanel extends JPanel {

    private final PuzzleTabContent _tabContent;

    /**
     * Panels mit Buttons
     */
    private ButtonPanel _button;

    /**
     * UML pattern diagram references
     */
    private Exercise _exercise;

    /**
     * Constructor
     *
     * @param content Puzzle tab content, which includes information from the pattern's xml pattern.
     * @author Lars Friedrich
     */
    public DrawPanel( PuzzleTabContent content ) {
        this._tabContent = content;
        /*
         * ID f�r die Aufgabe
         */
        this.init();
    }

    /**
     * initialisierung der Komponente
     *
     * @author Lars Friedrich
     */
    public void init() {
        /*
         * GridBagLayout temporary off
         */
        this.setLayout( new BorderLayout() );

        this._exercise =
                new Exercise( this._tabContent.getDataPath(), this._tabContent.getSolutionPath(), false, this );
        this._exercise.getUndo().addUndoStep( this._exercise.getClassList() );

        this.setSize( GUISets.DRAW_PANEL_DIMENSION );
        this.setMaximumSize( GUISets.DRAW_PANEL_DIMENSION );
        this.setPreferredSize( GUISets.DRAW_PANEL_DIMENSION );
        this.repaint();
    }

    /**
     * f�gt dem Graphen eine neue Klasse hinzu
     * und �ffnet den entsprechenden Frame
     *
     * @author Lars Friedrich und Demian Franke
     * <p>
     * Die Action Event Funktionalit�t ist noch nicht eingebunden.
     * Der Aufruf composite.write(); in addComponent dient
     * momentan dazu in der Compositum-�bung das bearbeitete
     * Diagramm wieder als XML Dokument zu speichern.
     * In den anderen �bungen ist das nicht eingebunden.
     * Das Konzept f�r die Maus/Action/Event Funktionen
     * m�sste nochmal �berdacht werden. Beispiel hierzu ist dann
     * das umlPuzzle.classview Panel Beispiel.
     * ClassGraph l�uft �brigens auch ohne das �brige
     * UML Puzzle (Rechte Maus auf ClassGraph.java -> run as java applet.
     */
    public void load( String file ) {
        // der alte Graph wird gel�scht und der neue gezeichnet
        this.getExercise().getUndo().reset();
        this.getExercise().getUndo().setActualPosition( 0 );
        this.getExercise().setLoad( true );
        this.getExercise().showNewGraph( file );
        this.getExercise().setLoad( false );
        this.getExercise().getUndo().addUndoStep( this._exercise.getClassList() );
        this.repaint();
        this.getRootPane().repaint();
    }

    /**
     * Funktion um die aktive �bung zur�ckzugeben
     *
     * @author Lars Friedrich
     */
    public umlPuzzle.exercise.Exercise getExercise() {
        return this._exercise;
    }

    /**
     * loadClassList
     *
     * @return the button
     */
    public ButtonPanel getButton() {
        return this._button;
    }

    /**
     * @param button the button to set
     */
    public void setButton( ButtonPanel button ) {
        this._button = button;
    }
}