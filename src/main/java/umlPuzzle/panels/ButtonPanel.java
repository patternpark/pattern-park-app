package umlPuzzle.panels;

import emi.GUIElementNames;
import emi.session.UserSettings;
import emi.util.PathUtil;
import emi.util.XmlUtil;
import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;
import umlPuzzle.frames.DeleteFrame;
import umlPuzzle.frames.InfoFrame;
import umlPuzzle.frames.InsertFrame;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * hier sind die Buttons des UML-Editors implementiert
 *
 * @author Lars Friedrich
 * @version 1.7 (14. November 2006)
 */
public class ButtonPanel extends JPanel {

    private final DrawPanel _drawPanel;
    private final FastHelpPanel _helpPanel;
    private final Text _text;

    private JButton _newClass;
    private JButton _startNew;
    private JButton _newAssociation;
    private JButton _newInheritance;
    private JButton _newAggregation;
    private JButton _newComposition;
    private JButton _newAttribute;
    private JButton _newOperation;
    private JButton _delete;
    private JButton _save;
    private JButton _load;
    private JButton _undo;
    private JButton _redo;
    private JButton _evaluate;

    /**
     * Konstruktor
     */
    public ButtonPanel( FastHelpPanel help, DrawPanel draw ) {
        this._helpPanel = help;
        this._drawPanel = draw;
        this._text = new Text();
        init();
    }

    public void init() {
        initializeDrawPanel();
        // das Panel wird in �berschrift und eigentliches Panel aufgeteilt
        JTextPane buttonHead = new JTextPane();
        JPanel buttonBottom = new JPanel();

        // Text f�r die �berschrift
        buttonHead.setText( this._text.getbButtonHeader() );
        buttonHead.setEditable( false );
        SimpleAttributeSet fett = new SimpleAttributeSet();
        StyleConstants.setBold( fett, true );

        buttonHead.getStyledDocument()
                  .setCharacterAttributes( buttonHead.getCaretPosition(), buttonHead.getText().length(), fett, true );

        // Gr��e des Panels
        buttonBottom.setPreferredSize( GUISets.BUTTON_PANEL_DIMENSION );

        // Grid-Bag-Layout der jeweiligen Panels
        GridBagLayout layout = new GridBagLayout();
        this.setLayout( layout );
        buttonHead.setLayout( layout );
        buttonBottom.setLayout( layout );

        // Festlegung der Hintergrund- und Schriftfarben und des Rahmens
        this.setBackground( ExerciseConstants.WHITE );
        buttonHead.setBackground( ExerciseConstants.BLUE );
        buttonHead.setForeground( ExerciseConstants.WHITE );
        buttonBottom.setBackground( ExerciseConstants.WHITE );
        buttonBottom.setBorder( BorderFactory.createLineBorder( ExerciseConstants.BLUE ) );

        this.createButtons();
        this.setUpMouseListeners();
        this.addComponents( buttonBottom, buttonHead );
        this.setUpActionListeners();
    }

    /**
     * Hier wird im drawPanel das ButtonPanel gesetzt
     */
    public void initializeDrawPanel() {
        this._drawPanel.setButton( this );
    }

    private void showHelpText( String helpText ) {
        this._helpPanel.setHelp( helpText );
    }

    /**
     * Hier wird neu gezeichnet.
     */
    public void repaintPanel() {
        this.repaint();
    }

    /**
     * ...
     */
    public void setEverythingEnable( boolean edit ) {
        this.setEnabled( edit );
        this._newClass.setEnabled( edit );
        this._startNew.setEnabled( edit );
        this._newAssociation.setEnabled( edit );
        this._newInheritance.setEnabled( edit );
        this._newAggregation.setEnabled( edit );
        this._newComposition.setEnabled( edit );
        this._newAttribute.setEnabled( edit );
        this._newOperation.setEnabled( edit );
        this._delete.setEnabled( edit );
        this._save.setEnabled( edit );
        this._load.setEnabled( edit );
        this._undo.setEnabled( edit );
        this._redo.setEnabled( edit );
        this._evaluate.setEnabled( edit );
    }

    /**
     * @return the helpPanel
     */
    public FastHelpPanel getHelpPanel() {
        return this._helpPanel;
    }

    /**
     * @return the text
     */
    public Text getText() {
        return this._text;
    }

    private void createButtons() {
        // Button, um eine neue Klasse hinzuzuf�gen
        this._newClass = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_CLASS_ICON_FILE_NAME ) ) ) )
        );
        this._newClass.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newClass.setToolTipText( _text.getTtNewClass() );

        // Button, um eine neue Klasse hinzuzuf�gen
        this._startNew = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.START_NEW_ICON_FILE_NAME ) ) )
        ) );
        this._startNew.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._startNew.setToolTipText( this._text.getTtStartNew() );

        this._newAttribute = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_ATTRIBUTE_ICON_FILE_NAME ) ) ) )
        );
        this._newAttribute.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newAttribute.setToolTipText( _text.getTtNewAttribute() );

        this._newOperation = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_OPERATION_ICON_FILE_NAME ) ) ) )
        );
        this._newOperation.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newOperation.setToolTipText( _text.getTtNewOperation() );

        this._newAssociation = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_ASSOCIATION_ICON_FILE_NAME ) ) ) )
        );
        this._newAssociation.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newAssociation.setToolTipText( _text.getTtNewAssociation() );

        this._newInheritance = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_INHERITANCE_ICON_FILE_NAME ) ) )
        ) );
        this._newInheritance.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newInheritance.setToolTipText( _text.getTtNewInheritance() );

        this._newAggregation = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_AGGREGATION_ICON_FILE_NAME ) ) ) )
        );
        this._newAggregation.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newAggregation.setToolTipText( _text.getTtNewAggregation() );

        this._newComposition = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.NEW_COMPOSITION_ICON_FILE_NAME ) ) ) )
        );
        this._newComposition.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._newComposition.setToolTipText( _text.getTtNewComposition() );

        this._delete = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.DELETE_ICON_FILE_NAME ) ) ) )
        );
        this._delete.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._delete.setToolTipText( this._text.getTtDelete() );

        this._save = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.SAVE_ICON_FILE_NAME ) ) ) )
        );
        this._save.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._save.setToolTipText( _text.getTtSave() );

        this._load = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.LOAD_ICON_FILE_NAME ) ) ) )
        );
        this._load.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._load.setToolTipText( this._text.getTtLoad() );

        this._undo = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.UNDO_ICON_FILE_NAME ) ) ) )
        );
        this._undo.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._undo.setToolTipText( _text.getTt_Undo() );

        this._redo = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.REDO_ICON_FILE_NAME ) ) ) )
        );
        this._redo.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._redo.setToolTipText( _text.getTt_Redo() );

        this._evaluate = new JButton( new ImageIcon(
                Objects.requireNonNull( getClass().getClassLoader().getResource(
                        String.format( Constants.PATH_TO_DIRECTORY, Constants.EVALUATE_ICON_FILE_NAME ) ) ) )
        );
        this._evaluate.setPreferredSize( GUISets.BUTTON_PIC_DIMENSION );
        this._evaluate.setToolTipText( this._text.getTtEval() );
    }

    private void addComponents( JPanel buttonBottomPanel, JTextPane buttonTextPaneHead ) {
        Utility.addComponent( buttonBottomPanel, this._startNew, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._evaluate, GUISets.SECOND_CELL, GUISets.FIRST_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._save, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._load, GUISets.SECOND_CELL, GUISets.SECOND_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._undo, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._redo, GUISets.SECOND_CELL, GUISets.THIRD_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newClass, GUISets.SECOND_CELL, GUISets.FOURTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._delete, GUISets.FIRST_CELL, GUISets.FOURTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newAttribute, GUISets.FIRST_CELL, GUISets.FIFTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newOperation, GUISets.SECOND_CELL, GUISets.FIFTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newAssociation, GUISets.FIRST_CELL, GUISets.SIXTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newInheritance, GUISets.SECOND_CELL, GUISets.SIXTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newAggregation, GUISets.FIRST_CELL, GUISets.SEVENTH_ROW, 1 );
        Utility.addComponent( buttonBottomPanel, this._newComposition, GUISets.SECOND_CELL, GUISets.SEVENTH_ROW, 1 );

        Utility.addComponent(
                this, buttonTextPaneHead, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 1, GridBagConstraints.NORTHWEST,
                GridBagConstraints.HORIZONTAL, GUISets.DEFAULT_INSETS
        );
        Utility.addComponent(
                this, buttonBottomPanel, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 1, GUISets.DEFAULT_INSETS );
    }

    private void setUpMouseListeners() {
        this._newClass.addMouseListener( new MouseListener() {
            public void mouseReleased( MouseEvent e ) {
                if ( _newClass.isEnabled() ) {
                    String className = JOptionPane.showInputDialog( null, _text.getClassName() );
                    if ( className != null ) {
                        if ( _drawPanel.getExercise().checkForValidClassName( className ) ) {
                            _drawPanel.getExercise().setAddToUndo( false );
                            _drawPanel.getExercise().addClass( className, 10, 10 );
                            _drawPanel.getExercise().addCellForClass( className, 10, 10 );
                            _drawPanel.getExercise().setAddToUndo( true );
                            repaintPanel();
                        } else {
                            _helpPanel.setHelp( ExerciseConstants.WARNING_INVALID_CLASSNAME );
                        }
                    } else {
                        _helpPanel.setHelp( _text.getStart() );
                    }
                }
            }

            public void mouseEntered( MouseEvent e ) {
            }

            public void mouseClicked( MouseEvent e ) {
            }

            public void mouseExited( MouseEvent e ) {
            }

            public void mousePressed( MouseEvent e ) {
            }
        } );
    }

    private void setUpActionListeners() {
        this._newClass.addActionListener( e -> showHelpText( getText().getNewClass() ) );
        this._startNew.addActionListener( e -> {
            this.showHelpText( getText().getStartNew() );
            _drawPanel.getExercise().initDocuments();
            _drawPanel.getExercise().setClassListFromDocument();
            _drawPanel.getExercise().getUndo().reset();
            _drawPanel.getExercise().getUndo().setActualPosition( 0 );
            _drawPanel.getExercise().getUndo().addUndoStep( _drawPanel.getExercise().getClassList() );
            _drawPanel.getExercise().showNewGraph( _drawPanel.getExercise().getDatei() );
        } );

        this._newAttribute.addActionListener( new ButtonActionListener(
                ExerciseConstants.ATTRIBUTE, getText().getNewAttribute(), this, this._drawPanel
        ) );

        this._newOperation.addActionListener( new ButtonActionListener(
                ExerciseConstants.OPERATION, getText().getNewOperation(), this, this._drawPanel )
        );

        this._newAssociation.addActionListener( new ButtonActionListener(
                ExerciseConstants.ASSOCIATION, getText().getNewAssociation(), this, this._drawPanel
        ) );

        this._newInheritance.addActionListener( new ButtonActionListener(
                ExerciseConstants.INHERITANCE, getText().getNewInheritance(), this, this._drawPanel )
        );

        this._newAggregation.addActionListener( new ButtonActionListener(
                ExerciseConstants.AGGREGATION, getText().getNewAggregation(), this, this._drawPanel
        ) );
        this._newComposition.addActionListener( new ButtonActionListener(
                ExerciseConstants.COMPOSITION, getText().getNewComposition(), this, this._drawPanel
        ) );

        this._delete.addActionListener( e -> {
            showHelpText( getText().getDelete() );
            setEverythingEnable( false );
            DeleteFrame deleteFrame = new DeleteFrame( this._drawPanel );
            deleteFrame.setVisible( true );
            deleteFrame.setAlwaysOnTop( true );
        } );

        this._save.addActionListener( e -> {
            showHelpText( getText().getSave() );
            File fileToSave = new File( Constants.DEFAUL_NEW_FILE_PATH );

            JFileChooser fileChooser = new JFileChooser( fileToSave );
            fileChooser.setSelectedFile( fileToSave );
            fileChooser.addChoosableFileFilter( new FileFilter() {
                public boolean accept( File f ) {
                    if ( f.isDirectory() ) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith( ".xml" );
                }

                public String getDescription() {
                    return "xml-File (*.xml)";
                }
            } );
            int state = fileChooser.showSaveDialog( _drawPanel );
            if ( state == JFileChooser.APPROVE_OPTION ) {
                File savedFile = fileChooser.getSelectedFile();
                Path saveFilePath = Paths.get(savedFile.getAbsolutePath());
                if (PathUtil.isChild(saveFilePath, Constants.UMLDATA_DIRECTORY_PATH)) {
                    initInvalidPathDialog();
                } else {
                    XmlUtil.writeXMLToAbsoluteFile( _drawPanel.getExercise().getDocument(), savedFile.getPath() );
                }
            }
        } );

        this._load.addActionListener( e -> {
            this.showHelpText( _text.getLoad() );
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.addChoosableFileFilter( new FileFilter() {
                public boolean accept( File f ) {
                    if ( f.isDirectory() ) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith( ".xml" );
                }

                public String getDescription() {
                    return "xml-File (*.xml)";
                }
            } );
            int state = fileChooser.showOpenDialog( _drawPanel );
            if ( state == JFileChooser.APPROVE_OPTION ) {
                File loadedFile = fileChooser.getSelectedFile();
                Path loadedFileAbsolute = Paths.get(loadedFile.toString()).toAbsolutePath();
                if (PathUtil.isChild(loadedFileAbsolute, Constants.UMLDATA_DIRECTORY_PATH)) {
                    initInvalidPathDialog();
                } else {
                    _drawPanel.load( loadedFile.toString() );
                }

            }
        } );

        this._redo.addActionListener( e -> {
            showHelpText( _text.getUndo() );
            _drawPanel.getExercise().getUndo().redoStep();
        } );

        this._evaluate.addActionListener( e -> {
            showHelpText( getText().getEval() );
            setEverythingEnable( false );
            InfoFrame info = new InfoFrame( ExerciseConstants.EVALUATE, _drawPanel );
            info.setVisible( true );
            info.setAlwaysOnTop( true );
        } );

        this._undo.addActionListener( e -> {
            showHelpText( _text.getUndo() );
            _drawPanel.getExercise().getUndo().showStep();
        } );
    }

    private static class ButtonActionListener implements ActionListener {

        private final int _ELEMENT_ID;
        private final String _HELP_TEXT;
        private final ButtonPanel _BUTTON_PANEL;
        private final DrawPanel _DRAW_PANEL;


        public ButtonActionListener( int elementID, String helpText, ButtonPanel buttonPanel, DrawPanel drawPanel ) {
            this._ELEMENT_ID = elementID;
            this._HELP_TEXT = helpText;
            this._DRAW_PANEL = drawPanel;
            this._BUTTON_PANEL = buttonPanel;
        }

        @Override
        public void actionPerformed( ActionEvent e ) {
            this._BUTTON_PANEL.showHelpText( this._HELP_TEXT );
            this._BUTTON_PANEL.setEverythingEnable( false );
            InsertFrame insertFrame = new InsertFrame( this._ELEMENT_ID, this._DRAW_PANEL );
            insertFrame.setVisible( true );
            insertFrame.setAlwaysOnTop( true );
        }
    }

    private void initInvalidPathDialog() {
        JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        String dialogTitle = UserSettings.getGUITextFromName(GUIElementNames.DIALOG_INVALID_DIRECTORY_TITLE);
        String dialogMessage = UserSettings.getGUITextFromName(GUIElementNames.DIALOG_INVALID_DIRECTORY_MESSAGE);

        JOptionPane.showMessageDialog(topFrame, dialogMessage, dialogTitle, JOptionPane.ERROR_MESSAGE);
    }
}