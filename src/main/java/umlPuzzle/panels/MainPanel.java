package umlPuzzle.panels;

import emi.tabs.PuzzleTabContent;
import emi.tabs.jpanels.TabPanel;
import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import java.awt.*;

/**
 * Die Klasse MainPanel ist diejenige,
 * in der alle Objekte und Panels angezeigt werden;
 * links das Zeichenfeld, rechts mittig die Werkzeugleiste,
 * rechts oben die Aufgabe und rechts unten die Schnellhilfe
 *
 * @author Lars Friedrich, Demian Franke
 * @version 1.6 (14. November 2006)
 */
public class MainPanel extends TabPanel<PuzzleTabContent> {


    /**
     * Panel which displays the task description.
     */
    private final TaskPanel _taskPanel;
    /**
     * Panel which contains help text and tips.
     */
    private final FastHelpPanel _fastHelpPanel;
    /**
     * Panel which displays the uml puzzle diagram.
     */
    private final DrawPanel _drawPanel;
    /**
     * Panel which displays the available action buttons to the user.
     */
    private final ButtonPanel _buttonPanel;

    /**
     * Constructor
     */
    public MainPanel( PuzzleTabContent tabContent ) {
        super( tabContent );
        this._fastHelpPanel = new FastHelpPanel();
        this._taskPanel = new TaskPanel( tabContent );
        this._drawPanel = new DrawPanel( tabContent );
        this._buttonPanel = new ButtonPanel( this._fastHelpPanel, this._drawPanel );
        this.init();
    }

    /**
     * Initialisation
     */
    public void init() {
        this.setLayout( new GridBagLayout() );
        this.setBackground( ExerciseConstants.WHITE );
        this._drawPanel.setBorder( BorderFactory.createLineBorder( ExerciseConstants.BLUE ) );

        Utility.addComponent(
                this, this._taskPanel, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
                GridBagConstraints.HORIZONTAL, GUISets.STANDARD_INSETS
        );
        Utility.addComponent(
                this, this._drawPanel, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 1, 2, 0.9, 1.0,
                GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, GUISets.STANDARD_INSETS
        );
        Utility.addComponent(
                this, this._buttonPanel, GUISets.SECOND_CELL, GUISets.SECOND_ROW, 1, 1, GridBagConstraints.NORTHWEST,
                GridBagConstraints.BOTH
        );
        Utility.addComponent(
                this, this._fastHelpPanel, GUISets.SECOND_CELL, GUISets.THIRD_ROW, 1, 1, GridBagConstraints.NORTHWEST,
                GridBagConstraints.BOTH
        );
    }
}