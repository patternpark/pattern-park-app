package umlPuzzle.panels;

import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;

/**
 * das Panel f�r die Hinweishilfe
 *
 * @author Lars Friedrich
 * @version 1.3 (14. November 2006)
 */
public class FastHelpPanel extends JPanel {

    private final JTextPane _helpTitle = new JTextPane();
    private final JCheckBox _onOffCheckbox = new JCheckBox();
    private final JLabel _fastHelp = new JLabel();
    private final Text _text = new Text();
    private final JTextArea _textArea = new JTextArea();

    /**
     * Konstruktor
     */
    public FastHelpPanel() {
        this.init();
    }

    /**
     * Initialisierung der Komponente
     */
    public void init() {
        // alle Texte werden initialisiert
        this._helpTitle.setText( this._text.getHeader() );

        // das Layout bestimmen
        this.setLayout( new GridBagLayout() );

        this.setSize( GUISets.FAST_HELP_PANEL_DIMENSION );

        this._onOffCheckbox.setSelected( true );
        this._onOffCheckbox.addActionListener( ignored -> {
            if ( this._onOffCheckbox.isSelected() ) {
                showFastHelp();
                return;
            }
            hideFastHelp();
        } );

        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setBold( attributeSet, true );
        int selTextPos = this._helpTitle.getCaretPosition();
        int selTextLen = this._helpTitle.getText().length();
        this._helpTitle.getStyledDocument().setCharacterAttributes( selTextPos, selTextLen, attributeSet, true );

        this._helpTitle.setEditable( false );
        this.setBackground( ExerciseConstants.WHITE );
        this._onOffCheckbox.setBackground( ExerciseConstants.WHITE );
        this._helpTitle.setBackground( ExerciseConstants.BLUE );
        this._helpTitle.setForeground( ExerciseConstants.WHITE );

        this._textArea.setPreferredSize( GUISets.TEXT_AREA_DIMENSION );
        this._textArea.setWrapStyleWord( true );
        this._textArea.setLineWrap( true );
        this._textArea.setText( this._text.getStart() );
        this._textArea.setBorder( BorderFactory.createLineBorder( ExerciseConstants.BLUE ) );
        this._textArea.setEditable( false );

        Utility.addComponent(
                this, this._onOffCheckbox, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, GUISets.DEFAULT_INSETS
        );
        Utility.addComponent(
                this, this._fastHelp, GUISets.SECOND_CELL, GUISets.THIRD_ROW, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, GUISets.DEFAULT_INSETS
        );

        this._fastHelp.setText( this._text.getLabelText() );

        if ( this._onOffCheckbox.isSelected() ) {
            this.showFastHelp();
        }

    }

    /**
     * hier wird Schnellhilfe-Panel angezeigt
     */
    public void showFastHelp() {
        this.remove( this._onOffCheckbox );
        this.remove( this._fastHelp );
        Utility.addComponent(
                this, this._helpTitle, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 2, GridBagConstraints.NORTHWEST,
                GridBagConstraints.HORIZONTAL, GUISets.DEFAULT_INSETS
        );
        Utility.addComponent(
                this, this._textArea, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 2, GridBagConstraints.NORTHWEST,
                GridBagConstraints.BOTH, GUISets.DEFAULT_INSETS
        );
        Utility.addComponent(
                this, this._onOffCheckbox, GUISets.FIRST_CELL, GUISets.THIRD_ROW, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, GUISets.DEFAULT_INSETS
        );
        Utility.addComponent(
                this, this._fastHelp, GUISets.SECOND_CELL, GUISets.THIRD_ROW, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, GUISets.DEFAULT_INSETS
        );

        this.repaint();
    }

    /**
     * hier wird Schnellhilfe-Panel versteckt
     */
    public void hideFastHelp() {
        this.remove( this._textArea );
        this.remove( this._helpTitle );
        this.repaint();
    }

    public void setHelp( String helpText ) {
        if ( this._onOffCheckbox.isSelected() ) {
            this._textArea.setText( helpText );
            this.repaint();
        }
    }
}