package umlPuzzle.panels;

import emi.tabs.PuzzleTabContent;
import settings.GUISets;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.config.Text;
import umlPuzzle.utility.Utility;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;

/**
 * hier erscheint die zu bearbeitende Aufgabe
 *
 * @author Lars Friedrich
 */
public class TaskPanel extends JPanel {

    /**
     * Konstruktor f�r das Aufgabenfeld
     */
    public TaskPanel( PuzzleTabContent tabContent ) {
        this.init( tabContent );
    }

    /**
     * Initialisierung des Aufgabenfelds
     */
    public void init( PuzzleTabContent tabContent ) {
        // alle Texte werden initialisiert
        Text text = new Text();
        JTextPane task = new JTextPane();
        JTextPane taskTitle = new JTextPane();
        // das Layout wird festgelegt
        GridBagLayout layout = new GridBagLayout();
        this.setLayout( layout );

        // alle Farben werden gesetzt
        this.setBackground( ExerciseConstants.WHITE );
        task.setBackground( ExerciseConstants.WHITE );
        task.setForeground( ExerciseConstants.BLACK );
        task.setEditable( false );

        // Felder, Gr��en und Farben setzen
        task.setText( tabContent.getDescription() );

        // ScrollPane for the task description
        JScrollPane scroll = new JScrollPane(
                task, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
        );
        scroll.setPreferredSize( GUISets.EXERCISE_AREA_DIMENSION );
        scroll.setMinimumSize( GUISets.EXERCISE_AREA_DIMENSION );

        taskTitle.setText( text.getTaskHead() );
        SimpleAttributeSet boldAttributeSet = new SimpleAttributeSet();
        StyleConstants.setBold( boldAttributeSet, true );
        int selTextPos = taskTitle.getCaretPosition();
        int selTextLen = taskTitle.getText().length();
        taskTitle.getStyledDocument().setCharacterAttributes( selTextPos, selTextLen, boldAttributeSet, true );

        taskTitle.setBackground( ExerciseConstants.BLUE );
        taskTitle.setForeground( ExerciseConstants.WHITE );
        task.setBorder( BorderFactory.createLineBorder( ExerciseConstants.BLUE ) );
        taskTitle.setEditable( false );

        // addition of a component
        Utility.addComponent(
                this, taskTitle, GUISets.FIRST_CELL, GUISets.FIRST_ROW, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
                GridBagConstraints.HORIZONTAL, GUISets.DEFAULT_INSETS
        );
        Utility.addComponent(
                this, scroll, GUISets.FIRST_CELL, GUISets.SECOND_ROW, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
                GridBagConstraints.BOTH, GUISets.DEFAULT_INSETS
        );
    }

}