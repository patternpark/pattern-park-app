package umlPuzzle.panels;

final class Constants {

    public static final String PATH_TO_DIRECTORY = "grafiken/icons/%s";
    public static final String NEW_CLASS_ICON_FILE_NAME = "class_02.gif";
    public static final String START_NEW_ICON_FILE_NAME = "default_02.gif";
    public static final String NEW_ATTRIBUTE_ICON_FILE_NAME = "attribute_01.gif";
    public static final String NEW_OPERATION_ICON_FILE_NAME = "operation_02.gif";
    public static final String NEW_ASSOCIATION_ICON_FILE_NAME = "association_02.gif";
    public static final String NEW_INHERITANCE_ICON_FILE_NAME = "inheritance.gif";
    public static final String NEW_AGGREGATION_ICON_FILE_NAME = "aggregation_02.gif";
    public static final String NEW_COMPOSITION_ICON_FILE_NAME = "composition_02.gif";
    public static final String DELETE_ICON_FILE_NAME = "delete.gif";
    public static final String SAVE_ICON_FILE_NAME = "save_02.gif";
    public static final String LOAD_ICON_FILE_NAME = "open_file_01.gif";
    public static final String UNDO_ICON_FILE_NAME = "undo_01.gif";
    public static final String REDO_ICON_FILE_NAME = "redo_01.gif";
    public static final String EVALUATE_ICON_FILE_NAME = "evaluate.gif";
    public static final String DEFAUL_NEW_FILE_PATH = "umlData/";
    public static final String UMLDATA_DIRECTORY_PATH = "umlPuzzle/umlData";

    private Constants() {
    }


}
