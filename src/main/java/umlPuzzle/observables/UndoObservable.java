package umlPuzzle.observables;

import umlPuzzle.interfaces.UndoObserver;

import java.util.ArrayList;

public abstract class UndoObservable {

    protected ArrayList<UndoObserver> observers = new ArrayList<>();

    public void addObserver( UndoObserver observer ) {
        this.observers.add( observer );
    }

    protected abstract void fireUndoStepAddedEvent();

    protected abstract void fireUndoStepEvent();

    protected abstract void fireRedoStepEvent();

    protected abstract void fireInvalidStepEvent( int typeOfWarning );
}
