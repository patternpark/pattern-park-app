package umlPuzzle.config;

final class Constants {

    public static String PATH_TO_TEXT_XML = "umlPuzzle/umlData/text.xml";
    public static String HEAD = "head";
    public static String TASK = "task";
    public static String HINT = "hint";
    public static String TOOL = "tool";
    public static String LABEL = "label";
    public static String ERROR_XML = "errorXML";
    public static String LOAD_ERROR = "loadError";
    public static String HELP = "help";
    public static String START = "start";
    public static String CONFIRM_CHANGES = "confirmChanges";
    public static String NEW_CLASS = "newClass";
    public static String START_NEW = "startNew";
    public static String NEW_ASSOCIATION = "newAssociation";
    public static String NEW_METHOD = "newMethod";
    public static String NEW_AGGREGATION = "newAggregation";
    public static String NEW_INHERITANCE = "newInheritance";
    public static String NEW_ATTRIBUTE = "newAttribute";
    public static String NEW_COMPOSITION = "newComposition";
    public static String DELETE_ELEMENT = "deleteElement";
    public static String SAVE_ALL = "saveAll";
    public static String LOAD = "load";
    public static String EVAL = "eval";
    public static String UNDO = "undo";
    public static String REDO = "redo";
    public static String LABEL_TEXT = "labelText";
    public static String TOOL_TIP = "tooltip";
    public static String FRAME = "frame";
    public static String DELETE_FRAME = "deleteFrame";
    public static String CLASS_FRAME = "classFrame";
    public static String INSERT_FRAME = "insertFrame";
    public static String FIRST_QUESTION = "firstQuestion";
    public static String SECOND_QUESTION = "secondQuestion";
    public static String DELETE_CLASS = "deleteClass";
    public static String DELETE_INHERITANCE = "deleteInheritance";
    public static String DELETE_ASSOCIATION = "deleteAssociation";
    public static String DELETE_AGGREGATION = "deleteAggregation";
    public static String DELETE_COMPOSITION = "deleteComposition";
    public static String DELETE_ATTRIBUTE = "deleteAttribute";
    public static String DELETE_METHOD = "deleteMethod";
    public static String OK_BUTTON = "okButton";
    public static String CANCEL_BUTTON = "cancelButton";
    public static String QUESTION = "question";
    public static String INSERT_CLASS = "insertClass";
    public static String INSERT_ATTRIBUTE = "insertAttribute";
    public static String INSERT_METHOD = "insertMethod";
    public static String INSERT_ASSOCIATION = "insertAssociation";
    public static String INSERT_INHERITANCE = "insertInheritance";
    public static String INSERT_AGGREGATION = "insertAggregation";
    public static String INSERT_COMPOSITION = "insertComposition";
    public static String SHORTCUT = "shortcut";
    public static String WARNING = "warning";
    public static String NO_UNDO = "noUndo";
    public static String NO_REDO = "noRedo";
    public static String SOLUTION = "solution";


    private Constants() {
    }
}
