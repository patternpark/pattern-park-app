package umlPuzzle.config;

//hier sind alle Text nach XML ausgegliedert worden!!!

import java.awt.*;

/**
 * hier stehen die Texte, die in der Aufgabenstellung gezeigt werden
 * au�erdem werden Variablen zum unterschiedlichen Aufruf bereitgestellt
 *
 * @author Lars Friedrich
 * @version 1.2 (14. November 2006)
 */
public final class ExerciseConstants {
    /**
     * Hintergrundfarbe f�r die Aufgabe
     */
    public static Color BLUE = new Color( 116, 161, 197 );
    /**
     * Textfarbe f�r die Aufgabe
     */
    public static Color WHITE = new Color( 255, 255, 255 );
    /**
     * Textfarbe f�r die Aufgabe
     */
    public static Color BLACK = new Color( 0, 0, 0 );
    /**
     * Werte f�r das Einf�gen von Beziehungen
     * Assoziation: Wert 0
     */
    public static int ASSOCIATION = 0;
    /**
     * Werte f�r das Einf�gen von Beziehungen
     * Vererbung: Wert 1
     */
    public static int INHERITANCE = 1;
    /**
     * Werte f�r das Einf�gen von Beziehungen
     * Aggregation: Wert 2
     */
    public static int AGGREGATION = 2;
    /**
     * Werte f�r das Einf�gen von Beziehungen
     * Komposition: Wert 3
     */
    public static int COMPOSITION = 3;
    /**
     * Werte f�r das Einf�gen von Beziehungen
     * Komposition: Wert 3
     */
    public static int OPERATION = 4;
    /**
     * Werte f�r das Einf�gen von Beziehungen
     * Komposition: Wert 3
     */
    public static int ATTRIBUTE = 5;

    //////////////////////////////////////////////////////////////
    ///////////////////////////PatternPark//////////////////////////
    //////////////////////////////////////////////////////////////
    public static final String PATTERN_PARK_TITLE = "Pattern Park";

    public static final String WARNING_INVALID_CLASSNAME
            = "Klassennamen duerfen in einem Diagramm nur einmal vorkommen! Das erste Zeichen muss ein gültiger Großbuchstabe sein und der Name darf nicht leer sein.";

    //////////////////////////////////////////////////////////////
    ///////////////////////////WARNUNGEN//////////////////////////
    //////////////////////////////////////////////////////////////
    /**
     * Wert f�r die Warnung beim Undo-Schritt
     */
    public static int WARNING_UNDO = 0;
    /**
     * Wert f�r die Warnung beim Redo-Schritt
     */
    public static int WARNING_REDO = 1;
    //////////////////////////////////////////////////////////////
    /////////////////////////�BERPR�FUNGEN////////////////////////
    //////////////////////////////////////////////////////////////
    /**
     * Wert f�r die Warnung beim Redo-Schritt
     */
    public static int EVALUATE = 2;

    private ExerciseConstants() {
    }
}