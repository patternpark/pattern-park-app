package umlPuzzle.config;

import emi.util.XmlUtil;
import org.jdom.Document;

public class Text {
    private static Document _germanText;

    //------------------------------------------------------------
    //-----------------TEXTE F�R DAS FEHLER-FENSTER---------------
    //------------------------------------------------------------
    private String _errorXml;
    private String _loadError;

    //------------------------------------------------------------
    //-----------------TEXTE F�R DIE �BERPR�FUNG------------------
    //------------------------------------------------------------
    private String _solution;

    //------------------------------------------------------------
    //--------------------SHORTCUTS-------------------------------
    //------------------------------------------------------------
    /**
     * Shortcut f�r A
     */
    private String _scA;
    /**
     * Shortcut f�r G
     */
    private String _scG;
    /**
     * Shortcut f�r K
     */
    private String _scK;
    /**
     * Shortcut f�r M
     */
    private String _scM;
    /**
     * Shortcut f�r O
     */
    private String _scO;
    /**
     * Shortcut f�r S
     */
    private String _scS;
    /**
     * Shortcut f�r V
     */
    private String _scV;

    //------------------------------------------------------------
    //--------------------WARNUNGEN-------------------------------
    //------------------------------------------------------------
    /**
     * Text mit Warnung
     */
    private String _warningUndo;
    /**
     * Text mit Warnung
     */
    private String _warningRedo;
    /**
     * Die �berschrift �ber dem Panel
     */
    private String _taskHead;

    private String _start;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Klasse gezeichnet werden soll
     */
    private String _graphChange;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Klasse gezeichnet werden soll
     */
    private String _newClass;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn die Aufgabe wieder neu geladen wird
     */
    private String _startNew;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Assoziation gezeichnet werden soll
     */
    private String _newAssociation;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Operation eingetragen werden soll
     */
    private String _newOperation;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Aggregation gezeichnet werden soll
     */
    private String _newAggregation;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Vererbung gezeichnet werden soll
     */
    private String _newInheritance;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Attribut eingetragen werden soll
     */
    private String _newAttribute;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn eine neue Komposition gezeichnet werden soll
     */
    private String _newComposition;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn etwas gel�scht werden soll
     */
    private String _delete;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn etwas gespeichert werden soll
     */
    private String _save;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn etwas geladen werden soll
     */
    private String _load;
    /**
     * Der Text wird in der Schnellhilfe gezeigt, wenn etwas �berpr�ft wird
     */
    private String _eval;
    /**
     * Der Text das Labels der Schnellhilfe
     */
    private String _labelText;
    /**
     * Die �berschrift �ber dem Panel
     */
    private String _header;
    /**
     * Frage im DeleteFrame
     */
    private String _question1;
    /**
     * Frage im DeleteFrame
     */
    private String _question2;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteClass;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteInheritance;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteAssociation;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteAggregation;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteComposition;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteAttirbute;
    /**
     * Label-Bezeichnung im DeleteFrame
     */
    private String _deleteOperation;
    /**
     * Text f�r die Abfrage, welcher Klassenname gegeben werden soll
     */
    private String _className;
    /**
     * Text f�r die Panel-�berschrift
     */
    private String _buttonHeader;
    /**
     * ToolTipText f�r den Button newClass
     */
    private String _ttNewClass;
    /**
     * ToolTipText f�r den Button Aufgabe neu laden
     */
    private String _ttStartNew;
    /**
     * ToolTipText f�r den Button newAssociation
     */
    private String _ttNewAssociation;
    /**
     * ToolTipText f�r den Button newInheritance
     */
    private String _ttNewInheritance;
    /**
     * ToolTipText f�r den Button newAggregation
     */
    private String _ttNewAggregation;
    /**
     * ToolTipText f�r den Button newComposition
     */
    private String _ttNewComposition;
    /**
     * ToolTipText f�r den Button newAttirbute
     */
    private String _ttNewAttribute;
    /**
     * ToolTipText f�r den Button newOperation
     */
    private String _ttNewOperation;
    /**
     * ToolTipText f�r den Button delete
     */
    private String _ttDelete;
    /**
     * ToolTipText f�r den Button speichern
     */
    private String _ttSave;
    /**
     * ToolTipText f�r den Button laden
     */
    private String _ttLoad;
    /**
     * ToolTipText f�r den Button laden
     */
    private String _ttEval;
    /**
     * zus�tzlicher Label-Text in f�r Attribute und Operation
     */
    private String _insertClass;
    /**
     * Label-Text beim Einf�gen des Attributs
     */
    private String _insertAttributeFirst;
    /**
     * Label-Text beim Einf�gen des Attributs
     */
    private String _insertAttributeSecond;
    /**
     * Label-Text beim Einf�gen der Operation
     */
    private String _insertMethodFirst;
    /**
     * Label-Text beim Einf�gen der Operation
     */
    private String _insertMethodSecond;
    /**
     * Label-Text beim Einf�gen der Assiziation
     */
    private String _insertAssociationFirst;
    /**
     * Label-Text beim Einf�gen der Assiziation
     */
    private String _insertAssociationSecond;
    /**
     * Label-Text beim Einf�gen der Vererbung
     */
    private String _insertInheritanceFirst;
    /**
     * Label-Text beim Einf�gen der Vererbung
     */
    private String _insertInheritanceSecond;
    /**
     * Label-Text beim Einf�gen der Aggregation
     */
    private String _insertAggregationFirst;
    /**
     * Label-Text beim Einf�gen der Aggregation
     */
    private String _insertAggregationSecond;
    /**
     * Label-Text beim Einf�gen der Komposition
     */
    private String _insertCompositionFirst;
    /**
     * Label-Text beim Einf�gen der Komposition
     */
    private String _insertCompositionSecond;
    /**
     * Button-Bezeichnung im DeleteFrame
     */
    private String _okButton;
    /**
     * Button-Bezeichnung im DeleteFrame
     */
    private String _cancelButton;
    /**
     * ToolTipText f�r den Undo-Button
     */
    private String _ttUndo;
    /**
     * ToolTipText f�r den Undo-Button
     */
    private String _ttRedo;
    /**
     * Hilfe-Text f�r den Undo-Button
     */
    private String _undo;

    public Text() {
        init();
    }

    public void init() {
        // Initialisierung des UML-Builders.
        // Initialisierung der XML-Datei.
        _germanText = XmlUtil.readXMLDocument( Constants.PATH_TO_TEXT_XML );

        // Fehler-Fenster beim Laden
        this._loadError = _germanText.getRootElement().getChild( Constants.LABEL )
                                     .getChildText( Constants.LOAD_ERROR );
        this._errorXml = _germanText.getRootElement().getChild( Constants.LABEL ).getChildText( Constants.ERROR_XML );

        // �berschriften
        this._taskHead = _germanText.getRootElement().getChild( Constants.HEAD ).getChildText( Constants.TASK );
        this._header = _germanText.getRootElement().getChild( Constants.HEAD ).getChildText( Constants.HINT );
        this._buttonHeader = _germanText.getRootElement().getChild( Constants.HEAD ).getChildText( Constants.TOOL );

        // Hinweis-Texte
        this._start = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.START );
        this._graphChange = _germanText.getRootElement().getChild( Constants.HELP )
                                       .getChildText( Constants.CONFIRM_CHANGES );
        this._newClass = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.NEW_CLASS );
        this._startNew = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.START_NEW );
        this._newAssociation = _germanText.getRootElement().getChild( Constants.HELP )
                                          .getChildText( Constants.NEW_ASSOCIATION );
        this._newOperation = _germanText.getRootElement().getChild( Constants.HELP )
                                        .getChildText( Constants.NEW_METHOD );
        this._newAggregation = _germanText.getRootElement().getChild( Constants.HELP )
                                          .getChildText( Constants.NEW_AGGREGATION );
        this._newInheritance = _germanText.getRootElement().getChild( Constants.HELP )
                                          .getChildText( Constants.NEW_INHERITANCE );
        this._newAttribute = _germanText.getRootElement().getChild( Constants.HELP )
                                        .getChildText( Constants.NEW_ATTRIBUTE );
        this._newComposition = _germanText.getRootElement().getChild( Constants.HELP )
                                          .getChildText( Constants.NEW_COMPOSITION );
        this._delete = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.DELETE_ELEMENT );
        this._save = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.SAVE_ALL );
        this._load = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.LOAD );
        this._eval = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.EVAL );
        this._undo = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.UNDO );
        this._labelText = _germanText.getRootElement().getChild( Constants.HELP ).getChildText( Constants.LABEL_TEXT );

        // Tool-Tip-Texte
        this._ttNewClass = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                      .getChildText( Constants.NEW_CLASS );
        this._ttStartNew = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                      .getChildText( Constants.START_NEW );
        this._ttNewAssociation = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                            .getChildText( Constants.NEW_ASSOCIATION );
        this._ttNewOperation = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                          .getChildText( Constants.NEW_METHOD );
        this._ttNewAggregation = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                            .getChildText( Constants.NEW_AGGREGATION );
        this._ttNewInheritance = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                            .getChildText( Constants.NEW_INHERITANCE );
        this._ttNewAttribute = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                          .getChildText( Constants.NEW_ATTRIBUTE );
        this._ttNewComposition = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                            .getChildText( Constants.NEW_COMPOSITION );
        this._ttDelete = _germanText.getRootElement().getChild( Constants.TOOL_TIP )
                                    .getChildText( Constants.DELETE_ELEMENT );
        this._ttSave = _germanText.getRootElement().getChild( Constants.TOOL_TIP ).getChildText( Constants.SAVE_ALL );
        this._ttLoad = _germanText.getRootElement().getChild( Constants.TOOL_TIP ).getChildText( Constants.LOAD );
        this._ttUndo = _germanText.getRootElement().getChild( Constants.TOOL_TIP ).getChildText( Constants.UNDO );
        this._ttRedo = _germanText.getRootElement().getChild( Constants.TOOL_TIP ).getChildText( Constants.REDO );
        this._ttEval = _germanText.getRootElement().getChild( Constants.TOOL_TIP ).getChildText( Constants.EVAL );

        // DeleteFrame
        this._question1 = _germanText.getRootElement().getChild( Constants.FRAME )
                                     .getChild( Constants.DELETE_FRAME ).getChildText( Constants.FIRST_QUESTION );
        this._question2 = _germanText.getRootElement().getChild( Constants.FRAME )
                                     .getChild( Constants.DELETE_FRAME ).getChildText( Constants.SECOND_QUESTION );
        this._deleteClass = _germanText.getRootElement().getChild( Constants.FRAME )
                                       .getChild( Constants.DELETE_FRAME ).getChildText( Constants.DELETE_CLASS );
        this._deleteInheritance = _germanText.getRootElement().getChild( Constants.FRAME )
                                             .getChild( Constants.DELETE_FRAME )
                                             .getChildText( Constants.DELETE_INHERITANCE );
        this._deleteAssociation = _germanText.getRootElement().getChild( Constants.FRAME )
                                             .getChild( Constants.DELETE_FRAME )
                                             .getChildText( Constants.DELETE_ASSOCIATION );
        this._deleteAggregation = _germanText.getRootElement().getChild( Constants.FRAME )
                                             .getChild( Constants.DELETE_FRAME )
                                             .getChildText( Constants.DELETE_AGGREGATION );
        this._deleteComposition = _germanText.getRootElement().getChild( Constants.FRAME )
                                             .getChild( Constants.DELETE_FRAME )
                                             .getChildText( Constants.DELETE_COMPOSITION );
        this._deleteAttirbute = _germanText.getRootElement().getChild( Constants.FRAME )
                                           .getChild( Constants.DELETE_FRAME )
                                           .getChildText( Constants.DELETE_ATTRIBUTE );
        this._deleteOperation = _germanText.getRootElement().getChild( Constants.FRAME )
                                           .getChild( Constants.DELETE_FRAME ).getChildText( Constants.DELETE_METHOD );
        this._okButton = _germanText.getRootElement().getChild( Constants.FRAME )
                                    .getChild( Constants.DELETE_FRAME ).getChildText( Constants.OK_BUTTON );
        this._cancelButton = _germanText.getRootElement().getChild( Constants.FRAME )
                                        .getChild( Constants.DELETE_FRAME ).getChildText( Constants.CANCEL_BUTTON );

        // Frame zur neuen Klsase
        this._className = _germanText.getRootElement().getChild( Constants.FRAME )
                                     .getChild( Constants.CLASS_FRAME ).getChildText( Constants.QUESTION );

        // Insert-Frame
        this._insertClass = _germanText.getRootElement().getChild( Constants.FRAME )
                                       .getChild( Constants.INSERT_FRAME ).getChildText( Constants.INSERT_CLASS );
        this._insertAttributeFirst = _germanText.getRootElement().getChild( Constants.FRAME )
                                                .getChild( Constants.INSERT_FRAME )
                                                .getChild( Constants.INSERT_ATTRIBUTE )
                                                .getChildText( Constants.FIRST_QUESTION );
        this._insertAttributeSecond = _germanText.getRootElement().getChild( Constants.FRAME )
                                                 .getChild( Constants.INSERT_FRAME )
                                                 .getChild( Constants.INSERT_ATTRIBUTE )
                                                 .getChildText( Constants.SECOND_QUESTION );
        this._insertMethodFirst = _germanText.getRootElement().getChild( Constants.FRAME )
                                             .getChild( Constants.INSERT_FRAME ).getChild( Constants.INSERT_METHOD )
                                             .getChildText( Constants.FIRST_QUESTION );
        this._insertMethodSecond = _germanText.getRootElement().getChild( Constants.FRAME )
                                              .getChild( Constants.INSERT_FRAME )
                                              .getChild( Constants.INSERT_METHOD )
                                              .getChildText( Constants.SECOND_QUESTION );
        this._insertAssociationFirst = _germanText.getRootElement().getChild( Constants.FRAME )
                                                  .getChild( Constants.INSERT_FRAME )
                                                  .getChild( Constants.INSERT_ASSOCIATION )
                                                  .getChildText( Constants.FIRST_QUESTION );
        this._insertAssociationSecond =
                _germanText.getRootElement().getChild( Constants.FRAME )
                           .getChild( Constants.INSERT_FRAME ).getChild( Constants.INSERT_ASSOCIATION )
                           .getChildText( Constants.SECOND_QUESTION );
        this._insertInheritanceFirst = _germanText.getRootElement().getChild( Constants.FRAME )
                                                  .getChild( Constants.INSERT_FRAME )
                                                  .getChild( Constants.INSERT_INHERITANCE )
                                                  .getChildText( Constants.FIRST_QUESTION );
        this._insertInheritanceSecond =
                _germanText.getRootElement().getChild( Constants.FRAME )
                           .getChild( Constants.INSERT_FRAME ).getChild( Constants.INSERT_INHERITANCE )
                           .getChildText( Constants.SECOND_QUESTION );
        this._insertAggregationFirst = _germanText.getRootElement().getChild( Constants.FRAME )
                                                  .getChild( Constants.INSERT_FRAME )
                                                  .getChild( Constants.INSERT_AGGREGATION )
                                                  .getChildText( Constants.FIRST_QUESTION );
        this._insertAggregationSecond =
                _germanText.getRootElement().getChild( Constants.FRAME )
                           .getChild( Constants.INSERT_FRAME ).getChild( Constants.INSERT_AGGREGATION )
                           .getChildText( Constants.SECOND_QUESTION );
        this._insertCompositionFirst = _germanText.getRootElement().getChild( Constants.FRAME )
                                                  .getChild( Constants.INSERT_FRAME )
                                                  .getChild( Constants.INSERT_COMPOSITION )
                                                  .getChildText( Constants.FIRST_QUESTION );
        this._insertCompositionSecond =
                _germanText.getRootElement().getChild( Constants.FRAME )
                           .getChild( Constants.INSERT_FRAME ).getChild( Constants.INSERT_COMPOSITION )
                           .getChildText( Constants.SECOND_QUESTION );

        // Shortcuts
        this._scA = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "a" );
        this._scG = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "g" );
        this._scK = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "k" );
        this._scM = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "m" );
        this._scO = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "o" );
        this._scS = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "s" );
        this._scV = _germanText.getRootElement().getChild( Constants.SHORTCUT ).getChildText( "v" );

        // WARNUNGEN
        this._warningUndo = _germanText.getRootElement().getChild( Constants.WARNING )
                                       .getChildText( Constants.NO_UNDO );
        this._warningRedo = _germanText.getRootElement().getChild( Constants.WARNING )
                                       .getChildText( Constants.NO_REDO );

        // Texte f�r die �berpr�fung
        this._solution = _germanText.getRootElement().getChildText( Constants.SOLUTION );
    }

    public String getSOLUTION() {
        return this._solution;
    }

    /**
     * @return the errorXml
     */
    public String getErrorXML() {
        return this._errorXml;
    }

    /**
     * @return the loadError
     */
    public String getLoadError() {
        return this._loadError;
    }

    /**
     * @return the taskHead
     */
    public String getTaskHead() {
        return this._taskHead;
    }

    /**
     * @return the delete
     */
    public String getDelete() {
        return this._delete;
    }

    /**
     * @return the header
     */
    public String getHeader() {
        return this._header;
    }

    /**
     * @return the labelText
     */
    public String getLabelText() {
        return this._labelText;
    }

    /**
     * @return the newAggregation
     */
    public String getNewAggregation() {
        return this._newAggregation;
    }

    /**
     * @return the newAssociation
     */
    public String getNewAssociation() {
        return this._newAssociation;
    }

    /**
     * @return the newAttribute
     */
    public String getNewAttribute() {
        return this._newAttribute;
    }

    /**
     * @return the newClass
     */
    public String getNewClass() {
        return this._newClass;
    }

    /**
     * @return the newComposition
     */
    public String getNewComposition() {
        return this._newComposition;
    }

    /**
     * @return the newInheritance
     */
    public String getNewInheritance() {
        return this._newInheritance;
    }

    /**
     * @return the newOperation
     */
    public String getNewOperation() {
        return this._newOperation;
    }

    /**
     * @return the save
     */
    public String getSave() {
        return this._save;
    }

    /**
     * @return the start
     */
    public String getStart() {
        return this._start;
    }

    /**
     * @return the cancelButton
     */
    public String getCancelButton() {
        return this._cancelButton;
    }

    /**
     * @return the deleteAggregation
     */
    public String getDeleteAggregation() {
        return this._deleteAggregation;
    }

    /**
     * @return the deleteAssociation
     */
    public String getDeleteAssociation() {
        return this._deleteAssociation;
    }

    /**
     * @return the deleteAttirbute
     */
    public String getDeleteAttirbute() {
        return this._deleteAttirbute;
    }

    /**
     * @return the deleteClass
     */
    public String getDeleteClass() {
        return this._deleteClass;
    }

    /**
     * @return the deleteComposition
     */
    public String getDeleteComposition() {
        return this._deleteComposition;
    }

    /**
     * @return the deleteInheritance
     */
    public String getDeleteInheritance() {
        return this._deleteInheritance;
    }

    /**
     * @return the deleteOperation
     */
    public String getDeleteOperation() {
        return _deleteOperation;
    }

    /**
     * @return the okButton
     */
    public String getOkButton() {
        return _okButton;
    }

    /**
     * @return the question1
     */
    public String getQuestion1() {
        return _question1;
    }

    /**
     * @return the question2
     */
    public String getQuestion2() {
        return _question2;
    }

    /**
     * @return the lOAD
     */
    public String getLoad() {
        return _load;
    }

    /**
     * @return the germanText
     */
    public static Document getGermanText() {
        return _germanText;
    }

    /**
     * @return the buttonHeader
     */
    public String getbButtonHeader() {
        return _buttonHeader;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return _className;
    }

    /**
     * @return the ttDelete
     */
    public String getTtDelete() {
        return _ttDelete;
    }

    /**
     * @return the ttLoad
     */
    public String getTtLoad() {
        return _ttLoad;
    }

    /**
     * @return the ttNewAggregation
     */
    public String getTtNewAggregation() {
        return _ttNewAggregation;
    }

    /**
     * @return the ttNewAssociation
     */
    public String getTtNewAssociation() {
        return _ttNewAssociation;
    }

    /**
     * @return the ttNewAttribute
     */
    public String getTtNewAttribute() {
        return _ttNewAttribute;
    }

    /**
     * @return the ttNewClass
     */
    public String getTtNewClass() {
        return _ttNewClass;
    }

    /**
     * @return the ttNewComposition
     */
    public String getTtNewComposition() {
        return _ttNewComposition;
    }

    /**
     * @return the ttNewInheritance
     */
    public String getTtNewInheritance() {
        return _ttNewInheritance;
    }

    /**
     * @return the ttNewOperation
     */
    public String getTtNewOperation() {
        return _ttNewOperation;
    }

    /**
     * @return the ttSave
     */
    public String getTtSave() {
        return _ttSave;
    }

    /**
     * @return the insertAggregationFirst
     */
    public String getInsertAggregationFirst() {
        return _insertAggregationFirst;
    }

    /**
     * @return the insertAggregationSecond
     */
    public String getinsertAggregationSecond() {
        return _insertAggregationSecond;
    }

    /**
     * @return the insertAssociationFirst
     */
    public String getInsertAssociationFirst() {
        return _insertAssociationFirst;
    }

    /**
     * @return the insertAssociationSecond
     */
    public String getInsertAssociationSecond() {
        return _insertAssociationSecond;
    }

    /**
     * @return the insertCompositionFirst
     */
    public String getInsertcompositionFirst() {
        return _insertCompositionFirst;
    }

    /**
     * @return the insertCompositionSecond
     */
    public String getInsertCompositionSecond() {
        return _insertCompositionSecond;
    }

    /**
     * @return the insertInheritanceFirst
     */
    public String getInsertInheritanceFirst() {
        return _insertInheritanceFirst;
    }

    /**
     * @return the insertInheritanceSecond
     */
    public String getinsertInheritanceSecond() {
        return _insertInheritanceSecond;
    }

    /**
     * @return the undo
     */
    public String getUndo() {
        return _undo;
    }

    /**
     * @return the ttUndo
     */
    public String getTt_Undo() {
        return _ttUndo;
    }

    /**
     * @return the ttRedo
     */
    public String getTt_Redo() {
        return _ttRedo;
    }

    /**
     * @return the insertAttributeFirst
     */
    public String getInsertAttributeFirst() {
        return _insertAttributeFirst;
    }

    /**
     * @return the insertAttributeSecond
     */
    public String getInsertAttributeSecond() {
        return _insertAttributeSecond;
    }

    /**
     * @return the insertOperationFirst
     */
    public String getInsertOperationFirst() {
        return _insertMethodFirst;
    }

    /**
     * @return the insertOperationSecond
     */
    public String getInsertOperationSecond() {
        return _insertMethodSecond;
    }

    /**
     * @return the insertClass
     */
    public String getInsertClass() {
        return _insertClass;
    }

    /**
     * @return the startNew
     */
    public String getStartNew() {
        return _startNew;
    }

    /**
     * @return the ttStartNew
     */
    public String getTtStartNew() {
        return _ttStartNew;
    }

    /**
     * @return the scA
     */
    public String getScA() {
        return this._scA;
    }

    /**
     * @return the scG
     */
    public String getScG() {
        return this._scG;
    }

    /**
     * @return the scK
     */
    public String getScK() {
        return this._scK;
    }

    /**
     * @return the scM
     */
    public String getScM() {
        return this._scM;
    }

    /**
     * @return the scO
     */
    public String getScO() {
        return this._scO;
    }

    /**
     * @return the scS
     */
    public String getScS() {
        return this._scS;
    }

    /**
     * @return the scV
     */
    public String getScV() {
        return this._scV;
    }

    /**
     * @return the warningRedo
     */
    public String getWarningRedo() {
        return this._warningRedo;
    }

    /**
     * @return the warningUndo
     */
    public String getWarningUndo() {
        return this._warningUndo;
    }

    /**
     * @return the ttEval
     */
    public String getTtEval() {
        return this._ttEval;
    }

    /**
     * @return the eval
     */
    public String getEval() {
        return this._eval;
    }

    /**
     * @return the graphChange
     */
    public String getGraphChange() {
        return this._graphChange;
    }

}