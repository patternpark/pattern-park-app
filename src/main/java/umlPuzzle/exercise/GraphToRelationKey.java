package umlPuzzle.exercise;

import java.util.Objects;

public final class GraphToRelationKey {

    private final int _graphConstant;
    private final boolean _graphFilled;

    public GraphToRelationKey( int graphConstant, boolean graphFilled ) {
        this._graphConstant = graphConstant;
        this._graphFilled = graphFilled;
    }

    public int getGraphConstant() {
        return this._graphConstant;
    }

    public boolean getGraphFilled() {
        return this._graphFilled;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null || getClass() != obj.getClass() ) {
            return false;
        }
        GraphToRelationKey that = (GraphToRelationKey) obj;
        return _graphConstant == that.getGraphConstant() && _graphFilled == that.getGraphFilled();
    }

    @Override
    public int hashCode() {
        return Objects.hash( _graphConstant, _graphFilled );
    }
}
