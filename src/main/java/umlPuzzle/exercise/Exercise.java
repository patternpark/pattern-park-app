package umlPuzzle.exercise;

import emi.util.XmlUtil;
import org.jdom.Document;
import org.jdom.Element;
import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelEvent.GraphModelChange;
import org.jgraph.event.GraphModelListener;
import org.jgraph.graph.*;
import umlPuzzle.classview.ClassCell;
import umlPuzzle.classview.ClassWrapper;
import umlPuzzle.config.ExerciseConstants;
import umlPuzzle.enums.ERelationType;
import umlPuzzle.enums.EXMLElementType;
import umlPuzzle.exercise.representations.*;
import umlPuzzle.interfaces.UndoObserver;
import umlPuzzle.panels.DrawPanel;
import umlPuzzle.utility.Undo;

import javax.swing.JOptionPane;
import java.awt.geom.Rectangle2D;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Lars Friedrich and Demian Franke (originally), NorthernSeaCharting and Soto (refactoring)
 * <p>
 * Diese Klasse enth�lt die wichtigsten Inhalte der Klasse ClassGraph
 * und GraphEd und ersetzt sie somit. Hier befindet sich eine Lsung
 * fr ein Panel Node welches weitere Swing Componenten enthlt
 * wie z.B. zwei JTable, Labels und eine Checkbox im Gegensatz zu dem
 * JLabel Component der ersten Version.
 */
public class Exercise implements GraphModelListener, UndoObserver {

    /**
     * XML-Dateipfad zu der XML-Datei des Puzzles.
     */
    private final String _xmlFile;

    /**
     * XML file path pointing to the pattern's solution.
     */
    private final String _xmlSolutionPath;
    /**
     * Represents the object, which is responsible for the visualisation of the exercise.
     */
    private final ExerciseGraph _exerciseGraph;
    /**
     * Represents the panel onto which the visual representation of the exercise is drawn.
     */
    private final DrawPanel _drawPanel;
    /**
     * Class that contains the current UMLPuzzle and the corresponding solution.
     */
    private final ExerciseUMLPuzzleContainer _exerciseUMLContainer;
    /**
     * der Undo-Manager
     */
    private final Undo _undo;

    /**
     * Describes whether a change made to the graph is to be added to the undo-list.
     */
    private boolean _addToUndo = false;
    /**
     * Describes whether a xml file is to be loaded into the application.
     */
    private boolean _load = false;
    /**
     * States whether changes to the uml puzzle are supposed to be written into the undo-file of the pattern.
     */
    private boolean _change = false;

    /**
     * Object representing the loaded content of the pattern xml file (including the classes and attributes).
     */
    private Document _document;
    /**
     * Represents the loaded content of the pattern's solution xml file.
     */
    private Document _solutionDoc;
    /**
     * List of all classes which have been entered into either the current undo file of the pattern or at the beginning
     * simply the pattern's uml puzzle xml file.
     */
    private List<Element> _classList;

    /**
     * @author Demian Franke (originally), NorthernSeaCharting and Soto (refactoring)
     */
    public Exercise(
            String xmlFilePath, String solutionPath, boolean regenerate, DrawPanel draw
    ) {
        this._xmlFile = xmlFilePath;
        this._xmlSolutionPath = solutionPath;

        this._drawPanel = draw;

        this.initDocuments();
        this._exerciseUMLContainer = new ExerciseUMLPuzzleContainer(
                this.generateClassesFromClassList( this._classList ),
                this.generateClassesFromClassList(
                        this._solutionDoc.getRootElement().getChildren( EXMLElementType.UML_CLASS.getXMLLabel() )
                ),
                Constants.SUCCESS_TEXT
        );

        this._undo = new Undo();
        this._undo.addObserver( this );

        this._exerciseGraph = new ExerciseGraph( this._drawPanel, this );
        this._exerciseGraph.setUpGraph();

        if ( regenerate ) {
            this._exerciseGraph.reInit();
        } else {
            this.init( this._xmlFile );
        }
        this._undo.addObserver( this._exerciseGraph );

    }

    /**
     * Hier werden alle Documents initialisiert!
     */
    public void initDocuments() {
        // Initialisierung der XML-Datei.
        this._document = XmlUtil.readXMLDocument( this._xmlFile );
        this._solutionDoc = XmlUtil.readXMLDocument( this._xmlSolutionPath );
        this.setClassListFromDocument();
    }

    public void setClassListFromDocument() {
        this._classList =
                (List<Element>) this._document.getRootElement().getChildren( EXMLElementType.UML_CLASS.getXMLLabel() );
    }

    /**
     * Diese Methode wird immer dann aufgerufen wenn sich am JGraph
     * etwas �ndert. Beispiele: �ndern der Position einer ClassCell(UML Klasse). Hinzufgen,
     * ndern oder l�schen eines Bestandteil einer ClassCell oder einer DefaultEdge (z.B. Name �ndern, Operation
     * hinzuf�gen, hinzuf�gen einer ClassCell). DefaultEdges repr�sentieren Beziehungen zwischen
     * Klassen z.B. Vererbungen, Kompositionen oder Assoziationen.
     * Auerdem mu� sp�testens bei Aufruf dieser Methode ein neuer Undo-Schritt hinzugef�gt werden damit keine
     * Informationen (Zwischenschritte) verloren gehen.
     * Auerdem findet dabei die Synchronisation der Model Schicht des JGraph
     * sowie des JDom Documents statt.
     *
     * @param graphModelEvent the GraphModelEvent
     * @author Demian Franke
     */
    public void graphChanged( GraphModelEvent graphModelEvent ) {
        // überflüssige Schritte überschreiben
        GraphModelChange changedModel = graphModelEvent.getChange();
        Object[] changedCells = changedModel.getChanged();
        try {
            // temporäre Variable, die pro Aufruf dieser Methode
            // mit der ClassCell belegt wird, welche zuletzt gendert wurde
            if ( changedCells.length == Constants.EDGE_COUNT_SINGLE_CLASS_CHANGE ) {
                this.handleSingleClassChange( changedCells );
            } else if ( changedCells.length == Constants.EDGE_COUNT_RELATION_CHANGE ) {
                this.handleSingleEdgeChange( changedCells[ 0 ] );
            }
        } catch ( NullPointerException ignored ) {
        }

        if ( this._addToUndo ) {
            this._undo.addUndoStep( this._classList );
        } else if ( !this._change ) {
            // reset undo-Datei bei Systemstart
            this._undo.reset();
            this._addToUndo = true;
            this._change = true;
        }
        this._exerciseUMLContainer.setPuzzleToBeChecked( this.generateClassesFromClassList( this._classList ) );
    }

    /*
     * Überprüfung der Eingaben
     *
     * @author Demian Franke (für Kompositum und Schablonenmethode)
     * @author Lars Friedrich (andere)
     */
    public String evaluateExercises() {
        String[] tips = this._exerciseUMLContainer.evaluateTask();
        return tips.length > 0 ? String.join( "\n", tips ) : this._exerciseUMLContainer.getSuccessText();
    }

    /**
     * Initialisierung
     *
     * @author Lars Friedrich, Demian Franke
     */
    public void init( String fileName ) {
        try {
            this.loadClassList( fileName );
        } catch ( InvalidParameterException exception ) {
            System.out.println( exception.getMessage() );
            return;
        }

        // Initialisierung der JGraph Elemente.
        this._exerciseGraph.initGraph( this._classList.stream().toArray( Element[]::new ) );

        try {
            this._exerciseGraph.setUpGraph();
        } catch ( NullPointerException ignore ) {
        }
    }

    public void init( List<Element> classList ) {
        this._classList = classList;

        this._exerciseGraph.initGraph( classList.stream().toArray( Element[]::new ) );

        try {
            this._exerciseGraph.setUpGraph();
        } catch ( NullPointerException ignore ) {
        }
    }

    /**
     * Methode zur Erstellung einer neuen Klasse
     *
     * @param name         Name der neuen Klasse
     * @param xCoordinates xCoordinates-Wert im Graph (in der Regel 10)
     * @param yCoordinates yCoordinates-Wert im Graph (in der Regel 10)
     * @author Lars Friedrich, Demian Franke
     */
    public void addClass( String name, int xCoordinates, int yCoordinates ) {
        // a new class element is created
        Element classToAdd = new Element( EXMLElementType.UML_CLASS.getXMLLabel() );

        // the class will receive the new values
        Element nameOfClass = new Element( EXMLElementType.NAME.getXMLLabel() );
        nameOfClass.setText( name );
        Element xCoordEl = new Element( EXMLElementType.XCOORD.getXMLLabel() );
        xCoordEl.setText( String.valueOf( xCoordinates ) );
        Element yCoordEl = new Element( EXMLElementType.YCOORD.getXMLLabel() );
        yCoordEl.setText( String.valueOf( yCoordinates ) );

        classToAdd.addContent( nameOfClass );
        classToAdd.addContent( xCoordEl );
        classToAdd.addContent( yCoordEl );

        // die Liste classList wird aktualisiert
        this._classList.add( classToAdd );

        // Undo-Schritt eintragen
        this._undo.addUndoStep( this._classList );
    }

    public void showNewGraph( String fileName ) {
        this._addToUndo = false;
        this._exerciseGraph.clearGraph( this );
        this.init( fileName );
        this._addToUndo = true;
    }

    public void showNewGraph( List<Element> classList ) {
        this._addToUndo = false;
        this._exerciseGraph.clearGraph( this );
        this.init( classList );
        this._addToUndo = true;
    }

    //region setters and getters

    /**
     * @return the classList
     */
    public List<Element> getClassList() {
        return this._classList;
    }

    /**
     * @return the d
     */
    public Document getDocument() {
        return this._document;
    }

    /**
     * @return the datei
     */
    public String getDatei() {
        return this._xmlFile;
    }

    /**
     * @param start the Start to set
     */
    public void setAddToUndo( boolean start ) {
        this._addToUndo = start;
    }

    /**
     * @param load the load to set
     */
    public void setLoad( boolean load ) {
        this._load = load;
    }

    /**
     * @return the undo
     */
    public Undo getUndo() {
        return this._undo;
    }
    //endregion

    /**
     * Suche von doppelten Klassennamen
     *
     * @param newName the newName from the gui user dialog gets checked
     * @return nameDuplikat false if the name exists
     * @author Demian Franke
     */
    public boolean checkForValidClassName( String newName ) {
        if ( newName.trim().isEmpty() ) {
            return false;
        }

        for ( Element element : this._classList ) {
            String checkName = element.getChild( EXMLElementType.NAME.getXMLLabel() ).getText();
            if ( newName.equalsIgnoreCase( checkName ) ) {
                return false;
            }
        }

        return !Character.isLowerCase( newName.charAt( 0 ) ) && Character.isJavaIdentifierStart( newName.charAt( 0 ) );
    }

    public boolean checkForValidClassName( String newName, Element currentElement ) {
        if ( newName.trim().isEmpty() ) {
            return false;
        }

        Optional<Element> duplicateElement = this._classList.stream().filter(
                ( potentialDuplicate ) -> !potentialDuplicate.equals( currentElement ) &&
                        potentialDuplicate.getChildText( EXMLElementType.NAME.getXMLLabel() ).equals( newName )
        ).findFirst();

        return !duplicateElement.isPresent() && !Character.isLowerCase( newName.charAt( 0 ) ) &&
                Character.isJavaIdentifierStart( newName.charAt( 0 ) );
    }

    public void addCellForClass( String name, int x, int y ) {
        // Vorbereiten der Arrays mit den Attributen und Operationen
        this._exerciseGraph.addCellForClass( name, x, y );
    }

    @Override
    public void reactToStepAdded() {
    }

    @Override
    public void reactToUndo( List<Element> classList ) {
        this.showNewGraph( classList );
    }

    @Override
    public void reactToRedo( List<Element> classList ) {
        this.showNewGraph( classList );
    }

    @Override
    public void reactToStepNotPossible( int typeOfWarning ) {
    }

    private void loadClassList( String fileName ) {
        if ( !this._load ) {
            return;
        }

        this._document = XmlUtil.readXMLDocument( fileName );

        if ( this._load && !this._document.getRootElement().getName().equals( "umlmuster" ) ) {
            JOptionPane.showMessageDialog(
                    null,
                    this._drawPanel.getButton().getText().getErrorXML(),
                    this._drawPanel.getButton().getText().getLoadError(),
                    JOptionPane.ERROR_MESSAGE
            );
            init( this._xmlFile );
            throw new InvalidParameterException( "The given file does not contain the valid syntax for a pattern!" );
        }
        this._load = false;
        this._classList = this._document.getRootElement().getChildren( EXMLElementType.UML_CLASS.getXMLLabel() );
    }

    private void handleSingleClassChange( Object[] changedCells ) {
        ClassCell changedCell;
        // die Lnge 5 ensteht bei Änderungen die eine ganze
        // ClassCell betreffen. In diesem Fall besteht changedCells aus
        // einer ClassCell sowie den 4 zugehrigen Ports. Der Fall tritt
        // beim Hinzufgen und dem Verschieben einer Klasse auf.
        for ( Object potentialCell : changedCells ) {
            if ( !( potentialCell instanceof ClassCell ) ) {
                continue;
            }

            changedCell = (ClassCell) potentialCell;
            // da die Ports in XML nicht gespeichert werden müssen
            // wird hier nach der ClassCell im Array gesucht.
            Optional<Element> elementOpt = this._classList.stream().filter(
                    ( umlClass ) -> umlClass.getChildText(
                            EXMLElementType.NAME.getXMLLabel()
                    ).equals( ( (ClassCell) potentialCell ).myWrapper.getLabel() )
            ).findFirst();

            if ( elementOpt.isPresent() ) {
                Element currentElement = elementOpt.get();
                Rectangle2D rectangle2D = GraphConstants.getBounds( changedCell.getAttributes() );
                currentElement.getChild( EXMLElementType.XCOORD.getXMLLabel() )
                              .setText( String.valueOf( rectangle2D.getX() ) );
                currentElement.getChild( EXMLElementType.YCOORD.getXMLLabel() )
                              .setText( String.valueOf( rectangle2D.getY() ) );
            }
        }
    }

    private void handleSingleEdgeChange( Object changedCell ) {
        // bei Änderungen an DefaultEdges sowie Bestandteilen einer ClassCell
        // hat changedCells die Länge 1.
        if ( changedCell instanceof DefaultEdge ) {
            this.handleRelationChange();
        } else {
            this.handleClassMemberChange( changedCell );
        }
    }

    private void handleRelationChange() {
        // Da DefaultEdges nicht ohne ein spezielles Attribut einer konkreten
        // Assoziation in der Datenschicht zugeordnet werden knnen,
        // werden bei nderung/ Lschung einer Beziehung smtliche Assoziationen,
        // Kompositionen, Aggregationen sowie Vererbungsbeziehungen
        // im JDom Document neu beschrieben.
        for ( Element currentElement : this._classList ) {
            // Löschen
            for ( ERelationType type : ERelationType.values() ) {
                currentElement.removeChildren( type.getXMLLabel() );
            }
        }

        DefaultGraphCell[] cells = this._exerciseGraph.getCells();
        for ( int edges = this._classList.size(); edges < cells.length; ++edges ) {
            // DefaultEdges werden in dem cells-Array stets nach den ClassCells
            // abgelegt. Die erste Edge befindet sich bei cells[classList.size()]
            // classList beinhaltet alle UML-Klassen.
            // cells speichert auf den ersten Speicherpltzen alle ClassCells
            // sowie anschlieend alle DefaultEdges. Diese Struktur ist erforderlich
            // um die Edges bei Initialisierung des Graphen zu Cells hinzuzufgen.
            // Werden nachtrglich Klassen angefgt, so wird diese Reihenfolge
            // beibehalten. Siehe: addClass().
            DefaultEdge defaultEdge = (DefaultEdge) cells[ edges ];
            // Jeweilige Ausgangsklasse einer Beziehung.
            DefaultPort sourcePort = (DefaultPort) defaultEdge.getSource();
            ClassCell sourceClassCell = (ClassCell) sourcePort.getParent();
            String sourceLabel = ( sourceClassCell.myWrapper.getLabel() );

            // Ziel einer Beziehung (z.B. bei einer Vererbung die Oberklasse).
            DefaultPort targetPort = (DefaultPort) defaultEdge.getTarget();
            ClassCell targetClassCell = (ClassCell) targetPort.getParent();
            String relTargetLabel = ( targetClassCell.myWrapper.getLabel() );

            AttributeMap attributeMap = defaultEdge.getAttributes();
            int arrowForm = GraphConstants.getLineEnd( attributeMap );
            boolean isFill = GraphConstants.isEndFill( attributeMap );

            // Die Art einer Beziehung ist im XML Dokument durch den Elementname
            // beschrieben. Um vom JGraph Model wieder die erforderlichen Informationen
            // zwecks Beziehungsart auszulesen wird die Pfeilform sowie Fllung einer
            // Edge ausgewertet. Vergleiche in der Methode init() den Umgekehrten
            // Vorgang der Erstellung der DefaultEdges anhand Informationen aus den
            // XML Elementen.

            for ( Element current : this._classList ) {
                if ( current.getChild( EXMLElementType.NAME.getXMLLabel() ).getText().equals( sourceLabel ) ) {
                    // Speicherort fr Beziehungen in den XML Dokumenten sind die
                    // jeweiligen "sourceLabel-Klassen" welche hier gesucht werden.
                    Element relationElement = new Element(
                            Constants.GRAPH_TO_RELATION_MAP.get( new GraphToRelationKey( arrowForm, isFill ) )
                                                           .getXMLLabel()
                    );
                    relationElement.setText( relTargetLabel );
                    current.addContent( relationElement );
                }
            }

        }
    }

    private void handleClassMemberChange( Object changedCellObject ) {
        // d.h. ein Element des GraphModels wurde gendert
        // und zwar keines vom Typ DefaultEdge also eine ClassCell
        ClassCell changedCell = (ClassCell) changedCellObject;
        ClassCell[] cells = Arrays.stream( this._exerciseGraph.getCells() )
                                  .filter( ClassCell.class::isInstance )
                                  .collect( Collectors.toList() ).stream().toArray( ClassCell[]::new );
        for ( int i = 0; i < this._classList.size(); ++i ) {
            if ( cells[ i ].equals( changedCell ) ) {
                Element currentElement = this._classList.get( i );

                String nameCellOld = currentElement.getChild( EXMLElementType.NAME.getXMLLabel() ).getText();
                boolean isAbstract = changedCell.myWrapper.isAbstract;
                boolean isInterface = changedCell.myWrapper.isInterface;

                String nameCell = changedCell.myWrapper.getLabel().trim();
                Rectangle2D r = GraphConstants.getBounds( changedCell.getAttributes() );
                currentElement.getChild( EXMLElementType.XCOORD.getXMLLabel() ).setText( String.valueOf( r.getX() ) );
                currentElement.getChild( EXMLElementType.YCOORD.getXMLLabel() ).setText( String.valueOf( r.getY() ) );

                boolean nameIsValid = this.checkForValidClassName( nameCell, currentElement );

                if ( nameIsValid ) {
                    currentElement.getChild( EXMLElementType.NAME.getXMLLabel() ).setText( nameCell );
                    this.changeRelationsToNewClassname( nameCellOld, nameCell );
                } else {
                    this.reactToInvalidClassName( changedCell, nameCellOld );
                }
                this.replaceElement(
                        currentElement, EXMLElementType.ABSTRACT.getXMLLabel(), String.format( "%b", isAbstract )
                );
                this.replaceElement(
                        currentElement, EXMLElementType.INTERFACE.getXMLLabel(), String.format( "%b", isInterface )
                );

                ClassWrapper classWrapper = (ClassWrapper) GraphConstants.getValue(
                        changedCell.getAttributes()
                );
                currentElement.removeChildren( EXMLElementType.ATTRIBUTE.getXMLLabel() );
                currentElement.removeChildren( EXMLElementType.METHOD.getXMLLabel() );

                currentElement.addContent(
                        this.createAttributeOrMethodElement(
                                classWrapper.attributeData,
                                EXMLElementType.ATTRIBUTE.getXMLLabel()
                        )
                );
                currentElement.addContent(
                        this.createAttributeOrMethodElement(
                                classWrapper.methodData, EXMLElementType.METHOD.getXMLLabel() )
                );

                break;
            }
        }
    }

    private void replaceElement( Element parentElement, String elementName, String textOfElement ) {
        Element replacementElement = new Element( elementName );
        replacementElement.setText( textOfElement );
        parentElement.removeChild( elementName );
        parentElement.addContent( replacementElement );
    }

    private void reactToInvalidClassName( ClassCell changedCell, String oldLabel ) {
        changedCell.myWrapper.setLabel( oldLabel );

        this._drawPanel.getButton().getHelpPanel().setHelp(
                ExerciseConstants.WARNING_INVALID_CLASSNAME
        );
        this._drawPanel.getButton().getHelpPanel().repaint();
    }

    private ArrayList<Element> createAttributeOrMethodElement(
            Object[][] xmlElements,
            String xmlElementName
    ) {
        ArrayList<Element> elementArrayList = new ArrayList<>();
        for ( Object[] objects : xmlElements ) {
            if ( ( (String) objects[ 0 ] ).isEmpty() || ( (String) objects[ 1 ] ).isEmpty() ) {
                continue;
            }

            Element element = new Element( xmlElementName );
            Element elementType = new Element( EXMLElementType.DATATYPE.getXMLLabel() );
            Element elementUMLName = new Element( EXMLElementType.NAME.getXMLLabel() );

            elementType.setText( ( (String) objects[ 0 ] ).trim() );
            elementUMLName.setText( ( (String) objects[ 1 ] ).trim() );

            element.addContent( elementType );
            element.addContent( elementUMLName );

            elementArrayList.add( element );
        }

        return elementArrayList;
    }

    private ExerciseUMLPuzzle generateClassesFromClassList(
            List<Element> classList
    ) {
        List<ExerciseUMLClass> createdClasses = new ArrayList<>();
        for ( Element classFromUML : classList ) {
            Element isAbstractItem = classFromUML.getChild( EXMLElementType.ABSTRACT.getXMLLabel() );
            Element isInterfaceItem = classFromUML.getChild( EXMLElementType.INTERFACE.getXMLLabel() );
            boolean isAbstract = isAbstractItem != null &&
                    isAbstractItem.getText().equals( Constants.IS_ABSTRACT_STRING );
            boolean isInterface = isInterfaceItem != null &&
                    isInterfaceItem.getText().equals( Constants.IS_INTERFACE_STRING );

            ExerciseUMLClass umlClass =
                    new ExerciseUMLClass(
                            classFromUML.getChildText( EXMLElementType.NAME.getXMLLabel() ), isAbstract, isInterface
                    );
            List<Element> operations = (List<Element>) classFromUML.getChildren( EXMLElementType.METHOD.getXMLLabel() );
            List<Element> attributes = (List<Element>) classFromUML.getChildren(
                    EXMLElementType.ATTRIBUTE.getXMLLabel() );

            for ( Element operationFromUML : operations ) {
                umlClass.addUMLOperations( new ExerciseUMLMethod(
                        operationFromUML.getChildText( EXMLElementType.NAME.getXMLLabel() ),
                        operationFromUML.getChildText( EXMLElementType.DATATYPE.getXMLLabel() )
                ) );
            }

            for ( Element attributeFromUML : attributes ) {
                umlClass.addUMLAttributes( new ExerciseUMLAttribute(
                        attributeFromUML.getChildText( EXMLElementType.NAME.getXMLLabel() ),
                        attributeFromUML.getChildText( EXMLElementType.DATATYPE.getXMLLabel() )
                ) );
            }

            for ( ERelationType relationType : ERelationType.values() ) {
                List<Element> relations = (List<Element>) classFromUML.getChildren( relationType.getXMLLabel() );

                for ( Element relation : relations ) {
                    umlClass.addUMLRelations( new ExerciseUMLRelation( relationType, relation.getText() ) );
                }
            }
            createdClasses.add( umlClass );
        }

        return new ExerciseUMLPuzzle( createdClasses.stream().toArray( ExerciseUMLClass[]::new ) );
    }

    private void changeRelationsToNewClassname( String oldName, String newName ) {
        for ( Element currentElement : this._classList ) {
            for ( ERelationType relationType : ERelationType.values() ) {
                List<Element> relationList = currentElement.getChildren( relationType.getXMLLabel() );

                for ( Element relation : relationList ) {
                    if ( relation.getText().equals( oldName ) ) {
                        relation.setText( newName );
                    }
                }
            }
        }
    }
}