package umlPuzzle.exercise.representations;

import umlPuzzle.enums.ERelationType;

import java.util.Objects;

public class ExerciseUMLRelation {

    private final ERelationType _relationType;
    private final String _relationTargetName;

    public ExerciseUMLRelation(
            final ERelationType relationType,
            final String relationTargetName
    ) {
        super();
        this._relationType = relationType;
        this._relationTargetName = relationTargetName;
    }

    public ERelationType getRelationType() {
        return this._relationType;
    }

    public String getRelationTarget() {
        return this._relationTargetName;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        ExerciseUMLRelation that = (ExerciseUMLRelation) o;
        return this._relationType == that.getRelationType() && this._relationTargetName.equals(
                that.getRelationTarget() );
    }

    @Override
    public int hashCode() {
        return Objects.hash( this._relationType, this._relationTargetName );
    }
}
