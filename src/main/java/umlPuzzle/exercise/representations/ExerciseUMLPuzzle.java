package umlPuzzle.exercise.representations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class ExerciseUMLPuzzle {
    private final ExerciseUMLClass[] _umlClasses;

    public ExerciseUMLPuzzle( final ExerciseUMLClass[] givenClasses ) {
        this._umlClasses = givenClasses;
    }

    public ExerciseUMLClass[] getUmlClasses() {
        return _umlClasses;
    }

    public String[] check( ExerciseUMLPuzzle puzzleToBeChecked ) {
        ArrayList<String> tips = new ArrayList<>();
        ExerciseUMLClass[] classesToBeChecked = puzzleToBeChecked.getUmlClasses();

        if ( classesToBeChecked.length > this._umlClasses.length ) {
            tips.add( Constants.TIP_TOO_MANY_CLASSES );
        }

        for ( ExerciseUMLClass solutionClass : this._umlClasses ) {
            Optional<ExerciseUMLClass> correspondingClass = Arrays.stream( classesToBeChecked ).filter(
                    ( potentialClass ) -> potentialClass.getClassName().equals( solutionClass.getClassName() )
            ).findFirst();

            if ( !correspondingClass.isPresent() ) {
                tips.add( String.format( Constants.TIP_MISSING_CLASS, solutionClass.getClassName() ) );
                continue;
            }

            tips.addAll( Arrays.asList( solutionClass.check( correspondingClass.get() ) ) );
        }

        return tips.stream().toArray( String[]::new );
    }
}
