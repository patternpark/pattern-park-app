package umlPuzzle.exercise.representations;

public class ExerciseUMLPuzzleContainer {
    private ExerciseUMLPuzzle _puzzleToBeChecked;
    private final ExerciseUMLPuzzle _puzzleSolution;
    private final String _successText;

    public ExerciseUMLPuzzleContainer(
            final ExerciseUMLPuzzle puzzleToBeChecked,
            final ExerciseUMLPuzzle puzzleSolution,
            final String givenSuccessText
    ) {
        this._puzzleToBeChecked = puzzleToBeChecked;
        this._puzzleSolution = puzzleSolution;
        this._successText = givenSuccessText;
    }

    public void setPuzzleToBeChecked( ExerciseUMLPuzzle exerciseUMLPuzzle ) {
        this._puzzleToBeChecked = exerciseUMLPuzzle;
    }

    public String getSuccessText() {
        return this._successText;
    }

    public String[] evaluateTask() {
        return this._puzzleSolution.check( this._puzzleToBeChecked );
    }
}
