package umlPuzzle.exercise.representations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class ExerciseUMLClass {

    private final String _className;
    private final ArrayList<ExerciseUMLAttribute> _attributes = new ArrayList<>();
    private final ArrayList<ExerciseUMLMethod> _methods = new ArrayList<>();
    private final ArrayList<ExerciseUMLRelation> _relations = new ArrayList<>();
    private final boolean _isAbstract;
    private final boolean _isInterface;

    public ExerciseUMLClass(
            final String className, boolean isAbstract,
            boolean isInterface
    ) {
        super();
        this._className = className;
        this._isAbstract = isAbstract;
        this._isInterface = isInterface;
    }

    public String getClassName() {
        return this._className;
    }

    public ExerciseUMLAttribute[] getAttributes() {
        return this._attributes.stream().toArray( ExerciseUMLAttribute[]::new );
    }

    public ExerciseUMLMethod[] getOperations() {
        return this._methods.stream().toArray( ExerciseUMLMethod[]::new );
    }

    public ExerciseUMLRelation[] getRelations() {
        return this._relations.stream().toArray( ExerciseUMLRelation[]::new );
    }

    public boolean getIsAbstract() {
        return this._isAbstract;
    }

    public boolean getIsInterface() {
        return this._isInterface;
    }

    public void addUMLAttributes( ExerciseUMLAttribute... attributes ) {
        this._attributes.addAll( Arrays.asList( attributes ) );
    }

    public void addUMLOperations( ExerciseUMLMethod... operations ) {
        this._methods.addAll( Arrays.asList( operations ) );
    }

    public void addUMLRelations( ExerciseUMLRelation... relations ) {
        this._relations.addAll( Arrays.asList( relations ) );
    }

    public String[] check( ExerciseUMLClass classToCheck ) {
        ArrayList<String> tips = new ArrayList<>();
        if ( !this._isAbstract == classToCheck.getIsAbstract() ) {
            tips.add( String.format( Constants.TIP_MISSING_ABSTRACT, this._className ) );
        }

        if ( !this._isInterface == classToCheck.getIsInterface() ) {
            tips.add( String.format( Constants.TIP_MISSING_INTERFACE, this._className ) );
        }

        tips.addAll( Arrays.asList( this.checkAttributes( classToCheck.getAttributes() ) ) );
        tips.addAll( Arrays.asList( this.checkMethods( classToCheck.getOperations() ) ) );
        tips.addAll( Arrays.asList( this.checkRelations( classToCheck.getRelations() ) ) );

        return tips.stream().toArray( String[]::new );
    }

    private String[] checkAttributes( ExerciseUMLAttribute[] attributesToCheck ) {
        ArrayList<String> tips = new ArrayList<>();
        if ( attributesToCheck.length > this._attributes.size() ) {
            tips.add( String.format( Constants.TIP_TOO_MANY_ATTRIBUTES, this._className ) );
        }

        for ( ExerciseUMLAttribute solutionAttribute : this._attributes ) {
            Optional<ExerciseUMLAttribute> correspondingAttribute = Arrays.stream( attributesToCheck ).filter(
                    ( attribute ) -> attribute.getAttributeName().equals( solutionAttribute.getAttributeName() )
            ).findFirst();

            if ( !correspondingAttribute.isPresent() ) {
                tips.add( String.format( Constants.TIP_MISSING_ATTRIBUTE, solutionAttribute.getAttributeName() ) );
                continue;
            }

            tips.addAll( Arrays.asList( solutionAttribute.check( correspondingAttribute.get() ) ) );
        }

        return tips.stream().toArray( String[]::new );
    }

    private String[] checkMethods( ExerciseUMLMethod[] operationsToCheck ) {
        ArrayList<String> tips = new ArrayList<>();
        if ( operationsToCheck.length > this._methods.size() ) {
            tips.add( String.format( Constants.TIP_TOO_MANY_METHODS, this._className ) );
        }

        for ( ExerciseUMLMethod solutionOperation : this._methods ) {
            Optional<ExerciseUMLMethod> correspondingOperation = Arrays.stream( operationsToCheck ).filter(
                    ( operation ) -> operation.getMethodName().equals( solutionOperation.getMethodName() )
            ).findFirst();

            if ( !correspondingOperation.isPresent() ) {
                tips.add( String.format( Constants.TIP_MISSING_METHOD, solutionOperation.getMethodName() ) );
                continue;
            }

            tips.addAll( Arrays.asList( solutionOperation.check( correspondingOperation.get() ) ) );
        }

        return tips.stream().toArray( String[]::new );
    }

    private String[] checkRelations( ExerciseUMLRelation[] relationsToCheck ) {
        ArrayList<String> tips = new ArrayList<>();
        if ( this._relations.size() < relationsToCheck.length ) {
            tips.add( String.format( Constants.TIP_TOO_MANY_RELATIONS, this._className ) );
        }

        for ( ExerciseUMLRelation relationSolution : this._relations ) {
            Optional<ExerciseUMLRelation> correspondingRelation = Arrays.stream( relationsToCheck ).filter(
                    relationSolution::equals
            ).findFirst();

            if ( !correspondingRelation.isPresent() ) {
                tips.add( String.format(
                        Constants.TIP_MISSING_RELATION,
                        relationSolution.getRelationType().getXMLLabel()
                ) );
            }
        }
        return tips.stream().toArray( String[]::new );
    }
}
