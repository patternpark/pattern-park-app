package umlPuzzle.exercise.representations;

/**
 * @author NorthernSeaCharting
 */

final class Constants {

    public static final String TIP_MISSING_CLASS = "Die Klasse '%s' fehlt noch.";
    public static final String TIP_TOO_MANY_CLASSES = "Es gibt zu viele Klassen in dem Puzzle.";
    public static final String TIP_MISSING_ABSTRACT =
            "Bei der Klasse '%s' gibt es einen Fehler bei dem Abstrakt-Modifikator.";
    public static final String TIP_MISSING_INTERFACE =
            "Bei der Klasse'%s' gibt es einen Fehler bei dem Interface-Modifikator.";
    public static final String TIP_MISSING_ATTRIBUTE = "Einer Klasse fehlt noch ein Attribut (%s).";
    public static final String TIP_FALSE_ATTRIBUTE_TYPE = "Das Attribut '%s' hat den falschen Datentyp.";
    public static final String TIP_TOO_MANY_ATTRIBUTES = "Die Klasse '%s' hat zu viele Attribute.";
    public static final String TIP_MISSING_METHOD = "Einer Klasse fehlt noch eine Methode (%s).";
    public static final String TIP_WRONG_METHOD_TYPE = "Die Methode '%s' hat den falschen Rückgabewert.";
    public static final String TIP_TOO_MANY_METHODS = "Die Klasse '%s' hat zu viele Methoden.";
    public static final String TIP_MISSING_RELATION = "Es fehlt noch eine Relation (%s) zwischen zwei Klassen.";
    public static final String TIP_TOO_MANY_RELATIONS = "Die Klasse '%s' hat zu viele Relationen.";

    private Constants() {
    }

}
