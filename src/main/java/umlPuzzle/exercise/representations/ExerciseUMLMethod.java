package umlPuzzle.exercise.representations;

import java.util.ArrayList;

public class ExerciseUMLMethod {
    private final String _methodName;
    private final String _returnType;

    public ExerciseUMLMethod(
            final String givenOperationName,
            final String givenReturnType
    ) {
        this._methodName = givenOperationName;
        this._returnType = givenReturnType;
    }

    public String getReturnType() {
        return this._returnType;
    }

    public String getMethodName() {
        return this._methodName;
    }

    public String[] check( ExerciseUMLMethod operationToCheck ) {
        ArrayList<String> tips = new ArrayList<>();
        if ( !this._returnType.equals( operationToCheck.getReturnType() ) ) {
            tips.add( String.format( Constants.TIP_WRONG_METHOD_TYPE, this._methodName ) );
        }

        return tips.stream().toArray( String[]::new );
    }
}
