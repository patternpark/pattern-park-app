package umlPuzzle.exercise.representations;

import java.util.ArrayList;

public class ExerciseUMLAttribute {

    private final String _attributeType;
    private final String _attributeName;

    public ExerciseUMLAttribute(
            final String attributeName,
            final String attributeType
    ) {
        super();
        this._attributeName = attributeName;
        this._attributeType = attributeType;
    }

    public String getAttributeName() {
        return this._attributeName;
    }

    public String getAttributeType() {
        return this._attributeType;
    }

    public String[] check( ExerciseUMLAttribute attributeToCheck ) {
        ArrayList<String> tips = new ArrayList<>();
        if ( !this._attributeType.equals( attributeToCheck.getAttributeType() ) ) {
            tips.add( String.format( Constants.TIP_FALSE_ATTRIBUTE_TYPE, this._attributeName ) );
        }

        return tips.stream().toArray( String[]::new );
    }
}
