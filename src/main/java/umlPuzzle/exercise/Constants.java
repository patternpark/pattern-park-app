package umlPuzzle.exercise;

import org.jgraph.graph.GraphConstants;
import umlPuzzle.enums.ERelationType;

import java.util.HashMap;

/**
 * @author NorthernSeaCharting
 */

final class Constants {

    public static final HashMap<GraphToRelationKey, ERelationType> GRAPH_TO_RELATION_MAP = fillHashMap();

    public static final int EDGE_COUNT_SINGLE_CLASS_CHANGE = 5;
    public static final int EDGE_COUNT_RELATION_CHANGE = 1;
    public static final int EDGE_PORT_WEST_INDEX = 0;
    public static final int EDGE_PORT_EAST_INDEX = 1;
    public static final int EDGE_PORT_NORTH_INDEX = 2;
    public static final int EDGE_PORT_SOUTH_INDEX = 3;

    public static final String SUCCESS_TEXT = "Gratulation! Das UML-Puzzle ist gelöst.";
    public static final String IS_ABSTRACT_INIT_STRING = "false";
    public static final String IS_INTERFACE_INIT_STRING = "false";
    public static final String IS_ABSTRACT_STRING = "true";
    public static final String IS_INTERFACE_STRING = "true";

    private Constants() {
    }

    private static HashMap<GraphToRelationKey, ERelationType> fillHashMap() {
        HashMap<GraphToRelationKey, ERelationType> map = new HashMap<>();

        map.put( new GraphToRelationKey( GraphConstants.ARROW_SIMPLE, true ), ERelationType.ASSOCIATION );
        map.put(
                new GraphToRelationKey( GraphConstants.ARROW_TECHNICAL, false ),
                ERelationType.INHERITANCE
        );
        map.put(
                new GraphToRelationKey( GraphConstants.ARROW_DIAMOND, true ),
                ERelationType.COMPOSITION
        );
        map.put(
                new GraphToRelationKey( GraphConstants.ARROW_DIAMOND, false ),
                ERelationType.AGGREGATION
        );

        return map;
    }
}
