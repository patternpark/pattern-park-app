package umlPuzzle.exercise;

import emi.tabs.PanelConstants;
import org.jdom.Element;
import org.jgraph.JGraph;
import org.jgraph.event.GraphModelListener;
import org.jgraph.event.GraphSelectionEvent;
import org.jgraph.event.GraphSelectionListener;
import org.jgraph.graph.*;
import umlPuzzle.classview.ClassCell;
import umlPuzzle.classview.ClassWrapper;
import umlPuzzle.enums.EPortDirection;
import umlPuzzle.enums.ERelationType;
import umlPuzzle.enums.EXMLElementType;
import umlPuzzle.factory.GraphDefaultCellViewFactory;
import umlPuzzle.frames.InfoFrame;
import umlPuzzle.interfaces.UndoObserver;
import umlPuzzle.panels.DrawPanel;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * @author NorthernSeaCharting, Soto
 */
public class ExerciseGraph implements GraphSelectionListener, UndoObserver {

    private final DrawPanel _drawPanel;
    private final ArrayList<DefaultGraphCell> _cells = new ArrayList<>();

    private JGraph _graph;

    public ExerciseGraph( DrawPanel drawPanel, GraphModelListener listener ) {
        try {
            DefaultGraphModel graphModel = new DefaultGraphModel();
            this._graph = new JGraph( graphModel );
            graphModel.addGraphModelListener( listener );
            this._graph.addGraphSelectionListener( this );
            this._graph.getGraphLayoutCache().setFactory( new GraphDefaultCellViewFactory() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }

        this._drawPanel = drawPanel;
    }

    @Override
    public void valueChanged( GraphSelectionEvent graphSelectionEvent ) {
        String helpText = "";
        Object temp = graphSelectionEvent.getCell();

        if ( temp instanceof ClassCell ) {
            helpText = this._drawPanel.getButton().getText().getGraphChange();
        }

        this._drawPanel.getButton().getHelpPanel().setHelp( helpText );
        this._drawPanel.getButton().getHelpPanel().repaint();
    }

    public void setUpGraph() {
        JScrollPane scroll = new JScrollPane( this._graph );
        scroll.setHorizontalScrollBarPolicy( ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        scroll.setVerticalScrollBarPolicy( ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED );
        scroll.getVerticalScrollBar().setUnitIncrement(PanelConstants.TAB_PANEL_SCROLL_SPEED);
        this._drawPanel.add( scroll );
    }

    public DefaultGraphCell[] getCells() {
        return this._cells.stream().toArray( DefaultGraphCell[]::new );
    }

    public void initGraph( Element[] classList ) {
        for ( Element currentElement : classList ) {
            double xCord = Double.parseDouble( currentElement.getChildText( EXMLElementType.XCOORD.getXMLLabel() ) );
            double yCord = Double.parseDouble( currentElement.getChildText( EXMLElementType.YCOORD.getXMLLabel() ) );

            String abstractText = currentElement.getChild( EXMLElementType.ABSTRACT.getXMLLabel() ) != null ?
                    currentElement.getChildText( EXMLElementType.ABSTRACT.getXMLLabel() ) : Boolean.FALSE.toString();
            String interfaceText = currentElement.getChild( EXMLElementType.INTERFACE.getXMLLabel() ) != null ?
                    currentElement.getChildText( EXMLElementType.INTERFACE.getXMLLabel() ) : Boolean.FALSE.toString();

            // Operationen werden eingelesen
            Object[][] operationData = getMemberData( currentElement, EXMLElementType.METHOD.getXMLLabel() );
            // Attribute werden eingelesen
            Object[][] attributeData = getMemberData( currentElement, EXMLElementType.ATTRIBUTE.getXMLLabel() );

            this._cells.add(
                    this.createDefaultGraphCell(
                            currentElement.getChildText( EXMLElementType.NAME.getXMLLabel() ),
                            xCord, yCord,
                            abstractText, interfaceText,
                            operationData, attributeData
                    )
            );
        }
        this.addRelationToCellsArray(
                ERelationType.ASSOCIATION,
                classList
        );

        // Vererbungen in das cells-Array einfügen.
        this.addRelationToCellsArray(
                ERelationType.INHERITANCE,
                classList
        );

        // Kompositionen in das cells-Array einfügen.
        this.addRelationToCellsArray(
                ERelationType.COMPOSITION,
                classList
        );

        // Aggregationen in das cells-Array einfgen.
        this.addRelationToCellsArray(
                ERelationType.AGGREGATION,
                classList
        );
        this._graph.getGraphLayoutCache().insert( this._cells.stream().toArray() );
    }

    /**
     * Reinitialisierung
     *
     * @author Lars Friedrich
     */
    public void reInit() {
        /*
         * alle Objekte sind Vector in der Klasse DrawPanel vorhanden
         */
        this._graph.setAutoResizeGraph( true );
    }

    public void addCellForClass( String name, int x, int y ) {
        Object[][] methodData, attributeData;
        methodData = new Object[][] { { "", "" } };
        attributeData = new Object[][] { { "", "" } };

        // eine ClassCell (Erweiterung von DefaultGraphCell) wird angelegt
        DefaultGraphCell cell =
                createDefaultGraphCell(
                        name, x, y, Constants.IS_ABSTRACT_INIT_STRING, Constants.IS_INTERFACE_INIT_STRING,
                        methodData, attributeData
                );
        this._cells.add( cell );
        this._graph.getGraphLayoutCache().insert( cell );
    }

    public void clearGraph( GraphModelListener graphModelListener ) {
        this._drawPanel.removeAll();
        // der neue Graph wird in XML gespeichert
        this._graph.getGraphLayoutCache().remove( this._cells.stream().toArray(), true, true );
        this._graph.removeAll();

        DefaultGraphModel graphModel = new DefaultGraphModel();
        this._graph = new JGraph( graphModel );
        graphModel.addGraphModelListener( graphModelListener );
        this._graph.getGraphLayoutCache().setFactory( new GraphDefaultCellViewFactory() );
        this._cells.clear();
    }

    @Override
    public void reactToStepAdded() {
        try {
            this._drawPanel.getButton().repaint();
        } catch ( NullPointerException ignored ) {

        }
    }

    @Override
    public void reactToUndo( List<Element> classList ) {
    }

    @Override
    public void reactToRedo( List<Element> classList ) {
    }

    @Override
    public void reactToStepNotPossible( int typeOfWarning ) {
        try {
            this._drawPanel.getButton().setEverythingEnable( false );
            InfoFrame info = new InfoFrame( typeOfWarning, this._drawPanel );
            info.setVisible( true );
            info.setAlwaysOnTop( true );
        } catch ( NullPointerException ignored ) {
        }
    }

    /**
     * Create vertex with random numbers of ports that have labels
     */
    protected DefaultGraphCell createDefaultGraphCell(
            String name, double x,
            double y, String abstractString,
            String interfaceString,
            Object[][] opData,
            Object[][] attData
    ) {
        ClassWrapper classWrapper = new ClassWrapper();
        classWrapper.methodData = opData;
        classWrapper.attributeData = attData;
        classWrapper.isAbstract = Boolean.parseBoolean( abstractString );
        classWrapper.isInterface = Boolean.parseBoolean( interfaceString );

        int classnodeHeight = ( ( opData.length + attData.length ) * 16 ) + 70;
        classWrapper.setLabel( name );

        ClassCell cell = new ClassCell( classWrapper ); //hier den schon gefüllten wrapper reingeben
        cell.setMyWrapper( classWrapper );

        cell.add( this.createPort( EPortDirection.EAST.getPortDirection(), 0, 400.0 ) );
        cell.add( this.createPort( EPortDirection.WEST.getPortDirection(), GraphConstants.PERMILLE, 400.0 ) );
        cell.add( this.createPort( EPortDirection.NORTH.getPortDirection(), GraphConstants.PERMILLE / 2f, 0.0 ) );
        cell.add( this.createPort( EPortDirection.SOUTH.getPortDirection(), GraphConstants.PERMILLE / 2f, 1000.0 ) );

        AttributeMap attributes = cell.getAttributes();
        // Add a Bounds Attribute to the Map
        GraphConstants.setBounds( attributes, new Rectangle2D.Double( x, y, 194, classnodeHeight ) );
        //66//48//84 height:  30 for classname, 18 per table row(att or op;
        GraphConstants.setOpaque( attributes, true );
        GraphConstants.setGradientColor( attributes, Color.white );
        GraphConstants.setBorderColor( attributes, Color.black );
        GraphConstants.setAutoSize( attributes, false ); // an autosized vertex has no handles
        GraphConstants.setConstrained( attributes, false );
        GraphConstants.setValue( attributes, classWrapper );

        return cell;
    }

    private Object[][] getMemberData( Element currentElement, String memberName ) {
        List<Element> members = currentElement.getChildren( memberName );
        boolean noMembers = members.isEmpty();
        int memCount = members.size();
        int arraySize = !noMembers ? memCount : 1;

        Object[][] memberData = new Object[ arraySize ][ 2 ];
        if ( !noMembers ) {
            for ( int i = 0; i < memCount; ++i ) {
                Element e = members.get( i );
                memberData[ i ][ 0 ] = e.getChildText( EXMLElementType.DATATYPE.getXMLLabel() );
                memberData[ i ][ 1 ] = e.getChildText( EXMLElementType.NAME.getXMLLabel() );
            }
        } else {
            memberData[ 0 ][ 0 ] = "";
            memberData[ 0 ][ 1 ] = "";
        }
        return memberData;
    }

    private void addRelationToCellsArray(
            ERelationType relationType, Element[] classList
    ) {
        int classListCount = classList.length;
        for ( int i = 0; i < classListCount; ++i ) {
            Element currentElement = classList[ i ];
            List<Element> relationList = currentElement.getChildren( relationType.getXMLLabel() );

            for ( Element relation : relationList ) {
                String relationTargetName = relation.getText();

                for ( int j = 0; j < classListCount; ++j ) {
                    Element potentialRelationTarget = classList[ j ];

                    if (
                            !potentialRelationTarget.getChildText( EXMLElementType.NAME.getXMLLabel() ).equals(
                                    relationTargetName
                            )
                    ) {
                        continue;
                    }

                    if ( relationType == ERelationType.INHERITANCE ) {
                        this.addInheritanceRelationEdge( i, j );
                    } else {
                        this.addOtherRelationEdge( relationType, i, j );
                    }

                    break;
                }
            }
        }
    }

    private void addOtherRelationEdge(
            ERelationType relationType, int sourceClassIndex, int targetClassIndex
    ) {
        DefaultEdge edge = new DefaultEdge();
        AttributeMap sourceCellAttributes = this._cells.get( sourceClassIndex ).getAttributes();
        AttributeMap targetCellAttributes = this._cells.get( targetClassIndex ).getAttributes();
        if ( GraphConstants.getBounds( sourceCellAttributes ).getX() <
                GraphConstants.getBounds( targetCellAttributes ).getX() ) {
            edge.setSource( this._cells.get( sourceClassIndex ).getChildAt( Constants.EDGE_PORT_EAST_INDEX ) );
            edge.setTarget( this._cells.get( targetClassIndex ).getChildAt( Constants.EDGE_PORT_WEST_INDEX ) );
        } else if (
                GraphConstants.getBounds( sourceCellAttributes ).getX() ==
                        GraphConstants.getBounds( targetCellAttributes ).getX()
        ) {
            edge.setSource( this._cells.get( sourceClassIndex ).getChildAt( Constants.EDGE_PORT_WEST_INDEX ) );
            edge.setTarget( this._cells.get( targetClassIndex ).getChildAt( Constants.EDGE_PORT_WEST_INDEX ) );
        } else {
            edge.setSource( this._cells.get( sourceClassIndex ).getChildAt( Constants.EDGE_PORT_WEST_INDEX ) );
            edge.setTarget( this._cells.get( targetClassIndex ).getChildAt( Constants.EDGE_PORT_EAST_INDEX ) );
        }
        this._cells.add( edge );

        int arrow;

        if ( !relationType.equals( ERelationType.ASSOCIATION ) ) {
            arrow = GraphConstants.ARROW_DIAMOND;
        } else {
            arrow = GraphConstants.ARROW_SIMPLE;
        }

        GraphConstants.setLineEnd( edge.getAttributes(), arrow );

        if ( relationType.equals( ERelationType.COMPOSITION ) || relationType.equals( ERelationType.ASSOCIATION ) ) {
            GraphConstants.setEndFill( edge.getAttributes(), true );
        }
    }

    private void addInheritanceRelationEdge( int sourceClassIndex, int targetClassIndex ) {
        DefaultEdge edge = new DefaultEdge();
        edge.setSource( this._cells.get( sourceClassIndex ).getChildAt( Constants.EDGE_PORT_NORTH_INDEX ) );
        edge.setTarget( this._cells.get( targetClassIndex ).getChildAt( Constants.EDGE_PORT_SOUTH_INDEX ) );
        this._cells.add( edge );

        GraphConstants.setLineEnd( edge.getAttributes(), GraphConstants.ARROW_TECHNICAL );
    }

    private DefaultPort createPort( String portDirection, double xValue, double yValue ) {
        DefaultPort port = new DefaultPort( portDirection );
        GraphConstants.setOffset( port.getAttributes(), new Point2D.Double( xValue, yValue ) );
        return port;
    }
}
