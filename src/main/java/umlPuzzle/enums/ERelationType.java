package umlPuzzle.enums;

public enum ERelationType {
    ASSOCIATION( Constants.REFERENCE ),
    INHERITANCE( Constants.INHERITANCE ),
    AGGREGATION( Constants.AGGREGATION ),
    COMPOSITION( Constants.COMPOSITION );

    private final String _xmlLabel;

    ERelationType( final String label ) {
        this._xmlLabel = label;
    }

    public String getXMLLabel() {
        return this._xmlLabel;
    }
}
