package umlPuzzle.enums;

public enum EPortDirection {
    NORTH( Constants.PORT_NORTH ),
    SOUTH( Constants.PORT_SOUTH ),
    WEST( Constants.PORT_WEST ),
    EAST( Constants.PORT_EAST );


    private final String _portDirection;

    EPortDirection( String direction ) {
        this._portDirection = direction;
    }

    public String getPortDirection() {
        return _portDirection;
    }
}
