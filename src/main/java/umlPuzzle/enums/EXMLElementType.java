package umlPuzzle.enums;


public enum EXMLElementType {

    UML_CLASS( Constants.UML_CLASS ),
    ATTRIBUTE( Constants.ATTRIBUTE ),
    METHOD( Constants.METHOD ),
    DATATYPE( Constants.DATATYPE ),
    NAME( Constants.NAME ),
    ABSTRACT( Constants.ABSTRACT ),
    INTERFACE( Constants.INTERFACE ),
    XCOORD( Constants.X_COORD ),
    YCOORD( Constants.Y_COORD );

    private final String _xmlLabel;

    EXMLElementType( String xmlLabel ) {
        this._xmlLabel = xmlLabel;
    }

    public String getXMLLabel() {
        return this._xmlLabel;
    }
}
