package umlPuzzle.enums;

final class Constants {

    // XML Elements magic Strings
    public static final String UML_CLASS = "umlclass";
    public static final String NAME = "name";
    public static final String X_COORD = "x";
    public static final String Y_COORD = "y";
    public static final String ABSTRACT = "abstract";
    public static final String INTERFACE = "interface";
    public static final String REFERENCE = "ref";
    public static final String INHERITANCE = "inh";
    public static final String AGGREGATION = "agg";
    public static final String COMPOSITION = "com";
    public static final String ATTRIBUTE = "attribute";
    public static final String METHOD = "method";
    public static final String DATATYPE = "datatype";
    public static final String PORT_NORTH = "Port NORTH";
    public static final String PORT_SOUTH = "Port SOUTH";
    public static final String PORT_EAST = "Port OST";
    public static final String PORT_WEST = "Port WEST";

    private Constants() {
    }
}
