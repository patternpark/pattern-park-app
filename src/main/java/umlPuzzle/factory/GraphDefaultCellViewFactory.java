package umlPuzzle.factory;

import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.EdgeView;
import org.jgraph.graph.PortView;
import org.jgraph.graph.VertexView;
import umlPuzzle.classview.ClassCell;
import umlPuzzle.classview.ClassVertexView;

public class GraphDefaultCellViewFactory extends DefaultCellViewFactory {

    @Override
    protected EdgeView createEdgeView( Object cell ) {
        return new EdgeView( cell );
    }

    /**
     * Constructs a PortView view for the specified object.
     */
    @Override
    protected PortView createPortView( Object cell ) {
        return new PortView( cell );
    }

    /**
     * Constructs a VertexView view for the specified object.
     */
    @Override
    protected VertexView createVertexView( Object cell ) {
        if ( cell instanceof ClassCell ) {
            return new ClassVertexView( cell );
        } else {
            return new VertexView( cell );
        }
    }
}
