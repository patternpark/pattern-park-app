package settings;

import java.awt.*;

/**
 * hier liegen alle voreingestellten Gr��en
 * f�r Buttons, Panels, ...
 * @author Lars Friedrich
 * @version 1.7 (14. November 2006)
 */
public abstract class GUISets {
	
	/** Komponenten der Aufgabe Kompositum */
	public static int COMPOSITE_SIZE = 5;
	/** Komponenten der Aufgabe Beobachter */
	public static int OBSERVER_SIZE = 12;
	/** Komponenten der Aufgabe Fassade */
	public static int FACADE_SIZE = 12;
	/** Komponenten der Aufgabe Proxy */
	public static int PROXY_SIZE = 12;
	/** Komponenten der Aufgabe Dekorierer */
	public static int DECORATOR_SIZE = 12;
	/** Komponenten der Aufgabe Iterator */
	public static int ITERATOR_SIZE = 12;
	/** Komponenten der Aufgabe Schablonenmethode */
	public static int TEMPLATE_METHOD_SIZE = 12;
	/** Komponenten der Aufgabe Zustand */
	public static int STATE_SIZE = 12;
	/** Komponenten der Aufgabe Fabrik Methode */
	public static int FACTORY_SIZE = 12;
	/** Komponenten der Aufgabe Adapter */
	public static int Adapter_SIZE = 12;
	/** Komponenten der Aufgabe Kompositum */
	public static int MKA_SIZE = 12;
	/** Komponenten der Aufgabe Kompositum */
	public static int BRIDGE_SIZE = 12;
	/** Komponenten der Aufgabe Kompositum */
	public static int SINGLETON_SIZE = 12;

	/** Kompositum-ID */
	public static int COMPOSITE = 1;
	/** Beobachter-ID */
	public static int OBSERVER = 2;
	/** Fassaden-ID */
	public static int FACADE = 3;
	/** Proxy-ID */
	public static int PROXY = 4;
	/** Dekorierer-ID */
	public static int DECORATOR = 5;
	/** Iterator-ID */
	public static int ITERATOR = 6;
	/** Schablonenmethoden-ID */
	public static int TEMPLATE_METHOD = 7;
	/** Zustands-ID */
	public static int STATE = 8;
	/** MKA-ID */
	public static int MKA = 9;
	/** FACTORY-ID */
	public static int FACTORY = 10;
	/** ADAPTER-ID */
	public static int ADAPTER = 11;
	/** MKA-ID */
	public static int BRIDGE = 13;
	/** Singleton-ID */
	public static int SINGLETON = 14;
	
	/** erste Reihe */
	public static int FIRST_ROW = 0;
	/** zweite Reihe */
	public static int SECOND_ROW = 1;
	/** dritte Reihe */
	public static int THIRD_ROW = 2;
	/** vierte Reihe */
	public static int FOURTH_ROW = 3;
	/** f�nfte Reihe */
	public static int FIFTH_ROW = 4;
	/** sechste Reihe */
	public static int SIXTH_ROW = 5;
	/** siebte Reihe */
	public static int SEVENTH_ROW = 6;
	/** achte Reihe */
	public static int EIGHTH_ROW = 7;
	/** neunte Reihe */
	public static int NINETH_ROW = 8;
	/** zehnte Reihe */
	public static int TENTH_ROW = 9;
	/** elfte Reihe */
	public static int ELEVENTH_ROW = 10;
	/** zw�lfte Reihe */
	public static int TWELTH_ROW = 11;
	
	/** erste Zelle */
	public static int FIRST_CELL = 0;
	/** zweite Zelle */
	public static int SECOND_CELL = 1;
	/** dritte Zelle */
	public static int THIRD_CELL = 2;
	/** vierte Zelle */
	public static int FOURTH_CELL = 3;
	/** f�nfte Zelle */
	public static int FIFTH_CELL = 4;
	/** sechste Zelle */
	public static int SIXTH_CELL = 5;
	/** siebte Zelle */
	public static int SEVENTH_CELL = 6;
	/** achte Zelle */
	public static int EIGHTH_CELL = 7;
	/** neunte Zelle */
	public static int NINETH_CELL = 8;
	/** zehnte Zelle */
	public static int TENTH_CELL = 9;
	
	/**
	 * Default Insets: alle 0
	 */
	public static Insets DEFAULT_INSETS = new Insets(0, 0, 0, 0);
	/**
	 * Standard Insets: alle 5
	 */
	public static Insets STANDARD_INSETS = new Insets(5, 5, 5, 5);
	/**
	 * Diag Insets f�r die Klassen im Diagramm
	 */
	public static Insets DIAG_INSETS = new Insets(0, 5, 0, 5);

	
	/* Default Button-Breite: 150 Pixel */
	public static int OVERVIEW_BUTTON_WIDTH = 150;
	/* Default Button-H�he: 27 Pixel */
	public static int OVERVIEW_BUTTON_HEIGHT = 27;
	
	/* Default Button-Breite: 150 Pixel */
	private static int BUTTON_WIDTH = 150;
	/* Default Button-H�he: 27 Pixel */
	private static int BUTTON_HEIGHT = 27;
	/**
	 * Default Button-Dimension: 150x27 Pixel
	 */
	public static Dimension BUTTON_DIMENSION = new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT);
	
	/* Default Button-Breite: 150 Pixel */
	private static int PIC_BUTTON_WIDTH = 25;
	/* Default Button-H�he: 50 Pixel */
	private static int PIC_BUTTON_HIGHT = 25;
	/**
	 * Default Button-Dimension: 25x25 Pixel
	 */
	public static Dimension BUTTON_PIC_DIMENSION = new Dimension(PIC_BUTTON_WIDTH, PIC_BUTTON_HIGHT);
	
	/* Default Panel-Breite: 200 Pixel */
	private static int PANEL_WIDTH = 200;
	/* Default Panel-H�he: 200 Pixel */
	private static int PANEL_HIGHT = 200;
	/**
	 * Default Panel-Dimension: 200x200 Pixel
	 */
	public static Dimension PANEL_DIMENSION = new Dimension(PANEL_WIDTH, PANEL_HIGHT);
	
	/* Default Panel-Breite: 200 Pixel */
	private static int BUTTON_PANEL_WIDTH = 150;
	/* Default Panel-H�he: 200 Pixel */
	private static int BUTTON_PANEL_HIGHT = 250;
	/**
	 * Default Panel-Dimension: 150x250 Pixel
	 */
	public static Dimension BUTTON_PANEL_DIMENSION = new Dimension(BUTTON_PANEL_WIDTH, BUTTON_PANEL_HIGHT);
	
	/* Draw-Panel-Breite: 500 Pixel */
	private static int DRAW_PANEL_WIDTH = 800;//600;//400;//800;
	/* Draw-Panel-H�he: 250 Pixel */
	private static int DRAW_PANEL_HIGHT = 400;////400;//400;//500;
	/**
	 * Draw-Panel-Dimension: 500x250 Pixel
	 */
	public static Dimension DRAW_PANEL_DIMENSION = new Dimension(DRAW_PANEL_WIDTH, DRAW_PANEL_HIGHT);
	
	/* Draw-Panel-Breite: 500 Pixel */
	public static int FRAME_WIDTH = 1024;
	/* Draw-Panel-H�he: 250 Pixel */
	public static int FRAME_HEIGHT = 768;
	/**
	 * Draw-Panel-Dimension: 500x250 Pixel
	 */
	public static Dimension FRAME_DIMENSION = new Dimension(FRAME_WIDTH, FRAME_HEIGHT);
	
	/* Text-Area-Breite: 150 Pixel */
	private static int TEXT_AREA_WIDTH = 150;
	/* Text-Area-H�he: 150 Pixel */
	private static int TEXT_AREA_HEIGHT = 150;
	/**
	 * Text-Area-Dimension: 150x150 Pixel
	 */
	public static Dimension TEXT_AREA_DIMENSION = new Dimension(TEXT_AREA_WIDTH, TEXT_AREA_HEIGHT);
	
	/* Text-Field-Breite: 30 Pixel */
	private static int NUMBER_TEXT_FIELD_WIDTH = 30;
	/* Text-Field-H�he: 21 Pixel */
	private static int NUMBER_TEXT_FIELD_HEIGHT = 21;
	/**
	 * Text-Field-Dimension: 30x21 Pixel
	 */
	public static Dimension TEXT_FIELD_NUMBER_DIMENSION = new Dimension(NUMBER_TEXT_FIELD_WIDTH, NUMBER_TEXT_FIELD_HEIGHT);
	
	/* Text-Field-Breite: 150 Pixel */
	private static int TEXT_FIELD_WIDTH = 150;
	/* Text-Field-H�he: 21 Pixel */
	private static int TEXT_FIELD_HEIGHT = 21;
	/**
	 * Text-Field-Dimension: 150x21 Pixel
	 */
	public static Dimension TEXT_FIELD_DIMENSION = new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT);
	
	/* Label-Breite: 30 Pixel */
	private static int SMALL_LABEL_WIDTH = 30;
	/* Label-H�he: 21 Pixel */
	private static int SMALL_LABEL_HEIGHT = 21;
	/**
	 * Label-Dimension: 30x21 Pixel
	 */
	public static Dimension SMALL_LABEL_DIMENSION = new Dimension(SMALL_LABEL_WIDTH, SMALL_LABEL_HEIGHT);
	
	/* Label-Breite: 150 Pixel */
	private static int LABEL_WIDTH = 150;
	/* Label-H�he: 21 Pixel */
	private static int LABEL_HEIGHT = 21;
	/**
	 * Label-Dimension: 150x21 Pixel
	 */
	public static Dimension LABEL_DIMENSION = new Dimension(LABEL_WIDTH, LABEL_HEIGHT);
	
	/* Text-Area-Breite: 150 Pixel */
	private static int WARNING_WIDTH = 150;
	/* Text-Area-H�he: 150 Pixel */
	private static int WARNING_HIGHT = 50;
	/**
	 * Text-Area-Dimension: 150x150 Pixel
	 */
	public static Dimension WARNING_DIMENSION = new Dimension(WARNING_WIDTH, WARNING_HIGHT);
	
	/* Text-Area-Breite f�r Aufgaben: 80 Pixel */
	private static int EXERCISE_AREA_WIDTH = 900;
	/* Text-Area-H�he f�r Aufgaben: 100 Pixel */
	private static int EXERCISE_AREA_HIGHT = 100;
	/**
	 * Text-Area-Dimension: 800x100 Pixel
	 */
	public static Dimension EXERCISE_AREA_DIMENSION = new Dimension(EXERCISE_AREA_WIDTH, EXERCISE_AREA_HIGHT);
	
	/* Text-Area-Breite f�r Aufgaben: 80 Pixel */
	private static int EXERCISE_AREA_MIN_WIDTH = 750;
	/* Text-Area-H�he f�r Aufgaben: 100 Pixel */
	private static int EXERCISE_AREA_MIN_HIGHT = 50;
	/**
	 * Text-Area-Dimension: 800x100 Pixel
	 */
	public static Dimension EXERCISE_AREA_MIN_DIMENSION = new Dimension(EXERCISE_AREA_MIN_WIDTH, EXERCISE_AREA_MIN_HIGHT);
	
	/* Fast-Help-Panel-Breite: 500 Pixel */
	private static int FAST_HELP_PANEL_WIDTH = 100;
	/* Fast-Help-Panel-H�he: 250 Pixel */
	private static int FAST_HELP_PANEL_HIGHT = 50;
	/**
	 * Fast-Help-Panel-Dimension: 100x50 Pixel
	 */
	public static Dimension FAST_HELP_PANEL_DIMENSION = new Dimension(FAST_HELP_PANEL_WIDTH, FAST_HELP_PANEL_HIGHT);
	
	/* Fast-Help-Panel-Breite: 500 Pixel */
	private static int INFO_PANEL_WIDTH = 200;
	/* Fast-Help-Panel-H�he: 250 Pixel */
	private static int INFO_PANEL_HIGHT = 270;
	/**
	 * Fast-Help-Panel-Dimension: 100x50 Pixel
	 */
	public static Dimension INFO_PANEL_DIMENSION = new Dimension(INFO_PANEL_WIDTH, INFO_PANEL_HIGHT);
}