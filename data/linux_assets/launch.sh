#!/bin/bash
# This script should be located where the main PatternPark.jar is located.
java --version
if [ $? -ne 0 ]; then
	echo "You must have java JRE 8 or newer installed to run this Programm!"
	exit
fi
chmod +x ./data/players/flashplayer
java -jar ./PatternPark.jar
