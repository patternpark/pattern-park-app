////////////////////////////////////////////////////////////////////////////////////////////
Klasse Fassade
////////////////////////////////////////////////////////////////////////////////////////////

package fassade;

/**
 * Die Fassade ist eine Schnittstelle die einen begrenzten/bestimmten Teil,
 * der Funktionalität des Subsystems implementiert.
 * Dadurch wird das Benutzen des Subsystems für den Klienten vereinfacht.
 * Funktionen, die der Klient nicht braucht oder nutzen will, werden in
 * der Fassade nicht implementiert.
 */

public class Fassade {
	
	//Implementierung der Klassen des Subsystems
	private Sub1 einSub1;
	private Sub2 einSub2;
	private Sub3 einSub3;

	// Konstruktor
	public Fassade() {
		//Implementierung des Konstruktors
		einSub1 = new Sub1();
		einSub2 = new Sub2();
		einSub3 = new Sub3();
	}

	//Beispielhafte Nutzung der Funktionen von Klassen des Subsystems
	//Nur die Funktionen implementieren die der Klient braucht
	
	//Eine Beispielfunktion
	public void funktion1(){
		einSub1.funk1();
	}

	//Eine Beispielfunktion
	public void funktion2(){
		einSub2.funk2();
	}

	//Eine Beispielfunktion
	public void funktion3(){
		einSub3.funk3();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package fassade;

/**
 * Der Klient richtet Anfragen an das Subsystem an die Fassade.
 */

public class Klient {

	//Implementierung der Fassade
	public Fassade eineFassade;

	//Main-Methode die die Fassade benutzt
	public void fassadeDemo() {
		
		eineFassade.funktion1();
		eineFassade.funktion2();
		eineFassade.funktion3();

	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Sub1
////////////////////////////////////////////////////////////////////////////////////////////

package fassade;

/**
 * In den Subklassen ist die Subsystemfunktionalität implementiert.
 * Sie kennen die Fassade selber nicht. 
 * Sie kann aber Verbindungen zu anderen Klassen des Subsystems haben.
 * Hier werden die von der Fassade weitergeleiteten Aufgaben bearbeitet.
 */

public class Sub1 {

	//Beispielhafte Verbindungen zu anderen Klassen des Subsystems
	private Sub2 einSub2;
	private Sub3 einSub3;
	
	//Konstruktor
	public Sub1() {
		//Implementierung des Konstruktors
		einSub2 = new Sub2();
        einSub3 = new Sub3();
	}

	//eine Funktion die der Klient benutzen will
	//die Funktion wird also in die Fassade implementiert
	public void funk1() {
		//Implementierung einer Funktion
	}

	//eine Funktion die der Klient nicht benutzen will
	//die Funktion wird also nicht in die Fassade implementiert
	public void meth1() {
		//Implementierung einer Funktion
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Sub2
////////////////////////////////////////////////////////////////////////////////////////////

package fassade;

/**
 * In den Subklassen ist die Subsystemfunktionalität implementiert.
 * Sie kennen die Fassade selber nicht. 
 * Sie kann aber Verbindungen zu anderen Klassen des Subsystems haben.
 * Hier werden die von der Fassade weitergeleiteten Aufgaben bearbeitet.
 */

public class Sub2 {

	//Beispielhafte Verbindungen zu anderen Klassen des Subsystems
	private Sub1 einSub1;
	
	//Konstruktor
	public Sub2() {
		//Implementierung des Konstruktors
		einSub1 = new Sub1();
	}

	//eine Funktion die der Klient benutzen will
	//die Funktion wird also in die Fassade implementiert
	public void funk2() {
		//Implementierung einer Funktion
	}

	//eine Funktion die der Klient nicht benutzen will
	//die Funktion wird also nicht in die Fassade implementiert
	public void meth2() {
		//Implementierung einer Funktion
	}
}


////////////////////////////////////////////////////////////////////////////////////////////
Klasse Sub3
////////////////////////////////////////////////////////////////////////////////////////////

package fassade;

/**
 * In den Subklassen ist die Subsystemfunktionalität implementiert.
 * Sie kennen die Fassade selber nicht. 
 * Sie kann aber Verbindungen zu anderen Klassen des Subsystems haben.
 * Hier werden die von der Fassade weitergeleiteten Aufgaben bearbeitet.
 */

public class Sub3 {

	//Konstruktor
	public Sub3() {
		//Implementierung des Konstruktors
	}

	//eine Funktion die der Klient benutzen will
	//die Funktion wird also in die Fassade implementiert
	public void funk3() {
		//Implementierung einer Funktion
	}

	//eine Funktion die der Klient nicht benutzen will
	//die Funktion wird also nicht in die Fassade implementiert
	public void meth3() {
		//Implementierung einer Funktion
	}
}