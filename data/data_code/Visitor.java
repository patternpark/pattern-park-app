//////////////////////////////////////////////
Interface Besucher
//////////////////////////////////////////////

/** Interface was die Methoden zum besuchen definiert.
 * jedes Konkreten Besuchbarem Objektes braucht hier eine eigene visit Methode, die in jedem Besucher implementiert werden muss.
 * Dadurch wird das Programm bei zu vielen Konkreten Besuchbaren Objekten nur schwer erweiterbar!
 */
public interface Besucher {
	// Methoden zum besuchen
	void visit(KonkretesBesuchbaresA ceA);
	void visit(KonkretesBesuchbaresB ceB);
}

//////////////////////////////////////////////
Interface Besuchbares
//////////////////////////////////////////////

/** Interface was eine Akzeptanzmethode definiert. Hier wird die visit() Methode auf den übergebenen Besucher aufgerufen.
 * Der Besucher hat auch die möglichkeit einen Besucher abzulehnen, indem die visit() Methode nicht aufgeruden wird.
 */
public interface Besuchbares {
	// Methode zum akzeptieren des Besuchers
	void accept(Besucher besucher);
}

//////////////////////////////////////////////
Klasse KonkreterBesucher1
//////////////////////////////////////////////

/** Besucher der operationen auf einem Besuchbarem Objekt durchführt.
 */
public class KonkreterBesucher1 implements Besucher {

	//Konstruktor
	public KonkreterBesucher1() {
		//Implementierung des Konstruktors
	}

	//Methode zum Besuchen des Objekts KonkretesBesuchbaresA
	public void visit(KonkretesBesuchbaresA ceA) {
		//Implementierung
	}

	//Methode zum Besuchen des Objekts KonkretesBesuchbaresB
	public void visit(KonkretesBesuchbaresB ceB) {
		//Implementierung
	}
}

//////////////////////////////////////////////
Klasse KonkreterBesucher2
//////////////////////////////////////////////

/** Besucher der operationen auf einem Besuchbarem Objekt durchführt.
 */
public class KonkreterBesucher2 implements Besucher {

	//Konstruktor
	public KonkreterBesucher2() {
		//Implementierung des Konstruktors
	}

	//Methode zum Besuchen des Objekts KonkretesBesuchbaresA
	public void visit(KonkretesBesuchbaresA ceA) {
		//Implementierung
	}

	//Methode zum Besuchen des Objekts KonkretesBesuchbaresB
	public void visit(KonkretesBesuchbaresB ceB) {
		//Implementierung
	}
}

//////////////////////////////////////////////
Klasse KonkretesBesuchbaresA
//////////////////////////////////////////////

/** Objekt auf dem operationen von dem Besucher vorgenommen werden
 * Wenn der Besucher zugriff auf einer der Variablen braucht muss hierfür natürlich wieder ein getter/setter erstellt werden.
 */
public class KonkretesBesuchbaresA implements Besuchbares {

	//Konstruktor
	public KonkretesBesuchbaresA() {
		//Implementierung des Konstruktors
	}

	public void accept(Besucher besucher){
        //Implementierung der Akzeptanzmethode
        besucher.visit(this);
        //Bei anderem wiedergabewert: return besucher.visit(this);
	}

	public void operationA(){
        //Implementierung einer Methode für dieses Objekt
	}
}

//////////////////////////////////////////////
Klasse KonkretesBesuchbaresB
//////////////////////////////////////////////

/** Objekt auf dem operationen von dem Besucher vorgenommen werden
 * Wenn der Besucher zugriff auf einer der Variablen braucht muss hierfür natürlich wieder ein getter/setter erstellt werden.
 */
public class KonkretesBesuchbaresB implements Besuchbares {
    // private Variablen
    private int exampleNumber;
    private string exampleString;

	//Konstruktor
	public KonkretesBesuchbaresB() {
		//Implementierung des Konstruktors
	}

	public void accept(Besucher besucher){
        //Implementierung der Akzeptanzmethode
        //Es ist auch möglich die visit Methode unter bestimmten bedingungen nicht aufzurufen
        besucher.visit(this);
        //Bei anderem wiedergabewert: return besucher.visit(this);
	}

}

//////////////////////////////////////////////
Klasse Klient
//////////////////////////////////////////////

/** Konsumentenklasse die einen Besucher über eine Objektstruktur laufen lassen will.
 */
public class Klient {

	//Konstruktor
	public Klient() {
		//Implementierung des Konstruktors
	}

	//Beispielhafte Implementierung, nicht teil des Entwurfsmuster
	/** Besucht alle elemente der Objektsturktur 'elemente' mit einem KonkreterBesucher2
	*/
	public void operation(){
        Besuchbares[] elemente = new Besuchbares[]{new KonkretesBesuchbaresA(), new KonkretesBesuchbaresB()};
        konkreterBesucher = new KonkreterBesucher2();
        for(Besuchbares element : elemente){
            element.accept(konkreterBesucher);
        }
	}

}
