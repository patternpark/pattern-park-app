////////////////////////////////////////////////////////////////////////////////////////////
Klasse Abstraktion
////////////////////////////////////////////////////////////////////////////////////////////

package bridge;

/**
* Die Abstraktion (eng. Abstraction) ist eine
* abstrakte Klasse.
* Sie ist das Kern des Brückenentwurfsmusters und
* enthält einen Verweis auf den Implementierer.
* Die Abstraktion und der Implementierer haben eine
* Aggregationsbeziehungen, in der die Abstraktion
* das Aggregat ist. Die Beziehung mit dem Implementierer
* ist auch die eigentliche Brücke (eng. bridge).
* Sie auch die Klasse, mit der der Klient kommuniziert.
*/

public abstract class Abstraktion {

	//Referenz Implementierer		
	protected Implementierer implementierer;

	//Konstruktor
	public Abstraktion(Implementierer implementierer)
	{
		this.implementierer = implementierer;
	}
	
	//Default Konstruktor
	public Abstraktion(){}

	//Die Methoden der Abstraktion
	//Hier operationen gennant um sie
	//von den methoden des Implementierer
	//zu unterscheiden. Natürlich können
	//die Methoden auch abstrakt sein.		
	public void operationen(){};
			
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse SpezialAbstraktion
////////////////////////////////////////////////////////////////////////////////////////////

package bridge;

/**
* Die SpezialAbstraktion (eng. RefinedAbstraction) 
* erweitert die Abstraktion. Sie arbeitet mit den
* verschiedenen Implementierungen durch das
* Interface Implementierer, genau wie ihre
* Elternklasse. Es kann auch mehrere
* SpezialAbstraktionen geben, die dann
* die Abstraktion unterschiedlich erweitern.
*/

public class SpezialAbstraktion extends Abstraktion {
			
	//Konstruktor		
	public SpezialAbstraktion(Implementierer implementierer)
	{
		super.implementierer = implementierer;
	}

	//Hier stehen die Methoden, die die
	//Elternklasse, also die Abstraktion,
	//nicht hat.		
	public void extraOperationen(){}
}

////////////////////////////////////////////////////////////////////////////////////////////
Interface Implementierer
////////////////////////////////////////////////////////////////////////////////////////////

package bridge;

/**
* Der Implementierer (eng. Implementer) ist das Interface
* das die verschiedenen Implementiererklassen nutzen.
* Die Abstraktion kann nur mit den Implementiererklassen
* kommunizieren über Methoden die hier deklariert werden.
* Der Implementierer kann eine abstrakte Klasse oder ein 
* Interface sein. Für gewöhnlich  ist es aber ein Interface,
* genau wie in diesem Beispiel.
*/

public interface Implementierer {
			
	//Hier stehen die Methoden, die
	//die Implementiererklassen
	//implementieren.		
	void methoden();
			
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkretImpA
////////////////////////////////////////////////////////////////////////////////////////////

package bridge;

/**
* Eine beispiehafte Implementiererklasse 
* (eng. ConcreteImplementation), die die 
* Methoden des Interfaces Implementierer
* nutzt.
*/

public class KonkretImpA implements Implementierer {
	//KonkretImpA steht für KonkreteImplementierungA

	//Nutzt die Methoden des Interfaces Implementierer
	@Override
	public void methoden(){}

}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkretImpB
////////////////////////////////////////////////////////////////////////////////////////////

package bridge;

/**
* Eine beispiehafte Implementiererklasse 
* (eng. ConcreteImplementation), die die 
* Methoden des Interfaces Implementierer
* nutzt.
*/

public class KonkretImpB implements Implementierer {
	//KonkretImpB steht für KonkreteImplementierungB

	//Nutzt die Methoden des Interfaces Implementierer
	@Override
	public void methoden(){}

}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package bridge;

/**
*Beispielhafte Nutzung der Bridge.
*/

public class Klient {

KonkretImpA beispielA = new KonkretImpA();
KonkretImpB beispielB = new KonkretImpB();

Abstraktion beispielAbstraktion = new Abstraktion(beispielA) {};
SpezialAbstraktion beispielSpezialAbstraktion = new SpezialAbstraktion(beispielB);

beispielAbstraktion.operationen();
beispielSpezialAbstraktion.operationen();
beispielSpezialAbstraktion.extraOperationen();
	
}