////////////////////////////////////////////////////////////////////////////////////////////
Klasse Originator
////////////////////////////////////////////////////////////////////////////////////////////

/**
* Der Originator kann Momentaufnahmen (Snapshots) seines eigenen Zustandes erstellen und bei Bedarf den Zustand aus den Momentaufnahmen (Snapshots) wiederherstellen.
*/

public class Originator {

   private String state;

   //setze den Zustand
   public void setState(String state){
      this.state = state;
   }

   //gib den Zustand zurück
   public String getState(){
      return state;
   }

   //speicher Zustand in Memento
   public Memento saveStateToMemento(){
      return new Memento(state);
   }

   //setze den Zustand auf den des genommenen Mementos zurück
   public void getStateFromMemento(Memento memento){
      state = memento.getState();
   }
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Memento
////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Das Memento ist ein Wertobjekt das als Momentaufnahme (Snapshot) des Originator fungiert.
 * In der Regel wird das Memento unveränderlich gemacht und die Daten werden ihm nur einmal über den Konstruktor übergeben.
 */

public class Memento {
    private String state;
 
    //Konstruktor
    public Memento(String state){
       this.state = state;
    }
 
    public String getState(){
       return state;
    }	
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Caretaker
////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Der Caretaker weiß wann und warum der Zustand des Originator gespeichert werden muss, sowie wann er wiederhergestellt werden muss.
 * Der Caretaker verfolgt außerdem die Historie des Originator, indem er einen Stapel von Mementos speichert.
 * Wenn der Originator einen vorherigen Zustand annehmen möchte,
 * wird das Memento das oben auf dem Stapel liegt genommen und an die Wiederherstellungsmethode des Originators übertragen.
 */

public class CareTaker {

   //Liste von Mementos
   private List<Memento> mementoList = new ArrayList<Memento>();

   //Füge Memento der Liste hinzu
   public void add(Memento state){
      mementoList.add(state);
   }

   //Nehme bestimmtes Memento aus der Liste
   public Memento get(int index){
      return mementoList.get(index);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse MementoPatternDemo
////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Die Demo zeigt beispielhaft den Gebrauch des Memento Patterns.
 */

public class MementoPatternDemo {

    public static void main(String[] args) {
    
       Originator originator = new Originator();
       CareTaker careTaker = new CareTaker();
       
       //Originator befindet sich im Zustand 1
       originator.setState("State #1");

       //Originator befindet sich im Zustand 2
       originator.setState("State #2");

       //speichere momentanen Zustand
       careTaker.add(originator.saveStateToMemento());

       //Originator befindet sich im Zustand 3
       originator.setState("State #3");

       //speichere momentanen Zustand
       careTaker.add(originator.saveStateToMemento());

       //Originator befindet sich im Zustand 4
       originator.setState("State #4");

       //Gebe momentanen Zustand wieder
       System.out.println("Current State: " + originator.getState());		
       
       //Gebe den ersten gespeicherten Zustand wieder
       originator.getStateFromMemento(careTaker.get(0));
       System.out.println("First saved State: " + originator.getState());

       //Gebe den zweiten gespeicherten Zustand wieder
       originator.getStateFromMemento(careTaker.get(1));
       System.out.println("Second saved State: " + originator.getState());
    }
}


/**
 * Output:
Current State: State #4
First saved State: State #2
Second saved State: State #3
*/
