/**
 * Die Klasse Ticketzaehler stellt im Singleton-Pattern hier das Singleton da.
 * Es existiert kein öffentlicher Konstruktur und die Klasse behält selber die Kontrolle darüber,
 * wie viele Instanzen (immer nur eine) nach außen gegeben werden.
 * 
 */
public class Ticketzaehler {

    // private und einzige Instanz der Klasse
    private static Ticketzaehler instance;

    private int count;

    private Ticketzaehler(int count) {
        this.count = count;
    }

    /**
     * getInstance ist der einzige weg an die Instanz der Klasse ranzukommen.
     * @return gibt entweder die vorhandene Instanz oder eine neue zurück
     */
    public static Ticketzaehler getInstance() {
        if (instance == null) {
            instance = new Ticketzaehler(0);
        }
        return instance;
    }

    // Methoden um mit dem Zaehler zu interagieren
    
    public void increment() {
        this.count++;
    }

    public void resetCount() {
        this.count = 0;
    }

    public int getCount() {
        return count;
    }
} 
			
			
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
		
/**
 * Die Klasse Kasse stellt im Singlepattern hier den Client da.
 * Von ihr können so viele Instanzen erzeugt werden wie nötig.
 * Alle Instanzen greifen allerdings immer auf den gleichen Counter zu.
 */
public class Kasse {

    private int id;
    private int cashAmount;

    /**
     * @param id id um die Kasse zu identifizieren
     * @param cashAmount Geldbetrag der sich am Tagesbeginn(bei Initialisierung) in der Kasse befindet
     */
    public Kasse(int id, int cashAmount) {
        this.id = id;
        this.cashAmount = cashAmount;
    }

    /**
     * bei jedem verkauften Ticket wird der Ticketpreis auf den cashAmount in der Klasse addiert.
     * Danach wird der Zähler für die verkauften Tickets erhöht.
     * Zugriff auf den Zähler erfolgt über Ticketzähler.getInstance()
     */
    public void sellTicket(int ticketPrice) {
        this.cashAmount += ticketPrice;
        Ticketzaehler.getInstance().increment();
    }
}


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
			
			
public class Main {

    public static void main(String[] args) {
        
        // Zu beginn des Tages werden die Kassen instanziiert
        Kasse kasse1 = new Kasse(1, 100);
        Kasse kasse2 = new Kasse(2, 100);

        // Über den Tag hinweg verkaufen die Kassen insgesamt 8 Tickets
        kasse1.sellTicket(12);
        kasse1.sellTicket(12);
        kasse1.sellTicket(5);
        
        kasse2.sellTicket(12);
        kasse2.sellTicket(10);
        kasse2.sellTicket(10);
        kasse2.sellTicket(5);
        kasse2.sellTicket(5);

        
        // Am Ende des Tages erfolgt die Auswertung :
        System.out.println(Ticketzaehler.getInstance().getCount()); // Output: 8
        
        // Zaehler wird für den nächsten Tag zurückgesetzt
        Ticketzaehler.getInstance().resetCount();
        System.out.println(Ticketzaehler.getInstance().getCount()); // Output: 0
    }
}