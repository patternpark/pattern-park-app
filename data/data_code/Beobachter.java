////////////////////////////////////////////////////////////////////////////////////////////
Klasse Beobachter
////////////////////////////////////////////////////////////////////////////////////////////

package beobachter;

/**
 * Das Interface Beobachter definiert eine Aktualisierungsschnittstelle für Objekte,
 * die über Änderungen eines Subjekts benachrichtigt werden wollen.
 * Die konkreten Beobachter implementieren dieses Interface.
 */

public interface Beobachter {

	// Funktion zum Aktualisieren des aktuellen Zustands
	void aktualisiere();
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreterBeobachter1
////////////////////////////////////////////////////////////////////////////////////////////

package beobachter;

/**
 * Die Klasse KonkreterBeobachter1 verwaltet eine Reaktion auf Änderungen im beobachteten Subjekt.
 * Sie implementiert das Beobachter-Interface.
 */

public class KonkreterBeobachter1 implements Beobachter {

	// Konstruktor
	public KonkreterBeobachter1() {
		// Implementierung des Konstruktors
	}


	// Funktion für die Reaktion auf die Änderung des Zustandes
	public void aktualisiere() {
		// Implementierung der Aktualisierung
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreterBeobachter2
////////////////////////////////////////////////////////////////////////////////////////////

package beobachter;

/**
 * Die Klasse KonkreterBeobachter2 verwaltet eine weitere Reaktion auf Änderungen im beobachteten Subjekt.
 * Sie implementiert das Beobachter-Interface.
 */

public class KonkreterBeobachter2 implements Beobachter {

	// Konstruktor
	public KonkreterBeobachter2() {
		// Implementierung des Konstruktors
	}

	// Funktion für die Reaktion auf die Änderung des Zustandes
	public void aktualisiere() {
		// Zweite implementierung der Aktualisierung
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Subjekt
////////////////////////////////////////////////////////////////////////////////////////////

package beobachter;

/**
 * Das Subjekt kennt seine Beobachter. 
 * Eine beliebige Anzahl von Beobachtern kann ein Subjekt beobachten.
 */

public abstract class Subjekt {

	// Eine Liste von angemeldeten Beobachtern
	public List<Beobachter> beobachterList;

	// Konstruktor
	public Subjekt() {
		// Implementierung des Konstruktors
	}

	// Mit dieser Methode kann sich ein Beobachter anmelden.
	// Er wird dann über Änderungen des Zustands benachrichtigt.
	public void meldeAn( Beobachter beobachter ) {
		// Trage den Beobachter in die Liste der angemeldeten Beobachter ein.
	}

	// Mit dieser Methode kann sich ein Beobachter abmelden.
	// Er wird dann nicht mehr über Änderungen des Zustands benachrichtigt.
	public void meldeAb( Beobachter beobachter ) {
		// Trage den Beobachter aus der Liste der angemeldeten Beobachter aus.
	}

	// Mit dieser Methode werden alle angemeldeten Beobachter über Zustandsänderungen benachrichtigt.
	public void benachrichtige() {
		// Durchlaufe die Liste der angemeldeten Beobachter und benachrichtige jeden über die Zustandsänderung.
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkretesSubjekt
////////////////////////////////////////////////////////////////////////////////////////////

package beobachter;

/**
 * In dieser Klasse befindet sich das Fachkonzept der Anwendung.
 * Die Klasse benachrichtigt außerdem seine Beobachter bei Änderungen des Zustands. 
 * Dies geschieht in der Methode benachrichtige() der Klasse Subjekt, von der diese Klasse erbt.
 */

public class KonkretesSubjekt extends Subjekt {

	// Ein Beispiel-Zustand dieses konkreten Subjekts, am Anfang 0
	private int zustandSubjekt = 0;

	// Konstruktor
	public KonkretesSubjekt() {
		// Implementierung des Konstruktors
	}

	// setter-Methode, setze den aktuellen Zustand
	public void setZustand( int zustand ) {
		this.zustandSubjekt = zustand;
		// Nach der Änderung die Beobachter informieren (hier nur beispielhaft im Setter angesiedelt)
		this.benachrichtige();
	}

	// getter-Methode, gibt den aktuellen Zustand zurück
	public int getZustand() {
		return this.zustandSubjekt;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package beobachter;

/**
 * Die Klasse Klient ist in diesem Beispiel diejenige Klasse, die das konkrete Subjekt aktiv nutzt und somit
 * Benachrichtungen an die Beobachter auslöst.
 */

public class Klient {

	// Ein Beispiel-Subjekt
	public KonkretesSubjekt konkretesSubjekt;

	// Konstruktor
	public Klient() {
		// Implementierung des Konstruktors
		// ...
		// Beispielhafte Zuweisung von Beobachtern zum Subjekt
		this.konkretesSubjekt.meldeAn( new KonkreterBeobachter1() );
		this.konkretesSubjekt.meldeAn( new KonkreterBeobachter2() );
	}

	// Funktion, die Zustand des konkreten Subjektes beispielhaft ändert
	public void löseÄnderungenAus() {
		// Hier wird der aktuelle Zustand des Subjektes um eins erhöht, um Benachrichtgungen
		// an die Beobachter auszulösen
		this.konkretesSubjekt.setZustand( this.konkretesSubjekt.getZustand() + 1 );
	}
}