////////////////////////////////////////////////////////////////////////////////////////////
Interface Produkt
////////////////////////////////////////////////////////////////////////////////////////////

package factory_method;

/**
* Das Interface / die abstrakte Klasse Product definiert die grundlegenden Eigenschaften aller Produkte, sodass der Klient immer
* ein Objekt vom Typ Produkt zurückerhält und theoretisch nicht genau wissen muss, welches Objekt nun konkret aus dem Erzeuger hervorgeht.
*/

public interface Product { }

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Erzeuger
////////////////////////////////////////////////////////////////////////////////////////////

package factory_method;

/**
* Die abstrakte Klasse / das Interface Creator definiert die grundlegenden Eigenschaften aller Erzeuger.
* Zum Beispiel, dass alle Erzeuger Objekte vom Typ Product erzeugen. Dadurch muss der Klient nicht genau wissen,
* welchen Erzeuger er nutzt und kann jeden beliebigen zur Erzeugung der Produkte verwenden. Erst die Unterklassen
* (konkrete Erzeuger) legen fest, welches Produkt sie erzeugen.
*/

public abstract class Creator {
	// Der Konstruktor des Erzeugers
	public Creator () {
		// Implementierung des Konstruktors
	}

	// Die abstrakte Operation zum Erzeugen eines Produktes,
	// welche durch die konkreten Erzeuger überschrieben wird.
	public abstract Product createProduct();
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse konkretes Produkt
////////////////////////////////////////////////////////////////////////////////////////////

package factory_method;

/**
* Die Klasse des konkreten Produktes beschreibt ein spezielles Produkt, das bestimmte Eigenschaften auf sich vereint
* und durch einen Erzeuger erstellt werden soll. Es ist somit eine Konkretisierung des allgemeinen Produktes von vorher,
* und implementiert dieses, damit es von den Erzeugern erstellt werden kann.
*/

public class ConcreteProduct implements Product {
	// Konstruktor
	public ConcreteProduct() {
		// Implementierung des Konstruktors
	}

	// Irgendwelche Operationen (Was auch immer das Produkt machen soll)
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse konkreter Erzeuger
////////////////////////////////////////////////////////////////////////////////////////////

package factory_method;

/**
* Die Klasse ConcreteCreator beschreibt eine spezielle Implementation des Erzeugers, der
* ein konkretes Produkt erzeugt. Er beerbt die abstrakte Erzeuger-Klasse.
*/

public class ConcreteCreator extends Creator {
	// Konstruktor
	public ConcreteCreator() {
		// Implementierung des Konstruktors
	}

	// Konkrete Umsetzung der Operation zum Erzeugen eines
	// konkreten Produkts
	public Product createProduct() {
		return new ConcreteProduct();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package factory_method;

/**
* Diese Klasse dient eher zu Veranschaulichungszwecken und ist in diesem Fall
* nur dazu da, die Main zu beherbergen und beispielhaft ein Produkt zu erzeugen.
*/

public class Client {
	// Die Main-Methode
	public static void main( String arg[] ) {
		Creator creator = new ConcreteCreator();
		Product concreteProduct = creator.createProduct();
		concreteProduct.doSomething();
	}
}