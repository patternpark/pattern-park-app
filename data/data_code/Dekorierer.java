////////////////////////////////////////////////////////////////////////////////////////////
Klasse Komponente
////////////////////////////////////////////////////////////////////////////////////////////

package dekorierer;

/**
 * Die abstrakte Klasse Komponente definiert die Schnittstelle für die Objekte in 
 * der zusammengesetzten Struktur des Kompositums. 
 * Die Klassen KonkreteKomponente, Dekorierer und die konkreten Dekorierer erben von ihr.
 * In dieser Klasse wird ein mögliches Standardverhalten der Blätter implementiert.
 * Der Zugriff auf die Objekte und dessen Verwaltung wird in dieser Klasse geregelt.
 * Optional wird auch der Zugriff auf das Elternobjekt deklariert und implementiert.
 */

public interface Komponente {
	
	//deklaration von irgend einer Operation
	void operation();
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreteKomponente
////////////////////////////////////////////////////////////////////////////////////////////

package dekorierer;

/**
 * Die konkrete Komponente definiert ein Objekt, das um zusätzliche Funktionalität erweitert werden kann.
 * Diese Klasse erbt von der abstrakten Klasse Komponente.
 */

public class KonkreteKomponente implements Komponente {
	
	//Konstruktor
	public KonkreteKomponente() {
		//Implementierung des Konstruktors
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Dekorierer
////////////////////////////////////////////////////////////////////////////////////////////

package dekorierer;

/**
 * Der abstrakte Dekorierer verwaltet eine Referenz auf ein Komponentenobjekt und 
 * definiert eine Schnittstelle, die der Schnittstelle der Komponente entspricht.
 * Die konkreten Dekorierer erben von dieser Klasse.
 */

public abstract class Dekorierer implements Komponente {

	protected Komponente eineKomponente;
	
	//Konstruktor
	public Dekorierer() {
		//Implementierung des Konstruktors
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreterDekoriererA
////////////////////////////////////////////////////////////////////////////////////////////

package dekorierer;

/**
 * Der konkrete Dekorierer fügt der Komponente Funktionalitäten hinzu.
 * Diese Klasse erbt von der abstrakten Klasse Dekorierer.
 */

public class KonkreterDekoriererA extends Dekorierer {
	
	//ein zusätzliches Attribut, um das die Komponente erweitert wird
	private int zusatzAttribut;	
	
	//Konstruktor
	public KonkreterDekoriererA() {
		//Implementierung des Konstruktors
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreterDekoriererB
////////////////////////////////////////////////////////////////////////////////////////////

package dekorierer;

/**
 * Der konkrete Dekorierer fügt der Komponente Funktionalitäten hinzu.
 * Diese Klasse erbt von der abstrakten Klasse Dekorierer.
 */

public class KonkreterDekoriererB extends Dekorierer {

	
	//eine zusätzliche Operation, um die die Komponente erweitert wird
	public void zusatzOperation() {
		//Implementierung der zusätzlichen Operation
	}

	//Konstruktor
	public KonkreterDekoriererB() {
		//Implementierung des Konstruktors
	}
}