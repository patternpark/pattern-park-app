////////////////////////////////////////////////////////////////////////////////////////////
Klasse Model
////////////////////////////////////////////////////////////////////////////////////////////

package mvc;

/**
* Das Model enthält die Daten der Anwendung
*/

public class Model {

	//Referenz Implementierer		
	protected Implementierer implementierer;

	//Konstruktor
	public Model(Implementierer implementierer)
	{
		this.implementierer = implementierer;
	}
	
	//Default Konstruktor
	public Model(){}

	//Die Methoden des Model
	//Hier operationen gennant um sie
	//von den methoden des Implementierer
	//zu unterscheiden. Natürlich können
	//die Methoden auch abstrakt sein.		
	public void operationen(){};
			
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse View
////////////////////////////////////////////////////////////////////////////////////////////

package mvc;

/**
* Die View enthält die Darstellung der Daten aus dem Model, 
* um diese für den Benutzer ansprechend darzustellen.
*/

public class View  {
			
	//Konstruktor		
	public View(Implementierer implementierer)
	{
		super.implementierer = implementierer;
	}

	// Hier kommt die Darstellung der Daten hin.
    // Das könnte eine graphische oberfläche sein oder auch einfach eine ausgabe auf der Konsole
	public void extraOperationen(){}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Controller
////////////////////////////////////////////////////////////////////////////////////////////

package mvc;

/**
* Der Controller besitzt mindestens ein Model und eine View.
* Außerdem muss er methoden enthalten, die von außen aufgerufen werden können,
* über die die Daten Abgefragt und verändert werden können.
*/

public class Controller {
			
	//Hier stehen die Methoden, die
	//der Controller
	//implementiert.		
	void methoden();
			
}