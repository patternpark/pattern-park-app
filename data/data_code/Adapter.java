////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package adapter;

public class Klient {

	private Adapter adapter;

	/**
	* Konstruktor
	*/
	public Klient() {
		// Implementierung des Konstruktors
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Interface Adapter
////////////////////////////////////////////////////////////////////////////////////////////

package adapter;

/**
* Generischer Adapter für die Kommunikation des Klienten mit einer Klasse mit unpassendem Interface.
* AdapterImpl implementiert das Interface.
*/
public interface Adapter {

	/**
	* Methode, die der Klient aufruft.
	* Besitzt das passende Interface für den Klienten.
	*/
	public void operation();
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreterAdapter
////////////////////////////////////////////////////////////////////////////////////////////

package adapter;

/**
* Konkrete Implementierung des Adapters für die Kommunikation des Klienten mit der Klasse Dienst.
* Implementiert das Interface Adapter.
*/
public class KonkreterAdapter implements Adapter {
	private Dienst dienst;

	/**
	* Konstruktor
	*/
	public KonkreterAdapter() {
		// Implementierung des Konstruktors
	}

	/**
	* Konkrete Implementierung der Methode, die der Klient aufruft.
	* Besitzt das passende Interface für den Klienten.
	*/
	@Override
	public void operation() {
		// Aufruf von dienst.konkreteOperation() unter Nutzung dessen Interfaces:
		dienst.konkreteOperation();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Dienst
////////////////////////////////////////////////////////////////////////////////////////////

package adapter;

/**
* Stellt die Funktionalität bereit, die der Klient braucht, allerdings mit dem falschen Interface.
*/
public class Dienst {

	/**
	* Konstruktor
	*/
	public Dienst() {
		// Implementierung des Konstruktors
	}

	/**
	* Methode, die der Klient eigentlich aufrufen möchte, es aber aufgrund des falschen Interfaces nicht kann.
	* D.h. auch die Methode, die der Adapter aufruft.
	*/
	public void konkreteOperation(){
		// Implementierung
	}
}