////////////////////////////////////////////////////////////////////////////////////////////
Klasse Subjekt
////////////////////////////////////////////////////////////////////////////////////////////

package proxy;

/**
 * Das Subjekt definiert die gemeinsame Schnittstelle des echten Subjekts und des Proxys,
 * so dass ein Proxy überall benutzt werden kann, wo ein echtes Subjekt erwartet wird.
 */

public interface Subjekt {


    // eine Operation, die sowohl zu Proxy als auch zu Subjekt gehört
    void operation();
}

////////////////////////////////////////////////////////////////////////////////////////////
    Klasse Proxy
////////////////////////////////////////////////////////////////////////////////////////////

package proxy;

/**
 * Die Klasse Proxy verwaltet eine Referenz, die es ermöglicht, auf das echte Subjekt zuzugreifen.
 * Der Proxy kann die Schnittstelle des Subjekts verwenden, wenn sie mit der des echten Subjekts identisch ist.
 * Proxy bietet eine Schnittstelle, die mit der des Subjekts identisch ist,
 * so dass ein Proxy für das echte Subjekt eingesetzt werden kann.
 * Die Klasse Proxy kontrolliert den Zugriff auf das echte Subjekt und ist dafür zuständig,
 * es zu erzeugen und zu löschen.
 */

public class Proxy implements Subjekt {

    public EchtesSubjekt einEchtesSubjekt;

    // Konstruktor
    public Proxy() {
        // Implementierung des Konstruktors

    }

    @Override
    public void operation() {
        // Spezielle Implementierung z.B. wenn als Smart Reference genutzt
        // ...
        // Aufruf der echten Operation
        this.einEchtesSubjekt.operation();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
    Klasse EchtesSubjekt
////////////////////////////////////////////////////////////////////////////////////////////

package proxy;

/**
 * Die Klasse EchtesSubjekt definiert das eigentliche Objekt,
 * das durch den Proxy repräsentiert wird.
 * Die Klasse erbt von der abstrakten Klasse Subjekt.
 */

public class EchtesSubjekt implements Subjekt {

    // Konstruktor
    public EchtesSubjekt() {
        // Implementierung des Konstruktors
    }

    @Override
    public void operation() {
        // Implementierung der Operation
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
    Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package proxy;

/**
 * Vom Klienten gehen Anfragen an das Subjekt.
 */

public class Klient {

    private Subjekt dasSubjekt;

    // Konstruktor
    public Klient() {
        // Implementierung des Konstruktors
        this.eineAktion();
    }

    // Klient nutzt den Proxy
    public void eineAktion() {
        this.dasSubjekt = new Proxy();
        this.dasSubjekt.operation();
    }
}