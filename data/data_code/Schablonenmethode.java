////////////////////////////////////////////////////////////////////////////////////////////
Klasse AbstrakteKlasse
////////////////////////////////////////////////////////////////////////////////////////////

package schablonenmethode;

/**
 * Die abstrakte Klasse definiert abstrakte primitive Operationen,
 * die von den konkreten Klassen implementiert werden müssen, 
 * damit die Schritte eines Algorithmus genau definiert werden.
 * In dieser Klasse gibt es die Schablonenmethode, 
 * in der das Skelett des Algorithmus definiert wird. 
 * Dabei werden auch die primitiven Operationen aufgerufen.
 */

public abstract class AbstrakteKlasse {
	
	//Konstruktor
	public AbstrakteKlasse() {
		// Implementierung des Konstruktors
	}
	
	public void schablonenmethode() {
		// Implementierung unter Verwendung der primitiven Operationen
	}
	
	public abstract void primitiveOperation1() {
		//wird in den konkreten Klassen implementiert	
	}
	
	public abstract void primitiveOperation2() {
		//wird in den konkreten Klassen implementiert
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreteKlasse1
////////////////////////////////////////////////////////////////////////////////////////////

package schablonenmethode;

/**
 * Die konkreteKlasse1 implementiert die primitiven Operationen,
 * die die speziellen Schritte des Algorithmus' ausführen.
 * Diese Klasse erbt von der abstrakten Klasse.
 */

public class KonkreteKlasse1 extends AbstrakteKlasse {

	//Konstruktor	
	public KonkreteKlasse1() {
		// Implementierung des Konstruktors
	}
	
	public void primitiveOperation1() {
		// Implementierung einer speziellen primitiven Operation,
		// die in der Schablonenmethode verwendet wird.
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreteKlasse2
////////////////////////////////////////////////////////////////////////////////////////////

package schablonenmethode;

/**
 * Die konkreteKlasse2 implementiert die primitiven Operationen,
 * die die speziellen Schritte des Algorithmus' ausführen.
 * Diese Klasse erbt von der abstrakten Klasse.
 */

public class KonkreteKlasse2 extends AbstrakteKlasse {

	//Konstruktor	
	public KonkreteKlasse2() {
		// Implementierung des Konstruktors
	}
	
	public void primitiveOperation1() {
		// Implementierung einer speziellen primitiven Operation,
		// die in der Schablonenmethode verwendet wird.
	}
}