//////////////////////////////////////////////
Klasse Kontext
//////////////////////////////////////////////

package zustand;

/**
 * Die Klasse Kontext definiert eine Schnittstelle, die den Klienten interessiert. 
 * Sie verwaltet ein Exemplar einer KonkretenZustands-Unterklasse, die den aktuellen Zustand definiert.
 */

public class Kontext {

	public Zustand einZustand;
	
	//Konstruktor
	public Kontext() {
		//Implementierung des Konstruktors
	}
	
	//irgend eine Operation
	public void operation() {
		//Implementierung
	}
}

//////////////////////////////////////////////
Klasse Zustand
//////////////////////////////////////////////

package zustand;

/**
 * Die abstrakte Klasse Zustand definiert eine Schnittstelle zur Kapselung
 * des mit einem bestimmten Zustand des Kontextobjekts verbundenen Verhaltens.
 */

public abstract class Zustand {

	//Konstruktor
	public Zustand() {
		//Implementierung des Konstruktors
	}
	
	//Methode zum Bearbeiten des Objekts, abhängig vom aktuellen Zustand
	public void bearbeite() {
		//Implementierung
	}
}

//////////////////////////////////////////////
Klasse KonkreterZustand1
//////////////////////////////////////////////

package zustand;

/**
 * Die Klasse KonkreterZustand1 implementiert ein Verhalten, 
 * das mit einem Zustand des Kontextobjekts verbunden ist.
 * Diese Klasse erbt von der abstrakten Klasse Zustand.
 */

public class KonkreterZustand1 extends Zustand {
	
	//Konstruktor
	public KonkreterZustand1() {
		//Implementierung des Konstruktors
	}
}

//////////////////////////////////////////////
Klasse KonkreterZustand2
//////////////////////////////////////////////

package zustand;

/**
 * Die Klasse KonkreterZustand2 implementiert ein Verhalten, 
 * das mit einem Zustand des Kontextobjekts verbunden ist.
 * Diese Klasse erbt von der abstrakten Klasse Zustand.
 */

public class KonkreterZustand2 extends Zustand {
	
	//Konstruktor
	public KonkreterZustand2() {
		//Implementierung des Konstruktors
	}
}

//////////////////////////////////////////////
Klasse KonkreterZustand3
//////////////////////////////////////////////

package zustand;

/**
 * Die Klasse KonkreterZustand3 implementiert ein Verhalten, 
 * das mit einem Zustand des Kontextobjekts verbunden ist.
 * Diese Klasse erbt von der abstrakten Klasse Zustand.
 */

public class KonkreterZustand3 extends Zustand {
	
	//Konstruktor
	public KonkreterZustand3() {
		//Implementierung des Konstruktors
	}
}

//////////////////////////////////////////////
Klasse KonkreterZustand4
//////////////////////////////////////////////

package zustand;

/**
 * Die Klasse KonkreterZustand4 implementiert ein Verhalten, 
 * das mit einem Zustand des Kontextobjekts verbunden ist.
 * Diese Klasse erbt von der abstrakten Klasse Zustand.
 */

public class KonkreterZustand4 extends Zustand {
	
	//Konstruktor
	public KonkreterZustand4() {
		//Implementierung des Konstruktors
	}
}