//////////////////////////////////////////////
Klasse Strategie
//////////////////////////////////////////////

/**
* Zuerst erstellen wir das Interface Strategie, welches unsere Funktion beinhaltet
*/

public interface Strategie {
	void folgeStrategie();
}

//////////////////////////////////////////////
Klasse GlockenStrategie
//////////////////////////////////////////////

/**
* Nun erstellen wir die Konkreten Strategie-Klassen, welche das Interface implementieren
* und unsere Funktion genau definieren
*/

public class GlockenStrategie implements Strategie{
	@Override
	public void folgeStrategie() {
		System.out.println("die Glocke wird geleutet");
	}
}

//////////////////////////////////////////////
Klasse RufStrategie
//////////////////////////////////////////////

public class RufStrategie implements Strategie{
	@Override
	public void folgeStrategie() {
			System.out.println("der Verkäufer ruft");
	}
}

//////////////////////////////////////////////
Klasse Verkäufer
//////////////////////////////////////////////

/**
* Jetzt können wir unsere Klasse Verkäufer erstellen, welche das interface und somit die
* konkreten Strategien verwenden kann.
*/

public class Verkäufer {
	private Strategie strategie;

	public Verkäufer(Strategie strategie){
		this.strategie = strategie;
	}

	public void strategieAusführen(){
		strategie.folgeStrategie();
	}
}

//////////////////////////////////////////////
Beispiel der Benutzung
//////////////////////////////////////////////

Verkäufer verkäufer1 = new Verkäufer(new GlockenStrategie());
verkäufer1.strategieAusführen(); //die Glocke wird geleutet

Verkäufer verkäufer2 = new Verkäufer(new RufStrategie());
verkäufer2.strategieAusführen(); //der Verkäufer ruft