////////////////////////////////////////////////////////////////////////////////////////////
Interface Komponente
////////////////////////////////////////////////////////////////////////////////////////////

package kompositum;

/**
 * Das Interface Komponente definiert die Schnittstelle für die
 * Objekte in der zusammengesetzten Struktur des Kompositums.
 * Hier kann ein mögliches Standardverhalten der Blätter implementiert werden.
 * Die Methoden zur Verwaltung der Komponenten werden in diesem Beipsiel in der 
 * konkreten Unterklasse Kompositum implementiert.
 * Optional wird hier auch der Zugriff auf das Elternobjekt deklariert und ggf. auch implementiert.
 */

public interface Komponente {

	// irgendeine Operation
	void operation();
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Kompositum
////////////////////////////////////////////////////////////////////////////////////////////


package kompositum;

/**
 * Die Klasse Kompositum definiert das Verhalten der Komponenten und speichert,
 * welche Kindobjekte sie hat. Sie erbt vom Interface Komponente.
 */

public class Kompositum implements Komponente {

	// Liste an Komponenten, die im Kompositum eingetragen sind
	private ArrayList<Komponente> komponenten;
	
	// Konstruktor
	public Kompositum() {
		// Implementierung des Konstruktors
	}

	// irgendeine Operation
	@Override
	public void operation() {
		// Implementierung der Operation
	}

	// Füge dem Kompositum eine weitere Komponente "komponente" hinzu
	public void fuegeHinzu(Komponente komponente) {
		// Implementierung
	}

	// Entferne die Komponente "komponente"
	public void entferneKomponente(Komponente komponente) {
		// Implementierung
	}
	
	// Gib das Kindobjekt mit index i zurück
	public Komponente gibKindobjekt(int index) {
		// Implementierung
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Blatt1
////////////////////////////////////////////////////////////////////////////////////////////

package kompositum;

/**
 * Die Klasse Blatt1 repräsentiert ein primitives Objekt in der Komposition.
 * Sie implementiert das Interface Komponente.
 * Das Blatt kann keine Kindobjekte haben. 
 * In der Klasse werden die konkreten Eigenschaften und das Verhalten der 
 * Objekte in der Komposition gespeichert.
 */

public class Blatt1 implements Komponente {
	
	// Konstruktor
	public Blatt1() {
		// Implementierung des Konstruktors
	}
	
	// irgendeine Operation
	@Override
	public void operation() {
		// hier kann die Operation der Oberklasse "Komponente" überschrieben werden.
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Blatt2
////////////////////////////////////////////////////////////////////////////////////////////

package kompositum;

/**
 * Die Klasse Blatt2 repräsentiert ein primitives Objekt in der Komposition.
 * Sie implementiert die Schnittstelle Komponente.
 * Das Blatt kann keine Kindobjekte haben. 
 * In der Klasse werden die konkreten Eigenschaften und das Verhalten der 
 * Objekte in der Komposition gespeichert.
 */

public class Blatt2 implements Komponente {
	
	// Konstruktor
	public Blatt2() {
		// Implementierung des Konstruktors
	}
	
	// irgendeine Operation
	@Override
	public void operation() {
		// hier kann die Operation der Oberklasse "Komponente" überschrieben werden.
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Blatt3
////////////////////////////////////////////////////////////////////////////////////////////

package kompositum;

/**
 * Die Klasse Blatt3 repräsentiert ein primitives Objekt in der Komposition.
 * Sie implementiert das Interface Komponente.
 * Das Blatt kann keine Kindobjekte haben. 
 * In der Klasse werden die konkreten Eigenschaften und das Verhalten der 
 * Objekte in der Komposition gespeichert.
 */

public class Blatt3 implements Komponente {
	
	// Konstruktor
	public Blatt3() {
		// Implementierung des Konstruktors
	}
	
	// irgendeine Operation
	@Override
	public void operation() {
		// hier kann die Operation der Oberklasse "Komponente" überschrieben werden.
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package kompositum;

/**
 * Durch den Klienten werden die einzelnen Objekte manipuliert.
 */

public class Klient {
	private Kompositum _kompositum;
	
	// Konstruktor
	public Klient() {
		// Implementierung des Konstruktors
		// ...
		this._kompositum = new Kompositum();
	}

	// Beispiel-Funktion, die exemplarisch einige Aktionen mit dem Kompositum durchführt, um dessen Funktionsweise zu zeigen
	public void zeigeBeispiel() {
		// Beispielhaft werden die verschiedenen Komponenten eingefügt
		this._kompositum.fuegeHinzu( new Blatt1() );
		this._kompositum.fuegeHinzu( new Blatt2() );
		this._kompositum.fuegeHinzu( new Blatt3() );

		// Beispielhaft wird eine Komponente entnommen, dann die Operation ausgeführt und dann wird sie entfernt
		Komponente beispielBlatt = this._kompositum.gibKindobjekt( 2 );
		beispielBlatt.operation();
		this._kompositum.entferneKomponente( beispielBlatt );
	}
}