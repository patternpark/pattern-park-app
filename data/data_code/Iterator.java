////////////////////////////////////////////////////////////////////////////////////////////
Klasse Iterator
////////////////////////////////////////////////////////////////////////////////////////////

package iterator;

/**
 * Der Iterator definiert eine Schnittstelle zum Zugriff auf und zum Durchlaufen von Elementen.
 */

public class Iterator {

	//der Index des aktuellen Elements
	private int index;

	//ein Array der Elemente, das durchlaufen wird
	private Object[] Quelle;

	public Aggregat einAggregat;
	
	//Konstruktor
	public Iterator() {
		//Implementierung des Konstruktors
	}
	
	
	public void start() {
		index = 0;
	}
	
	//Gib true zurück, wenn das Ende des Arrays erreicht ist
	public bool end() {
		//Wenn das aktuelle Element gleich der Länge des Arrays ist, gib true zurück, sonst false
		return index == Quelle.length();
	}
	
	//Gib das aktuelle Element zurück
	public int actualElement() {
		return index;
	}
	
	//Gehe zum nächsten Element
	public void goOn() {
		//Erhöhe das aktuelle Element um 1
		index++;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse KonkreterIterator
////////////////////////////////////////////////////////////////////////////////////////////

package iterator;

/**
 * Der konkrete Iterator implementiert die Schnittstelle aus dem Iterator.
 * Die Klasse verwaltet die aktuelle Position während der Traversierung des Aggregats.
 * Die Klasse erbt von der Klasse Iterator.
 */

public class KonkreterIterator extends Iterator {

	public KonkretesAggregat einKonkretesAggregat;
	
	//Konstruktor
	public KonkreterIterator() {
		//Implementierung des Konstruktors
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Aggregat
////////////////////////////////////////////////////////////////////////////////////////////

package iterator;

/**
 * Das Aggregat definiert eine Schnittstelle zum Erzeugen eines Objekts der Klasse Iterator.
 */

public class Aggregat {
	
	//Konstruktor
	public Aggregat() {
		//Implementierung des Konsturktors
	}
	
	//Operation zum Erzeugen eines Iterators
	public void ErzeugeIterator() {
		//erzeuge einen Iterator
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse konkretesAggregat
////////////////////////////////////////////////////////////////////////////////////////////

package iterator;

/**
 * Das konkrete Aggregat implementiert die Operation zum Erzeugen eines konkreten Iterators, 
 * indem es ein Objekt der passenden konkreten Iterator-Klasse zurückgibt.
 */

public class KonkretesAggregat extends Aggregat {
	
	//Konstruktor
	public KonkretesAggregat() {
		//Implementierung des Konstruktors
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
Klasse Klient
////////////////////////////////////////////////////////////////////////////////////////////

package iterator;

/**
 * Der Klient greift auf den Iterator und auf das Aggregat zu.
 */

public class Klient {

	public Aggregat einAggregat;
	public Iterator einIterator;
	
	//Konstruktor
	public Klient() {
		//Implementierung des Konstruktors
	}
}