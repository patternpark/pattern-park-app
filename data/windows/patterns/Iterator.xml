<!DOCTYPE pattern SYSTEM "pattern.dtd">
<pattern id="iterator">
    <name>Iterator</name>
    <tooltip_text>Iterator-Entwurfsmuster: Gewinnspiel</tooltip_text>
    <action_command>iterator</action_command>
    <title>Beispiel: Gewinnspiel</title>
    <sub_title>Entwurfsmuster: Iterator</sub_title>
    <icon>data/data_logo/Iterator_klein.png</icon>
    <map_number>2</map_number>

    <tabs>
        <tab type="overview" id="overview">
            <tab_name>%summary%</tab_name>
            <preview>data/data_logo/Iterator.png</preview>
            <description>Mit einem Iterator kann man nacheinander auf die Einträge einer Besucherliste zugreifen. In diesem Beispiel wird er dazu benutzt, den Gewinner eines Gewinnspiels zu bestimmen. Der Iterator durchläuft die Liste, bis er gestoppt wird.</description>
            <use_for>Die Reihenfolge der Elemente in einer Liste ist für den Iterator irrelevant. Er kann die Liste nach eigenen Kriterien, also alphabetisch oder nach zufälliger Reihenfolge durchgehen. Da alle Elemente überprüft werden, wird jeder Gast in der Liste für das Glücksspiel berücksichtigt.
            </use_for>
            <not_use_for>Bei großen Listen kann der Zeitaufwand sehr ansteigen, da möglicherweise die komplette Liste vom Iterator durchlaufen werden muss.</not_use_for>
        </tab>

        <tab type="animation" id="animation">
            <tab_name>%animation%</tab_name>
            <preview>data/data_animation/Iterator.jpg</preview>
            <info>Dieses Video zeigt das Durchlaufen einer Liste von Besuchern mithilfe des Iterators und erläutert den Vorgang anhand eines Beispiels. Hier wird mit einem Iterator der Gewinner eines Glücksspiels ermittelt.</info>
            <time>1:25 Minuten</time>
            <movie>data/data_animation/iterator.swf</movie>
            <movie_omu>data/data_animation/iterator_omu.swf</movie_omu>
        </tab>

        <tab type="participants" id="participants">
            <tab_name>Methoden</tab_name>
            <preview>data/data_o_uml/Iterator.jpg</preview>
            <info>Zuordnung von Funktionen zum Durchlaufen verschiedener Datenstrukturen</info>
            <time>etwa 10 Minuten</time>
            <movie>data/data_o_uml/iterator.swf</movie>
            <learning_goal>Verbesserung des Verständnisses für die Funktionsweise des Iterators</learning_goal>
            <preconditions>Datenstrukturen Liste und Binärbaum</preconditions>
        </tab>

        <tab type="puzzle" id="puzzle">
            <tab_name>%puzzle%</tab_name>
            <description>Das Klassendiagramm zeigt das Gerüst eines allgemeinen Iterator-Entwurfsmusters. Füge nun die Klassen "BesucherListe" und "BesucherListenIterator" hinzu und füge die fehlenden Verbindungen hinzu.</description>
            <puzzle>umlPuzzle/umlData/dataIterator.xml</puzzle>
            <solution>umlPuzzle/umlData/dataIteratorSolution.xml</solution>
        </tab>

        <tab type="thePattern" id="thePattern">
            <tab_name>%the_pattern%</tab_name>
            <preview>data/data_pattern/Iterator.jpg</preview>
            <description>Mit einem Iterator kann man nacheinander auf die Elemente eines zusammengesetzten Objekts (z.B. einer Liste) zugreifen. Das Iterator-Entwurfsmuster gehört zu der Familie der objektbasierten Verhaltensmuster.</description>
            <advantages>Die innere Struktur des zusammengesetzten Objekts bleibt verborgen.
Das zusammengesetzte Objekt kann gleichzeitig mehrfach auf verschiedene Arten, wie etwa alphabetisch oder zufällig, durchlaufen werden. Für jeden Durchlauf wird jeweils ein Iterator benötigt.
Man kann auf Objekte unterschiedlicher Datenstrukturen einheitlich zugreifen und sie durchlaufen.</advantages>
            <disadvantages>Wenn das Objekt, auf welches der Iterator angewendet wird, sehr viele Einträge hat, kann der Vorgang zeitaufwändig sein.</disadvantages>
            <participants>Iterator
Die Klasse "Iterator" definiert eine Schnittstelle zum Zugriff auf Elemente.

KonkreterIterator
Die Klasse "KonkreteIterator" implementiert die Schnittstelle aus dem Iterator.

Aggregat
Die Klasse "Aggregat" definiert eine Schnittstelle zum Erzeugen eines Iterators.

KonkretesAggregat
Durch die Klasse "KonkretesAggregat" wird ein konkreterIterator erzeugt.

Klient
Der Klient greift auf den Iterator und auf das Aggregat zu.
            </participants>
        </tab>

        <tab type="code" id="code">
            <tab_name>%code%</tab_name>
            <code>data/data_code/Iterator.java</code>
        </tab>
    </tabs>
</pattern>